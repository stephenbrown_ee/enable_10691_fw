//==============================================================

module I2cMaster
(
	input  wire        busClock,
	input  wire        busReset,
	input  wire        busChipSelect,
	input  wire        busWrite,
	input  wire        busRead,
	input  wire [19:0] busAddress,
	input  wire [31:0] busWrData,
	output reg  [31:0] busRdData,
	
	input  wire        sync, 
   	
   inout  wire        scl,	
	inout  wire        sda,
	output wire        sclTest,
	output wire        stpClock
);
 
//--- Bus Interface ----------------------------------------------

wire selReg  = (busChipSelect && (busAddress[19:16] == 0));

reg [7:0][31:0] register = 0;

always_comb begin
	if (selReg)
		busRdData = register[busAddress[4:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busReset)
		register <= 0;
	else if (selReg && busWrite) begin
		register[busAddress[4:2]] <= busWrData;
	end
	else begin 
		register[0][16]     <= txAck;
		register[0][19:17]  <= comPhase;
		register[1][7:0]    <= rxDataHold;
		register[1][16]     <= stretch;
	end
end

//---------------------------------------------------------------

wire [7:0]  txData       = register[0][7:0];
wire        start        = register[0][8];
wire        transmit     = register[0][9];
wire			receive      = register[0][10];
wire			nak          = register[0][11];
wire        stop         = register[0][12];
wire [11:0] baudDiv      = register[0][31:20]; // Bit rate = sysClock / baudDiv / 3
wire        vsync        = register[2][0];

reg [2:0]   comPhase   = 0;
reg [1:0]   bitPhase   = 0;
reg [3:0]   bitCount   = 0;
reg [11:0]  baudCount  = 0;
reg [10:0]  refCount   = 0;
reg         txAck      = 0;
reg [7:0]   rxData     = 0;
reg [7:0]   rxDataHold = 0;
reg         stretch    = 0;
reg         waitToSend = 0;

wire [3:0]  txBitNo = 4'd7 - bitCount;
wire [3:0]  rxBitNo = 4'd7 - bitCount;

reg         sclInt = 1;
reg         sdaInt = 1;

reg         scl_out;
wire        scl_buf;
wire        sda_buf;

assign sclTest = sclInt;

//---- Bidirectional Buffer needed for I2C -----------------------------

i2cMSP430_buf fpga_i2c1_inst
(
	.datain  ( { sclInt, sdaInt } ),
	.oe      ( { !sclInt, !sdaInt } ),
	.dataout ( { scl_buf, sda_buf } ),
	.dataio  ( { scl, sda } )
);

//---- I2C state machine -----------------------------------------------

always@(posedge busClock) begin

	if (refCount + 1 < baudDiv / 4)
		refCount <= refCount + 1'd1;
	else begin
		refCount <= 0;
		stpClock <= !stpClock;
	end
	
	// Idle phase
	if ((comPhase == 0) || waitToSend || busReset) begin
		baudCount <= 0;
		bitPhase  <= 0;
		bitCount  <= 0;
		sdaInt    <= 1;
		// SCL must remain low between bytes
		if (!vsync) begin
			waitToSend <= 0;
			if (selReg && busWrite && busWrData[13])
				comPhase <= 3'd1;
		end
		else if (selReg && busWrite && busWrData[13]) begin
			comPhase   <= 3'd1;
			waitToSend <= 1'b1;
		end
		else if (waitToSend) begin
			if(sync) begin
				waitToSend <= 0;
			end
		end
	end 
	// Running phases
	else begin
		// Baud rate divider
		if (baudCount + 1 < baudDiv) begin
			baudCount <= baudCount + 1'd1;
			// Only register the clock on the first quarter of the phase to avoid quick pulses
			if (baudCount < baudDiv[11:2])
				scl_out <= scl_buf;
		end
		else begin
			baudCount <= 0;
			case (comPhase)
			
				1:begin // comPhase 1 = START condition
					case (bitPhase)
						0:begin
							if (start)
								sclInt <= 1;
						end
						1:begin
							if (start)
								sdaInt <= 0;
						end
						2:begin
							sclInt <= 0;
							bitCount <= 0;
							comPhase <= 2;
						end
					endcase
				end
			
				2:begin // comPhase 2 = Transmit
					case (bitPhase)
						0:begin
							if (!stretch) begin
								sclInt <= 0;
								if (transmit && (bitCount < 8))
									sdaInt <= txData[txBitNo];
								else
									sdaInt <= 1; // Allow ack from slave
							end
						end
						1:begin
							if (transmit) begin
								sclInt <= 1;
								if (bitCount == 8)
									txAck <= sda_buf;
							end
						end
						2:begin
							if (scl_out) begin
								stretch <= 0;
								sclInt  <= 0;
								if (transmit && (bitCount < 8))
									bitCount <= bitCount + 1'd1;
								else begin
									rxData   <= 0;
									bitCount <= 0;
									comPhase <= 3;   // Add clock stretch support
								end
							end
							else begin
								if (transmit)
									stretch <= 1'b1;
								else begin
									stretch  <= 0;
									comPhase <= 3;
								end
							end
						end
					endcase
				end
			
				3:begin // comPhase 3 = Receive
					case (bitPhase)
						0:begin
							if(!stretch) begin
								sclInt <= 0;
								if (receive && (bitCount == 8))
									sdaInt <= nak;
								else
									sdaInt <= 1;
							end
						end
						1:begin
							if (receive) begin
								if (bitCount < 8) begin
									rxData[rxBitNo] <= sda_buf;
									sclInt <= 1;
								end
								else begin
									if ((nak == 1'b1) && (sda_buf == 0))  // Don't set the clock high on a NAK until SDA is high
										sclInt <= 0;
									else
										sclInt <= 1;
								end
							end
						end
						2:begin
							if (scl_out) begin
								stretch <= 0;
								sclInt  <= 0;
								if (receive && (bitCount < 8))
									bitCount <= bitCount + 1'd1;
								else begin
									rxDataHold <= rxData;
									comPhase   <= 4;
								end
							end
							else begin
								if (receive)
									stretch <= 1'b1;
								else begin
									stretch  <= 0;
									comPhase <= 4;
								end
							end
						end
					endcase
				end
			
				4:begin // comPhase 4 = STOP condition
					case (bitPhase)
						0:begin
							if (stop)
								sdaInt <= 0;
						end
						1:begin
							if (stop)
								sclInt <= 1;
						end
						2:begin
							sdaInt   <= 1;
							comPhase <= 0;
						end
					endcase
				end
				
			endcase
			
			// Step bitPhase 0>1>2
			if (bitPhase < 2) 
				bitPhase <= bitPhase + 1'd1;
			else
				bitPhase <= 0;
		end
	end	
end

endmodule

//==============================================================

//==============================================================

module PaxelStatsSub
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	
	input  wire                  inpClock,
	input  wire [2:0]            inpSync,
	input  wire                  inpStart,
	output reg                   outStart,
	
	output wire [119:0]          rdWord,
	input  wire [119:0]          wrWord,
	output reg                   init,
	output reg                   samp,
	output reg                   save
);

`include "PixelPipe.h";

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));
wire selRam = (busChipSelect && (busAddress[19:16] == 1'h1));

reg [3:0][31:0] register = 0;
wire [29:0]  q_a_ram;
reg         run         = 0;
reg  [7:0]  frame       = 0;

assign busRdData = (selReg) ? register[busAddress[3:2]] : {2'd0, q_a_ram[29:0]};

always@(posedge busClock) begin	
	if (selReg && busWrite)
		register[busAddress[3:2]] <= busWrData;
	else begin
		register[0][6]     <= run;
		register[0][31:24] <= frame;
		if (run)
			register[0][2]  <= 0; // Clear enable at start
	end
end

//---------------------------------------------------------------

wire        resetReg    = register[0][0];
wire        clear       = register[0][1];
wire        enable      = register[0][2];
wire        start       = register[0][3];
wire        size        = register[0][4];     // 0=2x2 1=4x4
wire [7:0]  enableFrame = register[0][23:16];
wire [11:0] colStart    = register[1][11:0];  
wire [3:0]  colMax      = register[1][15:12]; // Cols - 1  
wire [11:0] rowStart    = register[1][27:16]; 
wire [3:0]  rowMax      = register[1][31:28]; // Rows - 1
wire [11:0] colHorMax   = register[2][11:0];  // Width of col - 1
wire [11:0] rowVerMax   = register[2][27:16]; // Height of row - 1

reg         reset       = 0;

(* preserve *) reg  [1:0]  colRun      = 0;
(* preserve *) reg  [1:0]  rowRun      = 0;
reg  [3:0]  colCount    = 0;  // Up to 16 columns in pattern
reg  [3:0]  rowCount    = 0;  // Up to 16 rows in pattern
reg  [11:0] colHorCount = 0;  // Count pixels in a column
reg  [11:0] rowVerCount = 0;  // Count lines in a row
reg  [7:0]  address     = 0;


wire         activeCol = ((colRun == 1) && (((size == 0) && (colHorCount[0] == 0)) || ((size == 1) && (colHorCount[1:0] == 0))));
wire         activeRow = ((rowRun == 1) && (((size == 0) && (rowVerCount[0] == 0)) || ((size == 1) && (rowVerCount[1:0] == 0))));
        
wire [9:0]   address_a_ram = busAddress[11:2];
reg  [7:0]   address_b_ram = 0;
wire         wren_a_ram    = selRam && busWrite;
reg          wren_b_ram    = 0;
wire [29:0]  data_a_ram    = busWrData[29:0];
wire [119:0] data_b_ram    = run ? wrWord : 120'd0;

wire [119:0] q_b_ram;

assign rdWord = q_b_ram;

always@(posedge inpClock) begin

	if (reset) begin
		run           <= 0;
		colRun        <= 0;
		rowRun        <= 0;
		colCount      <= 0;
		rowCount      <= 0;
		colHorCount   <= 0;
		rowVerCount   <= 0;
		wren_b_ram    <= 0;
		address       <= 0;
		address_b_ram <= 0;
		init          <= 0;
		samp          <= 0;
		save          <= 0;
		frame         <= 0;
	end
	else begin
	
		if (inpStart)
			frame <= 0;
		else if (inpSync == SYNC_FRM_END)
			frame <= frame + 1'd1;
			
		outStart <= ((inpSync == SYNC_FRM_END) && start);
			
		if (!run) begin
			if (inpSync == SYNC_FRM_BEG) begin
				if (enable && (frame == enableFrame)) begin
					run  <= 1'd1;
					init <= 1'd1;
				end
				colRun <= (colStart == 0) ? 2'd1 : 2'd0;
				rowRun <= (rowStart == 0) ? 2'd1 : 2'd0;
			end
			else begin
				colRun <= 0;
				rowRun <= 0;
			end
			
			colCount      <= 0;
			rowCount      <= 0;
			colHorCount   <= 0;
			rowVerCount   <= 0;
			address       <= 0;
			samp          <= 0;
			save          <= 0;
			
			if (clear) begin
				wren_b_ram    <= 1'd1;
				address_b_ram <= address_b_ram + 1'd1;
			end
			else begin
				wren_b_ram    <= 0;
				address_b_ram <= 0;
			end
		end
		else begin
			if (inpSync == SYNC_LIN_BEG) begin
				
				if (rowRun == 0) begin
					if (rowVerCount < rowStart)
						rowVerCount   <= rowVerCount + 1'd1;
					else begin
						rowRun        <= 2'd1;
						rowVerCount   <= 0;
					end
				end
				else if (rowRun == 1) begin
					if (rowVerCount < rowVerMax) begin
						rowVerCount   <= rowVerCount + 1'd1;
					end
					else begin 
						rowVerCount   <= 0;
						if (rowCount < rowMax)
							rowCount <= rowCount + 1'd1;
						else
							rowRun <= 2'd2;
					end
				end
					
				colRun      <= (colStart == 0) ? 2'd1 : 2'd0;
				colCount    <= 0;
				colHorCount <= 0;
				
			end
			else if (inpSync == SYNC_PIX_OUT) begin
			
				if (colRun == 0) begin
					if (colHorCount < colStart)
						colHorCount   <= colHorCount + 1'd1;
					else begin
						colRun        <= 2'd1;
						colHorCount   <= 0;
					end
				end
				else if (colRun == 1) begin
				
					if (colHorCount < colHorMax) begin
						colHorCount <= colHorCount + 1'd1;
						if (activeRow && activeCol)
							samp <= 1'd1;
					end
					else begin
						if (!save && activeRow)
							save <= 1'd1;
						colHorCount <= 0;
						if (colCount < colMax) begin
							colCount <= colCount + 1'd1;
						end
						else
							colRun <= 2'd2;
					end
				end
				
			end
			else if ((inpSync == SYNC_LIN_END) || (inpSync == SYNC_FRM_END)) begin
				colHorCount <= 0;
				colRun      <= 0;
			end
			
			if (init)
				init <= 0;
			if (samp)
				samp <= 0;
			if (save)
				save <= 0;
				
			wren_b_ram    <= save;
			address       <= {rowCount[3:0], colCount[3:0]};
			address_b_ram <= address;
			
			if (inpSync == SYNC_FRM_END)
				run <= 0;
		end
	end
	reset <= resetReg;
end

RamTwoClockBidirAsym # 
(
	.dat_wid_a (30), 
	.dat_wid_b (120), 
	.adr_wid_a (10),
	.adr_wid_b (8)
)
ram
(
	.data_a    (data_a_ram),
	.data_b    (data_b_ram),  
	.wren_a    (wren_a_ram),
	.wren_b    (wren_b_ram),
	.q_a       (q_a_ram),
	.q_b       (q_b_ram),
	.address_a (address_a_ram),
	.address_b (address_b_ram),
	.clock_a   (busClock),
	.clock_b   (inpClock)
);

endmodule

//==============================================================
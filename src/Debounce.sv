// Steve Brown

module Debounce 
(
	input  wire   inpClock,
	input  wire   inpSignal,
	output reg    outSignal_db,
	output reg    outLong
);


parameter SHORT_PRESS_CLOCKS = 32'd5000000;
parameter LONG_PRESS_CLOCKS  = 32'd100000000;

reg [31:0] inpSignal_debounce_cnt = 0; 


always @(posedge inpClock) begin
	if (inpSignal == 1'b1) begin
		inpSignal_debounce_cnt <= 0;
		outSignal_db     <= 0;
		outLong          <= 0;
	end 
	else begin
		if (inpSignal_debounce_cnt >= SHORT_PRESS_CLOCKS) begin
			outSignal_db     <= 1'b1;
			if (inpSignal_debounce_cnt < LONG_PRESS_CLOCKS) begin
				inpSignal_debounce_cnt <= inpSignal_debounce_cnt + 'd1;
			end
			else begin
				inpSignal_debounce_cnt <= inpSignal_debounce_cnt;
			end
			if (inpSignal_debounce_cnt == LONG_PRESS_CLOCKS) begin
				outLong <= 1'b1;
			end
		end 
		else begin
			outSignal_db     <= 0;
			outLong          <= 0;
			inpSignal_debounce_cnt <= inpSignal_debounce_cnt + 'd1;
		end
	end
end


endmodule


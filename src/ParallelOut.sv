//==============================================================

module ParallelOut
(
	input  wire              busClock,
	input  wire              busReset,
	input  wire              busChipSelect,
	input  wire              busWrite,
	input  wire              busRead,
	input  wire [19:0]       busAddress,
	input  wire [31:0]       busWrData,
	output reg  [31:0]       busRdData,
	
	input  wire              inpClock,
	input  wire [23:0]       inpFifoData,
	input  wire [2:0]        inpFifoSync,
	
	input  wire              outClock,
	output wire [9:0]        wrusedw_fifo_inp,
	output reg               outVs,
	output reg               outHs,
	output reg               outDe,
	output wire [2:0][7:0]   outData             
);

`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));

reg [9:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[6:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busReset)
		register <= 0;
	else if (selReg && busWrite)
		register[busAddress[6:2]] <= busWrData;
end


//---------------------------------------------------------------


wire        reset      = register[0][0] || busReset;
wire [1:0]  source     = register[0][5:4];   // 0 = inpData, 1 = Color bars, 2 = Fixed data
wire [12:0] horScale   = register[0][20:8];
wire [11:0] horMax     = register[1][11:0];  // Last hor count in line
wire [11:0] verMax     = register[1][27:16]; // Last ver count in frame
wire [11:0] verVS0     = register[3][11:0];  // First line of no VS
wire [11:0] verVS1     = register[3][27:16]; // First line of active VS
wire [11:0] verDE0     = register[4][11:0];  // First line of no DE 
wire [11:0] verDE1     = register[4][27:16]; // First line of active DE 
wire [11:0] verPX0     = register[5][11:0];  // First line of no RAM reads
wire [11:0] verPX1     = register[5][27:16]; // First line of RAM reads 
wire [11:0] horVS0     = register[6][11:0];  // Hor pos of VS fall
wire [11:0] horVS1     = register[6][27:16]; // Hor pos of VS rise
wire [11:0] horHS0     = register[7][11:0];  // Hor pos of HS fall 
wire [11:0] horHS1     = register[7][27:16]; // Hor pos of HS rise
wire [11:0] horDE0     = register[8][11:0];  // Hor pos of DE fall
wire [11:0] horDE1     = register[8][27:16]; // Hor pos of DE rise
wire [11:0] horPX0     = register[9][11:0];  // Hor pos of end of RAM reads
wire [11:0] horPX1     = register[9][27:16]; // Hor pos of start of RAM reads

reg  [11:0]      horCount = 0;
reg  [11:0]      verCount = 0;
reg  [2:0][7:0]  pattData = 0;
reg              outVsDly = 0;
reg  [2:0][7:0]  outData_reg = 0;
reg  [11:0]      pattCount = 0;
reg  [2:0]       pattern  = 0;

reg              vs       = 0;
reg              hs       = 0;
reg              de       = 0;
reg              px       = 0;
reg              vsVer    = 0;
reg              deVer    = 0;
reg              pxVer    = 0;
reg              inpDe    = 0;

wire  inpDV;

//---------------------------------------------------------------

wire             wrclk_fifo_inp    = inpClock;
wire [23:0]      data_fifo_inp     = inpFifoData;
wire             wrreq_fifo_inp    = (inpFifoSync == SYNC_PIX_OUT);
wire [23:0]      q_fifo_inp;

wire [23:0] inpData = q_fifo_inp;

wire             rdreq_fifo_inp    = inpDV;
wire             rdclk_fifo_inp    = outClock;
wire             aclr_fifo_inp     = reset || (outVs & !outVsDly);

dcfifo #
(
	.lpm_type                ("dcfifo"),
	.lpm_numwords            (1024),
	.lpm_width               (24),
	.lpm_widthu              (10),
	.add_usedw_msb_bit       ("ON"),
	.lpm_showahead           ("OFF"),
	.overflow_checking       ("ON"),
	.underflow_checking      ("ON"),
	.wrsync_delaypipe        (4),
	.rdsync_delaypipe        (4),
	.write_aclr_synch        ("ON"),
	.read_aclr_synch         ("ON"),
	.add_ram_output_register ("ON"),
	.use_eab                 ("ON"),
	.intended_device_family  ("Cyclone V")
) 
fifo_inp_parOut
(
	.wrclk     (wrclk_fifo_inp),
	.data      (data_fifo_inp),
	.wrreq     (wrreq_fifo_inp),
	.wrusedw   (wrusedw_fifo_inp),
	.rdclk     (rdclk_fifo_inp),
	.q         (q_fifo_inp),
	.rdreq     (rdreq_fifo_inp),
	.rdusedw   (),
	.aclr      (aclr_fifo_inp)
);


//---------------------------------------------------------------

assign inpDV = px ;
assign outData = {outData_reg[2], outData_reg[1], outData_reg[0]};

//---------------------------------------------------------------

always@(posedge outClock) begin


	case (pattern)  
		0: pattData <= 24'h000000; // Color bars
		1: pattData <= 24'h0000FF;   
		2: pattData <= 24'h00FF00;         
		3: pattData <= 24'hFF0000;                      
		4: pattData <= 24'hFFFF00; 
		5: pattData <= 24'hFF00FF;               
		6: pattData <= 24'h00FFFF;   
		7: pattData <= 24'hFFFFFF;   
	endcase
	
	if (reset) begin
		horCount     <= 0;
		verCount     <= 0;
		vs           <= 0;
		hs           <= 0;
		de           <= 0;
		px           <= 0;
		vsVer        <= 0;
		deVer        <= 0;
		pxVer        <= 0;
		inpDe        <= 0;
		outVs        <= 0;
		outHs        <= 0;
		outDe        <= 0;
		outData_reg      <= 0;
	end
	else begin
		outVsDly <= outVs;
		outVs    <= vs;
		outHs    <= hs;
		outDe    <= de;
		inpDe    <= px;
		
	
		case (source)
			0: begin  // This case is for normal operation
				if (inpDe) begin
					outData_reg <= inpData;
				end
				else if (de) begin 
					outData_reg  <= {8'd16, 8'd16, 8'd16};
				end
				else begin 
					outData_reg  <= 24'd0;
				end
			end
			1: begin // This case displays the pattern data
				outData_reg <= pattData;
			end
			2: begin  // This case blanks the video
				outData_reg <= 0;
			end
			default: outData_reg <= 24'd0;
		endcase
	
		if (horCount == horHS0)
			hs <= 0;
		else if (horCount == horHS1)
			hs <= 1'd1;
	
		if ((horCount == horVS0) && !vsVer)
			vs <= 0;
		else if ((horCount == horVS1) && vsVer)
			vs <= 1'd1;
	
		if (horCount == horPX0) begin
			px <= 0;
		end
		else if ((horCount == horPX1) && pxVer) begin
			px <= 1'd1;
		end
	
		if (horCount == horDE0)
			de <= 0;
		else if ((horCount == horDE1) && deVer)
			de <= 1'd1;
	
		if (verCount == verVS0)
			vsVer <= 0;
		else if (verCount == verVS1)
			vsVer <= 1'd1;
	
		if (verCount == verDE0)
			deVer <= 0;
		else if (verCount == verDE1)
			deVer <= 1'd1;
			
		if (verCount == verPX0)
			pxVer <= 0;
		else if (verCount == verPX1)
			pxVer <= 1'd1;
		
		if (horCount < horMax) begin
			horCount <= horCount + 1'd1;
			if (pattCount < horScale) begin
				pattCount <= pattCount + 1'd1;
			end
			else begin
				pattCount <= 0;
				pattern <= pattern + 1'd1;
			end
		end
		else begin
			horCount   <= 0;
			pattCount  <= 0;
			pattern    <= 0;
		end
			
		if (horCount >= horMax) begin
			if (verCount < verMax)
				verCount   <= verCount + 1'd1;
			else begin
				verCount   <= 0;
			end
		end
		
	end
end

			

endmodule

//==============================================================
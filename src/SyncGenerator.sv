//==============================================================

module SyncGenerator
(
	input  wire              busClock,
	input  wire              busReset,
	input  wire              busChipSelect,
	input  wire              busWrite,
	input  wire              busRead,
	input  wire [19:0]       busAddress,
	input  wire [31:0]       busWrData,
	output reg  [31:0]       busRdData,
	
	input  wire              inpClock,
	input  wire [95:0]       inpFifoData,
	input  wire              inpFifoDV,
	
	output wire [6:0]        wrusedw_fifo_inp,
	output reg               inpVs,
    
	output reg [23:0]  		 outData,
	output reg [2:0]    		 outSync, 
	
	input  wire 				 frmGood          
);

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));

reg [6:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[6:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busReset)
		register <= 0;
	else if (selReg && busWrite)
		register[busAddress[6:2]] <= busWrData;
end


//---------------------------------------------------------------


wire        reset       = register[0][0] || busReset;
wire        enable      = register[0][1];
wire [1:0]  source      = register[0][5:4];   // 0 = inpData, 1 = Color bars, 2 = Fixed data

wire [12:0] imgHorMax  = register[1][12:0];  // Width of image + 2 sync - 1 
wire [12:0] imgVerMax  = register[1][28:16]; // Height of image - 1
wire [13:0] horMax     = register[2][13:0];  // Total clocks per line - 1
wire [14:0] verMax     = register[2][30:16]; // Total lines per frame - 1



reg         run         = 0;
reg  [13:0] horCount    = 0;
reg  [14:0] verCount    = 0;
reg  [1:0]  color       = 0;
reg  [2:0]  sync        = 0;
reg  [1:0]  inpCount    = 0;
reg         runFrame    = 0;
reg  [3:0]  frmGoodDly  = 0;

//---------------------------------------------------------------

wire             wrclk_fifo_inp    = inpClock;
wire [95:0]      data_fifo_inp     = inpFifoData;
wire             wrreq_fifo_inp    = inpFifoDV;
wire [95:0]      q_fifo_inp;

wire [3:0][23:0] inpData = q_fifo_inp;

wire             rdreq_fifo_inp    = fifoRd;
wire             aclr_fifo_inp     = reset || inpVs;

scfifo #
(
	.lpm_type                ("scfifo"),
	.lpm_numwords            (128),
	.lpm_width               (96),
	.lpm_widthu              (7),
	.lpm_showahead           ("ON"),
	.overflow_checking       ("ON"),
	.underflow_checking      ("ON"),
	.add_ram_output_register ("ON"),
	.use_eab                 ("ON"),
	.intended_device_family  ("Cyclone V")
) 
fifo_inp_parOut
(
	.clock     (wrclk_fifo_inp),
	.data      (data_fifo_inp),
	.wrreq     (wrreq_fifo_inp),
	.usedw     (wrusedw_fifo_inp),
	.q         (q_fifo_inp),
	.rdreq     (rdreq_fifo_inp),
	.aclr      (aclr_fifo_inp)
);

//---------------------------------------------------------------

assign fifoRd = ((horCount == 0) && !inpVS && (verCount != 0)) || (inpCount == 3);

//---------------------------------------------------------------

always@(posedge inpClock) begin

	frmGoodDly <= {frmGoodDly[2:0], frmGood};
		
	if (reset) begin
		run           <= 0;
		horCount      <= 0;
		verCount      <= 0;
		outData       <= 0;
		sync          <= SYNC_IDLE;
	end
	else if (frmGoodDly[2] && !frmGoodDly[3]) begin  // run one frame after rising edge of frmGood signal
		runFrame <= 1'd1;
	end
	else if (!run || !runFrame) begin
		run           <= enable;
		horCount      <= 0;
		verCount      <= 0;
		outData       <= 0;
		sync          <= SYNC_IDLE;
	end
	else begin
		outSync	<= sync;
		if (sync == SYNC_PIX_OUT) begin				
			outData  <= inpData[inpCount]; 
			if (inpCount < 3) 
				inpCount <= inpCount + 1'd1;
			else			
				inpCount <= 0;
		end
		else begin
			outData  <= 0;
			inpCount <= 0;
		end
					
		// Active image area
		if ((verCount <= imgVerMax) && (horCount <= imgHorMax)) begin
		
			// Line start sync
			if (horCount == 0) begin
				if (verCount == 0) begin
					sync        <= SYNC_FRM_BEG;
				end
				else begin
					sync     <= SYNC_LIN_BEG;
				end
			end
			
			// Line pixel content
			else if (horCount < imgHorMax) begin
				sync  <= SYNC_PIX_OUT;	
			end 
			
			// Line end sync
			else if (horCount == imgHorMax) begin
				if (verCount < imgVerMax) begin
					sync  <= SYNC_LIN_END;
				end
				else begin
					sync  <= SYNC_FRM_END;
				end
					
			end
			
			horCount <= horCount + 1'd1;
		end		
		
		// Idle area
		else begin
			sync <= SYNC_IDLE;
			
			 // set Vsync low one half frame before valid image to give FIFO time to flush data
			if ((verCount > imgVerMax) && (verCount < verMax))
				inpVS <= 1'd1;                                         
			else if ((verCount == verMax) && (horCount < (imgHorMax >> 1)))
				inpVS <= 1'd1;
			else
				inpVS <= 0;
			
			// Idle after image line
			if (horCount < horMax) begin
				horCount <= horCount + 1'd1;
			end	
			// End of line
			else begin
				horCount <= 0;
				
				// Idle lines after image
				if (verCount < verMax) begin
					verCount <= verCount + 1'd1;
				end
				// End of frame
				else begin
					verCount   <= 0;
					runFrame   <= 0;
					if (!enable)
						run <= 0;
				end
			end
		end
	end
end

endmodule

//==============================================================
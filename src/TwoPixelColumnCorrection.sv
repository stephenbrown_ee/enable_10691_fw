// Stephen Brown
// 

module TwoPixelColumnCorrection 
(
	input  wire                  busClock,
	input  wire                  busReset,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	
	input wire             inpClock,
	input wire  [2:0]      inpSync,
	input wire  [11:0]     inpData,
	
	output reg  [2:0]      outSync,
	output reg  [9:0]      outData
); 


`include "PixelPipe.h"

localparam ADDR_WID = 9;
localparam HOR_SIZE = 400;

wire [6:0] selRam;

//--- Bus Interface ----------------------------------------------
wire   selReg      = (busChipSelect && (busAddress[19:16] == 0));
assign selRam[0]   = (busChipSelect && (busAddress[19:16] == 1));
assign selRam[1]   = (busChipSelect && (busAddress[19:16] == 2));
assign selRam[2]   = (busChipSelect && (busAddress[19:16] == 3));
assign selRam[3]   = (busChipSelect && (busAddress[19:16] == 4));
assign selRam[4]   = (busChipSelect && (busAddress[19:16] == 5));
assign selRam[5]   = (busChipSelect && (busAddress[19:16] == 6));
assign selRam[6]   = (busChipSelect && (busAddress[19:16] == 7));

reg [3:0][31:0] register = 0;

always_comb begin
	if (selReg)
		busRdData = register[busAddress[3:2]];
	else if (selRam[0]) 	
		busRdData = { 24'd0, q_b_ram[0] };
	else if (selRam[1]) 	
		busRdData = { 24'd0, q_b_ram[1] };
	else if (selRam[2]) 	
		busRdData = { 24'd0, q_b_ram[2] };
	else if (selRam[3]) 	
		busRdData = { 24'd0, q_b_ram[3] };
	else if (selRam[4]) 	
		busRdData = { 24'd0, q_b_ram[4] };
	else if (selRam[5]) 	
		busRdData = { 24'd0, q_b_ram[5] };
	else if (selRam[6]) 	
		busRdData = { 24'd0, q_b_ram[6] };
	else 	
		busRdData = 0;
end


always@(posedge busClock) begin	
	if (busWrite && selReg)
		register[busAddress[3:2]] <= busWrData;
end

//---------------------------------------------------------------

wire                 reset          = register[0][0] || busReset;
wire                 bypass         = register[0][1];
wire                 collect_en     = register[0][2];
wire [2:0]           selRamReg      = register[0][8:6];
wire [2:0]           selCor1        = register[0][11:9];
wire [2:0]           selCor2        = register[0][14:12];
wire [3:0]           bilinearFactor = register[0][18:15];
wire [23:0]          multiplier     = register[1][23:0];

parameter IDLE              = 2'd0;
parameter COLLECT           = 2'd1;
parameter AVERAGE_AND_WRITE = 2'd2;

reg        [1:0]  cstate;

reg        [2:0]           inpSync_delay1;
reg        [11:0]          inpData_delay1;
reg        [2:0]           inpSync_delay2;
reg        [11:0]          inpData_delay2;
reg        [ADDR_WID-1:0]  horCount;
reg        [ADDR_WID-1:0]  horCount_cal;
reg        [2:0]           inpSync_d1;
reg [HOR_SIZE-1:0][19:0]   sumColData;
reg               [19:0]   sglSumColData;
reg                        active;
reg        [ADDR_WID-1:0]  horCount_d1;
reg        [19:0]          inpData_d1;
reg        [ADDR_WID-1:0]  inpAddr;
reg                        wren;
reg                        collect_en_dly;
reg        [2:0]           selCor1_reg;
reg        [2:0]           selCor2_reg;
reg        [3:0]           biFactor_reg;
wire              [43:0]   result;
wire       [11:0]          cor1_result;
wire       [11:0]          cor2_result;


wire [12:0] bilin_result = {1'b0, cor1_result} + {1'b0, cor2_result};
wire signed [12:0]  diff = {1'b0, inpData_delay2} - {4'b0, bilin_result[11:3]};

always @ (posedge inpClock) begin: correction
	if (reset) begin
		outData        <= 0;
		outSync        <= 0;
		horCount       <= 0;
	end
	else if (bypass) begin
		outData       <= inpData[9:0];
		outSync       <= inpSync;
	end
	else begin
		inpSync_delay1 <= inpSync;
		inpSync_delay2 <= inpSync_delay1;
		outSync        <= inpSync_delay2;
		if (inpSync == SYNC_FRM_BEG) begin
			selCor1_reg  <= selCor1;
			selCor2_reg  <= selCor2;
			biFactor_reg <= bilinearFactor;
		end

		if ((inpSync == SYNC_LIN_BEG) || (inpSync == SYNC_FRM_BEG)) begin
			horCount <= 0;
		end	   
		else begin
			horCount <= horCount + 1'd1;
		end
		
		inpData_delay1 <= inpData;
		inpData_delay2 <= inpData_delay1;
		
		outData        <= (diff < 0) ? 10'd0 : ((diff > 10'h3FF) ? 10'h3FF : diff[9:0]);
		
	end
end    


always @ (posedge inpClock) begin: collection

	collect_en_dly <= collect_en;
	
	if (reset) begin
		cstate <= IDLE;
	end
	else begin
		case (cstate)
			IDLE: 
				begin
					if (collect_en && !collect_en_dly)
						cstate <= COLLECT;
					else begin 
						horCount_cal  <= 0;
						sglSumColData <= 0;
						wren          <= 0;
						inpAddr       <= 0;
						sumColData    <= 0;
						cstate        <= IDLE;
					end
				end
			COLLECT: 
				begin
			
					inpSync_d1  <= inpSync;
					horCount_d1 <= horCount_cal;
					inpData_d1  <= {8'd0, inpData};
					if (inpSync == SYNC_FRM_BEG) begin
						active       <= 1'b1;
						horCount_cal <= 0;
					end
					else if (inpSync == SYNC_LIN_BEG) begin
						horCount_cal <= 0;
					end
					else if (active) begin
						if (inpSync == SYNC_FRM_END) begin
							cstate <= AVERAGE_AND_WRITE;
						end
						else begin
							horCount_cal <= horCount_cal + 1'd1;
						end
						
						if (inpSync_d1 == SYNC_PIX_OUT) begin
							sumColData[horCount_d1]  <= inpData_d1 + sumColData[horCount_d1];
						end
					end
								
				end
			AVERAGE_AND_WRITE:
				begin
					if (active) begin
						active   <= 0;
						horCount_cal <= 0;
					end
					else if (horCount_cal < (HOR_SIZE + 2)) begin
						sglSumColData <= sumColData[horCount_cal]; // This data is sent to multiplier
						horCount_cal      <= horCount_cal + 1'd1;
						if(horCount_cal >= 2) begin
							wren    <= 1'b1;
							inpAddr <= horCount_cal - 9'd2;
						end	
					end
					else begin 
						horCount_cal      <= 0;
						sglSumColData <= 0;
						wren          <= 0;
						inpAddr       <= 0;
						sumColData    <= 0;
						cstate        <= IDLE;
					end			
					
				end
			default: cstate <= IDLE;
		endcase
	end
end  

wire  [6:0]           ram_a_wren;
wire  [7:0]           ram_a_data = (result[43:18] > 8'hFF) ? 8'hFF : result[25:18];
wire  [ADDR_WID-1:0]  ram_a_addr = (cstate == AVERAGE_AND_WRITE) ? inpAddr : horCount;
wire  [6:0][7:0]      q_a_ram;

wire  [6:0]           ram_b_wren;	
wire  [7:0]           ram_b_data    = busWrData[7:0];	
wire  [ADDR_WID-1:0]  ram_b_addr    = busAddress[10:2];
wire  [6:0][7:0]      q_b_ram;

genvar i;
generate
	for (i = 0; i <= 6; i++) begin: inst_cal_data

		assign ram_a_wren[i]   = wren && (selRamReg == i);
		assign ram_b_wren[i]   = busWrite && selRam[i];
		
		RamTwoClockBidir #
		(	
			.dat_wid(8),
			.adr_wid(ADDR_WID)
		)
		ram_corr_inst
		(
			.clock_a    (inpClock),
			.wren_a     (ram_a_wren[i]),
			.address_a  (ram_a_addr),
			.data_a     (ram_a_data),
			.q_a        (q_a_ram[i]),

			.clock_b    (busClock),
			.wren_b     (ram_b_wren[i]),
			.address_b  (ram_b_addr),
			.data_b     (ram_b_data),
			.q_b        (q_b_ram[i])
		);
		
	end
endgenerate

MultiplyUnsigned #
(
	.dat_wid_a   (20),
	.dat_wid_b   (24),
	.res_wid     (44),
	.pipeline    (2)
)
avg_column_inst
(
	.clock       (inpClock),
	.data_a      (sglSumColData),
	.data_b      (multiplier),
	.result      (result)
);

MultiplyUnsigned #
(
	.dat_wid_a   (8),
	.dat_wid_b   (4),
	.res_wid     (12),
	.pipeline    (1)
)
cor1_inst
(
	.clock       (inpClock),
	.data_a      (q_a_ram[selCor1_reg]),
	.data_b      (biFactor_reg),
	.result      (cor1_result)
);

MultiplyUnsigned #
(
	.dat_wid_a   (8),
	.dat_wid_b   (4),
	.res_wid     (12),
	.pipeline    (1)
)
cor2_inst
(
	.clock       (inpClock),
	.data_a      (q_a_ram[selCor2_reg]),
	.data_b      (4'd8 - biFactor_reg),
	.result      (cor2_result)
);

endmodule

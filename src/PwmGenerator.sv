// File        :  PwmGenerator.sv
// Author      :  Stephen Brown
// Date        :  7/8/2017
// Version     :  0.1
// Abstract    :  This file is responsible for generating a PWM signal (pulse).
//                It is configured over the Avalon Bus typically by a Nios Processor.
//                There is an up-counter (upCount register) that restarts after it reaches 
//                the value programmed into the countMax register. 
//
// Clock Domains :
//   busClock - This registers the configuration data
//
//   clock - This is the input clock that will determine the frequency of the PWM in combination with countMax.
//           PWM Frequency = clock / countMax; 
// 
// Modification History: (see Git)
//====================================================================================


module PwmGenerator
(
	input  wire        busClock,
	input  wire        busReset,
	input  wire        busChipSelect,
	input  wire        busWrite,
	input  wire        busRead,
	input  wire [19:0] busAddress,
	input  wire [31:0] busWrData,
	output reg  [31:0] busRdData,
	
	input  wire			 clock,
	output reg			 pulse
);

wire selReg = (busChipSelect && (busAddress[19:16] == 0));

reg [3:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[4:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin
	if (busReset) 
		register[0] <= 0;
	else if (busChipSelect && (busAddress[19:16] == 0) && busWrite)
		register[busAddress[4:2]] <= busWrData;
end


// These registers are set by the NIOS processor.  The relevant file 
// is "lcd_display_out.c".
wire reset 		   = register[0][0] || busReset;
wire enable 		= register[0][1];

wire [23:0] countMax     = register[1][23:0];
wire [23:0] riseCount    = register[2][23:0];
wire [23:0] fallCount    = register[3][23:0];


reg [23:0]  upCount = 0;
reg [23:0] countMax_reg; 
reg [23:0] riseCount_reg;
reg [23:0] fallCount_reg;


always @ (posedge clock) begin

	if (reset) begin 
		upCount <= 0;
		countMax_reg  <= countMax;
		riseCount_reg <= 0;
		fallCount_reg <= 0;
	end
	else if (upCount < countMax_reg) begin
		upCount <= upCount + 1'b1;
	end
	else begin
		upCount <= 0;
		countMax_reg  <= countMax;
		riseCount_reg <= riseCount;
		fallCount_reg <= fallCount;
	end
		
	if (reset || !enable)
		pulse <= 0;
	else if (upCount == riseCount_reg)
		pulse <= 1'b1;
	else if (upCount == fallCount_reg)
		pulse <= 1'b0;
		
end

endmodule
		

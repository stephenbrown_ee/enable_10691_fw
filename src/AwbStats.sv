//==============================================================
// Stats collecting module for auto-white balance
//==============================================================

module AwbStats
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	
	input  wire                  inpClock,
	input  wire [2:0][11:0]      inpData, 
	
	input  wire [119:0]          rdWord,
	output reg  [119:0]          wrWord,
	input  wire                  init,
	input  wire                  samp,
	input  wire                  save
);

//--- Bus Interface ----------------------------------------------

reg [7:0][31:0] register = 0;

assign busRdData = register[busAddress[4:2]];

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[4:2]] <= busWrData;
end

//---------------------------------------------------------------

wire        resetReg    = register[0][0];
wire [3:0]  sumShift    = register[0][11:8];
wire [11:0] satLevel    = register[1][11:0];
wire [7:0]  minRG       = register[2][7:0];
wire [7:0]  maxRG       = register[2][15:8];
wire [7:0]  minBG       = register[2][23:16];
wire [7:0]  maxBG       = register[2][31:24];
wire [7:0]  minRB       = register[3][7:0];
wire [7:0]  maxRB       = register[3][15:8];
wire [7:0]  minSA       = register[3][23:16];
wire [7:0]  maxSA       = register[3][31:24];

reg              reset  = 0;
reg  [2:0][21:0] sum    = 0; // Big enough to sum 1K row x 12 bits 
reg  [9:0]       count  = 0; // Count of unsaturated pixels 
reg  [2:0][11:0] data   = 0;

wire [2:0][23:0] sums;

assign sums[0] = sum[0] >> sumShift;
assign sums[1] = sum[1] >> sumShift;
assign sums[2] = sum[2] >> sumShift;

wire [19:0] num_divRG = { inpData[0], 8'd0 }; 
wire [9:0]  den_divRG = inpData[1][11:2];  
wire [19:0] result_divRG;

wire [19:0] num_divBG = { inpData[2], 8'd0 }; 
wire [9:0]  den_divBG = inpData[1][11:2];  
wire [19:0] result_divBG;

wire [19:0] num_divRB = { inpData[0], 8'd0 }; 
wire [9:0]  den_divRB = inpData[2][11:2];  
wire [19:0] result_divRB;

wire [7:0] rg = result_divRG[13:6];
wire [7:0] bg = result_divBG[13:6];
wire [7:0] rb = result_divRB[13:6];

// sa = abs(rg - 1) + abs(bg - 1)
wire [7:0] sa = ((rg > 16) ? (rg - 8'd16) : (8'd16 - rg)) + ((bg > 16) ? (bg - 8'd16) : (8'd16 - bg));

wire ok = ((inpData[0] < satLevel) &&
           (inpData[1] < satLevel) && 
           (inpData[2] < satLevel) && 
           (rg > minRG) && (rg < maxRG) &&
           (bg > minBG) && (bg < maxBG) &&
           (rb > minRB) && (rb < maxRB) &&
           (sa > minSA) && (sa < maxSA));

always@(posedge inpClock) begin

	if (reset) begin
		sum      <= 0; 
		count    <= 0;
		wrWord   <= 0;
	end
	else if (init) begin
		sum      <= 0; 
		count    <= 0;
		wrWord   <= 0;
	end
	else begin
		if (save) begin
			wrWord[19:0]   <= rdWord[19:0]   + sums[0][19:0]; // Add sum to RAM sum
			wrWord[49:30]  <= rdWord[49:30]  + sums[1][19:0]; 
			wrWord[79:60]  <= rdWord[79:60]  + sums[2][19:0]; 
			wrWord[109:90] <= rdWord[109:90] + count;         // Add count
			sum            <= 0; 
			count          <= 0;
		end
		else if (samp) begin
			if (ok) begin
				sum[0]   <= sum[0] + data[0]; 
				sum[1]   <= sum[1] + data[1]; 
				sum[2]   <= sum[2] + data[2]; 
				count    <= count  + 1'd1;
			end
		end
		data <= inpData;
	end
	reset <= resetReg;
end

DivideUnsigned #
(
	.dat_wid_num(20),
	.dat_wid_den(10),
	.latency    (10)
)
divRG
(
	.clock     (inpClock),
	.num       (num_divRG),
	.den       (den_divRG),
	.result    (result_divRG),
	.remainder ()
);

DivideUnsigned #
(
	.dat_wid_num(20),
	.dat_wid_den(10),
	.latency    (10)
)
divBG
(
	.clock     (inpClock),
	.num       (num_divBG),
	.den       (den_divBG),
	.result    (result_divBG),
	.remainder ()
);

DivideUnsigned #
(
	.dat_wid_num(20),
	.dat_wid_den(10),
	.latency    (10)
)
divRB
(
	.clock     (inpClock),
	.num       (num_divRB),
	.den       (den_divRB),
	.result    (result_divRB),
	.remainder ()
);

endmodule

//==============================================================
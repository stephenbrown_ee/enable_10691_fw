//==============================================================

module CursorOverlay
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	
	input  wire                  inpClock,
	input  wire [2:0][7:0]       inpData,
	input  wire [2:0]            inpSync,
	
	output reg [2:0][7:0]        outData,
	output reg [2:0]             outSync
);


`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

reg [4:0][31:0] register = 0;

always begin
	if (busChipSelect)
		busRdData = register[busAddress[4:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[4:2]] <= busWrData;
end

//---------------------------------------------------------------

wire        resetReg    = register[0][0];
wire        enable      = register[0][1];
wire        source      = register[0][2];
wire [3:0]  antSize     = register[0][7:4];  
wire [7:0]  antRate     = register[0][15:8];  
wire [11:0] horStart    = register[1][11:0];  
wire [11:0] verStart    = register[1][27:16]; 
wire [11:0] horStop     = register[2][11:0];  
wire [11:0] verStop     = register[2][27:16]; 
wire [11:0] ovHorStart  = register[3][11:0];
wire [11:0] ovHorStop   = register[3][27:16];
wire [11:0] ovVerStart  = register[4][11:0];
wire [11:0] ovVerStop   = register[4][27:16];

reg         reset       = 0;
reg  [11:0] horCount    = 0;
reg  [11:0] verCount    = 0;
reg  [7:0]  antCount    = 0;    
reg  [7:0]  frameCount  = 0;

wire top   = (verCount == verStart) && (horCount >= horStart) && (horCount <= horStop);
wire bot   = (verCount == verStop)  && (horCount >= horStart) && (horCount <= horStop);
wire lft   = (horCount == horStart) && (verCount >= verStart) && (verCount <= verStop);
wire rgt   = (horCount == horStop)  && (verCount >= verStart) && (verCount <= verStop);
wire boxOn = enable && (inpSync == SYNC_PIX_OUT) && (top || bot || lft || rgt);

reg [11:0] ants; 
reg        color;
wire activeBlinkerZone = (horCount >= ovHorStart) && (horCount <= ovHorStop) && (verCount >= ovVerStart) && (verCount <= ovVerStop);

always@(posedge inpClock) begin

	if (reset) begin
		horCount      <= 0;
		verCount      <= 0;
		outSync       <= SYNC_IDLE;
		outData       <= 0;
	end
	else begin
	
		ants  <= horCount + verCount + antCount;
		color <= ants[antSize];
		
		outSync <= inpSync;
		if (source == 0)
			outData <= boxOn ? (color ? 24'hFFFFFF : 0) : inpData;
		else if (activeBlinkerZone) begin
			if (inpSync == SYNC_PIX_OUT)
				outData <= antCount[0] ? 24'h00FF00 : inpData;
			else
			   outData <= inpData;
		end
		else
			outData <= inpData;
		
		if (inpSync == SYNC_FRM_BEG) begin
			horCount      <= 0;
			verCount      <= 0;
			if (frameCount < antRate)
				frameCount <= frameCount + 1'd1;
			else begin
				frameCount <= 0;
				antCount   <= antCount + 1'd1;
			end
		end
		else if (inpSync == SYNC_LIN_BEG) begin
			horCount      <= 0;
			verCount      <= verCount + 1'd1;
		end
		else if (inpSync == SYNC_PIX_OUT) begin
			horCount <= horCount + 1'd1;
		end

		
	end
	reset <= resetReg;
end

endmodule

//==============================================================
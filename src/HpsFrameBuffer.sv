//==============================================================
// Steve Brown
//==============================================================

module HpsFrameBuffer
(
	input  wire             busClock,
	input  wire             busReset,
	input  wire             busChipSelect,
	input  wire             busWrite,
	input  wire             busRead,
	input  wire [19:0]      busAddress,
	input  wire [31:0]      busWrData,
	output wire [31:0]      busRdData,
	
	input  wire             outClock,
	input  wire             outVS,
	input  wire [2:0]       y_position,
	output reg              outDe,
	output reg  [2:0]       outSync,
	output reg  [23:0]      outData,
	
	input  wire             ovlyClock,
	input  wire             inpDV_fromMixer,
	output wire [127:0]     ovlyFifoData,
	
	input  wire             startOutput,
	input  wire [3:0]       inpBfr,
	
	input  wire [31:0]      hps_overlay_address,
	
	input  wire             ddr3_clk,
	input  wire             avl_ready,      
	output reg  [27:0]      avl_addr,       
	input  wire             avl_rdata_valid, 
	input  wire [127:0]     avl_rdata,     
	output reg              avl_read_req,
   output wire [7:0]	      avl_burstcount
	
);

`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));

//reg [7:0][31:0] register_notsynced = 0;
reg [7:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[4:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin
	if (busReset) 
		register[0] <= 0;
	else if (busChipSelect && (busAddress[19:16] == 0) && busWrite)
		register[busAddress[4:2]] <= busWrData;
	else begin
		register[0][31] <= enable;
	end
end

//---------------------------------------------------------------

wire         reset            = register[0][0];
wire         enable           = register[0][3];
wire [3:0]   burstSize        = register[0][7:4];
wire         stopRead         = register[0][14];
wire         startRead        = register[0][15];
  
wire [3:0]   bfrSize          = register[1][23:20];

wire [19:0]  outAdrStart      = register[2][19:0];
wire [11:0]  timeOut          = register[2][31:20];

wire [1:0]   lineStart        = register[3][1:0];

wire [7:0]   out_bursts_per_line   = register[4][7:0];
wire [12:0]  out_stride_blanking   = register[4][20:8];

wire [7:0]   ovly_bursts_per_line  = register[5][7:0];
wire [12:0]  ovly_stride_blanking  = register[5][20:8];

wire [11:0]  horMax                = register[6][11:0];
wire [11:0]  imgHorMax             = register[6][23:12];
wire [11:0]  imgVerMax             = register[7][11:0];
wire [8:0]   fifoLimit             = register[7][20:12];
wire [10:0]  ovlyFifoLimit         = register[7][31:21];

 
 
//--- DDR3 Clock Domain -------------------------------------------

reg [3:0]  outVsDly       = 0;
reg [7:0]  imgOutBursts   = 0;
reg        busy           = 0;
reg [20:0] outAdr         = 0;
reg [27:0] ovlyAdr        = 0; 
reg [3:0]  outBfr         = 0; 
reg [7:0]  imgReqBursts   = 0; 
reg [7:0]  ovlyReqBursts  = 0;
reg [3:0]  burstRdCount   = 0;
reg [2:0]  outDvDly       = 0;
reg        outStartFrame  = 0;
reg        outStartBank   = 0;
reg        outFirstLine   = 0;
reg [3:0]  avl_size       = 0;
reg [12:0] waitCnt        = 0;
reg [1:0]  ramOutNum      = 0;
reg [1:0]  beginRdOvly    = 0;
reg [10:0] lineCount      = 0;
reg [7:0]  ovlyOutBursts  = 0;
reg [3:0]  burstOvlyCount = 0;
reg [1:0]  beginRdImg     = 0;

//---- Output Clock Domain ----------------------------------------

reg [11:0]  x_out          = 0;
reg [11:0]  y_out          = 0;
reg         outStart       = 0;
reg         activeLine     = 0;
logic [1:0] pixel_index    = 0;
wire        startNewFrame;
reg [1:0]   startNewFrame_reg = 0;
reg [2:0]   last_y_position = 0;
reg         readOutFifo = 0;


//------ Output clock domain ---------------------------------------------------------

assign startNewFrame = (lineCount == 11'd4) && (imgOutBursts == 0) && (rdusedw_fifo_out == imgHorMax[8:0]);

always@(posedge outClock) begin
	
	
	if (reset) begin
		x_out   <= 0;
		y_out   <= 0;
		outDe   <= 0;
		readOutFifo <= 0;
		outStart     <= 0;
		outData      <= 0;
		outSync      <= SYNC_IDLE;
		pixel_index  <= 0;
	end
	else begin
		startNewFrame_reg <= {startNewFrame_reg[0], startNewFrame};
		if (startNewFrame_reg[0] && !startNewFrame_reg[1]) begin  // start a new frame after first line has been read from Frame Buffer
			x_out    <= 0;
			y_out    <= 0;
			outStart <= 1'b1;
			outDe    <= 0;
			outData  <= 0;
			outSync  <= SYNC_IDLE;
			readOutFifo     <= 1'b1;
			pixel_index     <= 0;
			last_y_position <= 0;
		end
		else if (outStart) begin
			if (x_out < horMax) begin
				x_out <= x_out + 1'd1;
			   if (x_out == 0) begin
				   if (y_out == 0) begin
					   outSync <= SYNC_FRM_BEG;
					end
				   else begin
					   outSync <= SYNC_LIN_BEG;
					end
				   outDe        <= 0;
					pixel_index  <= 0;
					outData      <= 0;
					readOutFifo  <= 0;
				end
				else if (x_out <= imgHorMax) begin
				   outSync      <= SYNC_PIX_OUT;
					outDe        <= 1'b1;
					case (pixel_index) 
					   2'd0:  outData <= q_fifo_out[23:0] ;
						2'd1:  outData <= q_fifo_out[47:24] ;
						2'd2:  outData <= q_fifo_out[71:48] ;
						2'd3:  outData <= q_fifo_out[95:72] ;
						default: outData <= 0;
					endcase
					activeLine   <= 1'b1;
					if (pixel_index == 3) begin
					   pixel_index <= 0;
                  readOutFifo <= 0;	
					end
					else begin
					   pixel_index  <= pixel_index + 2'd1;
						if (pixel_index == 2) begin
							if (x_out == (imgHorMax - 1))
								readOutFifo <= 0;
							else
								readOutFifo <= 1'b1;
			         end
	               else begin
	                  readOutFifo <= 0;
		            end							
				   end		
				end
				else if (activeLine) begin
				   outDe        <= 0;
					outData      <= 0;
					pixel_index  <= 0;
					activeLine   <= 0;
					readOutFifo  <= 0;
				   if (y_out == (imgVerMax - 1)) begin
					   outSync <= SYNC_FRM_END;
					end
					else begin
					   outSync <= SYNC_LIN_END;
					end
				end
				else begin
				   outSync      <= SYNC_IDLE;
					outDe        <= 0;
					outData      <= 0;
					activeLine   <= 0;
					pixel_index  <= 0;
					readOutFifo  <= 0;
				end
			end
			else begin
				outData      <= 0;
		      last_y_position         <= y_position;
					
				if (y_out < (imgVerMax - 1)) begin
				   if (y_out >= lineStart) begin
					   if (y_position != last_y_position) begin
					      y_out  <= y_out + 1'd1;
				         x_out  <= 0;	
				         readOutFifo <= 1'b1;	
						end
					end
					else begin
					   y_out <= y_out + 1'd1;
						x_out <= 0;			
				      readOutFifo <= 1'b1;	
					end
				end
				else begin
					outStart <= 0;
					x_out    <= 0;
				end
			end
		end
		else begin
		   outSync <= SYNC_IDLE;
			outDe   <= 0;
			outData <= 0;
		end
	end
			
end

//------ DDR3 clock domain ------------------------------------------------------
	
always@(posedge ddr3_clk) begin

   beginRdOvly <= {beginRdOvly[0], (wrusedw_fifo_ovly < ovlyFifoLimit)};
	beginRdImg   <= {beginRdImg[0], (wrusedw_fifo_out <= fifoLimit)};
	
	// Each time avl_rdata_valid is asserted there is valid data available.
	// Increment the ram block address and burst count accordingly.
	if (avl_rdata_valid && (imgOutBursts > 0)) begin
		if (burstRdCount < avl_size) begin
			burstRdCount <= burstRdCount + 2'd1;
		end
		else begin
			burstRdCount <= 2'd1;
			imgOutBursts <= imgOutBursts - 1'd1;
		end
	end

	if (avl_rdata_valid && (ovlyOutBursts > 0) && (imgOutBursts == 0)) begin
		if (burstOvlyCount < avl_size) begin
			burstOvlyCount <= burstOvlyCount + 2'd1;
		end
		else begin
			burstOvlyCount <= 2'd1;
			ovlyOutBursts  <= ovlyOutBursts - 1'd1;
		end
	end
	
	if (busReset || reset || !enable) begin
		avl_addr          <= 0;
		avl_read_req      <= 0;
		avl_size          <= 0;
		busy              <= 0;
		outAdr            <= 0;
		outVsDly          <= 0;
		imgReqBursts     <= 0;
		outBfr            <= 0;
		lineCount         <= 0;
		ovlyReqBursts    <= 0;
		ovlyAdr           <= 0;
		ovlyOutBursts     <= 0;
	end
	else begin
		
		
		if (!busy) begin
			outVsDly   <= {outVsDly[2:0], outVS};
			outDvDly   <= {outDvDly[1:0], outDe};
		
			if (outVsDly[1] && !outVsDly[2]) begin  // start a new frame at rising edge of outVS (This also sends first line)
				if (startOutput || startRead) begin
					outStartFrame  <= 1'b1;
					imgReqBursts   <= 0;
					ovlyReqBursts  <= 0;
				end
			end
			else if (!outDvDly[1] && outDvDly[2]) begin  // start a new line at falling edge of outDV (replace line that was just read)
				if (startOutput || startRead)
					outStartBank   <= 1'b1;
			end
			else if (avl_ready && (imgReqBursts > 0) && !stopRead && beginRdImg[1]) begin // 
				avl_read_req     <= 1'd1;
				busy             <= 1'd1;
				avl_addr         <= {3'b001, outBfr, outAdr[20:0]};  // Start reading at 512M
				outAdr           <= outAdr + burstSize;
				imgReqBursts    <= imgReqBursts - 1'd1;
			end
			else if (avl_ready && beginRdOvly[1] && (ovlyReqBursts > 0) ) begin // 
				avl_read_req     <= 1'd1;
				busy             <= 1'd1;
				avl_addr         <= ovlyAdr;  
				ovlyAdr          <= ovlyAdr + burstSize;
				ovlyReqBursts   <= ovlyReqBursts - 1'd1;
			end
			else if (outStartFrame && ((imgReqBursts == 0) || stopRead) && (ovlyReqBursts == 0)) begin
				outAdr         <= {2'd0, outAdrStart};
				ovlyAdr        <= hps_overlay_address[31:4];
				avl_size       <= burstSize;
				if (inpBfr > 0)
					outBfr  <= inpBfr - 1'd1;
				else
					outBfr  <= bfrSize;
					
				if ((imgOutBursts > 0) && (waitCnt < timeOut)) begin
				   waitCnt <= waitCnt + 1'd1;
				end
				else begin
					waitCnt        <= 0;
					outStartFrame  <= 0;
					outFirstLine   <= 1'b1;
					outStartBank   <= 1'b1;  // Write the first line without being triggered by read-out timing
					imgOutBursts   <= 0;
					lineCount      <= 0;
					ovlyOutBursts  <= 0;
				end
			end
			else if ((imgReqBursts == 0) && (imgOutBursts == 0)  && (ovlyReqBursts == 0) && (ovlyOutBursts == 0) && (lineCount < 11'd768) ) begin
				lineCount      <= lineCount + 1'd1;
				burstRdCount   <= 2'd1;
				ovlyReqBursts <= ovly_bursts_per_line;
				ovlyOutBursts  <= ovly_bursts_per_line;
				
				if (outStartBank) begin
					imgReqBursts  <= out_bursts_per_line;
					imgOutBursts   <= out_bursts_per_line;
					if (outFirstLine) begin
						outFirstLine <= 0;
					end
					else begin
						outAdr       <= outAdr + out_stride_blanking;
					end
					
					if (lineCount >= 3) begin
						outStartBank <= 0;
					end
					else begin
						outStartBank <= 1'b1;  // write first 4 lines without being triggered
					end

				end

					
				// Don't increment the address on the first line (use outAdrStart value)
				if (outFirstLine)
					outFirstLine  <= 0;
				else 
					ovlyAdr      <= ovlyAdr + ovly_stride_blanking;
			end
			else begin 
				avl_read_req   <= 0;
			end
		end
		else begin
			
			if (avl_ready) begin
				avl_read_req <= 0;
				busy         <= 0;
			end
		end	
	end
	
end


//---------------------------------------------------------------

// Frame buffer SDRAM
//   256M words x  32 bits (physical)
//    64M words x 128 bits (avalon interface) 

assign avl_burstcount = {4'd0, avl_size};

//---------------------------------------------------------------

//---------------------------------------------------------------
// Output FIFO

wire             wrclk_fifo_out    = ddr3_clk;
wire [95:0]      data_fifo_out     = { avl_rdata[119:96], avl_rdata[87:64], avl_rdata[55:32] , avl_rdata[23:0] };
wire             wrreq_fifo_out    = avl_rdata_valid && (imgOutBursts > 0);
wire [8:0]       wrusedw_fifo_out;
wire [95:0]      q_fifo_out;

wire             rdclk_fifo_out    = outClock;

wire [8:0]       rdusedw_fifo_out;
wire             rdreq_fifo_out    = readOutFifo;
wire             aclr_fifo_out     = reset || (outVsDly[0] & !outVsDly[1]);

dcfifo #
(
	.lpm_type                ("dcfifo"),
	.lpm_numwords            (512),
	.lpm_width               (96),
	.lpm_widthu              (9),
	.add_usedw_msb_bit       ("ON"),
	.lpm_showahead           ("OFF"),
	.overflow_checking       ("ON"),
	.underflow_checking      ("ON"),
	.wrsync_delaypipe        (4),
	.rdsync_delaypipe        (4),
	.write_aclr_synch        ("ON"),
	.read_aclr_synch         ("ON"),
	.add_ram_output_register ("ON"),
	.use_eab                 ("ON"),
	.intended_device_family  ("Cyclone V")
) 
fifo_inp_parOut
(
	.wrclk     (wrclk_fifo_out),
	.data      (data_fifo_out),
	.wrreq     (wrreq_fifo_out),
	.wrusedw   (wrusedw_fifo_out),
	.rdclk     (rdclk_fifo_out),
	.q         (q_fifo_out),
	.rdreq     (rdreq_fifo_out),
	.rdusedw   (rdusedw_fifo_out),
	.aclr      (aclr_fifo_out)
);


//---------------------------------------------------------------
//---------------------------------------------------------------

// Overlay Output FIFO
//   1024 words x 128 bits (4 pixel/word)
// FIFO is written when there is room for data
// FIFO is read when inpDe is true

wire             wrclk_fifo_ovly    = ddr3_clk;
wire [127:0]      data_fifo_ovly    = avl_rdata;
wire             wrreq_fifo_ovly    = avl_rdata_valid && (imgOutBursts == 0);
wire [10:0]       wrusedw_fifo_ovly;

wire             rdclk_fifo_ovly    = ovlyClock;
wire             rdreq_fifo_ovly    = inpDV_fromMixer;
wire [10:0]       rdusedw_fifo_ovly;
wire             rdempty_fifo_ovly;
wire             aclr_fifo_ovly     = reset || outFirstLine;

dcfifo #
(
	.lpm_type                ("dcfifo"),
	.lpm_numwords            (2048),
	.lpm_width               (128),
	.lpm_widthu              (11),
	.add_usedw_msb_bit       ("ON"),
	.lpm_showahead           ("OFF"),
	.overflow_checking       ("ON"),
	.underflow_checking      ("ON"),
	.wrsync_delaypipe        (4),
	.rdsync_delaypipe        (4),
	.write_aclr_synch        ("ON"),
	.read_aclr_synch         ("ON"),
	.use_eab                 ("ON"),
	.intended_device_family  ("Cyclone V")
) 
fifo_ovly_
(
	.wrclk     (wrclk_fifo_ovly),
	.data      (data_fifo_ovly),
	.wrreq     (wrreq_fifo_ovly),
	.wrusedw   (wrusedw_fifo_ovly),
	.rdclk     (rdclk_fifo_ovly),
	.q         (ovlyFifoData),
	.rdreq     (rdreq_fifo_ovly),
	.rdusedw   (rdusedw_fifo_ovly),
	.rdempty   (rdempty_fifo_ovly),
	.aclr      (aclr_fifo_ovly)
);

endmodule

///==============================================================
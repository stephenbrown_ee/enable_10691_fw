// Stephen Brown
//==============================================================

module ImageResizer
(
	input  wire                  busClock,
	input  wire                  busReset,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	input  wire                  regUpdate,

	input  wire                  inpClock,
	input  wire [23:0]           inpData,
	input  wire [2:0]            inpSync,
	input  wire [9:0]            wrusedw_fifo_out,
	
	input  wire                  outClock,
	input  wire                  outClockLocked,
	output wire [2:0]            y_position,
	output reg  [2:0][7:0]       outData,
	output reg  [2:0]            outSync
);

parameter RAM_ADDR_WIDTH = 8;      // The RAM buffers need to hold at least half of a line.
parameter NUM_OF_RAM_BUFFERS = 8; // This is the number of half line line buffers
parameter NUM_OF_LINES = 4;        // This is the number of lines stored NUM_OF_RAM_BUFFERS/2


`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

reg [5:0][31:0] register = 0;

assign busRdData = register[busAddress[5:2]];

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[5:2]] <= busWrData;
end

//---------------------------------------------------------------

wire             resetReg   = register[0][0] || busReset;
wire             test       = register[0][1];  // 0: normal data, 1: test output w/ color bars
wire             enable     = register[0][2];

wire [10:0]      imgHorMaxOut = register[1][10:0];
wire [10:0]      imgVerMaxOut = register[1][21:11];

wire [10:0]      imgHorMaxInp = register[2][10:0];
wire [10:0]      imgVerMaxInp = register[2][21:11];

wire [10:0]       row_coeff    = register[3][10:0];
wire [10:0]       col_coeff    = register[3][20:10];

wire [15:0]      horMaxOut    = register[4][15:0];  // maximum horizontal line count
wire [15:0]      verMaxOut    = register[4][31:16];

wire [2:0]       modMax       = register[5][2:0];


reg [10:0]      imgHorMaxOut_reg;
reg [10:0]      imgVerMaxOut_reg;

reg [10:0]      imgHorMaxInp_reg;
reg [10:0]      imgVerMaxInp_reg;

reg [10:0]       row_coeff_reg;
reg [10:0]       col_coeff_reg;

reg [15:0]      horMaxOut_reg;  // maximum horizontal line count
reg [15:0]      verMaxOut_reg;

reg [2:0]       modMax_reg;


reg              wren        = 0;
reg              pixel       = 0;
reg  [2:0]       lineMod     = 0;
reg              inpFirst    = 0;
reg              inpStart    = 0;
reg  [RAM_ADDR_WIDTH-1:0]       inpAddress  = 0;
reg  [15:0]      inpVerCount = 0 /* synthesis syn_noprune = 1 */;
reg  [10:0]      x_1 = 0;  // This is the first part of the horizontal numerator of the fractional part of the pixel
reg  [11:0]      x_2 = 0;  // This is the second part of the horizontal numerator of the fractional part of the pixel
reg  [1:0][10:0]      x_coeff1_reg;
reg  [1:0][10:0]      x_coeff2_reg;
reg  [10:0]      y_1 = 0;  // This is the first part of the horizontal numerator of the fractional part of the pixel
reg  [11:0]      y_2 = 0;  // This is the second part of the horizontal numerator of the fractional part of the pixel

reg              outRun      = 0; 
reg  [NUM_OF_RAM_BUFFERS-1:0][RAM_ADDR_WIDTH-1:0]  outAddress  = 0;
reg  [15:0]      horCount    = 0;
reg  [15:0]      verCount    = 0;
reg              runOut      = 0;
reg  [2:0]  	  sync        = 0;
reg  [14:0][2:0]  syncDly     = 0;
reg  [1:0]       verShift    = 0;
reg              newLine     = 0;
reg  [3:0]       outStart    = 0;

reg  [1:0]       currentState;
reg  [1:0]       nextState;

wire [23:0]      data_in = inpData;

reg  [23:0]      pattData;

reg  [23:0]      data_a_ram;
reg  [RAM_ADDR_WIDTH-1:0]           address_a_ram = 0;
wire [NUM_OF_RAM_BUFFERS-1:0]       wren_a_ram;

wire [NUM_OF_RAM_BUFFERS-1:0][23:0]  q_b_ram; 
wire [NUM_OF_RAM_BUFFERS-1:0][23:0]  data;
assign data = q_b_ram;

logic [23:0]  x1y1data;
logic [23:0]  x2y1data;
logic [23:0]  x1y2data;
logic [23:0]  x2y2data;

wire [2:0][29:0] x1y1_yresult;
wire [2:0][29:0] x2y1_yresult;
wire [2:0][29:0] x1y2_yresult;
wire [2:0][29:0] x2y2_yresult;

wire [2:0][18:0] x1y1_xresult;
wire [2:0][18:0] x2y1_xresult;
wire [2:0][18:0] x1y2_xresult;
wire [2:0][18:0] x2y2_xresult;

reg  [2:0][31:0] sumResult;
reg  [2:0][21:0] sumMultiplier;
reg  [2:0][22:0] xMultiplier;
wire [2:0][32:0] xresult;
wire [2:0][33:0] finalResult;

reg  reset = 0;

always@(posedge inpClock) begin
	reset <= resetReg;
	if (reset || test) begin
		address_a_ram <= 0;
		wren          <= 0;
		data_a_ram    <= 0;
		inpFirst      <= 0;
		inpStart      <= 0;
		inpAddress    <= 0;    
	end
	else begin
			
		if (inpSync == SYNC_FRM_BEG) begin
			address_a_ram       <= 0;
			wren                <= 1'd1;
			data_a_ram          <= 0;
			inpFirst            <= 1'd1;
			inpStart            <= 0;
			inpAddress          <= 0;  
			pixel               <= 1'd1;
			inpVerCount         <= 0;
			if(!regUpdate)
				modMax_reg <= modMax;
		end
		else if (inpSync == SYNC_LIN_BEG) begin
			address_a_ram       <= 0;
			wren                <= 1'd1;
			data_a_ram          <= 0;
			inpAddress          <= 0;  
			pixel               <= 1'd1;
			inpVerCount         <= inpVerCount + 1'd1; 
		end
		else if (inpSync == SYNC_PIX_OUT) begin
			address_a_ram       <= inpAddress;
			wren                <= 1'd1;
			data_a_ram          <= data_in;
			pixel               <= ~pixel;
			if (!pixel)
				inpAddress       <= inpAddress + 1'd1;    
		end
		else if (inpSync == SYNC_LIN_END) begin
			address_a_ram       <= 0;
			wren                <= 0;
			data_a_ram          <= 0;
			inpAddress          <= 0;
			if (inpFirst && (lineMod == modMax_reg)) begin				
				inpFirst        <= 0;
				inpStart        <= 1'd1;
			end
			else begin
				inpStart        <= 0;
			end
								
			if (lineMod < (NUM_OF_LINES - 1))
				lineMod  <= lineMod + 1'd1;
			else
				lineMod  <= 3'd0;  
		end
		else if (inpSync == SYNC_FRM_END) begin
			address_a_ram       <= 0;
			wren                <= 0;
			data_a_ram          <= 0;
			inpAddress          <= 0;
			inpVerCount         <= 0;
			lineMod             <= 3'd0;
		end
		
	end
end	

wire        repeatX  = ((syncDly[0] == SYNC_LIN_BEG) || (syncDly[0] == SYNC_FRM_BEG)) ? 1'd1 : ((x_2 + imgHorMaxInp_reg) < imgHorMaxOut_reg);  
wire [10:0] x_coeff1 = repeatX ? imgHorMaxInp_reg : x_1;
wire [10:0] x_coeff2 = repeatX ? 11'd0 : (imgHorMaxInp_reg - x_1);
wire        repeatY  = (y_2 + imgVerMaxInp_reg) < imgVerMaxOut_reg;  
wire [10:0] y_coeff1 = repeatY ? imgVerMaxInp_reg : y_1;
wire [10:0] y_coeff2 = repeatY ? 11'd0 : (imgVerMaxInp_reg - y_1);

logic [3:0]  pix1_pos;
logic [3:0]  pix2_pos;
logic [3:0]  pix3_pos;
logic [3:0]  pix4_pos;

reg  [2:0]  ypos;
assign y_position = ypos;

always@(*) begin  // There are 4 lines buffered and we're only using 2 at a time, so cycle through
	case (ypos)
		3'd0: begin
			pix1_pos = 4'd0;
			pix2_pos = 4'd1;
			pix3_pos = 4'd2;
			pix4_pos = 4'd3;
		end
		3'd1: begin
			pix1_pos = 4'd2;
			pix2_pos = 4'd3;
			pix3_pos = 4'd4;
			pix4_pos = 4'd5;
		end
		3'd2: begin
			pix1_pos = 4'd4;
			pix2_pos = 4'd5;
			pix3_pos = 4'd6;
			pix4_pos = 4'd7;
		end
		3'd3: begin
			pix1_pos = 4'd6;
			pix2_pos = 4'd7;
			pix3_pos = 4'd0;
			pix4_pos = 4'd1;
		end
		default: begin
			pix1_pos = 0;
			pix2_pos = 0;
			pix3_pos = 0;
			pix4_pos = 0;
		end
	endcase
end

always@(*) begin
	case (currentState)
		2'd0: begin
			x1y1data = data[pix1_pos];
			x2y1data = data[pix2_pos];
			x1y2data = data[pix3_pos];
			x2y2data = data[pix4_pos];
		end
		2'd1: begin
			x1y1data = data[pix1_pos];
			x2y1data = data[pix2_pos];
			x1y2data = data[pix3_pos];
			x2y2data = data[pix4_pos];
		end
		2'd2: begin
			x1y1data = data[pix2_pos];  // data switches in this state 
			x2y1data = data[pix1_pos];
			x1y2data = data[pix4_pos];
			x2y2data = data[pix3_pos];
		end
		2'd3: begin
			x1y1data = data[pix2_pos];  // data switches in this state 
			x2y1data = data[pix1_pos];
			x1y2data = data[pix4_pos];
			x2y2data = data[pix3_pos];
		end  
		default: begin
			x1y1data = 0;
			x2y1data = 0;
			x1y2data = 0;
			x2y2data = 0;
		end
	endcase
end

wire [2:0]  preSync;
assign preSync = syncDly[14];
		

always@(posedge outClock) begin
	
	if (resetReg || !outClockLocked) begin
	   sync       <= SYNC_IDLE;
		outSync    <= SYNC_IDLE;
		outData    <= 0;
		runOut     <= 0;
		horCount   <= 0;
		verCount   <= 0;
		x_2        <= 0;
		x_1        <= 0;
		ypos       <= 0;
		outRun     <= 0;
		newLine    <= 0;
		imgHorMaxOut_reg <= imgHorMaxOut;
		imgHorMaxInp_reg <= imgHorMaxInp;
		imgVerMaxInp_reg <= imgVerMaxInp;
		imgVerMaxOut_reg <= imgVerMaxOut;
		row_coeff_reg    <= row_coeff;
		col_coeff_reg    <= col_coeff;
		horMaxOut_reg    <= horMaxOut;
		verMaxOut_reg    <= verMaxOut;		
	end
	else if (!runOut) begin
		runOut        <= enable;
		horCount      <= 0;
		verCount      <= 0;
		outData       <= 0;
		sync          <= SYNC_IDLE;
		outSync       <= SYNC_IDLE;
		ypos          <= 0;
		newLine       <= 1'd1;
		outRun        <= 0;
	end
	else begin
		
		case (horCount[7:5])  
			0: pattData <= 24'h000000; // Color bars
			1: pattData <= 24'h0000FF;   
			2: pattData <= 24'h00FF00;         
			3: pattData <= 24'hFF0000;                      
			4: pattData <= 24'hFFFF00; 
			5: pattData <= 24'hFF00FF;               
			6: pattData <= 24'h00FFFF;   
			7: pattData <= 24'hFFFFFF;   
		endcase
		
		if (test) begin
			outData <= pattData;
		end
		else begin
			outData[2]       <= (finalResult[2][33:16] > 16'd255) ? 8'd255 : finalResult[2][23:16];
			outData[1]       <= (finalResult[1][33:16] > 16'd255) ? 8'd255 : finalResult[1][23:16];
			outData[0]       <= (finalResult[0][33:16] > 16'd255) ? 8'd255 : finalResult[0][23:16];
		end
		
		outSync <= syncDly[12];
		syncDly <= {syncDly[13:0], sync}; 
		sumResult[2]     <= {2'd0, x1y1_yresult[2]} + {2'd0, x2y1_yresult[2]} + {2'd0, x1y2_yresult[2]} + {2'd0, x2y2_yresult[2]};
		sumResult[1]     <= {2'd0, x1y1_yresult[1]} + {2'd0, x2y1_yresult[1]} + {2'd0, x1y2_yresult[1]} + {2'd0, x2y2_yresult[1]};
		sumResult[0]     <= {2'd0, x1y1_yresult[0]} + {2'd0, x2y1_yresult[0]} + {2'd0, x1y2_yresult[0]} + {2'd0, x2y2_yresult[0]};
		sumMultiplier[2] <= sumResult[2][31:10];
		sumMultiplier[1] <= sumResult[1][31:10];
		sumMultiplier[0] <= sumResult[0][31:10];
		xMultiplier[2]   <= xresult[2][32:10];
		xMultiplier[1]   <= xresult[1][32:10];
		xMultiplier[0]   <= xresult[0][32:10];	
		x_coeff1_reg <= {x_coeff1_reg[0], x_coeff1};
		x_coeff2_reg <= {x_coeff2_reg[0], x_coeff2};
		
		currentState <= nextState;		
		
		// This is the logic that will increment the RAM address when needed.
		if ((sync == SYNC_PIX_OUT) || (syncDly[8] == SYNC_PIX_OUT)) begin
			if (repeatX) begin
				if ((nextState == 2'd1)||(nextState == 2'd3))
					nextState <= 2'd3;
				else	
					nextState <= 0;
					
				if ((syncDly[0] == SYNC_LIN_BEG) || (syncDly[0] == SYNC_FRM_BEG)) begin
					x_1       <= imgHorMaxOut_reg - imgHorMaxInp_reg;
					x_2       <= imgHorMaxInp_reg;
				end
				else begin
					x_2       <= x_2 + imgHorMaxInp_reg;
					x_1       <= x_1 - imgHorMaxInp_reg;
				end		
			end
			else begin
				x_1 <= imgHorMaxOut_reg - x_coeff2;
				x_2 <= imgHorMaxInp_reg - x_1;
				if ((nextState == 0) || (nextState == 2'd2)) begin
					nextState <= 2'd1;
					if (newLine) begin
						newLine <= 0;
					end
					else begin	
						outAddress[pix2_pos] <= outAddress[pix2_pos] + 1'd1;
						outAddress[pix4_pos] <= outAddress[pix4_pos] + 1'd1;
					end						
				end								
				else begin // if (nextState == 2'd1 || 2'd3)
					nextState <= 2'd2;
					outAddress[pix1_pos] <= outAddress[pix1_pos] + 1'd1;
					outAddress[pix3_pos] <= outAddress[pix3_pos] + 1'd1;
				end
			end
		end
		else if (preSync == SYNC_LIN_END) begin
			x_1        <= 0;
			x_2        <= imgHorMaxInp_reg;
			outAddress <= 0;
			nextState  <= 0;
			newLine    <= 1'd1;
			if (repeatY) begin
				y_2   <= y_2 + imgVerMaxInp_reg;
				y_1   <= y_1 - imgVerMaxInp_reg;
			end
			else begin
				y_1 <= imgVerMaxOut_reg - y_coeff2;
				y_2 <= imgVerMaxInp_reg - y_1;
				if (ypos < (NUM_OF_LINES - 1))
					ypos <= ypos + 1'd1;
				else
					ypos <= 0;
			end
		end
		else if (preSync == SYNC_FRM_END) begin
			y_1        <= imgVerMaxOut_reg;
			y_2        <= 0;
			x_1        <= 0;
			x_2        <= imgHorMaxInp_reg;
			outAddress <= 0;
			ypos       <= 3'd0;
			nextState  <= 2'd0;
			newLine    <= 1'd1;
		end
		
		
		// Clock crossing of inpStart signal
		outStart <= {outStart[2:0], inpStart};
		
		
		if (!outRun) begin
			verCount <= 0;
		   horCount <= 0;
			if ((outStart[2] && !outStart[3]) || test)
				outRun <= 1'd1;
		end
		else begin
			if((verCount < imgVerMaxOut_reg) && (horCount < (imgHorMaxOut_reg + 2'd2))) begin
				
			   horCount <= horCount + 1'd1;	
				
				if (horCount == 0) begin
					if (verCount == 0) begin
						sync  <= SYNC_FRM_BEG;
					end
					else begin
						sync  <= SYNC_LIN_BEG;
					end
				end				
					
				// Line pixel content
				else if (horCount < imgHorMaxOut_reg) begin
					sync  <= SYNC_PIX_OUT;	
				end 		
			
				// Line end sync
				else if (horCount == imgHorMaxOut_reg + 1'd1) begin
					if (verCount < (imgVerMaxOut_reg - 1)) begin
						sync  <= SYNC_LIN_END;
					end
					else begin
						sync  <= SYNC_FRM_END;
					end
						
				end
			end
			// Idle area
			else begin
				sync <= SYNC_IDLE;
					
				if (horCount == horMaxOut_reg) begin
					
					// Idle lines after image
					if (verCount < verMaxOut_reg) begin
						if (wrusedw_fifo_out == 10'd0) begin
							verCount <= verCount + 1'd1;
							horCount <= 0;
						end
					end
					else begin
						verCount <= 0;
						outRun   <= 0;
						if (!enable)
							runOut <= 0;
													
						if (!regUpdate) begin
							imgHorMaxOut_reg <= imgHorMaxOut;
							imgHorMaxInp_reg <= imgHorMaxInp;
							imgVerMaxInp_reg <= imgVerMaxInp;
							imgVerMaxOut_reg <= imgVerMaxOut;
							row_coeff_reg    <= row_coeff;
							col_coeff_reg    <= col_coeff;
							horMaxOut_reg    <= horMaxOut;
							verMaxOut_reg    <= verMaxOut;
						end	
					end
				end
				else begin				   
			      horCount <= horCount + 1'd1;
				end
			end
		end
	end
end


//--- Half Line Buffers ---------------------------------------------------------

genvar m;
generate 
	for (m = 0; m <= (NUM_OF_RAM_BUFFERS-1); m++)  begin: ramBuffers
	
		assign wren_a_ram[m] = ({lineMod, pixel} == m) && wren;
		
		RamTwoClock # 
		(
			.dat_wid (24), 
			.adr_wid (RAM_ADDR_WIDTH)
		)
		ram
		(
			.data_a    (data_a_ram),
			.wren_a    (wren_a_ram[m]),
			.q_b       (q_b_ram[m]),
			.address_a (address_a_ram),
			.address_b (outAddress[m]),
			.clock_a   (inpClock),
			.clock_b   (outClock)
		);
	end
endgenerate


//--- Multiplication -------------------------------------------------------------

genvar n;
generate 
	for (n = 0; n <= 2; n++)  begin: multResize
		
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (8),
			.res_wid   (19),
			.pipeline  (2)
		)
		x1y1_x1_mult
		(
			.clock  (outClock),
			.data_a (x_coeff1_reg[1]),
			.data_b (x1y1data[8*n+7:8*n]),
			.result (x1y1_xresult[n])
		);
		
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (8),
			.res_wid   (19),
			.pipeline  (2)
		)
		x2y1_x2_mult
		(
			.clock  (outClock),
			.data_a (x_coeff2_reg[1]),
			.data_b (x2y1data[8*n+7:8*n]),
			.result (x2y1_xresult[n])
		);
		
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (8),
			.res_wid   (19),
			.pipeline  (2)
		)
		x1y2_x1_mult
		(
			.clock  (outClock),
			.data_a (x_coeff1_reg[1]),
			.data_b (x1y2data[8*n+7:8*n]),
			.result (x1y2_xresult[n])
		);
		
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (8),
			.res_wid   (19),
			.pipeline  (2)
		)
		x2y2_x2_mult
		(
			.clock  (outClock),
			.data_a (x_coeff2_reg[1]),
			.data_b (x2y2data[8*n+7:8*n]),
			.result (x2y2_xresult[n])
		);			
		
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (19),
			.res_wid   (30),
			.pipeline  (2)
		)
		x1y1_y1_mult
		(
			.clock  (outClock),
			.data_a (y_coeff1),
			.data_b (x1y1_xresult[n]),
			.result (x1y1_yresult[n])
		);
		
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (19),
			.res_wid   (30),
			.pipeline  (2)
		)
		x2y1_y2_mult
		(
			.clock  (outClock),
			.data_a (y_coeff1),
			.data_b (x2y1_xresult[n]),
			.result (x2y1_yresult[n])
		);
		
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (19),
			.res_wid   (30),
			.pipeline  (2)
		)
		x1y2_y1_mult
		(
			.clock  (outClock),
			.data_a (y_coeff2),
			.data_b (x1y2_xresult[n]),
			.result (x1y2_yresult[n])
		);
		
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (19),
			.res_wid   (30),
			.pipeline  (2)
		)
		x2y2_y2_mult
		(
			.clock  (outClock),
			.data_a (y_coeff2),
			.data_b (x2y2_xresult[n]),
			.result (x2y2_yresult[n])
		);	
				
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (22),
			.res_wid   (33),
			.pipeline  (2)
		)
		col_coeff_mult
		(
			.clock  (outClock),
			.data_a (col_coeff_reg),
			.data_b (sumMultiplier[n]),
			.result (xresult[n])
		);	
		
		MultiplyUnsigned #
		(
			.dat_wid_a (11),
			.dat_wid_b (23),
			.res_wid   (34),
			.pipeline  (2)
		)
		row_coeff_mult
		(
			.clock  (outClock),
			.data_a (row_coeff_reg),
			.data_b (xMultiplier[n]),
			.result (finalResult[n])
		);
	end
endgenerate
	
	
endmodule
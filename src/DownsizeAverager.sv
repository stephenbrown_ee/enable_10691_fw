//==============================================================

module DownsizeAverager
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,

	input  wire                  inpClock,
	input  wire [4:0][4:0][11:0] inpData,  // inpData[X][Y]
	input  wire [2:0]            inpSync,
	output reg  [2:0][11:0]      outData,
	output reg  [2:0]            outSync
);

//--- Bus Interface ----------------------------------------------

reg [1:0][31:0] register = 0;

assign busRdData = register[busAddress[2]];

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[2]] <= busWrData;
end

//---------------------------------------------------------------

wire        resetReg   = register[0][0];
wire        bypass     = register[0][1];
wire        size       = register[0][2];   // 0=2x2 1=4x4
wire [1:0]  startColor = register[0][5:4]; // YX

reg              reset = 0;
reg  [1:0]       phase = 0;  // YX CFA phase (always starts at 0)
reg  [2:0]       sync  = 0;
reg  [3:0][11:0] data  = 0;

`include "PixelPipe.h";

// Shiftable coordinates

wire [2:0] x0 = 3'd0 + phase[0]; // 0 1 0 1
wire [2:0] x1 = 3'd1 + phase[0]; // 1 2 1 2
wire [2:0] x2 = 3'd2 + phase[0]; // 2 3 2 3
wire [2:0] x3 = 3'd3 + phase[0]; // 3 0 3 0

wire [2:0] y1 = 3'd1 - phase[1]; // 1 1 0 0 
wire [2:0] y2 = 3'd2 - phase[1]; // 2 2 1 1
wire [2:0] y3 = 3'd3 - phase[1]; // 3 3 2 2
wire [2:0] y4 = 3'd4 - phase[1]; // 0 0 3 3

always@(posedge inpClock) begin
	if (reset) begin
		outSync   <= SYNC_IDLE;
		outData   <= 0;
		sync      <= 0;
		data      <= 0;
		phase     <= 0;
	end
	else if (bypass) begin
		outSync    <= inpSync;
		outData[0] <= inpData[2][2];
		outData[1] <= inpData[2][2];
		outData[2] <= inpData[2][2];
	end
	else begin
		outSync <= sync;
		sync    <= inpSync;
		
		if (inpSync == SYNC_FRM_BEG)
			phase    <= 0;
		else if (inpSync == SYNC_LIN_BEG) begin
			phase[0] <= 0;
			phase[1] <= ~phase[1];
		end
		else if (inpSync == SYNC_PIX_OUT)
			phase[0] <= ~phase[0];
		
		if (sync != SYNC_PIX_OUT) begin
			outData <= 0;
		end
		else begin
			outData[0] <=      data[{ startColor[1],  startColor[0]}];
			outData[1] <= avg2(data[{~startColor[1],  startColor[0]}], data[{ startColor[1], ~startColor[0]}]);
			outData[2] <=      data[{~startColor[1], ~startColor[0]}];
		end
		
		if (size == 0) begin
			data[0] <= inpData[x2][y2];
			data[1] <= inpData[x1][y2];
			data[2] <= inpData[x2][y3];
			data[3] <= inpData[x1][y3];
		end
		else begin
			data[0] <= avg4(inpData[x0][y2], inpData[x2][y2], inpData[x0][y4], inpData[x2][y4]);
			data[1] <= avg4(inpData[x1][y2], inpData[x3][y2], inpData[x1][y4], inpData[x3][y4]);
			data[2] <= avg4(inpData[x0][y1], inpData[x2][y1], inpData[x0][y3], inpData[x2][y3]);
			data[3] <= avg4(inpData[x1][y1], inpData[x3][y1], inpData[x1][y3], inpData[x3][y3]);
		end
	end
	reset <= resetReg;
end



// 2,2   3,2   2,1   3,1
// 1,2   2,2   1,1   2,1
// 2,3   3,3   2,2   3,2
// 1,3   2,3   1,2   2,2

//--- Two by two -----------------------------------------

//  phase   0            1            2            3 
//      0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0   - - - - -    - - - - -    - - - - -    - - - - -
//  1   - - - - -    - - - - -    - 1 0 - -    - - 1 0 -
//  2   - 1 0 - -    - - 1 0 -    - 3 2 - -    - - 3 2 -         
//  3   - 3 2 - -    - - 3 2 -    - - - - -    - - - - -
//  4   - - - - -    - - - - -    - - - - -    - - - - -

//  0: R start 
//        R            Gr           Gb           B
//    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0 - - - - -    - - - - -    - - - - -    - - - - -
//  1 - - - - -    - - - - -    - G R - -    - - G R -
//  2 - G R - -    - - G R -    - B G - -    - - B G -         
//  3 - B G - -    - - B G -    - - - - -    - - - - -
//  4 - - - - -    - - - - -    - - - - -    - - - - -

//  1: Gr start 
//          Gr           R            B            Gb   
//      0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0   - - - - -    - - - - -    - - - - -    - - - - -
//  1   - - - - -    - - - - -    - R G - -    - - R G -
//  2   - R G - -    - - R G -    - G B - -    - - G B -         
//  3   - G B - -    - - G B -    - - - - -    - - - - -
//  4   - - - - -    - - - - -    - - - - -    - - - - -

//  2: Gb start 
//          Gb           B            R            Gr   
//      0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0   - - - - -    - - - - -    - - - - -    - - - - -
//  1   - - - - -    - - - - -    - B G - -    - - B G -
//  2   - B G - -    - - B G -    - G R - -    - - G R -         
//  3   - G R - -    - - G R -    - - - - -    - - - - -
//  4   - - - - -    - - - - -    - - - - -    - - - - -

//  3: B start 
//          B            Gb           Gr           R   
//      0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0   - - - - -    - - - - -    - - - - -    - - - - -
//  1   - - - - -    - - - - -    - G B - -    - - G B -
//  2   - G B - -    - - G B -    - R G - -    - - R G -         
//  3   - R G - -    - - R G -    - - - - -    - - - - -
//  4   - - - - -    - - - - -    - - - - -    - - - - -

//--- Four by four -----------------------------------------

//  phase   0            1            2            3 
//      0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0   - - - - -    - - - - -    2 3 2 3 -    - 2 3 2 3
//  1   2 3 2 3 -    - 2 3 2 3    0 1 0 1 -    - 0 1 0 1
//  2   0 1 0 1 -    - 0 1 0 1    2 3 2 3 -    - 2 3 2 3         
//  3   2 3 2 3 -    - 2 3 2 3    0 1 0 1 -    - 0 1 0 1
//  4   0 1 0 1 -    - 0 1 0 1    - - - - -    - - - - -

//  0: R start 
//          R            Gr           Gb           B   
//      0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0   - - - - -    - - - - -    G B G B -    - G B G B
//  1   G B G B -    - G B G B    R G R G -    - R G R G
//  2   R G R G -    - R G R G    G B G B -    - G B G B         
//  3   G B G B -    - G B G B    R G R G -    - R G R G
//  4   R G R G -    - R G R G    - - - - -    - - - - -

//  1: Gr start 
//          Gr           R            B            Gb   
//      0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0   - - - - -    - - - - -    B G B G -    - B G B G
//  1   B G B G -    - B G B G    G R G R -    - G R G R
//  2   G R G R -    - G R G R    B G B G -    - B G B G         
//  3   B G B G -    - B G B G    G R G R -    - G R G R
//  4   G R G R -    - G R G R    - - - - -    - - - - -

//  2: Gb start 
//          Gb           B            R            Gr   
//      0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0   - - - - -    - - - - -    R G R G -    - R G R G
//  1   R G R G -    - R G R G    G B G B -    - G B G B
//  2   G B G B -    - G B G B    R G R G -    - R G R G         
//  3   R G R G -    - R G R G    G B G B -    - G B G B
//  4   G B G B -    - G B G B    - - - - -    - - - - -

//  3: B start 
//          B            Gb           Gr           R   
//      0 1 2 3 4    0 1 2 3 4    0 1 2 3 4    0 1 2 3 4
//  0   - - - - -    - - - - -    G R G R -    - G R G R
//  1   G R G R -    - G R G R    B G B G -    - B G B G
//  2   B G B G -    - B G B G    G R G R -    - G R G R         
//  3   G R G R -    - G R G R    B G B G -    - B G B G
//  4   B G B G -    - B G B G    - - - - -    - - - - -

function [11:0] avg2(input [11:0] a, input [11:0] b);
	integer avg;
	avg = (a + b) / 2;
	avg2 = avg[11:0];
endfunction

function [11:0] avg4(input [11:0] a, input [11:0] b, input [11:0] c, input [11:0] d);
	integer avg;
	avg = (a + b + c + d) / 4;
	avg4 = avg[11:0];
endfunction

endmodule

//==============================================================
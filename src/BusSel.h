//==============================================================
// WARNING: This header is used in both SystemVerilog and C files.
//          Declarations must be compatible with both.
//==============================================================

typedef enum
{
	bus_sysMgr,      // 0x10000000
	bus_i2cMas,      // 0x10100000
	bus_senCtl,      // 0x10200000
	bus_senInp,      // 0x10300000
	bus_imgGen,      // 0x10400000
	bus_paxInp,      // 0x10500000
	bus_fltBfr,      // 0x10600000
	bus_spaFlt,      // 0x10700000
	bus_whiBal,      // 0x10800000
	bus_bayBfr,      // 0x10900000
	bus_bayInt,      // 0x10A00000
	bus_corMat,      // 0x10B00000
	bus_gamLut,      // 0x10C00000
	bus_parHDMI,     // 0x10D00000
	bus_pipTst,      // 0x10E00000
	bus_crsOvr,      // 0x10F00000
	bus_paxOut,      // 0x11000000
	bus_hstSts,      // 0x11100000
	bus_dszAvg,      // 0x11200000
	bus_paxSts,      // 0x11300000
	bus_expSts,      // 0x11400000
	bus_awbSts,      // 0x11500000
	bus_frmBuf,      // 0x11600000
	bus_parLCD,      // 0x11700000
	bus_imgRsz,      // 0x11800000
	bus_lcddsp,      // 0x11900000
	bus_i2cMasMSP430,// 0x11A00000
	bus_pwm,         // 0x11B00000
	bus_chipID,      // 0x11C00000
	bus_hpsFb,       // 0x11D00000
	bus_pwmLed,      // 0x11E00000
	bus_defPix,      // 0x11F00000
	bus_pixCor,      // 0x12000000
	bus_imgRsz_hdmi, // 0x12100000
	bus_hps2fpga,    // 0x12200000
	bus_tmds,        // 0x12200000
	bus_xcvr,        // 0x12300000
	bus_devices
} BusSel;

//==============================================================

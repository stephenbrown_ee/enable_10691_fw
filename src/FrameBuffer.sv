// Steve Brown
// 
// Description:  Controls the input to the frame buffer in DDR3 memory
//               Also controls the output path for HDMI
//==============================================================

module FrameBuffer
(
	input  wire             busClock,
	input  wire             busReset,
	input  wire             busChipSelect,
	input  wire             busWrite,
	input  wire             busRead,
	input  wire [19:0]      busAddress,
	input  wire [31:0]      busWrData,
	output wire [31:0]      busRdData,
	
	input  wire             inpClock,
	input  wire             inpClockLocked,
	input  wire [2:0]       inpSync,
	input  wire [23:0]      inpData,	
	
	input  wire             outClock,
	input  wire             outVS,
	input  wire [2:0]       y_position,
	output reg              outDe,
	output reg  [2:0]       outSync,
	output wire [23:0]      outData,
	
	output reg              startOutput,
	output reg  [3:0]       inpBfr,
	
	input  wire             ddr3_clk,
	input  wire             avl_ready,      
	output reg  [27:0]      avl_addr,       
	input  wire             avl_rdata_valid, 
	input  wire [127:0]     avl_rdata,      
	output wire [127:0]     avl_wdata,     
	output reg              avl_read_req,    
	output reg              avl_write_req,
   output wire [7:0]	      avl_burstcount,
	output reg  [15:0]      avl_be
	
);

parameter NUM_OF_RAM = 2;

`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));

//reg [7:0][31:0] register_notsynced = 0;
reg [7:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[4:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin
	if (busReset) 
		register[0] <= 0;
	else if (busChipSelect && (busAddress[19:16] == 0) && busWrite)
		register[busAddress[4:2]] <= busWrData;
	else begin
		register[0][31] <= clearBuffer;
	end
end

//---------------------------------------------------------------

wire         reset            = register[0][0];
wire         enable           = register[0][3];
wire [3:0]   burstSize        = register[0][7:4];
wire         enableInpFifo    = register[0][12];
wire         clearBuffer      = register[0][13];
wire         stopRead         = register[0][14];
wire         startRead        = register[0][15];
  
wire [19:0]  adrStart         = register[1][19:0];
wire [3:0]   bfrSize          = register[1][23:20];
wire [7:0]   ramInpStartAdr   = register[1][31:24];

wire [19:0]  outAdrStart      = register[2][19:0];
wire [12:0]  timeOut          = register[2][31:20];

wire [7:0]   inp_bursts_per_line  = register[3][7:0];
wire signed [13:0]  inp_stride_blanking  = register[3][21:8];
wire [8:0]   fifoLimit            = register[3][30:22];

wire [7:0]   out_bursts_per_line  = register[4][7:0];
wire signed [13:0]  out_stride_blanking  = register[4][21:8];
wire [1:0]   lineStart        = register[4][23:22];

wire [15:0]  inpVsCntMax          = register[5][15:0];
wire [11:0]  waitCntMax           = register[5][27:16];

wire [11:0]  horMax               = register[6][11:0];
wire [11:0]  imgHorMax            = register[6][23:12];
wire [11:0]  imgVerMax            = register[7][11:0];
wire [7:0]   ramOutStartAddr      = register[7][19:12];

 
//--- Input Clock Domain ------------------------------------------

reg [3:0][23:0] inpDataReg     = 0;
reg [7:0]       ramInpWrAdr    = 0;
reg [7:0]       ramInpRdAdr    = 0;
reg [15:0]      inpVsCnt       = 0;
reg             ramInpWrBank   = 0;
reg             ramInpRdBank   = 0;
reg [1:0]       inpCount       = 0;
 
//--- DDR3 Clock Domain -------------------------------------------

reg        inpVS          = 0;
reg [3:0]  inpDvDly       = 0;
reg [3:0]  inpVsDly       = 0;
reg [3:0]  outVsDly       = 0;
reg [1:0]  beginRdBurst   = 0;
reg [7:0]  ramOutWrAdr    = 0;
reg [7:0]  ramOutBursts   = 0;
reg [1:0]  ramOutWrBank   = 0;
reg [6:0]  burstCount     = 0;
reg        busy           = 0;
reg        write          = 0;
reg signed [22:0] outAdr         = 0; 
reg signed [21:0] inpAdr         = 0;
reg [3:0]  outBfr         = 0;
reg [7:0]  inpLineBursts  = 0;
reg [7:0]  outLineBursts  = 0;
reg [3:0]  burstRdCount   = 0;
reg [2:0]  outDvDly       = 0;
reg        outStartFrame  = 0;
reg        outFirstLine   = 0;
reg        avl_burstbegin = 0;
reg [3:0]  avl_size       = 0;
reg [12:0] waitCnt        = 0;
reg [1:0]  ramOutNum      = 0;
reg [11:0] waitForFifoCnt = 0;
reg        startWait      = 0;
reg        newInpLineReady = 0;
reg        firstInpLine    = 0;
reg [10:0] lineCount      = 0;
reg        outStartBank   = 0;

//---- Output Clock Domain ----------------------------------------

reg [11:0] x_out          = 0;
reg [11:0] y_out          = 0;
reg        outStart       = 0;
reg         readOutFifo   = 0;
reg         activeLine     = 0;
logic [1:0] pixel_index    = 0;
wire        startNewFrame;
reg [1:0]   startNewFrame_reg = 0;
reg [2:0]   last_y_position = 0;
wire [8:0]  wrusedw_fifo_out;

//------ Input clock domain ---------------------------------------------------------
// This always block creates a 96 bit wide parallel data bus that is written into the input FIFO
// This also generates an input VSYNC signal that increments the frame buffer number and clears the FIFO
always@ (posedge inpClock) begin
	if (busReset || reset || !enable || !enableInpFifo || !inpClockLocked) begin
		inpDataReg    <= 0;
		ramInpWrAdr   <= 0;
		inpVS         <= 0;
		ramInpWrBank  <= 0;
	end
	else if (inpSync == SYNC_FRM_END) begin
	   inpVS        <= 1'b1;
		inpVsCnt     <= inpVsCntMax;
		ramInpWrBank <= 0;
	end
	else if (inpSync == SYNC_LIN_END) begin
	   ramInpWrBank <= ~ramInpWrBank;
	end
	else if (inpVsCnt > 0) begin
	   inpVsCnt <= inpVsCnt - 16'd1;
   end 
	else if (inpSync == SYNC_PIX_OUT) begin
	   inpDataReg[inpCount] <= inpData;
	   inpVS <= 0;
		if (inpCount < 3)
		   inpCount <= inpCount + 1'd1;
		else begin
		   inpCount <= 0;
			ramInpWrAdr <= ramInpWrAdr + 1'd1;
	   end
	end
	else begin
		inpDataReg  <= 0;
		ramInpWrAdr <= 0;
	   inpVS       <= 0;
		inpCount    <= 0;
	end
end

//------ Output clock domain ---------------------------------------------------------

assign startNewFrame = (lineCount == 11'd4) && (outLineBursts == 0) && (rdusedw_fifo_out == imgHorMax[8:0]);

always@(posedge outClock) begin
	
	
	if (reset) begin
		x_out   <= 0;
		y_out   <= 0;
		outDe   <= 0;
		readOutFifo  <= 0;
		outStart     <= 0;
		outData      <= 0;
		outSync      <= SYNC_IDLE;
		pixel_index  <= 0;
	end
	else begin
		startNewFrame_reg <= {startNewFrame_reg[0], startNewFrame};
		if (startNewFrame_reg[0] && !startNewFrame_reg[1]) begin  // start a new frame after first line has been read from Frame Buffer
			x_out    <= 0;
			y_out    <= 0;
			outStart <= 1'b1;
			outDe    <= 0;
			outData  <= 0;
			outSync  <= SYNC_IDLE;
			readOutFifo     <= 1'b1;
			pixel_index     <= 0;
			last_y_position <= 0;
		end
		else if (outStart) begin
			if (x_out < horMax) begin
				x_out <= x_out + 1'd1;
			   if (x_out == 0) begin
				   if (y_out == 0) begin
					   outSync <= SYNC_FRM_BEG;
					end
				   else begin
					   outSync <= SYNC_LIN_BEG;
					end
				   outDe        <= 0;
					pixel_index  <= 0;
					outData      <= 0;
					readOutFifo  <= 0;
				end
				else if (x_out <= imgHorMax) begin
				   outSync      <= SYNC_PIX_OUT;
					outDe        <= 1'b1;
					case (pixel_index) 
					   2'd0:  outData <= q_fifo_out[23:0] ;
						2'd1:  outData <= q_fifo_out[47:24] ;
						2'd2:  outData <= q_fifo_out[71:48] ;
						2'd3:  outData <= q_fifo_out[95:72] ;
						default: outData <= 0;
					endcase
					activeLine   <= 1'b1;
					if (pixel_index == 3) begin
					   pixel_index <= 0;
                  readOutFifo <= 0;	
					end
					else begin
					   pixel_index  <= pixel_index + 2'd1;
						if (pixel_index == 2) begin
							if (x_out == (imgHorMax - 1))
								readOutFifo <= 0;
							else
								readOutFifo <= 1'b1;
			         end
	               else begin
	                  readOutFifo <= 0;
		            end					
				   end		
				end
				else if (activeLine) begin
				   outDe        <= 0;
					outData      <= 0;
					pixel_index  <= 0;
					activeLine   <= 0;
					readOutFifo  <= 0;
				   if (y_out == (imgVerMax - 1)) begin
					   outSync <= SYNC_FRM_END;
					end
					else begin
					   outSync <= SYNC_LIN_END;
					end
				end
				else begin
				   outSync      <= SYNC_IDLE;
					outDe        <= 0;
					outData      <= 0;
					activeLine   <= 0;
					pixel_index  <= 0;
					readOutFifo  <= 0;
				end
			end
			else begin
				outData         <= 0;
		      last_y_position <= y_position;
					
				if (y_out < (imgVerMax - 1)) begin
				   if (y_out >= lineStart) begin
					   if (y_position != last_y_position) begin  // State will wait here for imgResizer to be ready
					      y_out       <= y_out + 1'd1;
				         x_out       <= 0;
				         readOutFifo <= 1'b1;		
						end
					end
					else begin
					   y_out       <= y_out + 1'd1;
						x_out       <= 0;
				      readOutFifo <= 1'b1;				
					end
				end
				else begin
					outStart <= 0;
					x_out    <= 0;
				end
			end
		end
		else begin
		   outSync <= SYNC_IDLE;
			outDe   <= 0;
			outData <= 0;
		end
	end
			
end

//------ DDR3 clock domain ------------------------------------------------------

	
always@(posedge ddr3_clk) begin
	beginRdBurst     <= {beginRdBurst[0], (wrusedw_fifo_out <= fifoLimit)};
	
	if (busReset || reset || !enable) begin
		avl_addr          <= 0;
		avl_write_req     <= 0;
		avl_read_req      <= 0;
		avl_burstbegin    <= 0;
		avl_size          <= 0;
		avl_be            <= 0;
		write             <= 0;
		busy              <= 0;
		outAdr            <= 0;
		inpAdr            <= 0;
		inpVsDly          <= 0;
		outVsDly          <= 0;
		burstCount        <= 0;
		outLineBursts     <= 0;
		inpLineBursts     <= 0;
		inpBfr            <= 0;
		outBfr            <= 0;
		startOutput       <= 0;
		lineCount         <= 0;
	end
	else begin
		
		
		if (!busy) begin
			inpVsDly   <= {inpVsDly[2:0], inpVS};
			outVsDly   <= {outVsDly[2:0], outVS};
			outDvDly   <= {outDvDly[1:0], outDe};
			inpDvDly   <= {inpDvDly[2:0], (inpSync == SYNC_PIX_OUT)};
		   if (!inpDvDly[2] && inpDvDly[3] && !newInpLineReady)
			   newInpLineReady <= 1'b1;
		
			if (!inpVsDly[2] && inpVsDly[3]) begin  // Reset input side on vertical sync falling edge
				inpAdr        <= {3'd0, adrStart};
				inpLineBursts <= 0;
				ramInpRdBank  <= 0;
				firstInpLine  <= 1'b1;
				if(inpBfr < bfrSize) 
					inpBfr <= inpBfr + 1'd1;
				else
					inpBfr <= 0;
				
				if(inpBfr == 4'd2)
					startOutput <= 1'b1;	  // Wait until the input buffer has incremented to 2 to start the output			
			end
			else if (outVsDly[1] && !outVsDly[2]) begin  // start a new frame at rising edge of outVS (This also sends first line)
				if (startOutput || startRead) begin
					outStartFrame  <= 1'b1;
					outLineBursts  <= 0;
				end
			end
			else if (!outDvDly[1] && outDvDly[2]) begin  // start a new line at falling edge of outDV (replace line that was just read)
				if (startOutput || startRead)
					outStartBank   <= 1'b1;
			end
			else if (avl_ready && (inpLineBursts > 0)) begin // Start a write burst if input FIFO has data
				avl_write_req    <= 1'd1;
				avl_read_req     <= 0;
				avl_burstbegin   <= 1'd1;
				avl_size         <= burstSize;
				avl_be           <= 16'hFFFF;
				burstCount       <= 1'd1;
				busy             <= 1'd1;
				write            <= 1'd1;
				avl_addr         <= {3'b001, inpBfr, inpAdr[20:0]};  // Only write above 512 MB
				inpAdr           <= inpAdr + burstSize;
				inpLineBursts    <= inpLineBursts - 1'd1;
				ramInpRdAdr      <= ramInpRdAdr + 1'd1;
			end
			else if (newInpLineReady && (inpLineBursts == 0) ) begin
				newInpLineReady <= 0;
				ramInpRdAdr     <= ramInpStartAdr;
				inpLineBursts   <= inp_bursts_per_line;
				if (firstInpLine) begin
				   firstInpLine <= 0;
				end
			   else begin
					ramInpRdBank    <= ~ramInpRdBank;
					inpAdr          <= $signed(inpAdr + {{8{inp_stride_blanking[13]}}, inp_stride_blanking});
				end
			end		
			else if (avl_ready && (outLineBursts > 0) && !stopRead && beginRdBurst[1]) begin // 
				avl_read_req     <= 1'd1;        
				avl_write_req    <= 0;
				avl_burstbegin   <= 1'd1;
				busy             <= 1'd1;
				write            <= 0;
				avl_addr         <= {3'b001, outBfr, outAdr[20:0]};  // Only read above 512MB
				outAdr           <= outAdr + burstSize;
				outLineBursts    <= outLineBursts - 1'd1;
				burstCount       <= 1'd1;
			end
			else if (outStartFrame && (outLineBursts == 0) && beginRdBurst[1]) begin
				outAdr         <= {3'd0, outAdrStart};
				if (inpBfr > 0)
					outBfr  <= inpBfr - 1'd1;
				else
					outBfr  <= bfrSize;
					
				if (waitCnt < timeOut) begin
				   waitCnt <= waitCnt + 1'd1;
				end
				else begin
					waitCnt        <= 0;
					outStartFrame  <= 0;
					outFirstLine   <= 1'b1;
					lineCount      <= 0;
               outStartBank   <= 1'b1;
				end
			end
			else if (outStartBank && (outLineBursts == 0) && beginRdBurst[1]) begin
				outLineBursts  <= out_bursts_per_line;
				burstRdCount   <= 2'd1;
				lineCount      <= lineCount + 1'd1;
				if (lineCount >= 3)
				   outStartBank <= 0;
				else
				   outStartBank <= 1'b1;
				
				// Don't increment the address on the first line (use outAdrStart value)
				if (outFirstLine)
					outFirstLine  <= 0;
				else 
					outAdr       <= outAdr + {{8{out_stride_blanking[13]}}, out_stride_blanking};
			end
			else begin 
				avl_write_req  <= 0;
				avl_read_req   <= 0;
				avl_burstbegin <= 0;	
			end
		end
		else begin
			avl_burstbegin <= 0;
			
			if (write) begin
				if (avl_write_req && avl_ready) begin
					if (burstCount < avl_size) begin
						burstCount     <= burstCount + 1'd1;
					   ramInpRdAdr    <= ramInpRdAdr + 1'd1;
					end
					else begin
						avl_write_req  <= 0;
						startWait      <= 1'b1;
					end	
				end
				if(startWait) begin				
					// This wait count was introduced because a beginWrBurst was being erroneously started
					// because the rdused_fifo count doesn't decrement fast enough
					// which caused a read of an empty FIFO and video glitches
					if (waitForFifoCnt < waitCntMax)
						waitForFifoCnt <= waitForFifoCnt + 1'd1;
					else begin
						waitForFifoCnt <= 0;
						busy           <= 0;
						startWait      <= 0;
					end
				end
			end
			else begin
				if (avl_ready) begin
					avl_read_req <= 0;
					busy         <= 0;
				end
			end
		end	
	end
	
end

//---------------------------------------------------------------

// Input RAM
//   Write - 256 words x 96 bits (4 pixels/word)
//   Read  - 256 words x 96 bits (4 pixels/word)


wire              clock_a_ram_inp   = inpClock;
wire [7:0]        address_a_ram_inp = ramInpWrAdr;

wire [95:0]       data_a_ram_inp    = {inpData, inpDataReg[2:0]};

wire              clock_b_ram_inp   = ddr3_clk;
wire [7:0]        address_b_ram_inp;
wire              wren_ram_inp;
wire [1:0]        wren_a_ram_inp;

wire [1:0][95:0]  q_b_ram_inp;


assign address_b_ram_inp = ramInpRdAdr;
assign wren_ram_inp      = (inpCount == 2'd3);

		
genvar j;
generate
	for (j = 0; j <= (NUM_OF_RAM-1); j++) begin: ram_inp_rollout
		
		assign wren_a_ram_inp[j]    = wren_ram_inp && (ramInpWrBank == j);
		
		RamTwoClock # 
		(
			.dat_wid (96), 
			.adr_wid (8)
		)
		ram_inp
		(
			.clock_a   (clock_a_ram_inp),
			.address_a (address_a_ram_inp),
			.wren_a    (wren_a_ram_inp[j]),
			.data_a    (data_a_ram_inp),
			.clock_b   (clock_b_ram_inp),
			.address_b (address_b_ram_inp),
			.q_b       (q_b_ram_inp[j])
		);

	end
endgenerate

//---------------------------------------------------------------

// Frame buffer SDRAM
//   256M words x  32 bits (physical)
//    64M words x 128 bits (avalon interface) 
/*
assign avl_wdata = { 8'd0, q_b_ram_inp[ramInpRdBank][23:0], 8'd0, q_b_ram_inp[ramInpRdBank][47:24],  
						   8'd0, q_b_ram_inp[ramInpRdBank][71:48], 8'd0, q_b_ram_inp[ramInpRdBank][95:72] } ; */
assign avl_wdata = { 8'd0, q_b_ram_inp[ramInpRdBank][95:72], 8'd0, q_b_ram_inp[ramInpRdBank][71:48],  
						   8'd0, q_b_ram_inp[ramInpRdBank][47:24], 8'd0, q_b_ram_inp[ramInpRdBank][23:0] } ;
 
assign avl_burstcount = {4'd0, avl_size};

//---------------------------------------------------------------


//---------------------------------------------------------------
// Output FIFO

wire             wrclk_fifo_out    = ddr3_clk;
wire [95:0]      data_fifo_out     = { avl_rdata[119:96], avl_rdata[87:64], avl_rdata[55:32] , avl_rdata[23:0] };
wire             wrreq_fifo_out    = avl_rdata_valid;
wire [95:0]      q_fifo_out;

wire             rdclk_fifo_out    = outClock;

wire [8:0]       rdusedw_fifo_out;
wire             rdreq_fifo_out    = readOutFifo;
wire             aclr_fifo_out     = reset || (outVsDly[0] & !outVsDly[1]);

dcfifo #
(
	.lpm_type                ("dcfifo"),
	.lpm_numwords            (512),
	.lpm_width               (96),
	.lpm_widthu              (9),
	.add_usedw_msb_bit       ("ON"),
	.lpm_showahead           ("OFF"),
	.overflow_checking       ("ON"),
	.underflow_checking      ("ON"),
	.wrsync_delaypipe        (4),
	.rdsync_delaypipe        (4),
	.write_aclr_synch        ("ON"),
	.read_aclr_synch         ("ON"),
	.add_ram_output_register ("ON"),
	.use_eab                 ("ON"),
	.intended_device_family  ("Cyclone V")
) 
fifo_inp_parOut
(
	.wrclk     (wrclk_fifo_out),
	.data      (data_fifo_out),
	.wrreq     (wrreq_fifo_out),
	.wrusedw   (wrusedw_fifo_out),
	.rdclk     (rdclk_fifo_out),
	.q         (q_fifo_out),
	.rdreq     (rdreq_fifo_out),
	.rdusedw   (rdusedw_fifo_out),
	.aclr      (aclr_fifo_out)
);


endmodule

///==============================================================
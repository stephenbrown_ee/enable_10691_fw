//==============================================================

module SensorSimulator
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	input  wire                  regUpdate,
	
	input  wire                  inpClock,
	input  wire [11:0]           inpData,
	input  wire                  href_in,
	input  wire                  vsync_in,
	
	output reg  [11:0]           outData,
	output reg                   href_out,
	output reg                   vsync_out
);

//--- Bus Interface ----------------------------------------------

reg [4:0][31:0] register = 0;

assign busRdData = register[busAddress[4:2]];

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[4:2]] <= busWrData;
end

//---------------------------------------------------------------

localparam 
	B_CNT  = 7'd11,
	Gb_CNT = 7'd6,
	Gr_CNT = 7'd17,
	R_CNT  = 7'd6,
	B_BLK  = 7'd14,
	Gb_BLK = 7'd6,
	Gr_BLK = 7'd20,
	R_BLK  = 7'd6,
	VBLANK = 7'd30,
	FRM_START = 7'd92;
	

wire        reset      = register[0][0];
wire        source     = register[0][1];
wire        enable     = register[0][2];


wire [11:0] BLANK_LVL  = register[1][11:0];  // Active pixels per line
wire [11:0] BLACK_LVL  = register[1][23:12]; // number of pixel counts for active vsync


wire hBlank;

logic verBlank = 0

always_comb begin
	hBlank = inpData < BLANK_LVL;
end

logic [9:0]       hBlank_reg  = 0;
logic [9:0][11:0] inpData_reg = 0;

always@(posedge inpClock) begin

	hBlank_reg  <= {hBlank_reg[8:0], hBlank};
	inpData_reg <= {inpData_reg[8:0], inpData};
	
	if (reset) begin
		run           <= 0;
		horCount      <= 0;
		verCount      <= 0;
		outData       <= 0;
		href_out      <= 0;
		vsync_out     <= 0;
	end
	else begin
		if (hBlank_reg[0]) begin
			blankCount <= blankCount + 12'd1;
		end
		else begin
			blankCount <= 0;
		end
		
		if (hBlank_reg[1] && !hBlank_reg[0] && !verBlank) begin
			if (blankCount >= VBLANK) begin
				verBlank   <= 1'b1;
				red_count  <= 0;
				blu_count  <= 0;
				gb_count   <= 0;
				gr_count   <= 0;
			end
			else if (blankCount == B_CNT)
	end

end

endmodule

//==============================================================
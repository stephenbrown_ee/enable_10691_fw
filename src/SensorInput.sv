//==============================================================

module SensorInput
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,

	input  wire                  inpClock,
	input  wire [11:0]           inpData,
	output logic                 verBlank,
	output logic                 frameStart,
	output logic                 led_sync,
	
	output reg  [11:0]           outData,
	output reg  [2:0]            outSync
);

`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

reg [8:0][31:0] register = 0;

always begin
	if (busChipSelect)
		busRdData = register[busAddress[5:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[5:2]] <= busWrData;
	else begin
		register[4][11:0]  <= blankMax;
		register[4][27:16] <= blankErrorLevel;
		register[5][11:0]  <= blankStart;
		register[6][31:0]  <= clkPerFrame;
		register[7][31:0]  <= frameCount;
		register[8][11:0]  <= black_avg_result[30:19];
	end
end

//---------------------------------------------------------------

wire        reset        = register[0][0];
wire        autoBlack    = register[0][1];
wire        enableBLC    = register[0][2];
wire [9:0]  multiplier   = register[0][12:3];
//wire [2:0]  black_end    = register[0][16:14];
wire [5:0]  alpha        = register[0][22:17];
wire        clearBlank   = register[0][23];
wire [11:0] BLANK_LEVEL  = register[1][11:0];  // black is subtracted first
wire signed [12:0] BLACK_LEVEL  = register[1][28:16]; 
wire [7:0]  BLACK_COUNT  = register[2][7:0];
wire [8:0]  ROW_START    = register[2][16:8];
wire [7:0]  black_target = register[2][24:17]; 
wire [8:0]  syncStart    = register[3][8:0]; 


localparam 
	B_CNT      = 7'd11,
	GB_CNT     = 7'd6,
	GR_CNT     = 7'd17,
	R_CNT      = 7'd6,
	B_BLK_CNT  = 7'd14,
	GB_BLK_CNT = 7'd6,
	GR_BLK_CNT = 7'd20,
	R_BLK_CNT  = 7'd6,
	VBLANK     = 7'd30,
	FRM_START  = 7'd92;
	
localparam
	BLUE   = 4'd0,
	GB     = 4'd1,
	GR     = 4'd2,
	RED    = 4'd3,
	BLANK  = 4'd4,
	B_BLK  = 4'd5,
	GB_BLK = 4'd6,
	GR_BLK = 4'd7,
	R_BLK  = 4'd8;

wire               hBlank;

logic      [2:0]   hBlank_reg = 0;
logic [2:0][11:0]  inpData_reg = 0;
logic      [6:0]   blankCount = 0;
logic      [7:0]   black_count_r = 0;  // This is the number of black counts before image data
logic      [3:0]   row_color = 0;
logic      [7:0]   pix_count = 0;
logic              start_wr = 0;
logic              verBlank_r = 0;
logic      [31:0]  frameCount  = 0;
logic      [31:0]  clkPerFrame = 0;
logic      [8:0]   inpRowCount = 0;
logic      [11:0]  blankMax = 0;
logic      [11:0]  blankStart = 0;
logic              blankError = 0;
logic      [11:0]  blankErrorLevel = 0;

logic signed [12:0] black_lvl_reg = 0;
logic [7:0]  blk_pix_count = 0;

wire [30:0]  black_avg_result;
wire [17:0]  result_prev_black;
wire [17:0]  result_black;

logic [11:0] prev_black_avg = 0;
logic [11:0] black_final = 0;
logic [20:0] black_sum = 0;
logic [20:0] black_sum_reg = 0;
logic        p_start0 = 0;
logic 		 p_start1 = 0;


assign hBlank = (inpData < BLANK_LEVEL);

always_ff @(posedge inpClock) begin

	if(reset) begin
		hBlank_reg    <= 0;
		inpData_reg   <= 0;
		blankCount    <= 0;
		verBlank      <= 0;
		frameStart    <= 0;
		row_color     <= 0;
		black_count_r <= 0;
		start_wr      <= 0;
		pix_count     <= 0;
		inpRowCount   <= 0;
		blk_pix_count <= 0;
		p_start0      <= 0;
		p_start1      <= 0;
		blankMax      <= 0;
		blankStart    <= 0;
		blankError    <= 0;
		led_sync      <= 0;
	end
	else begin
		hBlank_reg  <= {hBlank_reg[1:0], hBlank};
		inpData_reg <= {inpData_reg[1:0], inpData};

		if (inpRowCount > syncStart) begin
			led_sync <= 1'b1;
		end
		else begin
			led_sync <= 0;
		end
		
		if (hBlank_reg[0]) begin
			blankCount <= blankCount + 7'd1;
		end
		else if (!hBlank_reg[1]) begin
			blankCount <= 0;
		end

		if (hBlank_reg[0] && !hBlank_reg[1]) begin
			if(inpData_reg[0] > blankStart) begin
				blankStart <= inpData_reg[0];
			end
		end
		else if (clearBlank) begin
			blankStart <= 0;
		end
		
		if (blankError) begin
			blankErrorLevel <= inpData_reg[2];
		end
		
		if(hBlank_reg[1] && !hBlank_reg[0] ) begin
			if (blankCount == FRM_START) begin
				frameStart <= 1'b1;
				verBlank   <= 0;
				row_color  <= BLANK;
				inpRowCount <= 0;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			else if (blankCount == VBLANK) begin
				verBlank   <= 1'b1;
				frameStart <= 0;
				row_color  <= BLANK;
				inpRowCount <= 0;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			else if (blankCount == B_CNT) begin
				black_count_r <= BLACK_COUNT - B_CNT;
				row_color   <= BLUE;
				inpRowCount <= inpRowCount + 9'd1;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			else if ((blankCount == GB_CNT) && (row_color == BLUE)) begin
				black_count_r <= BLACK_COUNT - GB_CNT;
				row_color   <= GB;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			else if (blankCount == GR_CNT) begin
				black_count_r <= BLACK_COUNT - GR_CNT;
				row_color   <= GR;
				inpRowCount <= inpRowCount + 9'd1;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			else if ((blankCount == R_CNT) && (row_color == GR)) begin
				black_count_r  <= BLACK_COUNT - R_CNT;
				row_color    <= RED;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			else if ((blankCount == B_BLK_CNT) && !verBlank) begin
				black_count_r <= BLACK_COUNT - B_BLK_CNT;
				row_color     <= B_BLK;
				inpRowCount <= inpRowCount + 9'd1;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			else if ((blankCount == GB_BLK_CNT) && (row_color == B_BLK)) begin
				black_count_r  <= BLACK_COUNT - GB_BLK_CNT;
				row_color     <= GB_BLK;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			else if ((blankCount == GR_BLK_CNT) && !verBlank) begin
				black_count_r <= BLACK_COUNT - GR_BLK_CNT;
				row_color     <= GR_BLK;
				inpRowCount <= inpRowCount + 9'd1;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			else if ((blankCount == R_BLK_CNT) && (row_color == GR_BLK)) begin
				black_count_r  <= BLACK_COUNT - R_BLK_CNT;
				row_color     <= R_BLK;
				if (inpData_reg[1] > blankMax)
					blankMax <= inpData_reg[1];
			end
			
			else if ((inpRowCount >= ROW_START) && (pix_count > 0)) begin
				blankError <= 1'b1;
			end
		end
		else if (clearBlank) begin
			blankMax <= 0;
		end
		else begin
			blankError <= 0;
		end

			
		if ((row_color == B_BLK) || (row_color == GB_BLK) || (row_color == GR_BLK) || (row_color == R_BLK)) begin
			if (black_count_r > 0) begin
				black_count_r <= black_count_r - 8'd1;
				start_wr    <= 1'b1;
			end
			else if (start_wr && (blk_pix_count == 0)) begin
				blk_pix_count <= 8'd200;
				start_wr      <= 0;
			end
			else if (blk_pix_count > 0) begin
				blk_pix_count <= blk_pix_count - 8'd1;
			end		
		end
		else begin		
			if(black_count_r > 0) begin
				black_count_r <= black_count_r - 8'd1;
				start_wr    <= 1'b1;
			end
			else if (start_wr && (pix_count == 0)) begin
				pix_count <= 8'd200;
				start_wr  <= 0;
			end
			else if (pix_count > 0) begin
				pix_count <= pix_count - 8'd1;
			end
		end
		
		// Create a flag for when the real image data starts
		p_start0 <= (inpRowCount == ROW_START);
		p_start1 <= p_start0;
		
		
		black_final <= result_black[16:5] + result_prev_black[16:5];
		
		if(enableBLC) begin
			if(p_start0 && !p_start1) begin		
				black_sum_reg  <= black_sum;
				black_sum      <= 0;
				prev_black_avg <= black_data;
			end
			else if(black_wren) begin									 
				black_sum <= black_sum + inpData;
			end
		end
	end
end


logic blu_row_wrreq;
logic gb_row_wrreq;
logic gr_row_wrreq; 
logic red_row_wrreq;

logic [3:0] black_wren;
wire [11:0] black_data;

assign black_data = black_avg_result[30:19];
  

always_comb begin
	if (inpRowCount >= ROW_START) begin
		blu_row_wrreq = (row_color == BLUE) && (pix_count > 0);
		gb_row_wrreq  = (row_color == GB)  && (pix_count > 0);
		gr_row_wrreq  = (row_color == GR)  && (pix_count > 0);
		red_row_wrreq = (row_color == RED) && (pix_count > 0);
	end
	else begin
		blu_row_wrreq = 0;
		gb_row_wrreq  = 0;
		gr_row_wrreq  = 0;
		red_row_wrreq = 0;
	end
	
	if (enableBLC) begin
		black_wren = (blk_pix_count > 0) ;
	end
	else begin
		black_wren = 1'd0;
	end
end

wire [7:0] blu_row_fifo_usedw;
wire [7:0] gb_row_fifo_usedw;
wire [7:0] gr_row_fifo_usedw;
wire [7:0] red_row_fifo_usedw;

wire [11:0] blu_row_q;
wire [11:0] gb_row_q;
wire [11:0] gr_row_q;
wire [11:0] red_row_q;

logic      blu_row_rdreq;
logic      gb_row_rdreq;
logic      gr_row_rdreq;
logic      red_row_rdreq;

wire fifo_aclr = verBlank && !verBlank_r;  // falling edge of verBlank

scfifo #
(
    .lpm_width               (12),
    .lpm_widthu              (8),
    .lpm_numwords            (256),
    .lpm_showahead           ("ON"),
    .lpm_type                ("scfifo"),
    .lpm_hint                ("USE_EAB=ON"),
    .intended_device_family  ("Cyclone V"),
    .underflow_checking      ("ON"),
    .overflow_checking       ("ON"),
    .allow_rwcycle_when_full ("OFF"),
    .use_eab                 ("ON"),
    .add_ram_output_register ("OFF") 
)
blu_row_fifo
( 
	.data                     (inpData), 
    .clock                   (inpClock), 
    .wrreq                   (blu_row_wrreq), 
    .rdreq                   (blu_row_rdreq), 
    .aclr                    (fifo_aclr), 
    .sclr                    (),
    .q                       (blu_row_q), 
    .usedw                   (blu_row_fifo_usedw), 
    .full                    (),
	.eccstatus                (),
	.almost_full              (),
	.almost_empty             (),
    .empty                   ()
);

scfifo #
(
    .lpm_width               (12),
    .lpm_widthu              (8),
    .lpm_numwords            (256),
    .lpm_showahead           ("ON"),
    .lpm_type                ("scfifo"),
    .lpm_hint                ("USE_EAB=ON"),
    .intended_device_family  ("Cyclone V"),
    .underflow_checking      ("ON"),
    .overflow_checking       ("ON"),
    .allow_rwcycle_when_full ("OFF"),
    .use_eab                 ("ON"),
    .add_ram_output_register ("OFF")  
)
gb_row_fifo
( 
	.data                    (inpData), 
    .clock                   (inpClock), 
    .wrreq                   (gb_row_wrreq), 
    .rdreq                   (gb_row_rdreq), 
    .aclr                    (fifo_aclr), 
    .sclr                    (),
    .q                       (gb_row_q), 
    .usedw                   (gb_row_fifo_usedw), 
    .full                    (),
	.eccstatus               (),
	.almost_full             (),
	.almost_empty            (), 
    .empty                   ()
);

scfifo #
(
    .lpm_width               (12),
    .lpm_widthu              (8),
    .lpm_numwords            (256),
    .lpm_showahead           ("ON"),
    .lpm_type                ("scfifo"),
    .lpm_hint                ("USE_EAB=ON"),
    .intended_device_family  ("Cyclone V"),
    .underflow_checking      ("ON"),
    .overflow_checking       ("ON"),
    .allow_rwcycle_when_full ("OFF"),
    .use_eab                 ("ON"),
    .add_ram_output_register ("OFF") 
)
gr_row_fifo
( 
	.data                    (inpData), 
    .clock                   (inpClock), 
    .wrreq                   (gr_row_wrreq), 
    .rdreq                   (gr_row_rdreq), 
    .aclr                    (fifo_aclr), 
    .sclr                    (),
    .q                       (gr_row_q), 
    .usedw                   (gr_row_fifo_usedw), 
    .full                    (),
	.eccstatus               (),
	.almost_full             (),
	.almost_empty            (), 
    .empty                   ()
);

scfifo #
(
    .lpm_width               (12),
    .lpm_widthu              (8),
    .lpm_numwords            (256),
    .lpm_showahead           ("ON"),
    .lpm_type                ("scfifo"),
    .lpm_hint                ("USE_EAB=ON"),
    .intended_device_family  ("Cyclone V"),
    .underflow_checking      ("ON"),
    .overflow_checking       ("ON"),
    .allow_rwcycle_when_full ("OFF"),
    .use_eab                 ("ON"),
    .add_ram_output_register ("OFF") 
)
red_row_fifo
( 
	.data                    (inpData), 
    .clock                   (inpClock), 
    .wrreq                   (red_row_wrreq), 
    .rdreq                   (red_row_rdreq), 
    .aclr                    (fifo_aclr), 
    .sclr                    (),
    .q                       (red_row_q), 
    .usedw                   (red_row_fifo_usedw), 
    .full                    (),
	.eccstatus               (),
	.almost_full             (),
	.almost_empty            (), 
    .empty                   ()
);

wire blu_row_ready = (blu_row_fifo_usedw == 8'd200) && (gb_row_fifo_usedw == 8'd200);
wire red_row_ready = (gr_row_fifo_usedw == 8'd200) && (red_row_fifo_usedw == 8'd200);

wire signed [12:0] blc = data0 - {1'd0, black_final} + {5'd0, black_target};

logic [8:0] horCount = 0;
logic [8:0] verCount = 0;
logic [31:0] frameClkCount = 0;
logic [31:0] black_accum = 0;
logic [31:0] black_total = 0;

logic [2:0]  sync0 = 0;
logic [2:0]  sync1 = 0;

logic signed [12:0] data0 = 0;
logic signed [12:0] data1 = 0;

always_ff @(posedge inpClock) begin

	if (reset) begin
//		frameClkCount <= 0;
//		frameCount    <= 0;
		verBlank_r    <= 0;
		horCount      <= 9'h1FF;
		verCount      <= 9'h1FF;
		blu_row_rdreq <= 0;
		gb_row_rdreq <= 0;
		gr_row_rdreq <= 0;
		red_row_rdreq <= 0;
	end
	else begin
	
		verBlank_r <= verBlank;
		
		sync1   <= sync0;
		outSync <= sync1;
		
		if (autoBlack) begin
			if (blc < 0)
				data1 <= 13'd0;
			else
				data1 <= blc;
		end
		else begin
			if ((data0 - black_lvl_reg) < 0)
				data1 <= 13'd0;
			else
				data1 <= data0 - black_lvl_reg;
		end		
			
		outData <= data1[11:0];
		
		if (verBlank && !verBlank_r) begin  
			frameClkCount  <= 0;
			clkPerFrame    <= frameClkCount;
			frameCount     <= frameCount + 1'd1;
			black_lvl_reg  <= BLACK_LEVEL;
		end
		else begin
			frameClkCount <= frameClkCount +1'd1;
		end
		

		if (verCount >= 9'd400) begin
			sync0 <= SYNC_IDLE;
			data0 <= 0;
			if(verBlank_r && !verBlank) begin
				verCount <= 0;
			end
		end
		else if (horCount == 9'd400) begin
			horCount <= horCount + 9'd1;
			sync0    <= (verCount == 9'd399) ? SYNC_FRM_END : SYNC_LIN_END;
			verCount <= verCount + 9'd1;
		end
		else if (horCount < 9'd400) begin
			sync0  <= SYNC_PIX_OUT;
			horCount <= horCount + 9'd1;
			if (verCount[0]) begin
				if(!horCount[0]) begin
					red_row_rdreq  <= 1'b1;
					gr_row_rdreq   <= 0;
					data0        <= {1'd0, gr_row_q};
				end
				else begin
					gr_row_rdreq   <= (horCount == 9'd399) ? 1'b0 : 1'b1;
					red_row_rdreq  <= 0;
					data0          <= {1'd0, red_row_q};
				end
			end
			else begin
				if(!horCount[0]) begin
					gb_row_rdreq  <= 1'b1;
					blu_row_rdreq <= 0;
					data0         <= {1'd0, blu_row_q};
				end
				else begin
					blu_row_rdreq <= (horCount == 9'd399) ? 1'b0 : 1'b1;
					gb_row_rdreq  <= 0;
					data0         <= {1'd0, gb_row_q};
				end
			end
		end
		else begin
			data0 <= 0;		
			if(blu_row_ready || red_row_ready) begin
				horCount      <= 0;
				if (verCount == 9'd0) begin
					sync0  <= SYNC_FRM_BEG;
					blu_row_rdreq  <= 1'b1;
				end
				else begin
					sync0 <= SYNC_LIN_BEG;
					if(verCount[0]) begin
						gr_row_rdreq <= 1'b1;
					end
					else begin
						blu_row_rdreq  <= 1'b1;
					end
				end
			end
			else begin
				sync0 <= SYNC_IDLE;
			end
		end
	
	end
end


MultiplyUnsigned #
(
	.dat_wid_a   (21),
	.dat_wid_b   (10),
	.res_wid     (31),
	.pipeline    (2)
)
avg_bl_inst
(
	.clock       (inpClock),
	.data_a      (black_sum_reg),
	.data_b      (multiplier),
	.result      (black_avg_result)
);

MultiplyUnsigned #
(
	.dat_wid_a   (12),
	.dat_wid_b   (6),
	.res_wid     (18),
	.pipeline    (1)
)
cor1_inst
(
	.clock       (inpClock),
	.data_a      (prev_black_avg),
	.data_b      (alpha),
	.result      (result_prev_black)
);


MultiplyUnsigned #
(
	.dat_wid_a   (12),
	.dat_wid_b   (6),
	.res_wid     (18),
	.pipeline    (1)
)
cor2_inst
(
	.clock       (inpClock),
	.data_a      (black_data),
	.data_b      (6'd32 - alpha),
	.result      (result_black)
);

//MultiplyUnsigned #
//(
//	.dat_wid_a   (20),
//	.dat_wid_b   (11),
//	.res_wid     (31),
//	.pipeline    (2)
//)
//avg_gb_inst
//(
//	.clock       (inpClock),
//	.data_a      (gb_black_reg),
//	.data_b      (multiplier),
//	.result      (gb_result)
//);
//
//
//MultiplyUnsigned #
//(
//	.dat_wid_a   (20),
//	.dat_wid_b   (11),
//	.res_wid     (31),
//	.pipeline    (2)
//)
//avg_gr_inst
//(
//	.clock       (inpClock),
//	.data_a      (gr_black_reg),
//	.data_b      (multiplier),
//	.result      (gr_result)
//);
//
//
//MultiplyUnsigned #
//(
//	.dat_wid_a   (20),
//	.dat_wid_b   (11),
//	.res_wid     (31),
//	.pipeline    (2)
//)
//avg_red_inst
//(
//	.clock       (inpClock),
//	.data_a      (red_black_reg),
//	.data_b      (multiplier),
//	.result      (red_result)
//);
	
endmodule

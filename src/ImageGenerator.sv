//==============================================================

module ImageGenerator
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	
	input  wire                  regUpdate,
	
	input  wire                  inpClock,
	input  wire [11:0]           inpData,
	input  wire [2:0]            inpSync,
	output reg  [11:0]           outData,
	output reg  [2:0]            outSync
);
`include "PixelPipe.h"
//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));
wire selRam = (busChipSelect && (busAddress[19:16] == 1'h1));

reg [7:0][31:0] register_notsynced;
reg [7:0][31:0] register;

always@(*) begin
	if (selReg)
		busRdData = register_notsynced[busAddress[4:2]];
	else if (selRam) 	
		busRdData = { 20'd0, q_a_ram };
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (selReg && busWrite)
		register_notsynced[busAddress[4:2]] <= busWrData;
	else begin
		register_notsynced[0][8]     <= run;
		register_notsynced[4][3:0]   <= colCount;
		register_notsynced[4][6:4]   <= rowCount;
		register_notsynced[4][15:8]  <= frameCount;
		register_notsynced[4][27:16] <= verCount;
	end
end

wire        wren_a_ram    = selRam && busWrite;
wire [11:0] data_a_ram    = busWrData[11:0];
wire [8:0]  address_a_ram = busAddress[10:2];
wire [11:0] q_a_ram;

//---------------------------------------------------------------

wire        resetReg   = register_notsynced[0][0];
wire        source     = register_notsynced[0][1];
wire        enable     = register_notsynced[0][2];
wire        overlayOn  = register[0][3];
wire [1:0]  startColor = register[0][5:4];
wire        gradient   = register[0][6];
wire [7:0]  step       = register[0][23:16];
wire [7:0]  frame      = register[0][31:24];

wire [11:0] colHorMax  = register[1][11:0];  // Width of col - 1
wire [11:0] rowVerMax  = register[1][27:16]; // Height of row - 1
wire [11:0] imgHorMax  = register[2][11:0];  // Width of image + 2 sync - 1 
wire [11:0] imgVerMax  = register[2][27:16]; // Height of image - 1
wire [11:0] horMax     = register[3][11:0];  // Total clocks per line
wire [11:0] verMax     = register[3][27:16]; // Total lines per frame

reg         reset       = 0;
reg         run         = 0;
reg  [3:0]  colCount    = 0;  // Up to 16 columns in pattern
reg  [2:0]  rowCount    = 0;  // Up to 8 rows in pattern
reg  [11:0] colHorCount = 0;  // Count pixel pairs in a column
reg  [11:0] rowVerCount = 0;  // Count line pairs in a row
reg  [11:0] horCount    = 0;
reg  [11:0] verCount    = 0;
reg  [7:0]  frameCount  = 0;
reg  [1:0]  color       = 0;
reg  [1:0]  overlay     = 0;
reg  [2:0]  sync        = 0;
reg  [7:0]  base        = 0;

wire [8:0]  address_b_ram = { rowCount, colCount, color };
wire [11:0] q_b_ram;

always@(posedge inpClock) begin

	if (reset) begin
		run           <= 0;
		colCount      <= 0;
		rowCount      <= 0;
		colHorCount   <= 0;
		rowVerCount   <= 0;
		horCount      <= 0;
		verCount      <= 0;
		frameCount    <= 0;
		overlay       <= 0;
		base          <= 0;
		outData       <= 0;
		sync          <= SYNC_IDLE;
		outSync       <= SYNC_IDLE;
		register      <= register_notsynced;
	end
	
	// Normal image path
	else if (source == 0) begin
		if (overlayOn && (overlay < 2))
			outData <= (overlay ? 12'd4095 : 12'd0);
		else
			outData <= inpData;
		outSync    <= inpSync;
		
		// Counts are only for overlay
		if ((inpSync == SYNC_PIX_EXT) || (inpSync == SYNC_PIX_BLK) || (inpSync == SYNC_PIX_OUT)) begin
			horCount   <= horCount + 1'd1;
		end
		else if (inpSync == SYNC_LIN_END) begin
			horCount   <= 0;
			verCount   <= verCount + 1'd1;
		end
		else if (inpSync == SYNC_FRM_END) begin
			horCount   <= 0;
			verCount   <= 0;
			frameCount <= frameCount + 1'd1;
		end
		else if (inpSync == SYNC_FRM_BEG) begin
			horCount   <= 0;
			verCount   <= 0;
		end
	end
	
	// Image generator
	else if (!run) begin
		run           <= enable;
		colCount      <= 0;
		rowCount      <= 0;
		colHorCount   <= 0;
		rowVerCount   <= 0;
		horCount      <= 0;
		verCount      <= 0;
		frameCount    <= frame;
		overlay       <= 0;
		base          <= 0;
		outData       <= 0;
		sync          <= SYNC_IDLE;
		outSync       <= SYNC_IDLE;
		register      <= register_notsynced;
	end
	else begin
	
		outSync <= sync;
		if (sync == SYNC_PIX_OUT) begin
			if (overlayOn && (overlay < 2))
				outData <= (overlay ? 12'd4095 : 12'd0);
			else if (gradient) 
				outData <= {{horCount[5:0], 2'd0} + {verCount[6:0], 1'd0} + base, 4'd0};
			else
				outData <= q_b_ram;
		end
		else
			outData <= 0;
					
		// Active image area
		if ((verCount <= imgVerMax) && (horCount <= imgHorMax)) begin
		
			// Line start sync
			if (horCount == 0) begin
				if (verCount == 0) begin
					sync        <= SYNC_FRM_BEG;
					rowVerCount <= 0;
					rowCount    <= 0;
					color       <= startColor;
				end
				else begin
					sync     <= SYNC_LIN_BEG;
					color[0] <= startColor[0];
					color[1] <= ~color[1];
				end
				
				colHorCount <= 0;
				colCount    <= 0;
			end
			
			// Line pixel content
			else if (horCount < imgHorMax) begin
				sync <= SYNC_PIX_OUT;
				color[0] <= ~color[0];
				
				if (colHorCount < colHorMax)
					colHorCount <= colHorCount + 1'd1;
				else begin
					colHorCount <= 0;
					colCount    <= colCount + 1'd1;
				end
			end 
			
			// Line end sync
			else if (horCount == imgHorMax) begin
				if (verCount < imgVerMax)
					sync <= SYNC_LIN_END;
				else begin
					sync <= SYNC_FRM_END;
					base <= base + step;
				end
					
				if (rowVerCount < rowVerMax)
					rowVerCount <= rowVerCount + 1'd1;
				else begin
					rowVerCount <= 0;
					rowCount    <= rowCount + 1'd1;
				end
			end
			
			horCount <= horCount + 1'd1;
		end
		
		// Idle area
		else begin
			sync <= SYNC_IDLE;
					
			// Idle after image line
			if (horCount < horMax)
				horCount <= horCount + 1'd1;
				
			// End of line
			else begin
				horCount <= 0;
				
				// Idle lines after image
				if (verCount < verMax)
					verCount <= verCount + 1'd1;
				
				// End of frame
				else begin
					verCount   <= 0;
					frameCount <= frameCount + 1'd1;
					if (!enable)
						run <= 0;
						
					if (!regUpdate)
						register <= register_notsynced;
				end
			end
		end
		
	end
	
	if ((horCount < colHorMax) && (verCount < rowVerMax))
		overlay <= frameCount[7 - horCount[4:2]];
	else
		overlay <= 2'd2;
		
	reset <= resetReg;
end

RamTwoClockBidir # 
(
	.dat_wid (12), 
	.adr_wid (9)
)
ram
(
	.data_a    (data_a_ram),
	.data_b    (12'd0),  
	.wren_a    (wren_a_ram),
	.wren_b    (1'b0),
	.q_a       (q_a_ram),
	.q_b       (q_b_ram),
	.address_a (address_a_ram),
	.address_b (address_b_ram),
	.clock_a   (busClock),
	.clock_b   (inpClock)
);

endmodule

//==============================================================
// File        :  DynDefectPixelCorrection.sv
// Author      :  Stephen Brown
// Date        :  8/6/2017
// Version     :  0.1
// Abstract    :  This module corrects outlier pixels on a line by line basis of a bayer CFA pattern.  It does not account for the first and last 2 pixels.
//                It is a basic implementation that just registers a few clock cycles of data, then checks to see
//                if the middle pixel is greater or less than a programmable threshold. If it is beyond the threshold, then it 
//                takes the average of the two outer pixels and replaces the out-of-range pixel with the average value. 
//
// Clock Domains :
//   busClock - This registers the configuration data
//
//   inpClock - This should be the pixel clock associated with inpData and inpSync. 
// 
// Modification History: (see Git)
//====================================================================================

module DynDefectPixelCorrection
(
	input  wire                  busClock,
	input  wire                  busReset,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,

	input  wire                  inpClock,
	input  wire [11:0]           inpData,
	input  wire [2:0]            inpSync,
	output reg  [11:0]           outData,
	output reg  [2:0]            outSync
);


`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

reg [1:0][31:0] register = 0;

always begin
	if (busChipSelect)
		busRdData = register[busAddress[2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busWrite && busChipSelect)
		register[busAddress[2]] <= busWrData;
end

//---------------------------------------------------------------

wire          reset       = register[0][0] || busReset;
wire          bypass      = register[0][1];
wire [11:0]   threshold   = register[0][27:16];

reg [11:0] inp0, inp1, inp2, inp3;
reg [2:0]  sync0, sync1, sync2, sync3;

wire signed [12:0] diff0 = {1'b0, inp1}    - {1'b0, inpData};
wire signed [12:0] diff1 = {1'b0, inpData} - {1'b0, inp1};
wire signed [12:0] diff2 = {1'b0, inp3}    - {1'b0, inp1};
wire signed [12:0] diff3 = {1'b0, inp1}    - {1'b0, inp3};

always@(posedge inpClock) begin

	if (reset) begin
		outData <= 0;
		outSync <= SYNC_IDLE;
	end
	else if (bypass) begin
		outData <= inpData;
		outSync <= inpSync;
	end
	else begin
		
		sync0   <= inpSync;
		sync1   <= sync0;
		outSync <= sync1;
		sync2   <= sync1;
		sync3   <= sync2;
	 
     	inp0 <= inpData;
		inp1 <= inp0;
		inp2 <= inp1;
		inp3 <= inp2;
	
		// Only correct the pixels when the shift registers have valid data.
      // This means the first 2 pixels and last 2 pixels in each line will never be corrected. 
		if ((sync1 == 3'd3) && (sync3 == 3'd3) && (inpSync == 3'd3)) begin
	 
			if ((diff0 > 0) && (diff3 > 0)) begin
				if ((diff0 > threshold) && (diff3 > threshold))
					outData <= (inpData + inp3) >> 1;
		   	else
					outData <= inp1;
			end
	   	else if ((diff1 > 0) && (diff2 > 0)) begin
		   	if ((diff1 > threshold) && (diff2 > threshold))
					outData <= (inpData + inp3) >> 1;
		   	else 
					outData <= inp1;
			end	  
			else
		   	outData <= inp1;
				
		end	   
		else begin
	    	outData <= inp1;
		end 
	end
end

endmodule	
		
		
		
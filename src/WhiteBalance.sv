
// Registers
//  0 Control
//  1 x0,y0 offset, gain
//  2 x1,y0 offset, gain
//  3 x0,y1 offset, gain
//  4 x1,y1 offset, gain

module WhiteBalance
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,

	input  wire                  inpClock,
	input  wire [11:0]           inpData,
	input  wire [2:0]            inpSync,
	output reg  [11:0]           outData,
	output reg  [2:0]            outSync
);

`include "PixelPipe.h"


//--- Bus Interface ----------------------------------------------

reg [7:0][31:0] register = 0;

assign busRdData = register[busAddress[4:2]];

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[4:2]] <= busWrData;
end

//---------------------------------------------------------------

wire             resetReg   = register[0][0];
wire             bypass     = register[0][1];
wire [1:0]       startColor = register[0][5:4];
wire [11:0]      clip       = register[0][27:16];
wire [11:0]      clip0      = register[5][11:0];
wire [11:0]      clip1      = register[5][27:16];
wire [11:0]      clip2      = register[6][11:0];
wire [11:0]      clip3      = register[6][27:16];


reg              reset = 0;
reg  [2:0]       sync1 = 0;
reg  [2:0]       sync2 = 0;
reg  [1:0]       color = 0;

reg [3:0][12:0]  gain_reg = 0;
reg [3:0][11:0]  offset_reg = 0;

always@(posedge inpClock) begin
	if (reset) begin
		sync1     <= SYNC_IDLE;
		sync2     <= SYNC_IDLE;
		outSync   <= SYNC_IDLE;
		outData   <= 0;
		data_a_mult <= 0;
		data_b_mult <= 0;
	end
	else if (bypass) begin
		outSync   <= inpSync;
		outData   <= inpData;
	end
	else begin
		sync1     <= inpSync;
		sync2     <= sync1;
		outSync   <= sync2;
		outData   <= (sync2 == SYNC_PIX_OUT) ? result : 12'd0;
		
		if (inpSync == SYNC_FRM_BEG)
			color    <= startColor;
		else if (inpSync == SYNC_LIN_BEG) begin
			color[0] <= startColor[0];
			color[1] <= ~color[1];
		end
		else if (inpSync == SYNC_FRM_END) begin
			gain_reg[0]   <= register[1][12:0];
			offset_reg[0] <= register[1][27:16];
			gain_reg[1]   <= register[2][12:0];
			offset_reg[1] <= register[2][27:16];
			gain_reg[2]   <= register[3][12:0];
			offset_reg[2] <= register[3][27:16];
			gain_reg[3]   <= register[4][12:0];
			offset_reg[3] <= register[4][27:16];
		end
		else 
			color[0] <= ~color[0];
			
		case(color)
			2'd0: data_a_mult <= (sum[12:0] >= clip0) ? clip0 : ((sum < 0) ? 12'd0 : sum[11:0]);
			2'd1: data_a_mult <= (sum[12:0] >= clip1) ? clip1 : ((sum < 0) ? 12'd0 : sum[11:0]);
			2'd2: data_a_mult <= (sum[12:0] >= clip2) ? clip2 : ((sum < 0) ? 12'd0 : sum[11:0]);
			2'd3: data_a_mult <= (sum[12:0] >= clip3) ? clip3 : ((sum < 0) ? 12'd0 : sum[11:0]);
			default: data_a_mult <= sum[11:0];
		endcase
		
		data_b_mult <= gain;
		
	end
	reset <= resetReg;
end

wire        [12:0] gain       = gain_reg[color];   // Gain 0 to 8191 = 0 to +15.998
wire signed [11:0] offset     = offset_reg[color]; // Offset -2048 to +2047

// Sign extend the offset and add to pixel
wire signed [13:0] sum = {{2{offset[11]}}, offset} + {2'd0, inpData};

// Clip sum to 0 to 4095
reg         [11:0] data_a_mult;
reg         [12:0] data_b_mult;

wire        [24:0] result_mult;

// Divide product by 512 and clip
wire        [11:0] result = (result_mult[24:9] > clip) ? clip : result_mult[20:9];

MultiplyUnsigned	#
(
	.dat_wid_a (12),
	.dat_wid_b (13),
	.res_wid   (25)
)
mult
(
	.clock  (inpClock),
	.data_a (data_a_mult),
	.data_b (data_b_mult),
	.result (result_mult)
);

endmodule

//==============================================================
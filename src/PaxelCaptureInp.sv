//==============================================================

module PaxelCaptureInp
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	
	input  wire                  inpEnable,
	output wire                  outEnable,
	
	input  wire                  inpClock,
	input  wire [11:0]           inpData,
	input  wire [2:0]            inpSync
);

`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));
wire selRam = (busChipSelect && (busAddress[19:16] == 1'h1));

reg [1:0][31:0] register = 0;

always begin
	if (selReg && busRead)
		busRdData = register[busAddress[2]];
	else if (selRam && busRead) 	
		busRdData = {20'd0, q_a_ram};
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (selReg && busWrite)
		register[busAddress[2]] <= busWrData;
	else begin
		register[0][6]     <= run;
		if (run)
			register[0][2]  <= 0; // Clear enable at start
	end
end

//---------------------------------------------------------------

wire        resetReg    = register[0][0];
wire        clear       = register[0][1];
wire        enable      = register[0][2];
wire [11:0] horStart    = register[1][11:0];  
wire [11:0] verStart    = register[1][27:16]; 

reg         reset       = 0;
reg         run         = 0;
reg  [11:0] horCount    = 0;  // Count image pixels
reg  [11:0] verCount    = 0;  // Count image lines
reg  [5:0]  paxHorCount = 0;  // Count paxel pixels (0-31)
reg  [4:0]  paxVerCount = 0;  // Count paxel lines  (0-15)

wire [8:0]  address_a_ram = busAddress[10:2];
reg  [8:0]  address_b_ram = 0;
wire        wren_a_ram    = selRam && busWrite;
reg         wren_b_ram    = 0;
wire [11:0] data_a_ram    = busWrData[11:0];
reg  [11:0] data_b_ram    = 0;
wire [11:0] q_a_ram;

assign outEnable = run;

always@(posedge inpClock) begin

	if (reset) begin
		run           <= 0;
		horCount      <= 0;
		verCount      <= 0;
		paxHorCount   <= 0;
		paxVerCount   <= 0;
		address_b_ram <= 0;
		data_b_ram    <= 0;
		wren_b_ram    <= 0;
	end
	else begin
	
		if (!run) begin
			horCount      <= 0;
			verCount      <= 0;
			paxHorCount   <= 0;
			paxVerCount   <= 0;
		end
		else begin
			if (inpSync == SYNC_FRM_BEG) begin
				horCount      <= 0;
				verCount      <= 0;
				paxHorCount   <= 0;
				paxVerCount   <= 0;
			end
			if (inpSync == SYNC_LIN_BEG) begin
				horCount      <= 0;
				verCount      <= verCount + 1'd1;
				paxHorCount   <= 0;
				if ((verCount >= verStart) && (paxVerCount <= 15))
					paxVerCount <= paxVerCount + 1'd1;
			end
			else if (inpSync == SYNC_PIX_OUT) begin
				horCount <= horCount + 1'd1;
				if ((horCount >= horStart) && (paxHorCount <= 31))
					paxHorCount <= paxHorCount + 1'd1;
			end
		end
		
		if (run && (inpSync == SYNC_PIX_OUT)) begin
			data_b_ram    <= inpData;
			address_b_ram <= {paxVerCount[3:0], paxHorCount[4:0]};
			wren_b_ram    <= (horCount >= horStart) && (verCount >= verStart) && (paxHorCount <= 31) && (paxVerCount <= 15);
		end
		else begin
			data_b_ram    <= 0;
			if (clear) begin
				wren_b_ram    <= 1'd1;
				address_b_ram <= address_b_ram + 1'd1;
			end
			else begin
				wren_b_ram    <= 0;
				address_b_ram <= 0;
			end
		end
		
		if (!run && (enable || inpEnable) && (inpSync == SYNC_FRM_BEG))
			run <= 1'd1;
		else if (inpSync == SYNC_FRM_END)
			run <= 0;
		
	end
	reset <= resetReg;
end

RamTwoClockBidir # 
(
	.dat_wid (12), 
	.adr_wid (9)
)
ram
(
	.data_a    (data_a_ram),
	.data_b    (data_b_ram),  
	.wren_a    (wren_a_ram),
	.wren_b    (wren_b_ram),
	.q_a       (q_a_ram),
	.q_b       (),
	.address_a (address_a_ram),
	.address_b (address_b_ram),
	.clock_a   (busClock),
	.clock_b   (inpClock)
);

endmodule

//==============================================================
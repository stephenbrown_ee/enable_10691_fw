//  This module converts RGB888 input data to YUV422 data


module RGB2YCbCr
(
	input wire                inpClock,
	input wire                reset,
	input wire                inpVs,
	input wire                inpDe,
	input wire [2:0][7:0]     rgb888Data,
	
	output reg                outVs,
	output reg                outDe,
	output logic [1:0][7:0]   yuv422Data
	
);

//wire [23:0] R = {16'd0, rgb888Data[0]};
//wire [23:0] G = {16'd0, rgb888Data[1]};
//wire [23:0] B = {16'd0, rgb888Data[2]};

logic [7:0] Y;
logic [7:0] Cb;
logic [7:0] Cr;

logic [7:0] Y_reg;
logic [7:0] Cb_reg;
logic [7:0] Cr_reg;

logic [7:0]  Y_sum;
logic [8:0] Cb_sum;
logic [8:0] Cr_sum;

logic [2:0] outVs_sr;
logic       outDe1, outDe2, outDe3;
logic       color;
logic [7:0] Cb_out;
logic [7:0] Cr_out;
logic [7:0] Y_out;

// inpDE -> outDe1 -> outDe2 -> outDe3 -> outDe
// rgb   -> Y      -> Y_reg  -> Y_out  -> yuv422Data

assign yuv422Data[1] = color ? Cb_out : Cr_out;
assign yuv422Data[0] = Y_out;

always @(posedge inpClock)begin
	if(reset) begin
		outVs <= 0;
		outDe <= 0;
		Y_reg <= 0;
		Cb_reg <= 0;
		Cr_reg <= 0;
		outVs_sr <= 0;
	end
	else begin
	
		Y_reg  <= Y;
		Cb_reg <= Cb;
		Cr_reg <= Cr;
		
		outDe1 <= inpDe;
		outDe2 <= outDe1;
		outDe3 <= outDe2;
		outDe  <= outDe3;
		outVs_sr <= {outVs_sr[1:0], inpVs};
		outVs  <= outVs_sr[2];

		if (outDe2) begin
			Cb_sum <= {1'b0, Cb} + {1'b0, Cb_reg};
			Cr_sum <= {1'b0, Cr} + {1'b0, Cr_reg};
			Y_sum  <= Y_reg;
		end
		
		if (outDe3) begin
			color <= ~color;
			Y_out <= Y_sum;
			if (!color) begin
				Cr_out <= Cr_sum[8:1];
				Cb_out <= Cb_sum[8:1];
			end
		end
		else begin
			color <= 0;
		end


	end
end


// Y  = 16  + ( 47*R + 157*G + 16*B)/256
// Cb = 128 + (-22*R -  76*G + 98*B)/256
// Cr = 128 + (138*R - 126*G - 13*B)/256

parameter logic signed [8:0] Y_coeff[2:0]  = '{  9'd16,  9'd157,  9'd47 };
parameter logic signed [8:0] Cb_coeff[2:0] = '{  9'd98,  -9'd76, -9'd22 };
parameter logic signed [8:0] Cr_coeff[2:0] = '{ -9'd13, -9'd126, 9'd138 };

wire signed [2:0][17:0] result_Y ;
wire signed [2:0][17:0] result_u ;
wire signed [2:0][17:0] result_v ;

// Divide products by 256 and add offset
wire signed [17:0] sumY  = 18'd16  + {{8{result_Y[0][17]}}, result_Y[0][17:8]} + {{8{result_Y[1][17]}}, result_Y[1][17:8]} + {{8{result_Y[2][17]}}, result_Y[2][17:8]};
wire signed [17:0] sumCb = 18'd128 + {{8{result_u[0][17]}}, result_u[0][17:8]} + {{8{result_u[1][17]}}, result_u[1][17:8]} + {{8{result_u[2][17]}}, result_u[2][17:8]};
wire signed [17:0] sumCr = 18'd128 + {{8{result_v[0][17]}}, result_v[0][17:8]} + {{8{result_v[1][17]}}, result_v[1][17:8]} + {{8{result_v[2][17]}}, result_v[2][17:8]};

// Clip to 0 to 255
assign Y =  (sumY < 0) ? 8'd0 : ((sumY[17:0] > 255) ? 8'd255 : $unsigned(sumY[7:0]));	
assign Cb = (sumCb < 0) ? 8'd0 : ((sumCb[17:0] > 255) ? 8'd255 : $unsigned(sumCb[7:0]));	
assign Cr = (sumCr < 0) ? 8'd0 : ((sumCr[17:0] > 255) ? 8'd255 : $unsigned(sumCr[7:0]));	
	 
genvar y;
generate
	for (y = 0; y <= 2; y++) begin: inst_y
	
		wire signed [8:0]  data_a_mult_Y  = $signed({1'd0,rgb888Data[y]});         // Pixel is always positive
		wire signed [8:0]  data_b_mult_Y  = Y_coeff[y]; // Coef is signed
		
		MultiplySigned	#
		(
			.dat_wid_a (9),
			.dat_wid_b (9),
			.res_wid   (18)
		)
		multy
		(
			.clock  (inpClock),
			.data_a (data_a_mult_Y),
			.data_b (data_b_mult_Y),
			.result (result_Y[y])
		);

	end
endgenerate	 

		 
genvar u;
generate
	for (u = 0; u <= 2; u++) begin: inst_u
	
		wire signed [8:0]  data_a_mult_u  = $signed({1'd0,rgb888Data[u]});         // Pixel is always positive
		wire signed [8:0]  data_b_mult_u  = Cb_coeff[u]; // Coef is signed
		
		MultiplySigned	#
		(
			.dat_wid_a (9),
			.dat_wid_b (9),
			.res_wid   (18)
		)
		multu
		(
			.clock  (inpClock),
			.data_a (data_a_mult_u),
			.data_b (data_b_mult_u),
			.result (result_u[u])
		);
	
	end
endgenerate	 
		 
genvar v;
generate
	for (v = 0; v <= 2; v++) begin: inst_v
	
		wire signed [8:0]  data_a_mult_v  = $signed({1'd0,rgb888Data[v]});         // Pixel is always positive
		wire signed [8:0]  data_b_mult_v  = Cr_coeff[v]; // Coef is signed
		
		MultiplySigned	#
		(
			.dat_wid_a (9),
			.dat_wid_b (9),
			.res_wid   (18)
		)
		multv
		(
			.clock  (inpClock),
			.data_a (data_a_mult_v),
			.data_b (data_b_mult_v),
			.result (result_v[v])
		);
	
	end
endgenerate	 

	 
endmodule : RGB2YCbCr




//==============================================================

module ColorLut
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,

	input  wire                  inpClock,
	input  wire [2:0][11:0]      inpData,
	input  wire [2:0]            inpSync,
	output reg  [2:0][7:0]       outData,
	output reg  [2:0]            outSync
);


`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));

reg [1:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (selReg && busWrite)
		register[busAddress[2]] <= busWrData;
end

//---------------------------------------------------------------

wire             resetReg   = register[0][0];
wire             bypass     = register[0][1];


logic [2:0][7:0]  q_rom;

logic              reset = 0;
logic [2:0]        sync  = 0;

always@(posedge inpClock) begin
	if (reset) begin
	   sync       <= SYNC_IDLE;
		outSync    <= SYNC_IDLE;
		outData    <= 0;
	end
	else if (bypass) begin
		outSync    <= inpSync;
		outData[0] <= inpData[0][11:4];
		outData[1] <= inpData[1][11:4];
		outData[2] <= inpData[2][11:4];
	end
	else begin
		outSync    <= sync;
		outData    <= {q_rom[2], q_rom[1], q_rom[0]};
	   sync       <= inpSync;
	end
	reset <= resetReg;
end

genvar m;
generate
	for (m = 0; m <= 2; m++) begin: instRom
			
		RomOneClock 
		#( 	
			.dat_wid   (8),
			.adr_wid   (10),
			.mif_file  ("../src/gamma_sRGB_10b.mif")
		)
		gamma_rom_i
		(
			.clock     (inpClock),
			.address   (inpData[m][11:2]),
			.q_rom     (q_rom[m])
		);
		
	end
endgenerate


endmodule

//==============================================================
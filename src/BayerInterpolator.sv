
module BayerInterpolator
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,

	input  wire                  inpClock,
	input  wire [4:0][4:0][11:0] inpData,  // inpData[X][Y]
	input  wire [2:0]            inpSync,
	output reg  [2:0][11:0]      outData,
	output reg  [2:0]            outSync
);

`include "PixelPipe.h"


//--- Bus Interface ----------------------------------------------

reg [1:0][31:0] register = 0;

assign busRdData = register[busAddress[2]];

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[2]] <= busWrData;
end

//---------------------------------------------------------------

wire        resetReg   = register[0][0];
wire        bypass     = register[0][1];
wire        simple     = register[0][2];
wire [1:0]  startColor = register[0][5:4];

reg             reset  = 0;
reg  [1:0]      color  = 0;  // CFA color
reg  [1:0][2:0] sync   = 0;

reg signed [15:0] setHF = 0;
reg signed [15:0] setHN = 0;
reg signed [15:0] setVF = 0;
reg signed [15:0] setVN = 0;
reg signed [15:0] setDD = 0;
reg signed [15:0] setCC = 0;

reg signed [31:0] kerGrn    = 0;
reg signed [31:0] kerRowClr = 0;
reg signed [31:0] kerColClr = 0;
reg signed [31:0] kerOppClr = 0;
reg signed [31:0] kerCtr    = 0;

always@(posedge inpClock) begin
	if (reset) begin
		outSync    <= SYNC_IDLE;
		outData    <= 0;
		sync       <= 0;
	end
	else if (bypass) begin
		outSync    <= inpSync;
		outData[0] <= inpData[2][2];
		outData[1] <= inpData[2][2];
		outData[2] <= inpData[2][2];
	end
	else begin
		if (sync[1] == SYNC_FRM_BEG)
			color    <= startColor;
		else if (sync[1] == SYNC_LIN_BEG) begin
			color[0] <= startColor[0];
			color[1] <= ~color[1];
		end
		else
			color[0] <= ~color[0];
	
		if (sync[1] != SYNC_PIX_OUT) begin
			outData <= 0;
		end
		else begin
			case (color)
				0: begin // R
					outData[0] <= clip(kerCtr); 
					outData[1] <= clip(kerGrn); 
					outData[2] <= clip(kerOppClr); 
				end
				1: begin // Gr
					outData[0] <= clip(kerRowClr);
					outData[1] <= clip(kerCtr);  
					outData[2] <= clip(kerColClr);
				end
				2: begin // Gb
					outData[0] <= clip(kerColClr);
					outData[1] <= clip(kerCtr);  
					outData[2] <= clip(kerRowClr);
				end
				3: begin // B
					outData[0] <= clip(kerOppClr);  
					outData[1] <= clip(kerGrn); 
					outData[2] <= clip(kerCtr);  
				end
			endcase
		end
		
		if (simple) begin
			kerGrn    <= (setVN + setHN) / 4;
			kerOppClr <= setDD / 4;
			kerRowClr <= setHN / 2;
			kerColClr <= setVN / 2;
			kerCtr    <= setCC;
		end
		else begin
			kerGrn    <= ((4 * setCC) + (2 * (setHN + setVN)) - (setHF + setVF)) / 8;
			kerRowClr <= ((5 * setCC) + (4 * setHN) + (setVF / 2) - (setHF + setDD)) / 8;
			kerColClr <= ((5 * setCC) + (4 * setVN) + (setHF / 2) - (setVF + setDD)) / 8;
			kerOppClr <= ((6 * setCC) + (2 * setDD) - ((setHF + setVF) + ((setHF + setVF) / 2))) / 8;
			kerCtr    <= setCC;
		end
		
		setHF   <= inpData[0][2] + inpData[4][2]; // Hor far
		setHN   <= inpData[1][2] + inpData[3][2]; // Hor near
		setVF   <= inpData[2][0] + inpData[2][4]; // Ver far
		setVN   <= inpData[2][1] + inpData[2][3]; // Ver near
		setDD   <= inpData[1][1] + inpData[3][1] + inpData[1][3] + inpData[3][3]; // Diag near
		setCC   <= inpData[2][2]; // Center
		
		outSync <= sync[1];
		sync[1] <= sync[0];
		sync[0] <= inpSync;
	end
	reset <= resetReg;
end

// --  --  VF  --  --
// --  DD  VN  DD  --
// HF  HN  CC  HN  HF
// --  DD  VN  DD  --
// --  --  VF  --  --

// G at R, G at B
//          -1   
//          +2
//  -1  +2  +4  +2  -1
//          +2
//          -1

// R at Gr, B at Gb
//         +1/2  
//      -1      -1
//  -1  +4  +5  +4  -1
//      -1      -1
//         +1/2

// R at Gb, B at Gr
//          -1  
//      -1  +4  -1
// +1/2     +5     +1/2
//      -1  +4  -1
//          -1

// R at B, B at R
//         -3/2  
//      +2      +2
// -3/2     +6     -3/2
//      +2      +2
//         -3/2

function [11:0] clip(input signed [31:0] value);
	integer clp = (value < 0) ? 0 : ((value > 4095) ? 12'd4095 : value[11:0]);
	clip = $unsigned(clp[11:0]);
endfunction

endmodule

//==============================================================
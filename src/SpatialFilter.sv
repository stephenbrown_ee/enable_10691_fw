//==============================================================

module SpatialFilter
(
	input  wire                       busClock,
	input  wire                       busReset,
	input  wire                       busChipSelect,
	input  wire                       busWrite,
	input  wire                       busRead,
	input  wire [19:0]                busAddress,
	input  wire [31:0]                busWrData,
	output reg  [31:0]                busRdData,

	input  wire                       inpClock,
	input  wire [4:0][4:0][11:0]      inpData,  // 5x5 pixels
	input  wire [2:0]                 inpSync,
	output reg  [11:0]                outData,  
	output reg  [2:0]                 outSync
);

`include "PixelPipe.h";

//--- Bus Interface ----------------------------------------------

reg [5:0][31:0] register = 0;

assign busRdData = register[busAddress[4:2]];

always@(posedge busClock) begin	
	if (busReset)
		register <= 0;
	else if (busChipSelect && busWrite)
		register[busAddress[4:2]] <= busWrData;
end

//---------------------------------------------------------------

wire         reset       = register[0][0] | busReset;
wire         bypass      = register[0][1];
wire         simple      = register[0][2];
wire [1:0]   startColor  = register[0][5:4];

// Three coefficients for X and T patterns and Center
//  Coefficients in a set should sum to 64 since result is divided by 64
// Four sets for RB/G and smooth/edge
//  Edge filter is weaker - weight the Center more
//  Smooth filter is stronger - weight the X and T more
//  RB filter may be stronger than G to preserve luma sharpness

//      RB                 G
//      0 1 2 3 4          0 1 2 3 4 
//  0   x - t - x      0   - - t - - 
//  1   - - - - -      1   - x - x -	
//  2   t - c - t      2   t - c - t   
//  3   - - - - -      3   - x - x - 
//  4   x - t - x      4   - - t - -

//      0 1 2 3 4          0 1 2 3 4 
//  0   B - B - B      0   G - G - G 
//  1   - - - - -      1   - G - G -	
//  2   B - B - B      2   G - G - G   
//  3   - - - - -      3   - G - G - 
//  4   B - B - B      4   G - G - G

// 0 = smooth, 1 = edge

// Red/Blue coefficients (edge, smooth)
wire [1:0][5:0]   rbX        = {register[2][13:8],  register[2][5:0]};
wire [1:0][5:0]   rbT        = {register[2][29:24], register[2][21:16]};
wire [1:0][5:0]   rbC        = {register[3][13:8],  register[3][5:0]};
wire [11:0]       rbEdgeMax  = register[3][27:16];

// Green coefficients (edge, smooth)
wire [1:0][5:0]   gX         = {register[4][13:8],  register[4][5:0]};
wire [1:0][5:0]   gT         = {register[4][29:24], register[4][21:16]};
wire [1:0][5:0]   gC         = {register[5][13:8],  register[5][5:0]};
wire [11:0]       gEdgeMax   = register[5][27:16];

//---------------------------------------------------------------

reg  [4:0][2:0]        sync  = 0;
reg  [1:0]             color = 0;
reg  [4:0][4:0][11:0]  dataDly = 0;

always@(posedge inpClock) begin

	if (reset) begin
		outSync    <= SYNC_IDLE;
	end
	else if (bypass || simple) begin
		outSync    <= inpSync;
	end
	else begin
		outSync    <= sync[4];
	end
	
	sync[4:0]  <= {sync[3:0], inpSync};
	
end


// Is this a Red/Blue pixel?
wire            rbPixel = (color[0] == color[1]);   // set startColor so this condition occurs on red and blue pixels

// Select the coefficients and threshold based on color
wire [1:0][5:0] kX = rbPixel ? rbX : gX;
wire [1:0][5:0] kT = rbPixel ? rbT : gT;
wire [1:0][5:0] kC = rbPixel ? rbC : gC;

wire [11:0]     edgeMax = rbPixel ? rbEdgeMax : gEdgeMax;

reg  [13:0]     edgeLeft;
reg  [13:0]     edgeRight;
reg  [13:0]     edgeTop;
reg  [13:0]     edgeBot;
reg  [11:0]     edgeHor;
reg  [11:0]     edgeVer;

reg  [13:0]     sumXrb;  
reg  [13:0]     sumXg;  
reg  [13:0]     sumT;
reg  [11:0]     sumC;

wire [19:0]     resX;
wire [19:0]     resT;
wire [17:0]     resC;

wire            edg = (edgeHor > edgeMax) || (edgeVer > edgeMax);

reg  [17:0]     result; 

always@(posedge inpClock) begin

	if (reset) begin
		outData <= 0;
	end
	else if (bypass) begin
		outData <= inpData[2][2];
	end
	else begin
	
		if (inpSync == SYNC_FRM_BEG)
			color    <= startColor;
		else if (inpSync == SYNC_LIN_BEG) begin
			color[0] <= startColor[0];
			color[1] <= ~color[1];
		end
		else
			color[0] <= ~color[0];
				
		if (simple) begin
		
			if (rbPixel) // R, B  (+ pattern)
				outData <= filter(inpData[2][2], inpData[0][2], inpData[2][0], inpData[4][2], inpData[2][4]);
			else         // Gr,Gb (x pattern)
				outData <= filter(inpData[2][2], inpData[1][1], inpData[1][3], inpData[3][1], inpData[3][3]);
				
		end
		else begin
		
			//--- First clock - Compute edge values and sum pixels ---
			edgeLeft  <= edgeSum(inpData[0][0], inpData[0][2], inpData[0][4]);
			edgeRight <= edgeSum(inpData[4][0], inpData[4][2], inpData[4][4]);
			edgeTop   <= edgeSum(inpData[0][0], inpData[2][0], inpData[4][0]);
			edgeBot   <= edgeSum(inpData[0][4], inpData[2][4], inpData[4][4]);
			dataDly   <= inpData;
			
			//--- Second clock ---------------------------------------
			edgeHor   <= edgeResult(edgeLeft, edgeRight);
			edgeVer   <= edgeResult(edgeTop, edgeBot);
			
			sumXrb  <= {2'd0, inpData[0][0]} + {2'd0, dataDly[4][0]} + {2'd0, dataDly[0][4]} + {2'd0, dataDly[4][4]};
			sumXg   <= {2'd0, dataDly[1][1]} + {2'd0, dataDly[1][3]} + {2'd0, dataDly[3][1]} + {2'd0, dataDly[3][3]};
			sumT    <= {2'd0, dataDly[0][2]} + {2'd0, dataDly[2][0]} + {2'd0, dataDly[2][4]} + {2'd0, dataDly[4][2]};
			sumC    <= dataDly[2][2];
			
			//--- Fourth clock - result --- (included to resolve timing)
			result  <= resX[19:2] + resT[19:2] + resC[17:0];  // Sum the products, X and T are divided by 4
			
			//--- Fifth clock - Output result ---
			outData <= result[17:6]; // Divide by 64
			
		end
	end
end

//---------------------------------------------------------------

MultiplyUnsigned #
(
	.dat_wid_a (6),
	.dat_wid_b (14),
	.res_wid   (20),
	.pipeline  (2)
)
multX
(
	.clock  (inpClock),
	.data_a (kX[edg]),
	.data_b (rbPixel ? sumXrb : sumXg),
	.result (resX)
);

MultiplyUnsigned #
(
	.dat_wid_a (6),
	.dat_wid_b (14),
	.res_wid   (20),
	.pipeline  (2)
)
multT
(
	.clock  (inpClock),
	.data_a (kT[edg]),
	.data_b (sumT),
	.result (resT)
);

MultiplyUnsigned #
(
	.dat_wid_a (6),
	.dat_wid_b (12),
	.res_wid   (18),
	.pipeline  (2)
)
multC
(
	.clock  (inpClock),
	.data_a (kC[edg]),
	.data_b (sumC),
	.result (resC)
);	
			

//---------------------------------------------------------------

function [11:0] filter(input [11:0] a, input [11:0] b, input [11:0] c, input [11:0] d, input [11:0] e);
	integer sum = (a + (b >> 2) + (c >> 2) + (d >> 2) + (e >> 2));
	sum = sum >> 1;
	filter = sum[11:0];
endfunction

//---------------------------------------------------------------

function [13:0] edgeSum(input [11:0] d1, input [11:0] d2, input [11:0] d3);
	integer sum;
	begin
		sum     = (d1 + (d2 << 1) + d3);
		edgeSum = sum[13:0];
	end
endfunction

//---------------------------------------------------------------

function [11:0] edgeResult(input [13:0] d1, input [13:0] d2);
	integer diff;
	begin
		diff  = d1 - d2;
		diff  = ((diff < 0) ? (-diff) : diff ) / 8;
		edgeResult = diff[11:0];
	end
endfunction


endmodule

//==============================================================
//==============================================================

module LineBuffer
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,

	input  wire                  inpClock,
	input  wire [11:0]           inpData,
	input  wire [2:0]            inpSync,
	output reg  [4:0][4:0][11:0] outData,
	output reg  [2:0]            outSync
);

//  How a set of pixels shifts on outData
//    After one pixel clock                        
//    0 1 2 3 4    0 1 2 3 4    
//  0 - - - - -    - - - - -    
//  1 - - - - -    - - - - -    
//  2 - G R - -    - - G R -    
//  3 - B G - -    - - B G -    
//  4 - - - - -    - - - - -    
//
//    After one row                       
//    0 1 2 3 4    0 1 2 3 4    
//  0 - - - - -    - - - - -    
//  1 - G R - -    - - G R -    
//  2 - B G - -    - - B G -    
//  3 - - - - -    - - - - -    
//  4 - - - - -    - - - - -    

//--- Bus Interface ----------------------------------------------
`include "PixelPipe.h";

localparam BufferMemWidth = 11;

reg [1:0][31:0] register = 0;

assign busRdData = register[busAddress[2]];

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[2]] <= busWrData;
end

//---------------------------------------------------------------

wire    resetReg    = register[0][0];
wire    bypass      = register[0][1];
wire [15:0]   HCountReg   = register[1][15:0];
wire [9:0]    waitCntMax  = register[1][25:16];  



//
reg                   reset         = 0;
reg  [15:0]           inpAddress    = 0;
reg  [15:0]           outAddress    = 0;
reg  [4:0][4:0][11:0] ramData       = 0;

reg  [3:0]            inpBank       = 0;
reg  [3:0]            inpStart      = 0;
reg  [3:0]            inpCount      = 0;
reg                   inpWr         = 0;
reg  [3:0]            outBank       = 0;
reg                   outRun        = 0;
reg                   outRd         = 0;
reg  [4:0]            outRdQ        = 0;
reg                   outFrm        = 0;
reg  [15:0]           HCount        = 0;
reg                   flush         = 0;
reg                   flushQ        = 0;
reg                   waitStart     = 0;
reg  [9:0]            waitCnt       = 0;

reg  [11:0]           data_a_ram  = 0;
wire [5:0][11:0]      q_b_ram;

wire [BufferMemWidth-1:0]           address_a_ram = inpAddress[BufferMemWidth-1:0];
wire [BufferMemWidth-1:0]           address_b_ram = outAddress[BufferMemWidth-1:0];

wire [5:0][11:0]      qData;
reg  [3:0][3:0]       qSync, Sync;

wire [4:0][4:0][11:0] colData;
wire [4:0][4:0][11:0] rowData;

reg  [1:0]            top     = 0; 
reg  [1:0]            bottom  = 0;
reg  [1:0]            left    = 0;
reg  [1:0]            right   = 0;

always @ (posedge inpClock) begin
    HCount <= HCountReg;
end

always @ (posedge inpClock) begin
    outRdQ[0] <= outRd;
    outRdQ[1] <= outRdQ[0];
    outRdQ[2] <= outRdQ[1];
    outRdQ[3] <= outRdQ[2];
    outRdQ[4] <= outRdQ[3];
    flushQ    <= flush;
end

always @ (posedge inpClock) begin
    if((inpSync == SYNC_FRM_BEG) || (inpSync == SYNC_LIN_BEG) || (inpSync == SYNC_FRM_END) || flush) begin
        if(flush) begin
            if (!flushQ || (outRdQ[1] && !outRdQ[0])) begin
                qSync[0] <= SYNC_LIN_BEG;
                qSync[1] <= qSync[0];
                qSync[2] <= qSync[1];
                qSync[3] <= qSync[2];
            end
        end
        else begin
            qSync[0] <= inpSync;
            qSync[1] <= qSync[0];
            qSync[2] <= qSync[1];
            qSync[3] <= qSync[2];
        end
    end
end

always@(posedge inpClock) begin

	if (reset) begin
		inpBank       <= 0;
		inpStart      <= 0;
	   inpCount      <= 0;
		inpWr         <= 0;
		outBank       <= 0;
		outRun        <= 0;
		outRd         <= 0;
		outFrm        <= 0;
		inpAddress    <= 0;
		outAddress    <= 0;
		data_a_ram    <= 0;
		ramData[0]    <= 0;
		top           <= 0;
		bottom        <= 0;
		left          <= 0;
		right         <= 0;
		outSync       <= SYNC_IDLE;
		outData       <= 0;
	end
	else if (bypass) begin
		outSync   <= inpSync;
		outData   <= {25{inpData}};
	end
	else begin
		data_a_ram <= inpData;
        
		if ((inpSync == SYNC_FRM_BEG) || (inpSync == SYNC_LIN_BEG)) begin
			if (inpSync == SYNC_FRM_BEG) begin
				inpCount   <= 0;
				inpStart   <= (inpBank + 4'd1) % 4'd6;
			end
			else begin
				if (inpCount < 4'd3)
					inpCount <= inpCount + 4'd1;
			end
			inpBank       <= (inpBank + 4'd1) % 4'd6;
			inpAddress    <= 0;
		end
		else begin
            if ((inpSync == SYNC_PIX_BLK) || (inpSync == SYNC_PIX_EXT) || (inpSync == SYNC_PIX_OUT))
                inpWr   <= 1'd1;
            else
                inpWr   <= 1'd0;                
            
            if(inpWr)
                inpAddress <= inpAddress + 16'd1;
					  
		end
		
		if (!outRun) begin
			if (inpCount == 4'd3) begin
				outBank       <= inpStart;
				outRun        <= 1'd1;
				outFrm        <= 1'd1;
			end
			outAddress <= 0;
			outSync  <= SYNC_IDLE;
			outData  <= 0;
		end
		else begin
			if (outFrm  && !outRd) begin
				if (flush) begin			 					
					if (!waitStart) begin
						waitStart  <= 1'd1;
					end
					else begin
						// The wait count was introduced to insert some horizontal blanking
						// in the last few lines of the frame when the remaining data is being 
						// flushed out.  The frame buffer couldn't handle that rate, so the VIP FIFO
						// was filling up when the ready signal wasn't asserted.  Thus data was lost.
						if (waitCnt < waitCntMax)    
							waitCnt <= waitCnt + 1'd1;
						else begin
							waitStart <= 0;
							waitCnt   <= 0;
							outRd     <= 1'd1;
						end
					end
				end
				else 
					outRd <= 1'd1;
			end
        
			if (outAddress < HCount) begin
				if (outRd)
					  outAddress <= outAddress + 16'd1;
			end
			else begin
				if((inpSync == SYNC_FRM_BEG) || (inpSync == SYNC_LIN_BEG) || 
							 (flush && (!flushQ || (outRdQ[1] && !outRdQ[0])))) begin
				  outAddress  <= 0;
				  outBank     <= (outBank + 4'd1) % 4'd6;
				end
				outRd       <= 0;
			end
            
			if (inpSync == SYNC_FRM_END)
				 flush <= 1'b1;                
			if((outBank == inpBank) && (outAddress == HCount))
				 flush <= 0;
			
			
			if (outSync == SYNC_FRM_END) begin
				 outRun     <= 0;
				 outFrm     <= 0;
				 inpCount   <= 0;
			end
			
		// SYNC OUTPUT
			if(outRdQ[3])
				 if (!outRdQ[2]) begin
					  outSync <= ((outBank == inpBank) && (outAddress == HCount)) ? SYNC_FRM_END : SYNC_LIN_END;        // EOF or EOL
					  
					  
				 end    
				 else
					  outSync <= SYNC_PIX_OUT;
			else
				 if(outRdQ[2])
					  outSync <= ((qSync[3] == SYNC_FRM_BEG) && !flush) ? SYNC_FRM_BEG : SYNC_LIN_BEG;        // SOF or SOL
				 else
					  outSync <= SYNC_IDLE;
				 
		// DATA OUTPUT
			if(outRdQ[3])
				outData <= rowData;
			else
				outData <= 0;
			
			if (outAddress == 0)
				left <= 2'd2;
			else if ((left > 0) && outRdQ[2] && outRdQ[3])
				left <= left - 2'd1;
			
			if (outRd && (outAddress == HCount))
                right <= 2'd3;
			else
                if (right > 0)
                    right <= right - 2'd1;
			
			if (outRd && !outRdQ[0] && (qSync[3] == SYNC_FRM_BEG) && (outAddress == 0))
				top <= 2'd2;
			else if ((top > 0) && (outSync == SYNC_LIN_BEG))
				top <= top - 2'd1;
				
			if (qSync[2] == SYNC_FRM_END)
				bottom <= 2'd2;
			else if ((bottom > 0) && outRdQ[3] && !outRdQ[2])
				bottom <= bottom - 2'd1;

			if (outRdQ[0]) begin
				// RAM output into shift registers			
				ramData[0][0] <= qData[(outBank + 4'd4) % 6];
				ramData[0][1] <= qData[(outBank + 4'd5) % 6];
				ramData[0][2] <= qData[outBank];
				ramData[0][3] <= qData[(outBank + 4'd1) % 6];
				ramData[0][4] <= qData[(outBank + 4'd2) % 6];
			end
			else begin
				ramData[0] <= 0;
			end
		end
	end
	
	reset <= resetReg;
end

genvar n;
generate
	for (n = 0; n <= 4; n++) begin: instOut
	
		assign colData[0][n] = ((right == 2'd1) || (right == 2'd2)) ? ramData[2][n] : ramData[0][n];
		assign colData[1][n] = (right == 2'd1)                      ? ramData[3][n] : ramData[1][n];
		assign colData[2][n] =                                                        ramData[2][n];
		assign colData[3][n] = (left > 2'd1)                        ? ramData[1][n] : ramData[3][n];
		assign colData[4][n] = (left > 0)                           ? ramData[2][n] : ramData[4][n];
		
		assign rowData[n][0] = (top > 0)        ? colData[n][2] : colData[n][0];
		assign rowData[n][1] = (top > 2'd1)     ? colData[n][3] : colData[n][1];
		assign rowData[n][2] =                                    colData[n][2];
		assign rowData[n][3] = (bottom == 2'd1) ? colData[n][1] : colData[n][3];
		assign rowData[n][4] = (bottom > 2'd0)  ? colData[n][2] : colData[n][4];
		
		// Shift horizontally
		always@(posedge inpClock) begin
			if (outRun) begin
				ramData[4][n] <= ramData[3][n];
				ramData[3][n] <= ramData[2][n];
				ramData[2][n] <= ramData[1][n];
				ramData[1][n] <= ramData[0][n];
			end
		end
	end
endgenerate

generate
	for (n = 0; n <= 5; n++) begin: instRam
	
		assign qData[n] = q_b_ram[n];
	
		wire wren_a_ram = inpWr && (inpBank == n);
	
		RamOneClock #
		(
			.dat_wid (12),
			.adr_wid (BufferMemWidth)
		)
		ram
		(
			.data_a    (data_a_ram),
			.wren_a    (wren_a_ram),
			.q_b       (q_b_ram[n]),
			.address_a (address_a_ram),
			.address_b (address_b_ram),
			.clock     (inpClock)
		);
	end
endgenerate

endmodule

//==============================================================
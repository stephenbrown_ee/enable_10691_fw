//==============================================================


module LCDDisplayOut
(
	input  wire              busClock,
	input  wire              busReset,
	input  wire              busChipSelect,
	input  wire              busWrite,
	input  wire              busRead,
	input  wire [19:0]       busAddress,
	input  wire [31:0]       busWrData,
	output reg  [31:0]       busRdData,
	
	input  wire              inpClock,
	input  wire [2:0][7:0]   inpData,
	input  wire              inpDv,
	input  wire              inpHs,
	input  wire              inpVs,
	
	output wire [3:0]   		 lcdData,
	output wire              lcdClk          
);

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));

reg [2:0][31:0] register = 0;

// Read-able signals
wire         txLocked;

always begin
	if (selReg)
		busRdData = register[busAddress[6:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busReset)
		register <= 0;
	else if (selReg && busWrite)
		register[busAddress[6:2]] <= busWrData;
	else begin
	   register[0][31] <= txLocked;
	end
end

//-----------------------------------------------------------------

wire        reset       = register[0][0] || busReset;
wire        source      = register[0][1]; // 0 = data, 1 = test
wire        useSync     = register[0][2];
wire        invertSync  = register[0][3];

wire [15:0] horMax      = register[1][15:0];
wire [15:0] verMax      = register[1][31:16];

wire [15:0] imgHorMax   = register[2][15:0];
wire [15:0] imgVerMax   = register[2][31:16];

wire [6:0]   lcd_clock, lcd0, lcd1, lcd2, lcd3;
reg  [23:0]  outData    = 0;
reg          outValid   = 0;
reg  [11:0]  horCount   = 0;
reg  [11:0]  verCount   = 0;
reg  [3:0]   frameCount = 0;
reg          outHS      = 0;
reg          outVS      = 0;

parameter hor1 = 480;
parameter hor2 = 512;
parameter hor3 = 544;
parameter hor4 = 536;
parameter hor5 = 504;

parameter ver1 = 348;
parameter ver2 = 352;
parameter ver3 = 384;
parameter ver4 = 416;
parameter ver5 = 408;
parameter ver6 = 376; 

wire [1:0] antCount = frameCount[3:2];

wire box1 = (horCount >= hor1 + (antCount << 3)) && (horCount < (hor1 + 8 + (antCount << 3))) && (verCount >= ver1) && (verCount < ver1 + 4);
wire box2 = (horCount >= hor2 + (antCount << 3)) && (horCount < (hor2 + 8 + (antCount << 3))) && (verCount >= ver1) && (verCount < ver1 + 4);
wire box3 = (horCount >= hor3) && (horCount < (hor3 + 4)) && (verCount >= ver2 + (antCount << 3)) && (verCount < (ver2 + 8 + (antCount << 3)));
wire box4 = (horCount >= hor3) && (horCount < (hor3 + 4)) && (verCount >= ver3 + (antCount << 3)) && (verCount < (ver3 + 8 + (antCount << 3)));
wire box5 = (horCount >= hor4 - (antCount << 3)) && (horCount < (hor4 + 8 - (antCount << 3))) && (verCount >= ver4) && (verCount < ver4 + 4); 
wire box6 = (horCount >= hor5 - (antCount << 3)) && (horCount < (hor5 + 8 - (antCount << 3))) && (verCount >= ver4) && (verCount < ver4 + 4);
wire box7 = (horCount >= hor1) && (horCount < (hor1 + 4)) && (verCount >= ver5 - (antCount << 3)) && (verCount < (ver5 + 8 - (antCount << 3)));
wire box8 = (horCount >= hor1) && (horCount < (hor1 + 4)) && (verCount >= ver6 - (antCount << 3)) && (verCount < (ver6 + 8 - (antCount << 3)));  


wire vsync = (verCount > imgVerMax) && (verCount < verMax);

always @ (posedge inpClock) begin

	if (reset) begin
		outData  <= 0;
		outValid <= 0;
	end
	else begin
	
		if (source) begin
		   outVS <= useSync ? (invertSync ? !vsync : vsync) : 1'b0;
			if (horCount < horMax) begin
				horCount <= horCount + 1'd1;
			end
			else begin
				horCount <= 0;
				if (verCount < verMax) 
					verCount <= verCount + 1'd1;
				else begin	
					verCount <= 0;
					frameCount <= frameCount + 1'd1;
				end
			end
				
			if ((horCount < imgHorMax) && (verCount < imgVerMax)) begin
				outValid <= 1'b1;
            outHS    <= useSync ? !invertSync : 1'b0;	
				if (box1 || box2 || box3 || box4 || box5 || box6 || box7 || box8)
					outData <= { 8'hFF, 8'hFF, 8'hFF };  // White
				else
					outData <= 0;
//				case (horCount[6:4])
//					0: outData <= 0;                        // Black
//					1: outData <= { 8'h00, 8'h00, 8'hFF };  // Red
//					2: outData <= { 8'h00, 8'hFF, 8'h00 };  // Green
//					3: outData <= { 8'hFF, 8'h00, 8'h00 };  // Blue
//					4: outData <= { 8'hFF, 8'hFF, 8'h00 };  // Cyan
//					5: outData <= { 8'hFF, 8'h00, 8'hFF };  // Magenta
//					6: outData <= { 8'h00, 8'hFF, 8'hFF };  // Yellow
//					7: outData <= { 8'hFF, 8'hFF, 8'hFF };  // White
//				endcase
			end
			else begin
				outValid     <= 0;
				outHS        <= useSync ? !invertSync : 1'b0;
				outData      <= 0;
			end
		end
		else begin
			outValid <= inpDv;
			outHS    <= useSync ? (invertSync ? !inpHs : inpHs) : 1'b0;
			outVS    <= useSync ? (invertSync ? !inpVs : inpVs) : 1'b0;
			outData  <= inpData;
		end
	end
end

wire [7:0] B = outData[23:16];
wire [7:0] G = outData[15:8];
wire [7:0] R = outData[7:0];
//wire [34:0] txData;

// ALTLVDS_TX sends the MSb first (Page 7 of ALTVLDS_TX Parameter Settings "Register 'tx_in' input port using")
assign lcd_clock = 7'b1100011;
assign lcd0      = { G[0], R[5], R[4], R[3], R[2], R[1], R[0] };
assign lcd1      = { B[1], B[0], G[5], G[4], G[3], G[2], G[1] };
assign lcd2      = { outValid, outVS, outHS, B[5], B[4], B[3], B[2] };
assign lcd3      = { 1'b0, B[7], B[6], G[7], G[6], R[7], R[6] };

//assign txData = 
//	{
//// lane    3         2         1         0          Clock
//        lcd3[6],  lcd2[6],  lcd1[6],  lcd0[6],  lcd_clock[6],
//        lcd3[5],  lcd2[5],  lcd1[5],  lcd0[5],  lcd_clock[5],
//        lcd3[4],  lcd2[4],  lcd1[4],  lcd0[4],  lcd_clock[4],
//        lcd3[3],  lcd2[3],  lcd1[3],  lcd0[3],  lcd_clock[3],
//        lcd3[2],  lcd2[2],  lcd1[2],  lcd0[2],  lcd_clock[2],
//        lcd3[1],  lcd2[1],  lcd1[1],  lcd0[1],  lcd_clock[1],
//        lcd3[0],  lcd2[0],  lcd1[0],  lcd0[0],  lcd_clock[0]
//	};	
//		  

lcd_lvds_display lvds_out_inst
(
   .pll_areset     (reset),
	.tx_in          ({ lcd3, lcd2, lcd1, lcd0, lcd_clock }),
	.tx_inclock     (inpClock),
	.tx_locked      (txLocked),
	.tx_out         ({ lcdData, lcdClk })
);


endmodule
						
		
			
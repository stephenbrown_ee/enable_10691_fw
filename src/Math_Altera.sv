//==============================================================

module MultiplyUnsigned
#(
    parameter dat_wid_a = 10,
    parameter dat_wid_b = 10,
    parameter res_wid   = 20,
	 parameter pipeline  = 1
)
(
	input  wire [dat_wid_a-1:0] data_a,
	input  wire [dat_wid_b-1:0] data_b,
	output wire [res_wid-1:0]   result,
	input  wire                 clock
);
		
lpm_mult	#
(
	.lpm_type           ("LPM_MULT"),
	.lpm_representation ("UNSIGNED"),
	.lpm_hint           ("DEDICATED_MULTIPLIER_CIRCUITRY=YES,MAXIMIZE_SPEED=5"),
	.lpm_pipeline       (pipeline),
	.lpm_widtha         (dat_wid_a),
	.lpm_widthb         (dat_wid_b),
	.lpm_widthp         (res_wid)
)
mult
(
	.clock  (clock),
	.dataa  (data_a),
	.datab  (data_b),
	.result (result),
	.aclr   (1'b0),
	.clken  (1'b1),
	.sum    (1'b0)
);

endmodule

//==============================================================

module MultiplySigned
#(
    parameter dat_wid_a = 10,
    parameter dat_wid_b = 10,
    parameter res_wid   = 20,
	 parameter pipeline  = 1
)
(
	input  wire [dat_wid_a-1:0] data_a,
	input  wire [dat_wid_b-1:0] data_b,
	output wire [res_wid-1:0]   result,
	input  wire                 clock
);

lpm_mult	#
(
	.lpm_type           ("LPM_MULT"),
	.lpm_representation ("SIGNED"),
	.lpm_hint           ("DEDICATED_MULTIPLIER_CIRCUITRY=YES,MAXIMIZE_SPEED=5"),
	.lpm_pipeline       (pipeline),
	.lpm_widtha         (dat_wid_a),
	.lpm_widthb         (dat_wid_b),
	.lpm_widthp         (res_wid)
)
mult
(
	.clock  (clock),
	.dataa  (data_a),
	.datab  (data_b),
	.result (result),
	.aclr   (1'b0),
	.clken  (1'b1),
	.sum    (1'b0)
);

endmodule

//==============================================================

module DivideUnsigned
#(
    parameter dat_wid_num = 10,
    parameter dat_wid_den = 10,
    parameter latency     = 1
)
(
	input  wire [dat_wid_num-1:0] num,
	input  wire [dat_wid_den-1:0] den,
	output wire [dat_wid_num-1:0] result,
	output wire [dat_wid_den-1:0] remainder,
	input  wire                   clock
);

lpm_divide	#
(
	.lpm_type            ("LPM_DIVIDE"),
	.lpm_drepresentation ("UNSIGNED"),
	.lpm_nrepresentation ("UNSIGNED"),
	.lpm_hint            ("LPM_REMAINDERPOSITIVE=TRUE,MAXIMIZE_SPEED=9"),
	.lpm_pipeline        (latency),
	.lpm_widthd          (dat_wid_den),
	.lpm_widthn          (dat_wid_num)
)
div
(
	.clock    (clock),
	.numer    (num),
	.denom  	 (den),
	.quotient (result),
	.remain   (remainder),
	.aclr     (1'b0),
	.clken    (1'b1)
);

endmodule 

//==============================================================

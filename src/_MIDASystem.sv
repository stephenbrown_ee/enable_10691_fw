//============================================================================
// Stephen Brown
// 
// Description:  Top level module for MIDASystem video processing
// 
// Inputs/Outputs:  Signals from VPU board (ENA-10691-AS) and MitySOM-5CSX 
// 
//============================================================================
// Revisions :                
//============================================================================


module _MIDASystem
(
   // OV426 SIGNALS
	input wire [11:0] IMAGER_D,           
	output wire       FPGA_IMAGER_ACLK,        
	output wire       FPGA_IMAGER_CTRL,       
	output wire       VID_EN_CMD_ENn,

	output wire       FPGA_ADC_CLK,
	
	output wire       IMAGER_RESET_,       
	output wire       IMAGER_PWDN,        
	
	output wire       FPGA_I2C0_SCL,           
	inout  wire       FPGA_I2C0_SDA,	
	
	input  wire       UART1_RX,
	output wire       UART1_TX,
	
   // TMDS SIGNALS	
   output wire [3:0] GXB_TX,
	input  wire       GXB_REFCLK1,

	input  wire       HDMI_HPD,
	output wire       HDMI_REDRIVER_OEn,
	
	output wire       LED_DRV_PWM,        
	output wire       LED_DRV_SHDN,

   // LCD SIGNALS
   output wire [3:0] LCD_DATA,
   output wire       LCD_CLK,	
	output wire       BLEN,
	output wire       BLPWM,
	
	// INPUT CLOCKS
	input  wire       CLOCK_50MHZ,
	
	// HANDLE SIGNALS
	input  wire       SCOPE_DETECT,
	input  wire       BUTTON_FLG,
	
	inout  wire       FPGA_I2C1_SCL,           
	inout  wire       FPGA_I2C1_SDA, 
	
	// MISC
	output wire       MISC_GPIO,
	output wire       FAN_SWITCH,
	
	// LEDS
	output wire       FAULT_LED,
	output wire       REC_LED,
	output wire       NO_MCU_COM,
	output wire       SIG_LOST,
	
	// HPS SIGNALS
	input  wire       HPS_nPOR,
	
	// Overlay buttons (ACTIVE LOW)
	input  wire       SCALE,
	input  wire       DOWN, 
	input  wire       UP, 
	input  wire       RIGHT, 
	input  wire       LEFT,  
	input  wire       ENTER, 
	
	// USER DIP SWITCHES for brightness setting
	input  wire [2:0] USER_DIPSW,
	
	// HPS DDR3 memory
	output wire [14:0] ddr3_hps_mem_a,                //           ddr3_hps.mem_a
	output wire [2:0]  ddr3_hps_mem_ba,               //                   .mem_ba
	output wire        ddr3_hps_mem_ck,               //                   .mem_ck
	output wire        ddr3_hps_mem_ck_n,             //                   .mem_ck_n
	output wire        ddr3_hps_mem_cke,              //                   .mem_cke
	output wire        ddr3_hps_mem_cs_n,             //                   .mem_cs_n
	output wire        ddr3_hps_mem_ras_n,            //                   .mem_ras_n
	output wire        ddr3_hps_mem_cas_n,            //                   .mem_cas_n
	output wire        ddr3_hps_mem_we_n,             //                   .mem_we_n
	output wire        ddr3_hps_mem_reset_n,          //                   .mem_reset_n
	inout  wire [39:0] ddr3_hps_mem_dq,               //                   .mem_dq
	inout  wire [4:0]  ddr3_hps_mem_dqs,              //                   .mem_dqs
	inout  wire [4:0]  ddr3_hps_mem_dqs_n,            //                   .mem_dqs_n
	output wire        ddr3_hps_mem_odt,              //                   .mem_odt
	output wire [4:0]  ddr3_hps_mem_dm,               //                   .mem_dm
	input  wire        ddr3_hps_oct_rzqin,            //                   .oct_rzqin
	
	// HPS peripherals
	output wire        QSPI_SS1,   //             hps_io.hps_io_qspi_inst_SS1
	inout  wire        QSPI_IO0,   //                   .hps_io_qspi_inst_IO0
	inout  wire        QSPI_IO1,   //                   .hps_io_qspi_inst_IO1
	inout  wire        QSPI_IO2,   //                   .hps_io_qspi_inst_IO2
	inout  wire        QSPI_IO3,   //                   .hps_io_qspi_inst_IO3
	output wire        QSPI_SS0,   //                   .hps_io_qspi_inst_SS0
	output wire        QSPI_CLK,   //                   .hps_io_qspi_inst_CLK
	inout  wire        SDIO_CMD,   //                   .hps_io_sdio_inst_CMD
	inout  wire        SDIO_D0,    //                   .hps_io_sdio_inst_D0
	inout  wire        SDIO_D1,    //                   .hps_io_sdio_inst_D1
	output wire        SDIO_CLK,   //                   .hps_io_sdio_inst_CLK
	inout  wire        SDIO_D2,    //                   .hps_io_sdio_inst_D2
	inout  wire        SDIO_D3,    //                   .hps_io_sdio_inst_D3
	inout  wire        USB1_D0,    //                   .hps_io_usb1_inst_D0
	inout  wire        USB1_D1,    //                   .hps_io_usb1_inst_D1
	inout  wire        USB1_D2,    //                   .hps_io_usb1_inst_D2
	inout  wire        USB1_D3,    //                   .hps_io_usb1_inst_D3
	inout  wire        USB1_D4,    //                   .hps_io_usb1_inst_D4
	inout  wire        USB1_D5,    //                   .hps_io_usb1_inst_D5
	inout  wire        USB1_D6,    //                   .hps_io_usb1_inst_D6
	inout  wire        USB1_D7,    //                   .hps_io_usb1_inst_D7
	input  wire        USB1_CLK,   //                   .hps_io_usb1_inst_CLK
	output wire        USB1_STP,   //                   .hps_io_usb1_inst_STP
	input  wire        USB1_DIR,   //                   .hps_io_usb1_inst_DIR
	input  wire        USB1_NXT,   //                   .hps_io_usb1_inst_NXT
	inout  wire        USB1_ULPI_CS,
	inout  wire        USB1_ULPI_RESET_N,
	output wire        SPIM0_CLK,  //                   .hps_io_spim0_inst_CLK
	output wire        SPIM0_MOSI, //                   .hps_io_spim0_inst_MOSI
	input  wire        SPIM0_MISO, //                   .hps_io_spim0_inst_MISO
	output wire        SPIM0_SS0,  //                   .hps_io_spim0_inst_SS0
	input  wire        UART0_RX,   //                   .hps_io_uart0_inst_RX
	output wire        UART0_TX,   //                   .hps_io_uart0_inst_TX
	inout  wire        HPS_I2C0_SDA,   //                   .hps_io_i2c0_inst_SDA
	inout  wire        HPS_I2C0_SCL,    //                   .hps_io_i2c0_inst_SCL
	
	input  wire        TOUCHSCREEN_UART1_RX,
	output wire        TOUCHSCREEN_UART1_TX,

	output wire  [15:0]  UVC_DATA,
	output wire          FRAME_VALID,
	output wire          LINE_VALID,
	output wire          FX3_RESETn,
	
	output wire          PCLK,
	
	output wire          PMODE0_CON,
	input  wire          USB3_VBUS_DETECT
);

parameter sys_ref_clock_hz   = 50000000;
parameter sys_ref_clock_freq = "50.0 MHz";

//parameter system_clock_hz    = 50000000; 
//parameter system_clock_freq  = "50.0 MHz";
//
//parameter sys_ref_clock_hz   = 100000000;
//parameter sys_ref_clock_freq = "100.0 MHz";

parameter system_clock_hz    = 100000000; 
parameter system_clock_freq  = "100.0 MHz";

wire       HPS_nRST = 1'b1;

// Clocks
wire        sysRefClock;
wire        sysClock;
wire        pixelClock;
wire [3:0]  xcvrParClock;
wire        clk_74_25MHz;
wire        lcdClock;
wire        usecClock;
wire        msecClock;
wire        testClock;
wire        hpsClock;
wire        hdmiOutClock;
wire        adcclk;
wire        sensorClock;

assign  FPGA_IMAGER_ACLK = sensorClock;
assign  FPGA_ADC_CLK = adcclk;

//--- Bus Device enumeration -----------------------------------------------

`include "BusSel.h"

// Bus signals
wire [19:0] busAddress;
wire        busWrite;
wire        busRead;
wire [31:0] busWrData;
wire [bus_devices-1:0][31:0] busRdData;
wire [bus_devices-1:0]       busChipSelect;
//wire        busWaitRequest;

wire        lwhps_bus_waitrequest;    
wire [31:0] lwhps_bus_readdata;       
wire        lwhps_bus_readdatavalid;  
wire        lwhps_bus_burstcount;     
wire [31:0] lwhps_bus_writedata;      
wire [15:0] lwhps_bus_address;        
wire        lwhps_bus_write;          
wire        lwhps_bus_read;           
wire        lwhps_bus_byteenable;     
wire        lwhps_bus_debugaccess;

wire [31:0]  hps_f2h_irq0_irq = {31'd0, lcd_outVs};

wire [27:0]  f2h_sdram_address;       
wire [7:0]   f2h_sdram_burstcount;    
wire         f2h_sdram_waitrequest;   
wire [127:0] f2h_sdram_readdata;      
wire         f2h_sdram_readdatavalid; 
wire         f2h_sdram_read;         
wire [127:0] f2h_sdram_writedata;     
wire [15:0]  f2h_sdram_byteenable;    
wire         f2h_sdram_write;        

wire [27:0]  hps_f2h_sdram1_data_address;       // hps_f2h_sdram1_data.address
wire [7:0]   hps_f2h_sdram1_data_burstcount;    //                    .burstcount
wire         hps_f2h_sdram1_data_waitrequest;   //                    .waitrequest
wire [127:0] hps_f2h_sdram1_data_readdata;      //                    .readdata
wire         hps_f2h_sdram1_data_readdatavalid; //                    .readdatavalid
wire         hps_f2h_sdram1_data_read;          //                    .read
    

//// Dynamic PLL reconfig signals
//wire [63:0] reconfigToPll;
//wire [63:0] reconfigFromPll;

// System signals
wire        resetInput;
wire        sysReset;
wire        clkReset;
wire        hpsClockReset;
wire        clkLocked;
wire        outClkLocked;
wire        ledOut;
wire [7:0]  out_export;
wire        pwmFaultLED;
wire        pwmBacklight;
wire        pmode0_out;
wire        outSelect;
wire        sync_select;

wire [31:0] bitOutPin;
wire        regUpdate;
wire        outputClockInvert;
wire        scope_detect_LED;

assign sysRefClock  = CLOCK_50MHZ;

assign resetInput   = 1'b0;   // The busReset just locks up everything anyway, so doing nothing with this right now

//assign MISC_GPIO = !ledOut;     // This is the LED that indicates the FPGA is loaded
assign MISC_GPIO  = scope_detect_LED;  // The LED is ON when the signal is LOW
assign NO_MCU_COM = pwmFaultLED; // output LED signal to indicate FAULT (going to Overlay attached to MIDASystem) main indicator of a fault
assign BLPWM      = pwmBacklight;

assign FAULT_LED = NO_MCU_COM;
//assign hdmiOutClock = outputClockInvert ? ~hdmiClock : hdmiClock;
assign PMODE0_CON = pmode0_out ? 1'b1 : 1'bz;
assign FAN_SWITCH = BUTTON_FLG;
assign PCLK = xcvrClock_div2;
  

//--- Output signals from PIO_BASE_1 ---------------------------------------

assign SIG_LOST      = out_export[0]; // This LED indicates that Nios is running, originally thought to be used for signal lost indication

//--- Slow output signals from processor (busAddress parsed by System Manager module) -----------------------------------

assign  sync_select         = bitOutPin[12];
assign  outSelect           = bitOutPin[11]; // 0 = HDMI output; 1 = UVC output
assign  scope_detect_LED    = bitOutPin[10]; // indicates if scope is connected ON = 0 = connected
assign  pmode0_out          = bitOutPin[9];  // bootstrap pin on Cypress FX3  :  1 = USB boot  0 = I2C first, then USB 
assign  FX3_RESETn          = bitOutPin[8];  // output signal to reset Cypress FX3
assign  BLEN                = bitOutPin[7];  // output signal to enable the LED backlight on LCD display
assign  regUpdate           = bitOutPin[6];  // active when updating registers to synchronize updates to frame
assign  IMAGER_RESET_       = bitOutPin[5];  // output signal to OV426 (see schematic)
assign  IMAGER_PWDN         = bitOutPin[4];  // output signal to OV426 (see schematic)
assign  HDMI_REDRIVER_OEn   = bitOutPin[3];  // power down signal to redriver (active low)
assign  LED_DRV_PWM         = bitOutPin[2];  // PWM signal to LED driver
assign  LED_DRV_SHDN        = bitOutPin[1];  // shutdown signal to LED driver
assign  outputClockInvert   = bitOutPin[0];  // inverts polarity of parallel output clock to TFP410

//--- Clocks ---------------------------------------------------------------
// The 74.25MHz clock is a common data rate of the HDMI/UVC output path.
// The output clock will be supplied by the transceiver PLL.
// The lcdClock is the parallel output clock for the Mitsubishi LCD display.
// It is ~65MHz for 1024x768 at 60Hz.



outClkPll  outClock_inst
(
	.refclk                 (sysClock),
	.rst                    (clkReset),
	.outclk_0               (clk_74_25MHz),  // 74.25MHz
	.outclk_1               (lcdClock),      // ~65MHz
	.locked                 (outClkLocked)
);



//--------------------------------------------------------------------------


altclkctrl #
( 
	.lpm_type                                   ("altclkctrl"),
	.lpm_hint                                   ("unused"),
	.intended_device_family                     ("Cyclone V"),
	.clock_type                                 ("Auto"),
	.ena_register_mode                          ("falling edge"),
	.use_glitch_free_switch_over_implementation ("OFF"),
	.number_of_clocks                           (1),
	.width_clkselect									  (1)
) 
altclkctrl_sensorClock
(
	.inclk     (adcclk),
	.outclk    (pixelClock),
	.clkselect (1'd0),
	.ena       (1'd1)
);

//--- Debounce buttons -----------------------------------------------------

parameter                NUM_OF_INPUTS = 5;                // Maximum of 8
  
// Add ACTIVE LOW signals to this as necessary, PIO input of Nios processor is only configured for max of 8
wire [7:0]               buttonInputs    = {3'd0, !HDMI_HPD, SCOPE_DETECT, 1'b0, USB3_VBUS_DETECT, SCALE};  

wire [NUM_OF_INPUTS-1:0] buttonInputs_db;
wire [NUM_OF_INPUTS-1:0] longButtonPress;
  
//assign inp_export = (NUM_OF_INPUTS ==8) ? buttonInputs_db : {{(8 - NUM_OF_INPUTS){1'b0}}, buttonInputs_db};

genvar m;
generate 
	for (m = 0; m <= (NUM_OF_INPUTS-1); m++)  begin: debounce_inst_rollout
	
		Debounce #
		(
			.SHORT_PRESS_CLOCKS   (5000000),   //  (~50 ms)
			.LONG_PRESS_CLOCKS    (100000000)    // 1 sec for 100 MHz clock
		)
		debounce_buttons_inst
		(
			.inpClock        (sysClock),
			.inpSignal       (buttonInputs[m]),
			.outSignal_db    (buttonInputs_db[m]),
			.outLong         (longButtonPress[m])
		);
			
	end
endgenerate
 
wire  scaleButton  = buttonInputs_db[0];

//--- Processor core -------------------------------------------------------

wire  [9:0]   inp_export;

assign inp_export = { 
							 1'b0,
							 1'b0,
                      touchscreen_scale, 
                      start_calibration, 
							 touchscreen_video, 
							 touchscreen_up_arrow, 
							 touchscreen_down_arrow, 
							 clear_scope_flash, 
							 BUTTON_FLG, 
							 scaleButton };
 
Processor #
(
	.ref_clock_freq  (sys_ref_clock_freq),
	.sys_clock_freq  (system_clock_freq),
	.bus_devices     (bus_devices)
)
processor
(
	.busAddress      (busAddress),
	.busWrite        (busWrite),
	.busRead         (busRead),
	.busRdData       (busRdData),
	.busWrData       (busWrData),
	.busChipSelect   (busChipSelect),
//	.busWaitRequest  (busWaitRequest),

	.refClock        (sysRefClock),
	.sysClock        (sysClock),
	.sysReset        (sysReset),
	.clkReset        (clkReset),
	.clkLocked       (clkLocked),
	
	.out_export      (out_export),
	.inp_export      (inp_export),
	.trigger_export  ({buttonInputs_db[1],buttonInputs_db[4:3]}),   
	
	.uartRxd         (UART1_RX),
	.uartTxd         (UART1_TX),
	
	.adcclk_locked   (),
	.adcclk          (adcclk),
	.sensorclk_out   (sensorClock)
);


//--- Unique Chip ID Wrapper -------------------------------------------------------
 
UniqueChipID   uniq_chip_id_inst
(
	.busClock           (sysClock),
	.busReset           (sysReset),
	.busChipSelect      (busChipSelect[bus_chipID]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_chipID])
);

//--- Power LED --------------------------------------------------------------------	
// output LED signal to indicate system is running and indicates DE status for reuse prevention

PwmGenerator  fault_led_inst
(
   .busClock          (sysClock),
   .busReset          (sysReset),
   .busChipSelect     (busChipSelect[bus_pwmLed]),
   .busWrite          (busWrite),
   .busRead           (busRead),
   .busAddress        (busAddress),
   .busWrData         (busWrData),
   .busRdData         (busRdData[bus_pwmLed]),
	
	.clock             (msecClock),
	.pulse             (pwmFaultLED)
	
);

//--- HPS core -------------------------------------------------------

 hps_system hps0 
 (
     .hps_clk_out_clk               (hpsClock),
	  .hps_clk_reset_reset_n         (hpsClockReset),
//	  .hps_f2h_cold_reset_reset_n    (HPS_POR),    // hps_f2h_cold_reset.reset_n
//	  .hps_f2h_warm_reset_reset_n    (HPS_nRST),    // hps_f2h_warm_reset.reset_n
//	  .hps_h2f_reset_reset_n         (),          //      hps_h2f_reset.reset_n
	  .hps_io_hps_io_qspi_inst_SS1   (QSPI_SS1),  //             hps_io.hps_io_qspi_inst_SS1
	  .hps_io_hps_io_qspi_inst_IO0   (QSPI_IO0),  //                   .hps_io_qspi_inst_IO0
	  .hps_io_hps_io_qspi_inst_IO1   (QSPI_IO1),  //                   .hps_io_qspi_inst_IO1
	  .hps_io_hps_io_qspi_inst_IO2   (QSPI_IO2),  //                   .hps_io_qspi_inst_IO2
	  .hps_io_hps_io_qspi_inst_IO3   (QSPI_IO3),  //                   .hps_io_qspi_inst_IO3
	  .hps_io_hps_io_qspi_inst_SS0   (QSPI_SS0),  //                   .hps_io_qspi_inst_SS0
	  .hps_io_hps_io_qspi_inst_CLK   (QSPI_CLK),  //                   .hps_io_qspi_inst_CLK
	  .hps_io_hps_io_sdio_inst_CMD   (SDIO_CMD),  //                   .hps_io_sdio_inst_CMD
	  .hps_io_hps_io_sdio_inst_D0    (SDIO_D0),  //                   .hps_io_sdio_inst_D0
	  .hps_io_hps_io_sdio_inst_D1    (SDIO_D1),  //                   .hps_io_sdio_inst_D1
	  .hps_io_hps_io_sdio_inst_CLK   (SDIO_CLK),  //                   .hps_io_sdio_inst_CLK
	  .hps_io_hps_io_sdio_inst_D2    (SDIO_D2),  //                   .hps_io_sdio_inst_D2
	  .hps_io_hps_io_sdio_inst_D3    (SDIO_D3),  //                   .hps_io_sdio_inst_D3
	  .hps_io_hps_io_usb1_inst_D0    (USB1_D0),  //                   .hps_io_usb1_inst_D0
	  .hps_io_hps_io_usb1_inst_D1    (USB1_D1),  //                   .hps_io_usb1_inst_D1
	  .hps_io_hps_io_usb1_inst_D2    (USB1_D2),  //                   .hps_io_usb1_inst_D2
	  .hps_io_hps_io_usb1_inst_D3    (USB1_D3),  //                   .hps_io_usb1_inst_D3
	  .hps_io_hps_io_usb1_inst_D4    (USB1_D4),  //                   .hps_io_usb1_inst_D4
	  .hps_io_hps_io_usb1_inst_D5    (USB1_D5),  //                   .hps_io_usb1_inst_D5
	  .hps_io_hps_io_usb1_inst_D6    (USB1_D6),  //                   .hps_io_usb1_inst_D6
	  .hps_io_hps_io_usb1_inst_D7    (USB1_D7),  //                   .hps_io_usb1_inst_D7
	  .hps_io_hps_io_usb1_inst_CLK   (USB1_CLK),  //                   .hps_io_usb1_inst_CLK
	  .hps_io_hps_io_usb1_inst_STP   (USB1_STP),  //                   .hps_io_usb1_inst_STP
	  .hps_io_hps_io_usb1_inst_DIR   (USB1_DIR),  //                   .hps_io_usb1_inst_DIR
	  .hps_io_hps_io_usb1_inst_NXT   (USB1_NXT),  //                   .hps_io_usb1_inst_NXT
	  .hps_io_hps_io_gpio_inst_GPIO00 (USB1_ULPI_CS),
	  .hps_io_hps_io_gpio_inst_GPIO09 (USB1_ULPI_RESET_N),
	  .hps_io_hps_io_spim0_inst_CLK  (SPIM0_CLK),  //                   .hps_io_spim0_inst_CLK
	  .hps_io_hps_io_spim0_inst_MOSI (SPIM0_MOSI),  //                   .hps_io_spim0_inst_MOSI
	  .hps_io_hps_io_spim0_inst_MISO (SPIM0_MISO),  //                   .hps_io_spim0_inst_MISO
	  .hps_io_hps_io_spim0_inst_SS0  (SPIM0_SS0),  //                   .hps_io_spim0_inst_SS0
	  .hps_io_hps_io_uart0_inst_RX   (UART0_RX),  //                   .hps_io_uart0_inst_RX
	  .hps_io_hps_io_uart0_inst_TX   (UART0_TX),  //                   .hps_io_uart0_inst_TX
	  .hps_io_hps_io_i2c0_inst_SDA   (HPS_I2C0_SDA),  //                   .hps_io_i2c0_inst_SDA
	  .hps_io_hps_io_i2c0_inst_SCL   (HPS_I2C0_SCL),  //                   .hps_io_i2c0_inst_SCL
	  .ddr3_hps_mem_a                (ddr3_hps_mem_a),  //             memory.mem_a
	  .ddr3_hps_mem_ba               (ddr3_hps_mem_ba),  //                   .mem_ba
	  .ddr3_hps_mem_ck               (ddr3_hps_mem_ck),  //                   .mem_ck
	  .ddr3_hps_mem_ck_n             (ddr3_hps_mem_ck_n),  //                   .mem_ck_n
	  .ddr3_hps_mem_cke              (ddr3_hps_mem_cke),  //                   .mem_cke
	  .ddr3_hps_mem_cs_n             (ddr3_hps_mem_cs_n),  //                   .mem_cs_n
	  .ddr3_hps_mem_ras_n            (ddr3_hps_mem_ras_n),  //                   .mem_ras_n
	  .ddr3_hps_mem_cas_n            (ddr3_hps_mem_cas_n),  //                   .mem_cas_n
	  .ddr3_hps_mem_we_n             (ddr3_hps_mem_we_n),  //                   .mem_we_n
	  .ddr3_hps_mem_reset_n          (ddr3_hps_mem_reset_n),  //                   .mem_reset_n
	  .ddr3_hps_mem_dq               (ddr3_hps_mem_dq),  //                   .mem_dq
	  .ddr3_hps_mem_dqs              (ddr3_hps_mem_dqs),  //                   .mem_dqs
	  .ddr3_hps_mem_dqs_n            (ddr3_hps_mem_dqs_n),  //                   .mem_dqs_n
	  .ddr3_hps_mem_odt              (ddr3_hps_mem_odt),  //                   .mem_odt
	  .ddr3_hps_mem_dm               (ddr3_hps_mem_dm),  //                   .mem_dm
	  .ddr3_hps_oct_rzqin            (ddr3_hps_oct_rzqin),  //                   .oct_rzqin
	  
	  
	  .bus_clock_clk						(sysClock),
	  .bus_reset_reset               (sysReset),
	  .lwhps_bus_waitrequest         (lwhps_bus_waitrequest),
     .lwhps_bus_readdata            (lwhps_bus_readdata),
     .lwhps_bus_readdatavalid       (lwhps_bus_readdatavalid),
     .lwhps_bus_burstcount          (lwhps_bus_burstcount),
     .lwhps_bus_writedata           (lwhps_bus_writedata),
     .lwhps_bus_address             (lwhps_bus_address),
     .lwhps_bus_write               (lwhps_bus_write),
     .lwhps_bus_read                (lwhps_bus_read),
     .lwhps_bus_byteenable          (lwhps_bus_byteenable),
     .lwhps_bus_debugaccess         (lwhps_bus_debugaccess),
	  
	  .hps_f2h_irq0_irq              (hps_f2h_irq0_irq),
	 
     .hps_f2h_sdram0_data_address         (f2h_sdram_address),
     .hps_f2h_sdram0_data_burstcount      (f2h_sdram_burstcount),
     .hps_f2h_sdram0_data_waitrequest     (f2h_sdram_waitrequest),
     .hps_f2h_sdram0_data_readdata        (f2h_sdram_readdata),
     .hps_f2h_sdram0_data_readdatavalid   (f2h_sdram_readdatavalid),
     .hps_f2h_sdram0_data_read            (f2h_sdram_read),
     .hps_f2h_sdram0_data_writedata       (f2h_sdram_writedata),
     .hps_f2h_sdram0_data_byteenable      (f2h_sdram_byteenable),
     .hps_f2h_sdram0_data_write           (f2h_sdram_write),
	 
     .hps_f2h_sdram1_data_address        (hps_f2h_sdram1_data_address),      // hps_f2h_sdram1_data.address
     .hps_f2h_sdram1_data_burstcount     (hps_f2h_sdram1_data_burstcount),   //                    .burstcount
     .hps_f2h_sdram1_data_waitrequest    (hps_f2h_sdram1_data_waitrequest),  //                    .waitrequest
     .hps_f2h_sdram1_data_readdata       (hps_f2h_sdram1_data_readdata),     //                    .readdata
     .hps_f2h_sdram1_data_readdatavalid  (hps_f2h_sdram1_data_readdatavalid),//                    .readdatavalid
     .hps_f2h_sdram1_data_read           (hps_f2h_sdram1_data_read),          //                    .read 
	  
 
     .hps_uart1_cts       (),
     .hps_uart1_dsr       (),
     .hps_uart1_dcd       (),
     .hps_uart1_ri        (),
     .hps_uart1_dtr       (),
     .hps_uart1_rts       (),
     .hps_uart1_out1_n    (),
     .hps_uart1_out2_n    (),
     .hps_uart1_rxd       (TOUCHSCREEN_UART1_RX),
     .hps_uart1_txd       (TOUCHSCREEN_UART1_TX)

 );


//--- System Manager ------------------------------------------

wire [31:0] bitInpPin = {29'd0, USER_DIPSW[2:0]};

SystemManager #
(
	.ref_clock_hz     (sys_ref_clock_hz),
	.sys_clock_hz     (system_clock_hz)
)
sysMgr
(
	.busChipSelect    (busChipSelect[bus_sysMgr]),
	.busWrite         (busWrite),
	.busRead          (busRead),
	.busAddress       (busAddress),
	.busWrData        (busWrData),
	.busRdData        (busRdData[bus_sysMgr]),
	
	.refClock         (sysRefClock),
	.sysClock         (sysClock),
	.sysReset         (sysReset),
	.clkReset         (clkReset),
	.clkLocked        (clkLocked && outClkLocked),
	.usecClock        (usecClock),
	.msecClock        (msecClock),
	.resetInp         (resetInput),
	.ledOut           (ledOut),
	.pinOut           (bitOutPin),
	.pinInp           (bitInpPin)
);

//--- Device Communication -----------------------------------------

I2cMaster i2c_inst
(
	.busClock         (sysClock),
	.busChipSelect    (busChipSelect[bus_i2cMas]),
	.busWrite         (busWrite),
	.busRead          (busRead),
	.busAddress       (busAddress),
	.busWrData        (busWrData),
	.busRdData        (busRdData[bus_i2cMas]),
	
	.sync             (sync_select ? led_sync : verBlank),
	
	.scl              (FPGA_I2C0_SCL),
	.sda              (FPGA_I2C0_SDA)
); 

//---------------------------------------------------------------

wire [31:0]  hps_overlay_address;
wire         ovlyDone;
wire         ignoreAlpha;
wire         start_calibration;
wire         get_batch_num;
wire         set_batch_num;
wire         touchscreen_scale;
wire         touchscreen_video;
wire         touchscreen_up_arrow;
wire         touchscreen_down_arrow;
wire         set_scope_time;
wire         clear_scope_flash;
wire         toggleAGC;

I2cMasterMSP430 i2c_inst_handle
(
	.busClock                 (sysClock),
	.busReset                 (sysReset),
	.busChipSelect            (busChipSelect[bus_i2cMasMSP430]),
	.busWrite                 (busWrite),
	.busRead                  (busRead),
	.busAddress               (busAddress),
	.busWrData                (busWrData),
	.busRdData                (busRdData[bus_i2cMasMSP430]),
	  
	.scl                      (FPGA_I2C1_SCL),
	.sda                      (FPGA_I2C1_SDA)
); 


Hps2FPGA_registers  hps2fpga_inst
(
	.busClock                 (sysClock),
	.busReset                 (sysReset),
	.busChipSelect            (busChipSelect[bus_hps2fpga]),
	.busWrite                 (busWrite),
	.busRead                  (busRead),
	.busAddress               (busAddress),
	.busWrData                (busWrData),
	.busRdData                (busRdData[bus_hps2fpga]),
	
   .lwhps_bus_waitrequest    (lwhps_bus_waitrequest),
   .lwhps_bus_readdata       (lwhps_bus_readdata),
   .lwhps_bus_readdatavalid  (lwhps_bus_readdatavalid),
   .lwhps_bus_burstcount     (lwhps_bus_burstcount),
   .lwhps_bus_writedata      (lwhps_bus_writedata),
   .lwhps_bus_address        (lwhps_bus_address),
   .lwhps_bus_write          (lwhps_bus_write),
   .lwhps_bus_read           (lwhps_bus_read),
   .lwhps_bus_byteenable     (lwhps_bus_byteenable),
	
	.inpBfr                   (inpBfr),
	.hps_overlay_address      (hps_overlay_address),
	.ovlyDone                 (ovlyDone),
	.ignoreAlpha              (ignoreAlpha),
	
	.clear_scope_flash        (clear_scope_flash),
	.set_scope_time           (set_scope_time),
	.touchscreen_scale        (touchscreen_scale),
	.touchscreen_video        (touchscreen_video),
	.touchscreen_up_arrow     (touchscreen_up_arrow),
	.touchscreen_down_arrow   (touchscreen_down_arrow),
	.get_batch_num            (get_batch_num),
	.set_batch_num            (set_batch_num),
	.start_calibration        (start_calibration)
);

//--- Pixel Processing ------------------------------------------

wire        verBlank;
wire        frameStart;

SensorCtrlData senCtrl_inst
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_senCtl]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_senCtl]),
	
	.clock     			  (sensorClock),
	.verBlank  			  (verBlank),
	.frameStart			  (frameStart),
	.imagerCtrl			  (FPGA_IMAGER_CTRL),
	.vid_en    			  (VID_EN_CMD_ENn)
);

wire [11:0] siData;
wire [2:0]  siSync;

SensorInput sensorInput
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_senInp]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_senInp]),
	
	.inpClock           (pixelClock),
	.inpData            (IMAGER_D),
	
	.verBlank           (verBlank),
	.frameStart         (frameStart),
	.led_sync           (led_sync),
	
	.outData            (siData),
	.outSync            (siSync)
);

wire [9:0]  pcData;
wire [2:0]  pcSync;

TwoPixelColumnCorrection   pix_corr_inst
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_pixCor]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_pixCor]),
	
	.inpClock           (pixelClock),
	.inpData            (siData),
	.inpSync            (siSync),
	
	.outData            (pcData),
	.outSync            (pcSync)
);

wire [9:0] igData;
wire [2:0]  igSync;

ImageGenerator imageGenerator
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_imgGen]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_imgGen]),
	
	.inpClock           (pixelClock),
	.inpData            (pcData),
	.inpSync            (pcSync),
	
	.outData            (igData),
	.outSync            (igSync)
);


wire paxEnable;
wire [4:0][4:0][11:0] lbData;
wire [2:0]            lbSync;

LineBuffer filterBuffer
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_fltBfr]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_fltBfr]),
	
	.inpClock           (pixelClock),
	.inpData            ({pcData, 2'd0}),
	.inpSync            (pcSync),
	.outData            (lbData),
	.outSync            (lbSync)
);

// fbData/fbSync also feed stats modules

wire [11:0] sfData;
wire [2:0]  sfSync;

SpatialFilter spatialFilter
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_spaFlt]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_spaFlt]),
	
	.inpClock           (pixelClock),
	.inpData            (lbData),
	.inpSync            (lbSync),
	.outData            (sfData),
	.outSync            (sfSync)
);


PaxelCaptureInp paxelCaptureInp
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_paxInp]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_paxInp]),
	
	.inpEnable          (0),
	.outEnable          (paxEnable),
	
	.inpClock           (pixelClock),
	.inpData            (sfData),
	.inpSync            (sfSync)
);

wire [11:0] wbData;
wire [2:0]  wbSync;

WhiteBalance whiteBalance
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_whiBal]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_whiBal]),
	
	.inpClock           (pixelClock),
	.inpData            (sfData),
	.inpSync            (sfSync),
	.outData            (wbData),
	.outSync            (wbSync)
);

wire [4:0][4:0][11:0] bbData;
wire [2:0]            bbSync;

LineBuffer bayerBuffer
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_bayBfr]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_bayBfr]),
	
	.inpClock           (pixelClock),
	.inpData            (wbData),
	.inpSync            (wbSync),
	.outData            (bbData),
	.outSync            (bbSync)
);

wire [2:0][11:0] biData;
wire [2:0]       biSync;

BayerInterpolator bayerInterpolator
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_bayInt]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_bayInt]),
	
	.inpClock           (pixelClock),
	.inpData            (bbData),
	.inpSync            (bbSync),
	.outData            (biData),
	.outSync            (biSync)
);

wire [2:0][11:0] ccData;
wire [2:0]       ccSync;

ColorMatrix correctionMatrix
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_corMat]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_corMat]),
	
	.inpClock           (pixelClock),
	.inpData            (biData),
	.inpSync            (biSync),
	.outData            (ccData),
	.outSync            (ccSync)
);

wire [2:0][7:0]  glData;
wire [2:0]       glSync;

ColorLut gammaLut
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_gamLut]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_gamLut]),
	
	.inpClock           (pixelClock),
	.inpData            (ccData),
	.inpSync            (ccSync),
	.outData            (glData),
	.outSync            (glSync)
);


PaxelCaptureOut paxelCaptureOut
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_paxOut]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_paxOut]),
	
	.inpEnable          (paxEnable),
	.outEnable          (),
	
	.inpClock           (pixelClock),
	.inpData            (glData),
	.inpSync            (glSync)
);


wire [2:0][7:0]  coData;
wire [2:0]       coSync;

CursorOverlay cursorOverlay
(
	.busClock           (sysClock),
	.busChipSelect      (busChipSelect[bus_crsOvr]),
	.busWrite           (busWrite),
	.busRead            (busRead),
	.busAddress         (busAddress),
	.busWrData          (busWrData),
	.busRdData          (busRdData[bus_crsOvr]),
	
	.inpClock           (pixelClock),
	.inpData            (glData),
	.inpSync            (glSync),
	
	.outData            (coData),
	.outSync            (coSync)
);


wire [23:0] fbData;
wire        fbDv;
wire        startOutput;
wire [3:0]  inpBfr;
wire [2:0]  fbSync;
wire [1:0]  fb_y_position;


FrameBuffer frameBuffer
(
	.busClock          (sysClock),
	.busReset          (sysReset),
	.busChipSelect     (busChipSelect[bus_frmBuf]),
	.busWrite          (busWrite),
	.busRead           (busRead),
	.busAddress        (busAddress),
	.busWrData         (busWrData),
	.busRdData         (busRdData[bus_frmBuf]),
	
	.inpClock          (pixelClock),
	.inpClockLocked    (1'b1),
	.inpSync           (coSync),
	.inpData           (coData),
	
	.outClock          (clk_74_25MHz),
	.outDe             (fbDv),
	.outVS             (parOutVs),
	.outData           (fbData),
	.outSync           (fbSync),
	.y_position        (fb_y_position),
	
	.startOutput       (startOutput),
	.inpBfr            (inpBfr),
	
	.ddr3_clk           (hpsClock),
	.avl_ready          (!f2h_sdram_waitrequest),
	.avl_addr           (f2h_sdram_address),       
	.avl_rdata_valid    (f2h_sdram_readdatavalid), 
	.avl_rdata          (f2h_sdram_readdata),      
	.avl_wdata          (f2h_sdram_writedata),     
   .avl_read_req       (f2h_sdram_read),    
   .avl_write_req      (f2h_sdram_write),   
	.avl_burstcount     (f2h_sdram_burstcount),  // This must be a static number (NOT an incrementing burst count)
	.avl_be             (f2h_sdram_byteenable)
);


wire [2:0][7:0]  irhData;
wire [2:0]       irhSync;
wire [9:0]       wrusedw_fifo_out;

ImageResizer imgRsz_hdmi_inst
(
   .busClock          (sysClock),
   .busReset          (sysReset),
   .busChipSelect     (busChipSelect[bus_imgRsz_hdmi]),
   .busWrite          (busWrite),
   .busRead           (busRead),
   .busAddress        (busAddress),
   .busWrData         (busWrData),
   .busRdData         (busRdData[bus_imgRsz_hdmi]),
	.regUpdate         (regUpdate),

	.inpClock          (clk_74_25MHz),	
	.inpData           (fbData),
	.inpSync           (fbSync),
	.wrusedw_fifo_out  (wrusedw_fifo_out),

	.outClock          (clk_74_25MHz),
	.outClockLocked    (outClkLocked),
	.y_position        (fb_y_position),
	.outData           (irhData),
	.outSync           (irhSync)

);

wire  parOutVs;
wire  parOutHs;
wire  parOutDe;
wire  [2:0][7:0]  parOutData;
wire  outClock_sw = (outSelect == 1'b1)  ? xcvrClock_div2 : xcvrParClock[3];

ParallelOut parOut_hdmi_inst
(
   .busClock          (sysClock),
   .busReset          (sysReset),
   .busChipSelect     (busChipSelect[bus_parHDMI]),
   .busWrite          (busWrite),
   .busRead           (busRead),
   .busAddress        (busAddress),
   .busWrData         (busWrData),
   .busRdData         (busRdData[bus_parHDMI]),
	
	.inpClock          (clk_74_25MHz),
	.inpFifoSync       (irhSync),
	.inpFifoData       (irhData),

	.outClock          (outClock_sw),
	.wrusedw_fifo_inp  (wrusedw_fifo_out),
	.outVs             (parOutVs),
	.outHs             (parOutHs),
	.outDe             (parOutDe),
	.outData           (parOutData)
);


wire usb3Vs_rgb;
wire usb3De_rgb;
wire [2:0][7:0] usb3data_rgb;

RGB2YCbCr  RGB2YCbCr_inst
(
	.inpClock          (outClock_sw),
	.reset             (sysReset),
	.inpVs             (usb3Vs_rgb),
	.inpDe             (usb3De_rgb),
	.rgb888Data        (usb3data_rgb),
	
	.outVs             (FRAME_VALID),
	.outDe             (LINE_VALID),
	.yuv422Data        (UVC_DATA)
);
  
wire        hdmiVs;
wire        hdmiHs;
wire        hdmiDe;
wire [23:0] hdmiData;

assign hdmiVs = (outSelect == 1'b1)  ? 1'b0 : parOutVs;
assign hdmiHs = (outSelect == 1'b1)  ? 1'b0 : parOutHs;
assign hdmiDe = (outSelect == 1'b1)  ? 1'b0 : parOutDe;
assign hdmiData = (outSelect == 1'b1)  ? 24'd0 : parOutData;

assign usb3Vs_rgb = (outSelect == 1'b1) ? parOutVs : 1'b0;
assign usb3De_rgb = (outSelect == 1'b1)  ? parOutDe : 1'b0;
assign usb3data_rgb = (outSelect == 1'b1)  ? parOutData : 24'd0;


wire [23:0]  hpsfbData;
wire [9:0]   lcd_wrusedw_fifo_out;
wire [2:0]   hpsOutSync;
wire         hpsfbDv;
wire         inpDV_fromMixer;
wire [127:0] ovlyFifoData;
wire [2:0]   y_position;

HpsFrameBuffer hps_frame_buffer_inst
(
	.busClock          (sysClock),
	.busReset          (sysReset),
	.busChipSelect     (busChipSelect[bus_hpsFb]),
	.busWrite          (busWrite),
	.busRead           (busRead),
	.busAddress        (busAddress),
	.busWrData         (busWrData),
	.busRdData         (busRdData[bus_hpsFb]),
	
	.outClock          (lcdClock),
	.outSync           (hpsOutSync),
	.outDe             (hpsfbDv),
	.y_position        (y_position),
	.outVS             (lcd_outVs),
	.outData           (hpsfbData),
	
	.ovlyClock           (lcdClock),
	.hps_overlay_address (hps_overlay_address),
	.inpDV_fromMixer     (inpDV_fromMixer),
	.ovlyFifoData        (ovlyFifoData),
	
	.startOutput       (startOutput),
	.inpBfr            (inpBfr),
	
	.ddr3_clk           (hpsClock),
	.avl_ready          (!hps_f2h_sdram1_data_waitrequest),
	.avl_addr           (hps_f2h_sdram1_data_address),       
	.avl_rdata_valid    (hps_f2h_sdram1_data_readdatavalid), 
	.avl_rdata          (hps_f2h_sdram1_data_readdata),      
   .avl_read_req       (hps_f2h_sdram1_data_read),    
	.avl_burstcount     (hps_f2h_sdram1_data_burstcount)
);


wire [2:0][7:0]  irData;
wire [2:0]       irSync;

ImageResizer imgResize_inst
(
   .busClock          (sysClock),
   .busReset          (sysReset),
   .busChipSelect     (busChipSelect[bus_imgRsz]),
   .busWrite          (busWrite),
   .busRead           (busRead),
   .busAddress        (busAddress),
   .busWrData         (busWrData),
   .busRdData         (busRdData[bus_imgRsz]),
	.regUpdate         (regUpdate),

	.inpClock          (lcdClock),	
	.inpData           (hpsfbData),
	.inpSync           (hpsOutSync),
	.wrusedw_fifo_out  (lcd_wrusedw_fifo_out),

	.outClock          (lcdClock),
	.outClockLocked    (outClkLocked),
	.y_position        (y_position),
	.outData           (irData),
	.outSync           (irSync)

);


wire        lcd_outVs;
wire        lcd_outHs;
wire        lcd_outDe;
wire [23:0] lcd_outData;


ParallelOutMixer parOut_lcd_inst
(
   .busClock          (sysClock),
   .busReset          (sysReset),
   .busChipSelect     (busChipSelect[bus_parLCD]),
   .busWrite          (busWrite),
   .busRead           (busRead),
   .busAddress        (busAddress),
   .busWrData         (busWrData),
   .busRdData         (busRdData[bus_parLCD]),
	
	.inpClock          (lcdClock),
	.inpFifoSync       (irSync),
	.inpFifoData       (irData),
	
	.outClock          (lcdClock),
	.ovlyFifoData      (ovlyFifoData),
	.inpDV_ovly        (inpDV_fromMixer),
	.ovlyDone          (ovlyDone),
	.ignoreAlpha       (ignoreAlpha),
	
	.wrusedw_fifo_inp  (lcd_wrusedw_fifo_out),
	.outVs             (lcd_outVs),
	.outHs             (lcd_outHs),
	.outDe             (lcd_outDe),
	.outData           (lcd_outData)
);

LCDDisplayOut  lcd_disp
(
   .busClock          (sysClock),
   .busReset          (sysReset),
   .busChipSelect     (busChipSelect[bus_lcddsp]),
   .busWrite          (busWrite),
   .busRead           (busRead),
   .busAddress        (busAddress),
   .busWrData         (busWrData),
   .busRdData         (busRdData[bus_lcddsp]),
	
	.inpClock          (lcdClock),
	.inpData           (lcd_outData),
	.inpDv             (lcd_outDe),
	.inpVs             (lcd_outVs),
	.inpHs             (lcd_outHs),
	
	.lcdClk            (LCD_CLK),
	.lcdData           (LCD_DATA)
);

PwmGenerator  pwm_led_backlight
(
   .busClock          (sysClock),
   .busReset          (sysReset),
   .busChipSelect     (busChipSelect[bus_pwm]),
   .busWrite          (busWrite),
   .busRead           (busRead),
   .busAddress        (busAddress),
   .busWrData         (busWrData),
   .busRdData         (busRdData[bus_pwm]),
	
	.clock             (usecClock),
	.pulse             (pwmBacklight)
	
);
wire [9:0]  hdmiBlu0;
wire [9:0]  hdmiGrn1;
wire [9:0]  hdmiRed2;
wire [9:0]  hdmiClk3;


TmdsEncoder tmds_inst
(
   .busClock          (sysClock),
   .busReset          (sysReset),
   .busChipSelect     (busChipSelect[bus_tmds]),
   .busWrite          (busWrite),
   .busRead           (busRead),
   .busAddress        (busAddress),
   .busWrData         (busWrData),
   .busRdData         (busRdData[bus_tmds]),
	
	.inpClock          (xcvrParClock[3]),
	.inpVs             (hdmiVs),
	.inpHs             (hdmiHs),
	.inpDe             (hdmiDe),
	.inpData           (hdmiData),

	.tx_ready          (tx_ready | !tx_reconfig_cal_busy),
	
	.out_blu0          (hdmiBlu0),
	.out_grn1          (hdmiGrn1),
	.out_red2          (hdmiRed2),
	.out_clk3          (hdmiClk3)
	
);

wire [3:0]   tx_cal_busy;
wire         tx_reconfig_cal_busy;
wire [3:0]   tx_ready;
wire [3:0]   xcvr_pll_locked;
wire [559:0] reconfig_to_xcvr;
wire [367:0] reconfig_from_xcvr;
wire [31:0]  xcvr_readdata;
wire [31:0]  xcvr_writedata;
wire [6:0]   xcvr_address;
wire         xcvr_write = busWrite && busChipSelect[bus_xcvr];
wire         xcvr_read = busRead && busChipSelect[bus_xcvr];
//wire [69:0]  pll_reconfig_to_xcvr;
//wire [45:0]  pll_reconfig_from_xcvr;
wire         pll_powerdown;
wire         tx_analogreset;
wire         tx_digitalreset;

reg          xcvrClock_div2;

assign busRdData[bus_xcvr] = xcvr_readdata;

always begin
	if(xcvr_write || xcvr_read) begin
		xcvr_writedata = busWrData;
		xcvr_address   = busAddress[8:2];
	end		
end

always@(posedge xcvrParClock[3]) begin
   xcvrClock_div2 <= !xcvrClock_div2;
end
	
tmds_serializer  tmds_ser_inst
(
	.pll_powerdown                      (pll_powerdown),         
	.tx_analogreset                     (tx_analogreset),        
	.tx_digitalreset                    (tx_digitalreset),       
	.tx_pll_refclk                      (GXB_REFCLK1),
	.pll_locked				               (xcvr_pll_locked),         
	.tx_serial_data                     (GXB_TX),                  
	.tx_std_coreclkin                   (xcvrParClock),      
	.tx_std_clkout                      (xcvrParClock),         
	.tx_std_polinv                      (4'hF),         
	.tx_cal_busy                        (tx_cal_busy),           
	.reconfig_to_xcvr                   (reconfig_to_xcvr),      
	.reconfig_from_xcvr                 (reconfig_from_xcvr),    
	.tx_parallel_data                   ({34'd0, hdmiRed2, 34'd0, hdmiGrn1, 34'd0, hdmiBlu0, 34'd0,hdmiClk3})

);

xcvr_reconfig xcvr_reconfig_inst
(

	.mgmt_clk_clk              (sysClock),  
	.mgmt_rst_reset            (sysReset),
	.reconfig_mgmt_address     (xcvr_address),
	.reconfig_mgmt_read        (xcvr_read),
	.reconfig_mgmt_readdata    (xcvr_readdata),
	.reconfig_mgmt_waitrequest (),
	.reconfig_mgmt_write       (xcvr_write),
	.reconfig_mgmt_writedata   (xcvr_writedata),
	
	.reconfig_busy             (),
	.tx_cal_busy               (tx_reconfig_cal_busy),
	.rx_cal_busy               (),
	.reconfig_to_xcvr          (reconfig_to_xcvr),
	.reconfig_from_xcvr        (reconfig_from_xcvr)
);

wire rst_ctrl_pll_locked =| xcvr_pll_locked;

xcvr_rst_ctrl  xcvr_rst_controller
(
	.clock						(sysClock),
	.reset						(sysReset),
	.pll_powerdown				(pll_powerdown),
	.tx_analogreset			(tx_analogreset),
	.tx_digitalreset			(tx_digitalreset),
	.tx_ready					(tx_ready),
	.pll_locked					(rst_ctrl_pll_locked),
	.pll_select					(1'b0),
	.tx_cal_busy				(tx_cal_busy)
);

//--- Stats ----------------------------------------------


PaxelStatsExposure paxelStatsExposure
(
    .busClock           (sysClock),
    .busChipSelect      (busChipSelect[bus_expSts]),
    .busWrite           (busWrite),
    .busRead            (busRead),
    .busAddress         (busAddress),
    .busWrData          (busWrData),
    .busRdData          (busRdData[bus_expSts]),
    
    .inpClock           (pixelClock),
    .inpData            (pcData),
    .inpSync            (pcSync)
);



//==============================================================

endmodule

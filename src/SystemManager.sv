//==============================================================

// Registers
//  0 15:8 (0xA5 = reset hold, 0x5A = reset pulse)
//  1 Watchdog
//  2 usec Count
//  3 msec Count

module SystemManager
(
	input  wire        busChipSelect,
	input  wire        busWrite,
	input  wire        busRead,
	input  wire [19:0] busAddress,
	input  wire [31:0] busWrData,
	output reg  [31:0] busRdData,
	
	input  wire        refClock,
	input  wire        sysClock,
	output reg         usecClock,
	output reg         msecClock,
	input  wire        resetInp,
	output wire        sysReset,
	output wire        clkReset,
	input  wire        clkLocked,
	output reg         ledOut,
	output reg  [31:0] pinOut,
	input  wire [31:0] pinInp
);

parameter ref_clock_hz = 100000000;
parameter sys_clock_hz = 100000000; 

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));
wire selOut = (busChipSelect && (busAddress[19:16] == 1));
wire selInp = (busChipSelect && (busAddress[19:16] == 2));

reg [3:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[3:2]];
	else if (selOut)	
		busRdData = {31'd0, pinOut[busAddress[9:2]]};
	else if (selInp) 	
		busRdData = {31'd0, pinInp[busAddress[9:2]]};
	else	
		busRdData = 0;
end

always@(posedge sysClock) begin	
	if (resetState < 2)
		register[1]  <= 16'hFFFF;
	else if (selReg && busWrite)
		register[busAddress[3:2]] <= busWrData;
	else if (selOut && busWrite)
		pinOut[busAddress[9:2]]   <= busWrData[0];
	else begin
		register[2] <= usecCount;
		register[3] <= msecCount;
		if ((usecPrescale == 0) && (msecPrescale == 0) && (register[1] > 0) && (register[1] < 16'hFFFF))
			register[1] <= register[1] - 1'd1;
		if ((resetReg == 8'h5A) && (resetState <= 1))
			register[0][15:8] <= 0;
	end
end

//---------------------------------------------------------------

wire       ledEnable = register[0][0];
wire [7:0] resetReg  = register[0][15:8];
//         status    = register[0][31:28]; // software only

reg [1:0]  resetState = 0;
// 0 - In reset
// 1 - In reset, timing
// 2 - Out of reset

reg [31:0] resetCount = 31'd1000000;

assign clkReset  = (resetState == 0);
assign sysReset  = (resetState <= 1);

//--- Reset timer ------------------------------------------------------

always@(posedge refClock) begin	

//	if (resetInp || ((resetState == 2) && (register[1] == 0))) begin
	if (resetInp) begin
		resetState <= 0;
		resetCount <= 31'd1000000;
	end
	else if ((resetReg == 8'hA5) || (resetReg == 8'h5A)) begin
		resetState <= 1'd1;
		resetCount <= 31'd1000000;
	end
	else if (resetState < 2) begin
		if (resetCount > 0)
			resetCount <= resetCount - 1'd1;
		else if ((resetState == 0) || clkLocked) begin
			resetCount <= 31'd1000000;
			resetState <= resetState + 1'd1;
		end
	end
end

//--- Usec/Msec Counters ----------------------------------------------

reg [31:0] usecCount = 0;
reg [31:0] msecCount = 0;

reg [9:0]  usecPrescale = 0;
reg [9:0]  msecPrescale = 0;
reg [31:0] secUsec = 0;

// Because sysClock may not divide to usec exactly, 
//  we add 1 to the usec prescaler Count for part of each second.
// The result is exact long term timing with very small error for times < 1 sec.

parameter usec_div_nom  = sys_clock_hz / 1000000;
parameter usec_div_frac = sys_clock_hz % 1000000;

wire [31:0] usec_div = (secUsec > usec_div_frac) ? usec_div_nom : usec_div_nom + 1;   

always@(posedge sysClock) begin	
	if (resetState < 2) begin
		usecCount    <= 0;
		msecCount    <= 0;
		usecPrescale <= 0;
		msecPrescale <= 0;
		secUsec      <= 0;
	end
	else begin
	
		if (usecPrescale < usec_div - 1) begin
			if (usecPrescale >= (usec_div / 2))
				usecClock <= 0;
			usecPrescale <= usecPrescale + 1'd1;
		end
		else begin
			usecPrescale <= 0;
			usecCount    <= usecCount + 1'd1;
			usecClock    <= 1'd1;
			if (msecPrescale < 999) begin
				if (msecPrescale >= 500)
					msecClock <= 0;
				msecPrescale <= msecPrescale + 1'd1;
			end
			else begin
				msecPrescale <= 0;
				msecCount    <= msecCount + 1'd1;
				msecClock    <= 1'd1;
			end
		end
		
		if (secUsec < 999999)
			secUsec <= secUsec + 1'd1;
		else
			secUsec <= 0;
			
	end
end

//--- LED Blinker ------------------------------------------------

reg [31:0] ledCount      = 0;
reg        ledToggle     = 0;
reg        ledEnableLast = 0;

assign ledOut = ledEnable ^ ledToggle;

always@(posedge sysClock) begin
	if (ledEnable != ledEnableLast) begin
		ledCount <= 0;
		ledToggle <= 0;
	end
	else if (ledCount < sys_clock_hz) begin
		ledCount <= ledCount + 1'd1;
	end
	else begin
		ledCount <= 0;
		ledToggle <= ~ledToggle;
	end
	ledEnableLast <= ledEnable;
end

endmodule

//==============================================================

module SystemTimers
(
	input  wire        busClock,
	input  wire        busChipSelect,
	input  wire        busWrite,
	input  wire        busRead,
	input  wire [19:0] busAddress,
	input  wire [31:0] busWrData,
	output reg  [31:0] busRdData
);

parameter bus_clock_hz = 100000000; 

//--- Bus Interface ----------------------------------------------

reg [3:0][31:0] register = 0;

assign busRdData = register[busAddress[3:2]];

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[3:2]] <= busWrData;
	else begin
		register[2] <= usecCount;
		register[3] <= msecCount;
	end
end

//--- Usec/Msec Counters ----------------------------------------------

reg  [31:0] usecCount = 0;
reg  [31:0] msecCount = 0;

reg  [9:0]  usecPrescale = 0;
reg  [9:0]  msecPrescale = 0;

wire [9:0]  usecPrescaleMax = bus_clock_hz / 1000000;

always@(posedge busClock) begin	
	if (usecPrescale + 1 < usecPrescaleMax)
		usecPrescale <= usecPrescale + 1'd1;
	else begin
		usecPrescale <= 0;
		usecCount    <= usecCount + 1'd1;
		if (msecPrescale < 999)
			msecPrescale <= msecPrescale + 1'd1;
		else begin
			msecPrescale <= 0;
			msecCount    <= msecCount + 1'd1;
		end
	end
end

endmodule

//==============================================================
//==============================================================

module Processor
(
	output wire [bus_devices-1:0]       busChipSelect,
	output wire                         busWrite,
	output wire                         busRead,
	output wire                  [19:0] busAddress,
	output wire                  [31:0] busWrData,
	input  wire [bus_devices-1:0][31:0] busRdData,
	
	input  wire        refClock,
	output wire        sysClock,
	input  wire        sysReset,
	input  wire        clkReset,
	output wire        clkLocked,
	
	output wire [7:0]  out_export,
	input  wire [9:0]  inp_export,
	input  wire [1:0]  trigger_export,
	
	input  wire        uartRxd,
	output wire        uartTxd,
	
	output wire        adcclk_locked,
	output wire        adcclk,
	output wire        sensorclk_out
);

parameter ref_clock_freq = "100.0 MHz";
parameter sys_clock_freq = "100.0 MHz";
parameter bus_devices    = 1;

//--- Bus Interface ----------------------------------------------

wire        busChipSelectSys;
wire [25:0] busAddressSys;
wire [31:0] busRdDataSys;

wire [7:0]  busSub     = busAddressSys[25:18];
assign      busAddress = {busAddressSys[17:0], 2'd0};

assign busRdDataSys = busRdData[busSub];

genvar n;
generate
	for (n = 0; n < bus_devices; n++) begin: inst
		assign busChipSelect[n] = (busChipSelectSys & (busSub == n));
	end
endgenerate

//--- qSys system ------------------------------------------------
 
system sys
(
	.clk_clk                        (sysClock),
	.clock_reset_reset_n            (!sysReset),
	
	.bus_address                    (busAddressSys),
	.bus_write                      (busWrite),
	.bus_read                       (busRead),
	.bus_readdata                   (busRdDataSys),
	.bus_writedata                  (busWrData),
	.bus_chipselect                 (busChipSelectSys),
		
	.uart_rxd                       (uartRxd),
	.uart_txd                       (uartTxd),
	
	.inp_export                     (inp_export),
	.trigger_export                 (trigger_export),
	.out_export                     (out_export),                 
	.adcclk_pll_locked_export 	     (adcclk_locked), 
	.adcclk_clk                     (adcclk),               
	.sensorclk_clk                  (sensorclk_out) 
);


//---------------------------------------------------------------

altera_pll #
(
	.fractional_vco_multiplier  ("false"),
	.reference_clock_frequency  (ref_clock_freq),
	.operation_mode             ("normal"),
	.number_of_clocks           (1),
	.output_clock_frequency0    (sys_clock_freq),  
	.phase_shift0               ("0 ps"),
	.duty_cycle0                (50),
	.pll_type                   ("General"),
	.pll_subtype                ("General")
) 
processorPll
(
	.outclk             (sysClock),
	.locked             (clkLocked),
	.fboutclk           (),
	.fbclk              (1'b0),
	.rst                (clkReset),
	.refclk             (refClock)
);

endmodule

//==============================================================

//==============================================================

// M10K configurations:
//  256 x 40
//  512 x 20
// 1024 x 10
// 2048 x  5
// 4096 x  2 (20% loss)
// 8192 x  1 (20% loss)


// RamOneClock - Single clock, a port is write only, b port is read only


module RamOneClock
#(	parameter dat_wid = 10,
	parameter adr_wid = 10,
	device_family = "Cyclone V"
)
(
	input  wire [dat_wid-1:0] data_a,
	input  wire               wren_a,
	output wire [dat_wid-1:0] q_b,
	input  wire [adr_wid-1:0] address_a,
	input  wire [adr_wid-1:0] address_b,
	input  wire               clock
);

altsyncram # 
(
	.lpm_type                ("altsyncram"),
	.operation_mode          ("DUAL_PORT"),
	.intended_device_family  (device_family),
	.width_a                 (dat_wid), 
	.width_b                 (dat_wid), 
	.numwords_a              (1 << adr_wid),
	.numwords_b              (1 << adr_wid),
	.widthad_a               (adr_wid),
	.widthad_b               (adr_wid),
	.address_aclr_b          ("NONE"),
	.address_reg_b           ("CLOCK0"),
	.width_byteena_a         (1),
	.clock_enable_input_a    ("BYPASS"),
	.clock_enable_input_b    ("BYPASS"),
	.clock_enable_output_b   ("BYPASS"),
	.outdata_aclr_b          ("NONE"),
	.outdata_reg_b           ("UNREGISTERED"),
	.power_up_uninitialized  ("FALSE")
)
ram
(
	.data_a          (data_a),
	.data_b          (),  
	.wren_a          (wren_a),
	.wren_b          (),
	.q_a             (),
	.q_b             (q_b),
	.rden_a          (1'b1),
	.rden_b          (1'b1),
	.address_a       (address_a),
	.address_b       (address_b),
	.byteena_a       (1'b1),
	.byteena_b       (1'b1),
	.addressstall_a  (1'b0),
	.addressstall_b  (1'b0),
	.clock0          (clock),
	.clock1          (1'd1),
	.aclr0           (1'b0),
	.aclr1           (1'b0),
	.clocken0        (1'b1),
	.clocken1        (1'b1),
	.clocken2        (1'b1),
	.clocken3        (1'b1),
	.eccstatus       ()
);

endmodule

//==============================================================

// RamOneClockBidir - Single clock, a port is read/write, b port is read/write


module RamOneClockBidir
#(	parameter dat_wid = 10,
	parameter adr_wid = 10,
	device_family = "Cyclone V"
)
(
	input  wire [dat_wid-1:0] data_a,
	input  wire               wren_a,
	input  wire [dat_wid-1:0] data_b,
	input  wire               wren_b,
	output wire [dat_wid-1:0] q_a,
	output wire [dat_wid-1:0] q_b,
	input  wire [adr_wid-1:0] address_a,
	input  wire [adr_wid-1:0] address_b,
	input  wire               clock
);

altsyncram # 
(
	.lpm_type                ("altsyncram"),
	.operation_mode          ("BIDIR_DUAL_PORT"),
	.intended_device_family  (device_family),
	.width_a                 (dat_wid), 
	.width_b                 (dat_wid), 
	.numwords_a              (1 << adr_wid),
	.numwords_b              (1 << adr_wid),
	.widthad_a               (adr_wid),
	.widthad_b               (adr_wid),
	.address_aclr_b          ("NONE"),
	.address_reg_b           ("CLOCK0"),
	.clock_enable_input_a    ("BYPASS"),
	.clock_enable_input_b    ("BYPASS"),
	.clock_enable_output_a   ("BYPASS"),
	.clock_enable_output_b   ("BYPASS"),
	.outdata_aclr_a          ("NONE"),
	.outdata_aclr_b          ("NONE"),
	.outdata_reg_a           ("UNREGISTERED"),
	.outdata_reg_b           ("UNREGISTERED"),
	.indata_reg_b            ("CLOCK0"),
	.read_during_write_mode_mixed_ports ("DONT_CARE"),
	.read_during_write_mode_port_a      ("NEW_DATA_NO_NBE_READ"),
	.read_during_write_mode_port_b      ("NEW_DATA_NO_NBE_READ"),
	.width_byteena_a 			 (1),
	.width_byteena_b 			 (1),
	.wrcontrol_wraddress_reg_b ("CLOCK0"),
	.power_up_uninitialized    ("FALSE")
)
ram
(
	.data_a          (data_a),
	.data_b          (data_b),  
	.wren_a          (wren_a),
	.wren_b          (wren_b),
	.q_a             (q_a),
	.q_b             (q_b),
	.rden_a          (1'b1),
	.rden_b          (1'b1),
	.address_a       (address_a),
	.address_b       (address_b),
	.byteena_a       (1'b1),
	.byteena_b       (1'b1),
	.addressstall_a  (1'b0),
	.addressstall_b  (1'b0),
	.clock0          (clock),
	.clock1          (1'd1),
	.aclr0           (1'b0),
	.aclr1           (1'b0),
	.clocken0        (1'b1),
	.clocken1        (1'b1),
	.clocken2        (1'b1),
	.clocken3        (1'b1),
	.eccstatus       ()
);

endmodule

//==============================================================

// RamTwoClock - Dual clock, a port is write only, b port is read only



module RamTwoClock
#(	parameter dat_wid = 10,
	parameter adr_wid = 10,
	parameter device_family = "Cyclone V"
)	
(
	input  wire [dat_wid-1:0] data_a,
	input  wire               wren_a,
	output wire [dat_wid-1:0] q_b,
	input  wire [adr_wid-1:0] address_a,
	input  wire [adr_wid-1:0] address_b,
	input  wire               clock_a,
	input  wire               clock_b
);
altsyncram # 
(
	.lpm_type                      ("altsyncram"),
	.operation_mode                ("DUAL_PORT"),
	.intended_device_family        (device_family),
	.width_a                       (dat_wid), 
	.width_b                       (dat_wid), 
	.numwords_a                    (1 << adr_wid),
	.numwords_b                    (1 << adr_wid),
	.widthad_a                     (adr_wid),
	.widthad_b                     (adr_wid),
	.address_reg_b                 ("CLOCK1"),
	.indata_reg_a                  ("CLOCK0"),
	.indata_reg_b                  ("CLOCK1"),
	.outdata_reg_b                 ("UNREGISTERED"), 
	.address_aclr_b                ("NONE"),
	.outdata_aclr_b                ("NONE"),
	.width_byteena_a               (1),
	.clock_enable_input_a          ("BYPASS"),
	.clock_enable_input_b          ("BYPASS"),
	.clock_enable_output_b         ("BYPASS"),
	.power_up_uninitialized        ("FALSE")
)
ram
(
	.data_a          (data_a),
	.wren_a          (wren_a),
	.q_a             (),
	.q_b             (q_b),
	.rden_a          (1'b1),
	.rden_b          (1'b1),
	.address_a       (address_a),
	.address_b       (address_b),
	.byteena_a       (1'b1),
	.byteena_b       (1'b1),
	.addressstall_a  (1'b0),
	.addressstall_b  (1'b0),
	.clock0          (clock_a),
	.clock1          (clock_b),
	.aclr0           (1'b0),
	.aclr1           (1'b0),
	.clocken0        (1'b1),
	.clocken1        (1'b1),
	.clocken2        (1'b1),
	.clocken3        (1'b1),
	.eccstatus       ()
);

endmodule

//==============================================================

// RamTwoClockAsym - Dual clock, a is write only, b is read only, a and b can be different sizes



module RamTwoClockAsym
#( 	parameter dat_wid_a = 10,
	parameter dat_wid_b = 10,
	parameter adr_wid_a = 10,
	parameter adr_wid_b = 10,
	parameter device_family = "Cyclone V"
)
(
	input  wire [dat_wid_a-1:0] data_a,
	input  wire                 wren_a,
	output wire [dat_wid_b-1:0] q_b,
	input  wire [adr_wid_a-1:0] address_a,
	input  wire [adr_wid_b-1:0] address_b,
	input  wire                 clock_a,
	input  wire                 clock_b
);


altsyncram # 
(
	.lpm_type                      ("altsyncram"),
	.operation_mode                ("DUAL_PORT"),
	.intended_device_family        (device_family),
	.width_a                       (dat_wid_a), 
	.width_b                       (dat_wid_b), 
	.numwords_a                    (1 << adr_wid_a),
	.numwords_b                    (1 << adr_wid_b),
	.widthad_a                     (adr_wid_a),
	.widthad_b                     (adr_wid_b),
	.address_reg_b                 ("CLOCK1"),
	.outdata_reg_b                 ("UNREGISTERED"), 
	.address_aclr_b                ("NONE"),
	.outdata_aclr_b                ("NONE"),
	.width_byteena_a               (1),
	.clock_enable_input_a          ("BYPASS"),
	.clock_enable_input_b          ("BYPASS"),
	.clock_enable_output_b         ("BYPASS"),
	.power_up_uninitialized        ("FALSE")
)
ram
(
	.data_a          (data_a),
	.wren_a          (wren_a),
	.q_a             (),
	.q_b             (q_b),
	.rden_a          (1'b1),
	.rden_b          (1'b1),
	.address_a       (address_a),
	.address_b       (address_b),
	.byteena_a       (1'b1),
	.byteena_b       (1'b1),
	.addressstall_a  (1'b0),
	.addressstall_b  (1'b0),
	.clock0          (clock_a),
	.clock1          (clock_b),
	.aclr0           (1'b0),
	.aclr1           (1'b0),
	.clocken0        (1'b1),
	.clocken1        (1'b1),
	.clocken2        (1'b1),
	.clocken3        (1'b1),
	.eccstatus       ()
);

endmodule
//==============================================================

// RamTwoClockBidir - Dual clock, a and b ports are read/write


module RamTwoClockBidir
#(	parameter dat_wid = 10,
	parameter adr_wid = 10,
	parameter device_family = "Cyclone V"
)
(
	input  wire [dat_wid-1:0] data_a,
	input  wire [dat_wid-1:0] data_b, 
	input  wire               wren_a,
	input  wire               wren_b,
	output wire [dat_wid-1:0] q_a,
	output wire [dat_wid-1:0] q_b,
	input  wire [adr_wid-1:0] address_a,
	input  wire [adr_wid-1:0] address_b,
	input  wire               clock_a,
	input  wire               clock_b
);


RamTwoClockBidirAsym # 
(
	.dat_wid_a (dat_wid), 
	.dat_wid_b (dat_wid), 
	.adr_wid_a (adr_wid),
	.adr_wid_b (adr_wid)
)
ram_font
(
	.data_a    (data_a),
	.data_b    (data_b),  
	.wren_a    (wren_a),
	.wren_b    (wren_b),
	.q_a       (q_a),
	.q_b       (q_b),
	.address_a (address_a),
	.address_b (address_b),
	.clock_a   (clock_a),
	.clock_b   (clock_b)
);

endmodule

//==============================================================

// RamTwoClockBidirAsym - Dual clock, a and b ports are read/write and can be different sizes

module RamTwoClockBidirAsym
#(	parameter dat_wid_a = 10,
	parameter dat_wid_b = 10,
	parameter adr_wid_a = 10,
	parameter adr_wid_b = 10,
	parameter device_family = "Cyclone V"
)
(
	input  wire [dat_wid_a-1:0] data_a,
	input  wire [dat_wid_b-1:0] data_b, 
	input  wire                 wren_a,
	input  wire                 wren_b,
	output wire [dat_wid_a-1:0] q_a,
	output wire [dat_wid_b-1:0] q_b,
	input  wire [adr_wid_a-1:0] address_a,
	input  wire [adr_wid_b-1:0] address_b,
	input  wire                 clock_a,
	input  wire                 clock_b
);



altsyncram # 
(
	.lpm_type                      ("altsyncram"),
	.operation_mode                ("BIDIR_DUAL_PORT"),
	.intended_device_family        (device_family),
	.width_a                       (dat_wid_a), 
	.width_b                       (dat_wid_b), 
	.numwords_a                    (1 << adr_wid_a),
	.numwords_b                    (1 << adr_wid_b),
	.widthad_a                     (adr_wid_a),
	.widthad_b                     (adr_wid_b),
//	.address_reg_a                 ("CLOCK0"),
	.address_reg_b                 ("CLOCK1"),
//	.indata_reg_a                  ("CLOCK0"),
	.indata_reg_b                  ("CLOCK1"),
	.outdata_reg_a                 ("UNREGISTERED"),
	.outdata_reg_b                 ("UNREGISTERED"),
//	.wrcontrol_wraddress_reg_a     ("CLOCK0"),
	.wrcontrol_wraddress_reg_b     ("CLOCK1"),
	.address_aclr_a                ("NONE"),
	.address_aclr_b                ("NONE"),
//	.outdata_aclr_1                ("NONE"),
	.outdata_aclr_b                ("NONE"),
	.width_byteena_a               (1),
	.width_byteena_b               (1),
	.clock_enable_input_a          ("BYPASS"),
	.clock_enable_input_b          ("BYPASS"),
	.clock_enable_output_a         ("BYPASS"),
	.clock_enable_output_b         ("BYPASS"),
	.read_during_write_mode_port_a ("NEW_DATA_NO_NBE_READ"),
	.read_during_write_mode_port_b ("NEW_DATA_NO_NBE_READ"),
	.power_up_uninitialized        ("FALSE")
)
ram
(
	.data_a          (data_a),
	.data_b          (data_b),  
	.wren_a          (wren_a),
	.wren_b          (wren_b),
	.q_a             (q_a),
	.q_b             (q_b),
	.rden_a          (1'b1),
	.rden_b          (1'b1),
	.address_a       (address_a),
	.address_b       (address_b),
	.byteena_a       (1'b1),
	.byteena_b       (1'b1),
	.addressstall_a  (1'b0),
	.addressstall_b  (1'b0),
	.clock0          (clock_a),
	.clock1          (clock_b),
	.aclr0           (1'b0),
	.aclr1           (1'b0),
	.clocken0        (1'b1),
	.clocken1        (1'b1),
	.clocken2        (1'b1),
	.clocken3        (1'b1),
	.eccstatus       ()
);

endmodule

//==============================================================

// RamTwoClockBidirBank - Dual clock, a and b ports are read/write


module RamTwoClockBidirBank
#(	parameter dat_wid   = 10,
	parameter adr_wid   = 10,
	parameter bnk_wid   = 0,
	parameter bnk_max   = 0
)
(
	input  wire [dat_wid-1:0] data_a,
	input  wire [dat_wid-1:0] data_b, 
	input  wire               wren_a,
	input  wire               wren_b,
	output wire [dat_wid-1:0] q_a,
	output wire [dat_wid-1:0] q_b,
	input  wire [adr_wid-1:0] address_a,
	input  wire [adr_wid-1:0] address_b,
	input  wire               clock_a,
	input  wire               clock_b
);



RamTwoClockBidirBankAsym # 
(
	.dat_wid_a (dat_wid), 
	.dat_wid_b (dat_wid), 
	.adr_wid_a (adr_wid),
	.adr_wid_b (adr_wid),
	.bnk_wid   (bnk_wid),
	.bnk_max   (bnk_max)
)
ram
(
	.data_a    (data_a),
	.data_b    (data_b),  
	.wren_a    (wren_a),
	.wren_b    (wren_b),
	.q_a       (q_a),
	.q_b       (q_b),
	.address_a (address_a),
	.address_b (address_b),
	.clock_a   (clock_a),
	.clock_b   (clock_b)
);

endmodule

//==============================================================

// RamTwoClockBidirBankAsym - Dual clock, a and b ports are read/write and can be different sizes

module RamTwoClockBidirBankAsym
#(	parameter dat_wid_a = 10,
	parameter dat_wid_b = 10,
	parameter adr_wid_a = 10,
	parameter adr_wid_b = 10,
	parameter bnk_wid   = 0,
	parameter bnk_max   = 0,
	parameter adr_wid_ram_a = adr_wid_a - bnk_wid,
	parameter adr_wid_ram_b = adr_wid_b - bnk_wid,
	parameter device_family = "Cyclone V"
)
(
	input  wire [dat_wid_a-1:0] data_a,
	input  wire [dat_wid_b-1:0] data_b, 
	input  wire                 wren_a,
	input  wire                 wren_b,
	output wire [dat_wid_a-1:0] q_a,
	output wire [dat_wid_b-1:0] q_b,
	input  wire [adr_wid_a-1:0] address_a,
	input  wire [adr_wid_b-1:0] address_b,
	input  wire                 clock_a,
	input  wire                 clock_b
);



reg [bnk_wid-1:0] bank_a = 0;
reg [bnk_wid-1:0] bank_b = 0;

wire [bnk_max:0][dat_wid_a-1:0] q_a_ram;
wire [bnk_max:0][dat_wid_b-1:0] q_b_ram;

assign q_a = q_a_ram[bank_a];
assign q_b = q_b_ram[bank_b];

always@(posedge clock_a) begin	
	bank_a <= address_a[adr_wid_a-1 -: bnk_wid];
end

always@(posedge clock_b) begin	
	bank_b <= address_b[adr_wid_b-1 -: bnk_wid];
end

genvar b;
generate
	for (b = 0; b <= bnk_max; b++) begin: instBank
		altsyncram # 
		(
			.lpm_type                      ("altsyncram"),
			.operation_mode                ("BIDIR_DUAL_PORT"),
			.intended_device_family        (device_family),
			.width_a                       (dat_wid_a), 
			.width_b                       (dat_wid_b), 
			.numwords_a                    (1 << adr_wid_ram_a),
			.numwords_b                    (1 << adr_wid_ram_b),
			.widthad_a                     (adr_wid_ram_a),
			.widthad_b                     (adr_wid_ram_b),
			.address_reg_a                 ("CLOCK0"),
			.address_reg_b                 ("CLOCK1"),
			.indata_reg_a                  ("CLOCK0"),
			.indata_reg_b                  ("CLOCK1"),
			.outdata_reg_a                 ("UNREGISTERED"),
			.outdata_reg_b                 ("UNREGISTERED"),
			.wrcontrol_wraddress_reg_a     ("CLOCK0"),
			.wrcontrol_wraddress_reg_b     ("CLOCK1"),
			.address_aclr_a                ("NONE"),
			.address_aclr_b                ("NONE"),
			.outdata_aclr_1                ("NONE"),
			.outdata_aclr_b                ("NONE"),
			.width_byteena_a               (1),
			.width_byteena_b               (1),
			.clock_enable_input_a          ("BYPASS"),
			.clock_enable_input_b          ("BYPASS"),
			.clock_enable_output_a         ("BYPASS"),
			.clock_enable_output_b         ("BYPASS"),
			.read_during_write_mode_port_a ("NEW_DATA_NO_NBE_READ"),
			.read_during_write_mode_port_b ("NEW_DATA_NO_NBE_READ"),
			.power_up_uninitialized        ("FALSE")
		)
		ram
		(
			.data_a          (data_a),
			.data_b          (data_b),  
			.wren_a          (wren_a & (b == bank_a)),
			.wren_b          (wren_b & (b == bank_b)),
			.q_a             (q_a_ram[b]),
			.q_b             (q_b_ram[b]),
			.rden_a          (1'b1),
			.rden_b          (1'b1),
			.address_a       (address_a[adr_wid_ram_a-1:0]),
			.address_b       (address_b[adr_wid_ram_b-1:0]),
			.byteena_a       (1'b1),
			.byteena_b       (1'b1),
			.addressstall_a  (1'b0),
			.addressstall_b  (1'b0),
			.clock0          (clock_a),
			.clock1          (clock_b),
			.aclr0           (1'b0),
			.aclr1           (1'b0),
			.clocken0        (1'b1),
			.clocken1        (1'b1),
			.clocken2        (1'b1),
			.clocken3        (1'b1),
			.eccstatus       ()
		);
	end
endgenerate

endmodule

//==============================================================

module RomOneClock
#( 	parameter dat_wid = 10,
	parameter adr_wid = 10,
	parameter mif_file = "../source/sensorColOffsets.mif"
)
(
	input wire  clock,
	input wire 	[adr_wid-1:0] address,
	output wire [dat_wid-1:0] q_rom
);



altsyncram #
(
	.address_aclr_a          ("NONE"),
	.clock_enable_input_a    ("BYPASS"),
	.clock_enable_output_a   ("BYPASS"),
	.init_file               (mif_file),
	.intended_device_family  ("Arria V"),
	.lpm_hint                ("ENABLE_RUNTIME_MOD=NO"),
	.lpm_type                ("altsyncram"),
	.numwords_a              (1 << adr_wid),
	.operation_mode          ("ROM"),
	.outdata_aclr_a          ("NONE"),
	.outdata_reg_a           ("UNREGISTERED"),
	.widthad_a               (adr_wid),
	.width_a                 (dat_wid),
	.width_byteena_a         (1)
)	
rom 
(
	.address_a (address),
	.clock0 (clock),
	.q_a (q_rom),
	.aclr0 (1'b0),
	.aclr1 (1'b0),
	.address_b (1'b1),
	.addressstall_a (1'b0),
	.addressstall_b (1'b0),
	.byteena_a (1'b1),
	.byteena_b (1'b1),
	.clock1 (1'b1),
	.clocken0 (1'b1),
	.clocken1 (1'b1),
	.clocken2 (1'b1),
	.clocken3 (1'b1),
	.data_a ({dat_wid{1'b1}}),
	.data_b (1'b1),
	.eccstatus (),
	.q_b (),
	.rden_a (1'b1),
	.rden_b (1'b1),
	.wren_a (1'b0),
	.wren_b (1'b0)
);

endmodule

//==============================================================

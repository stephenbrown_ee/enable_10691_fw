//==============================================================
// Copyright 2013, D3 Engineering
// www.d3engineering.com
//==============================================================


module Histogram
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	
	input  wire                  inpClock,
	input  wire [11:0]           inpData,
	input  wire [2:0]            inpSync,
	input  wire                  inpStart,
	output reg                   outStart
);

`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));
wire selRam = (busChipSelect && (busAddress[19:16] == 1'h1));

reg [15:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[5:2]];
	else if (selRam) 	
		busRdData = {12'd0, q_a_ram[busAddress[10]]};
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (selReg && busWrite)
		register[busAddress[5:2]] <= busWrData;
	else begin
		register[0][6]     <= run;
		register[0][31:24] <= frame;
		register[1][23:0]  <= count[0];
		register[2][23:0]  <= count[1];
		register[3][23:0]  <= count[2];
		register[4][23:0]  <= count[3];
		register[5][31:0]  <= sum0;
		register[6][31:0]  <= sum1;
		register[7][31:0]  <= sum2;
		register[8][31:0]  <= sum3;
		if (run)
			register[0][2]  <= 0; // Clear enable at start
	end
end

//---------------------------------------------------------------

wire        resetReg    = register[0][0];
wire        clear       = register[0][1];
wire        enable      = register[0][2];
wire        start       = register[0][3];
wire [1:0]  startColor  = register[0][5:4];
wire [7:0]  enableFrame = register[0][23:16];

reg              reset     = 0;
reg              run       = 0;
reg              stop      = 0;
reg  [1:0]       color     = 0; // CFA color YX
reg  [3:0][23:0] count     = 0; // Total pixel count in each histogram
reg  [3:0][35:0] sum       = 0; // Total of pixel values for each color
wire [31:0] sum0 = sum[0][35:4];
wire [31:0] sum1 = sum[0][35:4];
wire [31:0] sum2 = sum[0][35:4];
wire [31:0] sum3 = sum[0][35:4];
reg  [7:0]       frame     = 0;

wire [8:0]       address_a_ram = {busAddress[11], busAddress[9:2]};
wire [19:0]      data_a_ram    = busWrData[19:0];
wire [1:0][19:0] q_a_ram;

always@(posedge inpClock) begin
	if (reset) begin
		run           <= 0;
		stop          <= 0;
		count         <= 0;
		sum           <= 0;
		frame         <= 0;
	end
	else begin
	
		if (inpStart)
			frame <= 0;
		else if (inpSync == SYNC_FRM_END)
			frame <= frame + 1'd1;
			
		outStart <= ((inpSync == SYNC_FRM_END) && start);
			
		if (!run) begin
			if (inpSync == SYNC_FRM_BEG) begin
				if (enable && (frame == enableFrame))
					run   <= 1'd1;
			end
			if (clear) begin
				count <= 0;
				sum   <= 0;
			end
			color <= startColor;
			stop  <= 0;
		end
		else begin
			if (inpSync == SYNC_PIX_OUT) begin
				count[color] <= count[color] + 1'd1;
				sum[color]   <= sum[color]   + inpData;
			end
			
			if (inpSync == SYNC_FRM_BEG)
				color    <= startColor;
			else if (inpSync == SYNC_LIN_BEG) begin
				color[0] <= startColor[0];
				color[1] <= ~color[1];
			end
			else
				color[0] <= ~color[0];
				
			if (stop)
				run  <= 0;
				
			if (inpSync == SYNC_FRM_END)
				stop <= 1'd1;
		end
	end
	reset <= resetReg;
end

genvar n;
generate
	for (n = 0; n <= 1; n++) begin: inst
	
		wire        wren_a_ram = selRam && busWrite && (busAddress[10] == n);
		
		reg         read;
		reg         wren_b_ram;
		reg  [8:0]  address_b_ram;
		wire [1:0][19:0] q_b_ram; // S. Brown edit
		wire [1:0][19:0] data_b_ram; // S. Brown edit to resolve heirarchical error in Synplify
		assign data_b_ram[n] = run ? (q_b_ram[n] + ((q_b_ram[n] < 20'hFFFFF) ? 1'd1 : 1'd0)) : 20'd0;  //S. Brown Edit for Microsemi
		
		always@(posedge inpClock) begin
			if (reset) begin
				read          <= 0;
				wren_b_ram    <= 0;
				address_b_ram <= 0;
			end
			else begin
				if (!run) begin
					read <= 0;
					if (clear) begin
						address_b_ram <= address_b_ram + 1'd1;
						wren_b_ram    <= 1'd1;
					end
					else begin
						address_b_ram <= 0;
						wren_b_ram    <= 0;
					end
				end
				else begin
					if ((inpSync == SYNC_PIX_OUT) && (color[0] == n)) begin
					// Read cycle
						read          <= 1'd1;
						address_b_ram <= {color[1], inpData[11:4]};
					end
					// Write cycle
					else if (read)
						read <= 0;
					wren_b_ram <= read;
				end
			end
		end

		RamTwoClockBidir # 
		(
			.dat_wid (20), 
			.adr_wid (9)
		)
		ram
		(
			.data_a    (data_a_ram),
			.data_b    (data_b_ram[n]),  
			.wren_a    (wren_a_ram),
			.wren_b    (wren_b_ram),
			.q_a       (q_a_ram[n]),
			.q_b       (q_b_ram[n]),
			.address_a (address_a_ram),
			.address_b (address_b_ram),
			.clock_a   (busClock),
			.clock_b   (inpClock)
		);
	end
endgenerate

endmodule

//==============================================================
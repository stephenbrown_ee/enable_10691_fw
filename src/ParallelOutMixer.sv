//==============================================================

module ParallelOutMixer
(
	input  wire              busClock,
	input  wire              busReset,
	input  wire              busChipSelect,
	input  wire              busWrite,
	input  wire              busRead,
	input  wire [19:0]       busAddress,
	input  wire [31:0]       busWrData,
	output reg  [31:0]       busRdData,
	
	input  wire              inpClock,
	input  wire [23:0]       inpFifoData,
	input  wire [2:0]        inpFifoSync,
	
	input  wire [3:0][31:0]  ovlyFifoData,
	input  wire              ovlyDone,
	input  wire              ignoreAlpha,
	
	output wire              inpDV_ovly,
	
	input  wire              outClock,
	output wire [9:0]        wrusedw_fifo_inp,
	output reg               outVs,
	output reg               outHs,
	output reg               outDe,
	output reg  [2:0][7:0]   outData             
);

`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));

reg [15:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[6:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busReset)
		register <= 0;
	else if (selReg && busWrite)
		register[busAddress[6:2]] <= busWrData;
end


//---------------------------------------------------------------


wire        reset      = register[0][0] || busReset;
wire [1:0]  source     = register[0][5:4];   // 0 = inpData, 1 = Logo, 2 = Fixed data
wire [3:0]  horScale   = register[0][11:8];
wire [3:0]  verScale   = register[0][15:12];
wire        manOvlyEn  = register[0][21];
wire [7:0]  modCount   = register[0][29:22];
wire [11:0] horMax     = register[1][11:0];  // Last hor count in line
wire [11:0] verMax     = register[1][27:16]; // Last ver count in frame
wire [11:0] verCR0     = register[2][11:0];  // First line of crop
wire [11:0] verCR1     = register[2][27:16]; // First line of active image
wire [11:0] verVS0     = register[3][11:0];  // First line of no VS
wire [11:0] verVS1     = register[3][27:16]; // First line of active VS
wire [11:0] verDE0     = register[4][11:0];  // First line of no DE 
wire [11:0] verDE1     = register[4][27:16]; // First line of active DE 
wire [11:0] verPX0     = register[5][11:0];  // First line of no RAM reads
wire [11:0] verPX1     = register[5][27:16]; // First line of RAM reads 
wire [11:0] horVS0     = register[6][11:0];  // Hor pos of VS fall
wire [11:0] horVS1     = register[6][27:16]; // Hor pos of VS rise
wire [11:0] horHS0     = register[7][11:0];  // Hor pos of HS fall 
wire [11:0] horHS1     = register[7][27:16]; // Hor pos of HS rise
wire [11:0] horDE0     = register[8][11:0];  // Hor pos of DE fall
wire [11:0] horDE1     = register[8][27:16]; // Hor pos of DE rise
wire [11:0] horPX0     = register[9][11:0];  // Hor pos of end of RAM reads
wire [11:0] horPX1     = register[9][27:16]; // Hor pos of start of RAM reads
wire [15:0] upperLeftX = register[10][15:0];
wire [15:0] upperLeftY = register[10][31:16];
wire [15:0] logoWidth  = register[11][15:0];
wire [15:0] logoHeight = register[11][31:16];
wire [15:0] msgX       = register[12][15:0];
wire [15:0] msgY       = register[12][31:16];
wire [15:0] msgWidth   = register[13][15:0];
wire [15:0] msgHeight  = register[13][31:16];
wire [11:0] horCR0     = register[14][11:0];  // Hor pos of end of RAM reads
wire [11:0] horCR1     = register[14][27:16]; // Hor pos of start of RAM reads
wire [11:0] barX       = register[15][11:0];  // bar X
wire [11:0] barY       = register[15][27:16]; // bar Y


reg  [11:0]      horCount  = 0;
reg  [11:0]      verCount  = 0;
reg  [2:0][7:0]  pattData  = 0;
reg              outVsDly  = 0;
reg  [2:0][7:0]  intData   = 0;
reg  [1:0]       ovlyCount = 0;
reg  [3:0][7:0]  ovlyData  = 0;  // [3] = Alpha, [2] = B, [1] = G, [0] = R
reg              ovlyDone_reg = 0;

logic [7:0]       alpha;
logic [7:0]       alphaRem;
logic [2:0][15:0] data_result;
logic [2:0][15:0] ovly_result;
logic [2:0][15:0] result;

//---------------------------------------------------------------
//  ovlyFifoData -> ovlyData -> ovly_result -> result -> outData
//  outDe0       -> outDe1   -> outDe2      -> outDe3 -> outDe4
//---------------------------------------------------------------

reg              vs       = 0;
reg              hs       = 0;
reg              de       = 0;
reg              px       = 0;
reg              vsVer    = 0;
reg              deVer    = 0;
reg              pxVer    = 0;
reg              inpDe    = 0;
reg  [15:0]      frameCount = 0;
logic  [4:0]     outHs_int;
logic  [4:0]     outDe_int;
logic            newSource;
logic  [8:0]     fillCount;
reg              crop     = 0;
reg              crVer    = 0;
reg              inpCrop  = 0;

wire  inpDV;
//---------------------------------------------------------------

reg        logoEn;
reg        logoEn_dly;
reg        logoEn_dly2;
reg [12:0] logoAddress;

reg        msgEn;
reg        msgEn_dly;
reg        msgEn_dly2;
reg [12:0] msgAddress;

parameter boxLength = 64;

wire boxArea = (horCount >= barX) && (horCount < (barX + 2*frameCount)) && (horCount > (barX + 2*frameCount - boxLength)) && (horCount < (barX + logoWidth)) && (verCount >= barY) && (verCount < barY + 8);

wire logoArea = (horCount >= upperLeftX) && (horCount < (upperLeftX + logoWidth)) && (verCount >= upperLeftY) && (verCount < (upperLeftY + logoHeight));

wire msgArea  = (horCount >= msgX) && (horCount < (msgX + msgWidth)) && (verCount >= msgY) && (verCount < (msgY + msgHeight));

wire fillArea = (horCount >= barX) && (horCount < (barX + fillCount)) && (verCount >= barY) && (verCount < barY + 8);
wire grayArea = (horCount >= barX) && (horCount < (barX + logoWidth)) && (verCount >= barY) && (verCount < barY + 8); 

//---------------------------------------------------------------

wire             wrclk_fifo_inp    = inpClock;
wire [23:0]      data_fifo_inp     = inpFifoData;
wire             wrreq_fifo_inp    = (inpFifoSync == SYNC_PIX_OUT);
wire [23:0]      q_fifo_inp;

wire [23:0]      inpData = q_fifo_inp;

wire             rdclk_fifo_inp    = outClock;
wire             rdreq_fifo_inp    = inpDV;
wire             aclr_fifo_inp     = reset || (outVs & !outVsDly);

dcfifo #
(
	.lpm_type                ("dcfifo"),
	.lpm_numwords            (1024),
	.lpm_width               (24),
	.lpm_widthu              (10),
	.lpm_showahead           ("OFF"),
	.wrsync_delaypipe        (4),
	.rdsync_delaypipe        (4),
	.overflow_checking       ("ON"),
	.underflow_checking      ("ON"),
	.add_ram_output_register ("ON"),
	.use_eab                 ("ON"),
	.intended_device_family  ("Cyclone V")
) 
fifo_inp_parOut
(
	.wrclk     (wrclk_fifo_inp),
	.data      (data_fifo_inp),
	.wrreq     (wrreq_fifo_inp),
	.wrusedw   (wrusedw_fifo_inp),
	.rdclk     (rdclk_fifo_inp),
	.q         (q_fifo_inp),
	.rdreq     (rdreq_fifo_inp),
	.aclr      (aclr_fifo_inp)
);

//---------------------------------------------------------------

assign inpDV      = px ;
assign inpDV_ovly = de && (!outDe_int[0] || (ovlyCount == 2'd3));

//---------------------------------------------------------------

always@(posedge outClock) begin


	case (horCount[horScale +: 3])  
		0: pattData <= 24'h000000; // Color bars
		1: pattData <= 24'h0000FF;   
		2: pattData <= 24'h00FF00;         
		3: pattData <= 24'hFF0000;                      
		4: pattData <= 24'hFFFF00; 
		5: pattData <= 24'hFF00FF;               
		6: pattData <= 24'h00FFFF;   
		7: pattData <= 24'hFFFFFF;   
	endcase
	
	if (reset) begin
		horCount     <= 0;
		verCount     <= 0;
		vs           <= 0;
		hs           <= 0;
		de           <= 0;
		px           <= 0;
		vsVer        <= 0;
		deVer        <= 0;
		pxVer        <= 0;
		inpDe        <= 0;
		outVs        <= 0;
		outHs        <= 0;
		outDe        <= 0;
		outData      <= 0;
		outDe_int    <= 0;
		ovlyDone_reg <= 0;
		ovlyData     <= 0;
		ovlyCount    <= 0;
	end
	else begin
		outVsDly  <= outVs;
		outVs     <= vs;
		outHs     <= outHs_int[4];
		outDe     <= outDe_int[4];
		inpDe     <= px;
		inpCrop   <= crop;
		outHs_int <= {outHs_int[3:0], hs};
		outDe_int <= {outDe_int[3:0], de};
		// Swap the red and blue of the overlay
		result[2] <= data_result[2] + ovly_result[0];
		result[1] <= data_result[1] + ovly_result[1];
		result[0] <= data_result[0] + ovly_result[2];
		
		outData[2] <= result[2][15:8];
		outData[1] <= result[1][15:8];
		outData[0] <= result[0][15:8];

		
		if (outDe_int[0]) begin
		   if (ovlyCount < 3)
			   ovlyCount <= ovlyCount + 2'd1;
			else
				ovlyCount <= 0;
		end
		else
			ovlyCount <= 0;
		
		logoEn      <= logoArea;
		logoEn_dly  <= logoEn;
		logoEn_dly2 <= logoEn_dly;
		
		msgEn      <= msgArea;
		msgEn_dly  <= msgEn;
		msgEn_dly2 <= msgEn_dly;
						
		if (outDe_int[0] && (ovlyDone_reg || manOvlyEn)) begin
			ovlyData <= ovlyFifoData[ovlyCount];
		end
		else begin
			ovlyData <= 0;
		end
		
		case (source)
			0: begin  // This case is for normal operation
				if (inpDe && inpCrop) begin
					intData <= inpData;
				end
				else begin
					intData <= 24'd0;
				end
				   
			end
			1: begin // This case displays the logo and a moving bar below it
				
				if (boxArea)
					intData <= { 8'h00, 8'hFF, 8'h00 };  // Green
				else if (logoEn || logoEn_dly || logoEn_dly2) begin
					if(logoEn)
						logoAddress <= logoAddress + 1'd1;
					if (logoEn_dly2)
						intData <= {rom_b_q, rom_gr_q, rom_q};
					else
						intData <= 0;
				end
				else begin
					intData <= 0;
					if (verCount == 0)
						logoAddress <= 0;
				end
			end
			2: begin	 // This case displays a 37 second filling green bar			
				if (grayArea) begin
				   if (fillArea) begin
					   intData <= { 8'h00, 8'hFF, 8'h00 };  // Green
					end
					else begin
					   intData <= { 8'h20, 8'h20, 8'h20 };  // dark gray
					end
				end
				else if (logoEn || logoEn_dly || logoEn_dly2) begin
					if(logoEn)
						logoAddress <= logoAddress + 1'd1;
					if (logoEn_dly2)
						intData <= {rom_b_q, rom_gr_q, rom_q};
					else
						intData <= 0;
				end
				else begin
					intData <= 0;
					if (verCount == 0)
						logoAddress <= 0;
				end
			end
			default: intData <= 24'd0;
		endcase
	
		if (horCount == horHS0)
			hs <= 0;
		else if (horCount == horHS1)
			hs <= 1'd1;
	
		if ((horCount == horVS0) && !vsVer)
			vs <= 0;
		else if ((horCount == horVS1) && vsVer)
			vs <= 1'd1;
	
		if (horCount == horPX0) begin
			px <= 0;
		end
		else if ((horCount == horPX1) && pxVer) begin
			px <= 1'd1;
		end
	
		if (horCount == horDE0)
			de <= 0;
		else if ((horCount == horDE1) && deVer)
			de <= 1'd1;
	
		if (horCount == horCR0)
			crop <= 0;
		else if ((horCount == horCR1) && crVer)
			crop <= 1'd1;
					
		if (verCount == verCR0)
			crVer <= 0;
		else if (verCount == verCR1)
			crVer <= 1'd1;
			
		if (verCount == verVS0)
			vsVer <= 0;
		else if (verCount == verVS1)
			vsVer <= 1'd1;
	
		if (verCount == verDE0)
			deVer <= 0;
		else if (verCount == verDE1)
			deVer <= 1'd1;
			
		if (verCount == verPX0)
			pxVer <= 0;
		else if (verCount == verPX1)
			pxVer <= 1'd1;
		
		if (horCount < horMax)
			horCount <= horCount + 1'd1;
		else 
			horCount   <= 0;
			
		if (horCount >= horMax) begin
			if (verCount < verMax)
				verCount   <= verCount + 1'd1;
			else begin
				verCount     <= 0;
				ovlyDone_reg <= ovlyDone;
				if (source == 1) begin
				   newSource <= 1'b1;
					if (frameCount < ((logoWidth + boxLength) >> 1))
						frameCount <= frameCount + 1'd1;
					else
						frameCount <= 0;
				end
				else if (source == 2) begin
				   if (newSource) begin
					   newSource  <= 0;
						fillCount  <= 0;
					end
					else begin
					   if (frameCount < modCount) begin
					      frameCount <= frameCount + 1'd1;
						end
						else begin
						   frameCount <= 0;
							fillCount  <= fillCount + 9'd2;
						end
					end
				end
				else begin
					frameCount <= 0;
					newSource  <= 1'b1;
				end
			end
		end
		
	end
end


//----------------------------------------------------------------------

assign alpha    = ignoreAlpha ? 8'hFF : ovlyData[3];
assign alphaRem = ignoreAlpha ? 8'h0  : 8'd255 - ovlyData[3];


genvar n;
generate 
	for (n = 0; n <= 2; n++)  begin: image_mult_alpha
		
		MultiplyUnsigned #
		(
			.dat_wid_a (8),
			.dat_wid_b (8),
			.res_wid   (16),
			.pipeline  (1)
		)
		x1y1_x1_mult
		(
			.clock  (outClock),
			.data_a (alphaRem),
			.data_b (intData[n]),
			.result (data_result[n])
		);
	end
endgenerate


genvar m;
generate 
	for (m = 0; m <= 2; m++)  begin: ovly_mult_alpha
		
		MultiplyUnsigned #
		(
			.dat_wid_a (8),
			.dat_wid_b (8),
			.res_wid   (16),
			.pipeline  (1)
		)
		x1y1_x1_mult
		(
			.clock  (outClock),
			.data_a (alpha),
			.data_b (ovlyData[m]),
			.result (ovly_result[m])
		);
	end
endgenerate

//------------------------------------------------------------------------		





wire          rom_rden = logoEn;
wire [12:0]   rom_address = logoAddress;
wire [7:0]    rom_q;

logoROM inst_logoROM
(
	.clock     (outClock),
	.rden      (rom_rden),
	.address   (rom_address),
	.q         (rom_q)
);

wire          rom_gr_rden = logoEn;
wire [12:0]   rom_gr_address = logoAddress;
wire [7:0]    rom_gr_q;

logoROM_Gr inst_logoROM_Gr
(
	.clock     (outClock),
	.rden      (rom_gr_rden),
	.address   (rom_gr_address),
	.q         (rom_gr_q)
);		

wire          rom_b_rden = logoEn;
wire [12:0]   rom_b_address = logoAddress;
wire [7:0]    rom_b_q;

logoROM_b inst_logoROM_B
(
	.clock     (outClock),
	.rden      (rom_b_rden),
	.address   (rom_b_address),
	.q         (rom_b_q)
);			
			

endmodule

//==============================================================

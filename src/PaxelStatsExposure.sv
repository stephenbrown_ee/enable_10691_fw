//==============================================================
// Copyright 2013, D3 Engineering
// www.d3engineering.com
//==============================================================

module PaxelStatsExposure
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	
	input  wire                  inpClock,
	input  wire [2:0]            inpSync,
	input  wire [9:0]            inpData
);

`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

wire selReg = (busChipSelect && (busAddress[19:16] == 0));

reg [11:0][31:0] register = 0;

always begin
	if (selReg)
		busRdData = register[busAddress[5:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (selReg && busWrite)
		register[busAddress[5:2]] <= busWrData;
	else begin
		register[0][31:24] <= frame;
		register[5]        <= R_frame;
		register[6]        <= G_frame;
		register[7]        <= B_frame;
		register[8]        <= C_frame;
		register[9]        <= R_cnt_reg;
		register[10]       <= G_cnt_reg;
		register[11]       <= B_cnt_reg;
	end
end

//---------------------------------------------------------------

wire        resetReg    = register[0][0];
wire        clear       = register[0][1];
wire        enable      = register[0][2];
wire        start       = register[0][3];
wire [11:0] colHorMax   = register[1][11:0];  // Width of col - 1
wire [11:0] rowVerMax   = register[1][27:16]; // Height of row - 1
wire [11:0] clipThresh  = register[2][11:0];
wire [11:0] horMin      = register[3][11:0];  
wire [11:0] horMax      = register[3][27:16]; 
wire [11:0] verMin      = register[4][11:0]; 
wire [11:0] verMax      = register[4][27:16]; 

reg         reset       = 0;
reg  [7:0]  frame       = 0;

logic [1:0]        selColor  = 0;
logic [11:0]       horCount  = 0;
logic [11:0]       verCount  = 0;

logic [31:0] R_total = 0;
logic [31:0] G_total = 0;
logic [31:0] B_total = 0;
logic [31:0] C_total = 0;
logic [31:0] R_frame = 0;
logic [31:0] G_frame = 0;
logic [31:0] B_frame = 0;
logic [31:0] C_frame = 0;
logic [31:0] R_cnt = 0;
logic [31:0] G_cnt = 0;
logic [31:0] B_cnt = 0;
logic [31:0] R_cnt_reg = 0;
logic [31:0] G_cnt_reg = 0;
logic [31:0] B_cnt_reg = 0;

always@(posedge inpClock) begin

	if (reset) begin
		frame         <= 0;
		R_total       <= 0;
		G_total       <= 0;
		B_total       <= 0;
		C_total       <= 0;
		selColor      <= 0;
	end
	else begin

		
		if (inpSync == SYNC_FRM_END) begin
			frame <= frame + 8'd1;
			selColor      <= 0;
			R_total       <= 0;
			G_total       <= 0;
			B_total       <= 0;
			C_total       <= 0;
			R_cnt         <= 0;
			G_cnt         <= 0;
			B_cnt         <= 0;
			horCount      <= 0;
			verCount      <= 0;
			R_frame       <= R_total;
			G_frame       <= G_total;
			B_frame       <= B_total;
			C_frame       <= C_total;
			R_cnt_reg     <= R_cnt;
			G_cnt_reg     <= G_cnt;
			B_cnt_reg     <= B_cnt;
		end
		else begin
			if (inpSync == SYNC_LIN_BEG) begin
				selColor[1]   <= ~selColor[1];
				selColor[0]   <= 0;
				horCount      <= 0;
				verCount      <= verCount + 12'd1;
			end
			else if (inpSync == SYNC_PIX_OUT) begin
				horCount <= horCount + 12'd1;
				selColor[0] <= ~selColor[0];
				
				// The horMin/Max and verMin/Max definitions are for windowing the AE algorithm
				if((horCount >= horMin) && (horCount <= horMax) && (verCount >= verMin) && (verCount <= verMax)) begin
								
					case (selColor) 
						2'd0: begin
							B_total <= B_total + inpData;
							B_cnt   <= B_cnt + 32'd1;
						end
						2'd1: begin
							G_total <= G_total + inpData;
							G_cnt   <= G_cnt +32'd1;
						end
						2'd2: begin
							R_total <= R_total + inpData;
							R_cnt   <= R_cnt +32'd1;
						end
						2'd3: begin 
							G_total <= G_total + inpData;
							G_cnt   <= G_cnt +32'd1;
						end
					endcase
						
					if (inpData > clipThresh) begin
						C_total <= C_total + 1'd1;
					end
					
				end
			end

		end
	end
	reset <= resetReg;
end


endmodule

//==============================================================
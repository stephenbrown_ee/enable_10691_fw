//==============================================================

// Registers
//  0 Control
//  1 RO (R out offset)
//  2 GO (G out offset)
//  3 BO (B out offset)
//  4 RR (R out from R in coef)
//  5 RG (R out from G in coef)
//  6 RB (R out from B in coef)
//  7 GR (G out from R in coef)
//  8 GG (G out from G in coef)
//  9 GB (G out from B in coef)
// 10 BR (B out from R in coef)
// 11 BG (B out from G in coef)
// 12 BB (B out from B in coef)

// Offsets -2048 to +2047
// Coefs   -2048 to +2047 = -8.000 to +7.996 (256 = X1)

module ColorMatrix
(
	input  wire                  busClock,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,

	input  wire                  inpClock,
	input  wire [2:0][11:0]      inpData,
	input  wire [2:0]            inpSync,
	output reg  [2:0][11:0]      outData,
	output reg  [2:0]            outSync
);


`include "PixelPipe.h"

//--- Bus Interface ----------------------------------------------

reg [15:0][31:0] register = 0;

assign busRdData = register[busAddress[5:2]];

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[5:2]] <= busWrData;
end

//---------------------------------------------------------------

wire             resetReg  = register[0][0];
wire             bypass    = register[0][1];
wire             clipSafe  = register[0][2];
wire [11:0]      clipLevel = register[1][11:0];
wire [11:0]      clipOut0  = register[1][27:16];
wire [11:0]      clipOut1  = register[2][11:0];
wire [11:0]      clipOut2  = register[2][27:16];

reg              reset = 0;
reg  [2:0]       sync  = 0;
reg  [2:0][11:0] data  = 0;
wire [2:0][11:0] result;

always@(posedge inpClock) begin
	if (reset) begin
		sync      <= SYNC_IDLE;
		outSync   <= SYNC_IDLE;
		outData   <= 0;
	end
	else if (bypass) begin
		outSync   <= inpSync;
		outData   <= inpData;
	end
	else begin
		outSync   <= sync;
		if (sync != SYNC_PIX_OUT)
			outData   <= 36'd0;
		else if (clipSafe) begin
			if (data[2] >= clipOut2)
				outData[2] <= clipOut2;
			else
				outData[2] <= result[2];
				
			if (data[1] >= clipOut1)
				outData[1] <= clipOut1;
			else
				outData[1] <= result[1];
	
			if (data[0] >= clipOut0)
				outData[0] <= clipOut0;
			else
				outData[0] <= result[0];	
		end
		else
			outData <= result;
		sync      <= inpSync;
		data      <= inpData;
	end
	reset <= resetReg;
end

genvar i,o;
generate
	for (o = 0; o <= 2; o++) begin: instOut
	
		wire signed [16:0] offset = $signed({register[12+o][12:0], 4'd0}); // x16
		wire signed [24:0] result_mult [3];
		
		for (i = 0; i <= 2; i++) begin: instIn
		
			wire signed [12:0] data_a_mult = $signed({1'd0,inpData[i]});         // Pixel is always positive
			wire signed [11:0] data_b_mult = $signed(register[3+(o*3)+i][11:0]); // Coef is signed
			
			MultiplySigned	#
			(
				.dat_wid_a (13),
				.dat_wid_b (12),
				.res_wid   (25)
			)
			mult
			(
				.clock  (inpClock),
				.data_a (data_a_mult),
				.data_b (data_b_mult),
				.result (result_mult[i])
			);
			
		end

		// Divide products by 16 and add offset
		wire signed [24:0] sum = {{8{offset[16]}}, offset} + {{4{result_mult[0][24]}}, result_mult[0][24:4]} + {{4{result_mult[1][24]}}, result_mult[1][24:4]} + {{4{result_mult[2][24]}}, result_mult[2][24:4]};
		
		// Divide sum by 16 and clip to 0/4095 or -2048/+2047
		assign result[o] = (sum < 0) ? 12'd0 : ((sum[24:4] > 4095) ? 12'd4095 : $unsigned(sum[15:4]));		
		
	end
endgenerate

endmodule

//==============================================================
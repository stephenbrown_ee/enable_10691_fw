module SensorCtrlData
(
	input  wire         busClock,
	input  wire         busChipSelect,
	input  wire         busWrite,
	input  wire         busRead,
	input  wire [19:0]  busAddress,
	input  wire [31:0]  busWrData,
	output reg  [31:0]  busRdData,
	
	input  wire         clock,
	input  wire         verBlank,
	input  wire         frameStart,
	output reg          imagerCtrl,
	output reg          vid_en
	
);

//--- Bus Interface ----------------------------------------------

reg [4:0][31:0] register = 0;

always begin
	if (busChipSelect)
		busRdData = register[busAddress[4:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busChipSelect && busWrite)
		register[busAddress[4:2]] <= busWrData;
end

//---------------------------------------------------------------
wire       reset   = register[0][0];
wire [7:0] addr    = register[1][7:0];
wire [7:0] spi0reg = register[1][15:8];
wire [7:0] spi1reg = register[1][23:16];
wire [7:0] spi2reg = register[1][31:24];
wire [7:0] spi3reg = register[2][7:0];
wire [7:0] spi4reg = register[2][15:8];
wire [7:0] spi5reg = register[2][23:16];
wire [7:0] spi6reg = register[2][31:24];
wire [7:0] spi7reg = register[3][7:0];
wire [3:0] WAIT_CT = register[3][11:8];

logic [71:0] spiWrites = 0; //{addr, spi0reg, spi1reg, spi2reg, spi3reg, spi4reg, spi5reg, spi6reg, spi7reg};

logic [6:0]  spiCount = 0;
logic [1:0]  baudCount = 0;
logic        spiDone  = 0;
logic [3:0]  waitCnt = 0;

always_ff @(posedge clock) begin

	if (reset) begin
		baudCount  <= 0;
		spiCount   <= 0;
		vid_en     <= 1'b1;
		imagerCtrl <= 0;
		waitCnt    <= 0;
		spiWrites  <= 0;
	end
	else if (!verBlank || spiDone) begin
		baudCount  <= 0;
		waitCnt    <= WAIT_CT;
		spiCount   <= 0;
		vid_en     <= 1'b1;
		imagerCtrl <= 0;
		spiWrites  <= {addr, spi0reg, spi1reg, spi2reg, spi3reg, spi4reg, spi5reg, spi6reg, spi7reg};
		if(frameStart) begin
			spiDone <= 0;
		end
	end
	else if (verBlank && (waitCnt > 0)) begin
		waitCnt  <= waitCnt - 4'd1;
		spiCount <= 7'd72;
	end
	else if (verBlank && (spiCount > 7'd0)) begin
		vid_en <= 0;
		if(baudCount < 2'd3) begin
			baudCount <= baudCount + 2'd1;
		end
		else begin
			baudCount <= 0;
		end
		if (baudCount == 0) begin
			imagerCtrl <= spiWrites[spiCount - 9'd1];
			spiCount   <= spiCount - 7'd1;
		end
		if (spiCount == 7'd1) begin
			spiDone <= 1'b1;
		end
	end
end

endmodule		



`timescale 1 ps / 1 ps

module Avalon_Slave_Bridge #(
        parameter p_addr_width = 32,
        parameter p_data_width = 32
    ) (
        output wire [p_addr_width-1:0] avalon_master_address,    // avalon_master.address
        output wire                    avalon_master_chipselect, //              .chipselect
        output wire                    avalon_master_read,       //              .read
        input  wire [p_data_width-1:0] avalon_master_readdata,   //              .readdata
        output wire                    avalon_master_write,      //              .write
        output wire [p_data_width-1:0] avalon_master_writedata,  //              .writedata
        input  wire [p_addr_width-1:0] avalon_slave_address,     //  avalon_slave.address
        input  wire                    avalon_slave_chipselect,  //              .chipselect
        input  wire                    avalon_slave_read,        //              .read
        output wire [p_data_width-1:0] avalon_slave_readdata,    //              .readdata
        input  wire                    avalon_slave_write,       //              .write
        input  wire [p_data_width-1:0] avalon_slave_writedata,   //              .writedata
        input  wire                    clock_clk,                //         clock.clk
        input  wire                    reset                     //         reset.reset
    );


    assign avalon_master_chipselect = avalon_slave_chipselect;

    assign avalon_master_address = avalon_slave_address;

    assign avalon_master_read = avalon_slave_read;

    assign avalon_master_write = avalon_slave_write;

    assign avalon_master_writedata = avalon_slave_writedata;

    assign avalon_slave_readdata = avalon_master_readdata;
	 

endmodule

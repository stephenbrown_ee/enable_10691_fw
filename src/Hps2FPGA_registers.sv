//  This is the HPS to FPGA register map

module Hps2FPGA_registers
(
	input  wire        busClock,
	input  wire        busReset,
	input  wire        busChipSelect,
	input  wire        busWrite,
	input  wire        busRead,
	input  wire [19:0] busAddress,
	input  wire [31:0] busWrData,
	output reg  [31:0] busRdData,
	
	output  wire       lwhps_bus_waitrequest,   
	output  reg [31:0] lwhps_bus_readdata,      
	output  reg        lwhps_bus_readdatavalid, 	
	input  wire [31:0] lwhps_bus_writedata,     
	input  wire [15:0] lwhps_bus_address,       
	input  wire        lwhps_bus_write,         
	input  wire        lwhps_bus_read,
   output wire        lwhps_bus_burstcount,
	input  wire        lwhps_bus_byteenable,
	
	input  logic [3:0]  inpBfr,
	output wire [31:0] hps_overlay_address,
	
	output wire        ovlyDone,
	output wire        ignoreAlpha,
	output wire        touchscreen_scale,
	output wire        touchscreen_video,
	output wire        touchscreen_up_arrow,
	output wire        touchscreen_down_arrow,
	output wire        set_scope_time,
	output wire        clear_scope_flash,
	output wire        get_batch_num,
	output wire        set_batch_num,
	output wire        start_calibration,
	output wire        toggleAGC
);

assign lwhps_bus_waitrequest = 1'b0;

reg [31:0][31:0] hps_register = 0;
reg [3:0]       hpsBfr = 0;


// IMPORTANT!!! Check the register map before making changes
assign ovlyDone                = hps_register[1][4];
assign ignoreAlpha             = hps_register[1][5];
assign toggleAGC               = hps_register[4][14];
assign start_calibration       = hps_register[4][12];
assign get_batch_num           = hps_register[4][11];
assign set_batch_num           = hps_register[4][10];
assign clear_scope_flash       = hps_register[4][9];
assign set_scope_time          = hps_register[4][8];
assign touchscreen_scale       = hps_register[4][4];
assign touchscreen_video       = hps_register[4][2];
assign hps_overlay_address     = hps_register[6][31:0];
assign touchscreen_up_arrow    = hps_register[7][9];
assign touchscreen_down_arrow  = hps_register[7][8];

always_comb begin
	if (busChipSelect)
		busRdData = hps_register[busAddress[6:2]];
	else 	
		busRdData = 0;
end

always@(posedge busClock) begin	
	if (busReset) begin
		hps_register <= 0;
		lwhps_bus_readdatavalid <= 0;
	end
	else if (lwhps_bus_read) begin
		lwhps_bus_readdata      <= hps_register[lwhps_bus_address[6:2]];
		lwhps_bus_readdatavalid <= 1'b1;
	end
	else if (lwhps_bus_write) begin
		hps_register[lwhps_bus_address[6:2]] <= lwhps_bus_writedata;
		lwhps_bus_readdatavalid <= 0;
	end
	else if (busChipSelect && busWrite) begin
		hps_register[busAddress[6:2]] <= busWrData;
		lwhps_bus_readdatavalid <= 0;
	end
	else begin
		lwhps_bus_readdatavalid <= 0;
		hps_register[5]         <= {3'b001, hpsBfr, 25'd0};
	end
	
	if (inpBfr == 4'd0)
	   hpsBfr <= 4'd15;
	else
	   hpsBfr <= inpBfr - 1'd1;
end


endmodule

	
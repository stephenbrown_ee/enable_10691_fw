module TmdsEncoder
(
	input  wire                  busClock,
	input  wire                  busReset,
	input  wire                  busChipSelect,
	input  wire                  busWrite,
	input  wire                  busRead,
	input  wire [19:0]           busAddress,
	input  wire [31:0]           busWrData,
	output reg  [31:0]           busRdData,
	
	input wire                   inpClock,
	input wire                   inpVs,
	input wire                   inpHs,
	input wire                   inpDe,
	input wire  [23:0]           inpData,
	
	input wire  [3:0]            tx_ready,
	
	output wire [9:0]            out_blu0,
	output wire [9:0]            out_grn1,
	output wire [9:0]            out_red2,
	output wire [9:0]            out_clk3
	
);

reg [31:0] register = 0;
wire  ready = &tx_ready;

always@(posedge busClock) begin	
	if (busWrite && busChipSelect)
		register <= busWrData;
end

wire reset = register[0] || busReset || !ready;


assign out_clk3 = 10'b1111100000;


tmds tmds_blu
(
	.clk      (inpClock),
	.reset    (reset),
	.de       (inpDe),
	.ctrl     ({inpVs, inpHs}),
	.d_in     (inpData[23:16]),
	
	.d_out    (out_blu0)
);

tmds tmds_grn
(
	.clk      (inpClock),
	.reset    (reset),
	.de       (inpDe),
	.ctrl     (2'b00),
	.d_in     (inpData[15:8]),
	
	.d_out    (out_grn1)
);


tmds tmds_red
(
	.clk      (inpClock),
	.reset    (reset),
	.de       (inpDe),
	.ctrl     (2'b00),
	.d_in     (inpData[7:0]),
	
	.d_out    (out_red2)
);


endmodule
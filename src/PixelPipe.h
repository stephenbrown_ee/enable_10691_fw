//==============================================================

typedef enum bit [2:0]
{
	SYNC_IDLE,     // 0
	SYNC_PIX_EXT,  // 1
	SYNC_PIX_BLK,  // 2
	SYNC_PIX_OUT,  // 3
	SYNC_LIN_BEG,  // 4
	SYNC_LIN_END,  // 5
	SYNC_FRM_BEG,  // 6
	SYNC_FRM_END   // 7
} SyncCode;

//==============================================================
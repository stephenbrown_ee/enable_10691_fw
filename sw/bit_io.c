

#include <stdio.h>
#include <time.h>

#include "bus_io.h"
#include "bit_io.h"

//=======================================================================

#define BITOUTPIN(pin) (BITOUTPIN_MAX - 1 - (pin))
#define BITINPPIN(pin) (BITINPPIN_MAX - 1 - (pin))

#define PIN_OUT(pin) BUS_REG_SUB(bus_sysMgr, 1, BITOUTPIN(pin))
#define PIN_INP(pin) BUS_REG_SUB(bus_sysMgr, 2, BITINPPIN(pin))

//=======================================================================

void out(BitOutPin pin, int value)
{
	PIN_OUT(pin) = value;
}

//-----------------------------------------------------------------------

int inp(BitInpPin pin)
{
	return PIN_INP(pin);
}

//-----------------------------------------------------------------------

int outPinStatus(BitOutPin pin)
{
	return PIN_OUT(pin);
}
//=======================================================================

// IntraVu_AE.cpp : Defines the entry point for the console application.
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "console.h"
//#include <memory.h>
#include "IntraVu_AE.h"
#include "util.h"
#include "debug.h"
#include "sensor.h"

// AE Default Configuration Items
#define  AE_MaxIntegrationLines  450
#define  AE_LineTimeus           83
#define  AE_BaseISO              50
#define  AE_ApertureFNo          2.8
#define  AE_MaxGain              3.75
#define  AE_MinGain              1.0
#define  AE_DampingFactor        0.4
#define  AE_ClippedWhite         971
#define  AE_BlackLevelOffset     1
#define  AE_RhoMax               2.0
#define  AE_RhoAveSetPoint       0.18

#define AE_MinExpLines           15
#define AE_MaxExpLines           450

#define  AE_MinLED_PWR           0.15
#define  AE_NominalLED_PWR       25
#define  AE_MaxLED_PWR           100

//static int lastEVState = 0;

void Init_AE(AEConfig *pAEConfig)
{
	AEConfig TmpConfig = {
	/* AEConfig->MaxIntegrationLines = */   AE_MaxIntegrationLines,
	/* AEConfig->LineTimeus =          */   (float)AE_LineTimeus,
	/* AEConfig->ApertureFNo =         */   (float)AE_ApertureFNo,
	/* AEConfig->DampingFactor =       */   (float)AE_DampingFactor,
	/* AEConfig->ClippedWhite =        */   AE_ClippedWhite,
	/* AEConfig->BlackLevelOffset =    */   AE_BlackLevelOffset,
	/* AEConfig->RhoMax =              */   (float)AE_RhoMax,
	/* AEConfig->RhoAveSetPoint =      */   (float)AE_RhoAveSetPoint,
	/* AEConfig->MinExpLines =         */   AE_MinExpLines,
	/* AEConfig->MaxExpLines =         */   AE_MaxExpLines,
	/* AEConfig->MaxGain =             */   (float)AE_MaxGain,
	/* AEConfig->MinGain =             */   (float)AE_MinGain,
	/* AEConfig->MinLED_PWR =          */   (float)AE_MinLED_PWR,
	/* AEConfig->NominalLED_PWR =      */   (float)AE_NominalLED_PWR,
	/* AEConfig->MaxLED_PWR =          */   (float)AE_MaxLED_PWR,
	/* AEConfig->WBGains[] =           */   {1.178f,        1.0f,   1.506f},
	/* AEConfig->CCMat[] =             */   {  1.9632f,-0.83141f, -0.1318f,
	                                         -0.36464f,  1.6162f, -0.25161f,
											 -0.01172f,-0.62886f,1.640576f}
	};

	memcpy(pAEConfig,&TmpConfig,sizeof(AEConfig));
};

void CalcExposureParams(AEExpParams *pCurrentExp, float DeltaEV, AEConfig *pConfig, AEExpParams *pNextExp);
void CalcExpOV6948(AEExpParams *pCurrentExp, float DeltaEV, AEConfig *pConfig, AEExpParams *pNextExp);

int AE_Compute(AEPaxelImage *pImage, AEConfig *pConfig, AEExpParams  *pResult, sensor_type_t sensor)
{
	float LumaEst, TargetCV;
	// Estimate the scene Luminance
//	if (sensor == OV6948)
	LumaEst = ComputeLumaEstimate(&pImage->AE_Img, pConfig)-pConfig->BlackLevelOffset;
//	else
//		LumaEst = (float)sensorGet(0x5693);
	if(debugLog(0))
		printf("LumaEst: %1.2f\n", LumaEst);

	// Compute the Target CV
//	if (sensor == OV6948)
	TargetCV = pConfig->RhoAveSetPoint*(pConfig->ClippedWhite-pConfig->BlackLevelOffset)/pConfig->RhoMax;
//	else
//		TargetCV = 58;
//	printf("TargetCV: %f\n", TargetCV);
	
	// Compute the Current Exposure Delta:  DV
	float DV = (float)log10(TargetCV/LumaEst)/(float)log10(2.);
	if(debugLog(0))
		printf("DV: %f\n", DV);

	// Compute the Desired Exposure
	if(sensor == OV6946)
		CalcExposureParams(&pImage->ExpSettings, pConfig->DampingFactor*DV, pConfig, pResult);
	else
		CalcExpOV6948(&pImage->ExpSettings, pConfig->DampingFactor*DV, pConfig, pResult);
	
	return 0;
}
/*  
    Computes a Luma Estimate based upon the Paxel Image
   The return estimate is in the same metric as the Paxel Image
*/
float ComputeLumaEstimate(PaxelImage *pImg, AEConfig *pAEConfig)
{
	unsigned int ImgSize = 1; //PAX_HEIGHT * PAX_WIDTH;
//	unsigned long Accum = 0;  // Note this will not overflow until Image size > 4 MP (10 bit Image)
	unsigned int *pImgPtr = pImg->pRGBPlanes;

//	unsigned int NumPixels = ImgSize;
	float Luma;
	unsigned int NumClipped = pImg->pRGBNumSatPlanes;
	
	// Compute the color corrected, White balanced RGB Weights
	float LumaWt_R = pAEConfig->WBGains[0]*(0.3f*pAEConfig->CCMat[0] +
		                                    0.6f*pAEConfig->CCMat[3] +
										    0.1f*pAEConfig->CCMat[6]);
	
	float LumaWt_G = pAEConfig->WBGains[1]*(0.3f*pAEConfig->CCMat[1] +
		                                    0.6f*pAEConfig->CCMat[4] +
										    0.1f*pAEConfig->CCMat[7]);
	
	float LumaWt_B = pAEConfig->WBGains[2]*(0.3f*pAEConfig->CCMat[2] +
		                                    0.6f*pAEConfig->CCMat[5] +
										    0.1f*pAEConfig->CCMat[8]);

//	printf("LWR: %2.3f LWG: %2.3f  LWB: %2.3f\n", LumaWt_R, LumaWt_G, LumaWt_B);

	// Increment through the planer data
	Luma = LumaWt_R * *pImgPtr +
		   LumaWt_G * *(pImgPtr+   ImgSize) +
		   LumaWt_B * *(pImgPtr+ 2*ImgSize) ;

	if(debugLog(0)) {
		printf("Luma: %f  Clipped: %d\n", Luma, NumClipped);
	}
	
	return Luma;

}
void CalcExposureParams(AEExpParams *pCurrentExp, float DeltaEV, AEConfig *pConfig, AEExpParams *pNextExp)
{
//	int currentEVState;
	// Determine the current EV
	float CurrentEV = (float)log10(pCurrentExp->Gain * 
		                           pCurrentExp->IntegrationLines * 
					               pCurrentExp->LED_Power/pConfig->NominalLED_PWR)/(float)log10(2.);


	double TargetEV = CurrentEV + DeltaEV;
	if(debugLog(0))
		printf("CurrentEV: %f\nTargetEV: %g\n", CurrentEV, TargetEV);
   // Convert the TargetEV to Exposure Params.
   // The Exposure Program is divided into 4 regions from Least Exposure to Most:
   // EVDomain1:  MinGain, LinesOfExposure:  Decrease LED from Nominal 
   // EVDomain2:  Nominal LED Pwr, Gain = 1.0:  Change LinesOfExposure upto MaxLines, Gain = 1.0 + as needed to prevent line time quantization
   // EVDomain3:  Max LinesOfExposure, Nominal LED Pwr:  Increase Gain up to Max Gain
   // EVDomain4:  Max LinesOfExposure, MaxGain, Increase LED PWR to Max Pwr (Not Used)

   float EVDomain12= (float)log10(pConfig->MinGain*pConfig->MinExpLines)/(float)log10(2.);
   float EVDomain23= (float)log10(pConfig->MinGain*pConfig->MaxExpLines)/(float)log10(2.);
   float EVDomain34= (float)log10(pConfig->MaxGain*pConfig->MaxExpLines)/(float)log10(2.);

   // EVDomain1:  Modulate LED Power
   if(TargetEV < EVDomain12)
   {
	   pNextExp->Gain = pConfig->MinGain;
	   pNextExp->IntegrationLines=pConfig->MinExpLines;
	   if(fabs(DeltaEV) > 0.03) {
		   pNextExp->LED_Power = ((float)pow(2.0,TargetEV) * pConfig->NominalLED_PWR)/pConfig->MinGain/pConfig->MinExpLines;
	   }
	   else {
		   pNextExp->LED_Power = pCurrentExp->LED_Power;
	   }

//	   currentEVState = 1;
//	   printf("LED_Power: %f\n", pNextExp->LED_Power);
   }
   // EVDomain 3:  Modulate Gain
   else if ((TargetEV > EVDomain23) && (TargetEV <= EVDomain34))
   {
	   pNextExp->LED_Power=pConfig->NominalLED_PWR;
	   pNextExp->IntegrationLines = pConfig->MaxExpLines;
	   pNextExp->Gain = (float)pow(2.0,TargetEV)/pConfig->MinGain/pConfig->MaxExpLines;
//	   currentEVState = 3;
//	   printf("Gain3: %f\n", pNextExp->Gain);
   }
   // EVDomain 4: Modulate LED Power
   else if (TargetEV > EVDomain34)
   {
	   pNextExp->LED_Power= ((float)pow(2.0,TargetEV) * pConfig->NominalLED_PWR)/pConfig->MaxGain/pConfig->MaxExpLines;
	   pNextExp->IntegrationLines = pConfig->MaxExpLines;
	   pNextExp->Gain = pConfig->MaxGain;
   }
   // EVDomain2: Modulate IntegrationTime
   else
   {
	   
	   pNextExp->LED_Power=pConfig->NominalLED_PWR;
	   float Lines = (float)pow(2.0,TargetEV)/pConfig->MinGain;
	   pNextExp->IntegrationLines = (int)Lines;
//	   printf("Lines: %f\n", Lines);
	   // Feather in some gain to make up for the exposure lost in the integer quantization of integration lines
	   pNextExp->Gain = pConfig->MinGain*Lines/pNextExp->IntegrationLines;
	   pNextExp->Gain = CLIP(pNextExp->Gain, pConfig->MinGain, 1.125);
//	   currentEVState = 2;

   }

//   if(currentEVState != lastEVState)
//	   printf("EV State Change: %d to %d\n", lastEVState, currentEVState);

//   lastEVState = currentEVState;

   // Need to clip the gain and lines values. Should this be done here?  I think so. Otherwise there's invalid settings that cause flickering.
   pNextExp->Gain = CLIP(pNextExp->Gain, pConfig->MinGain, pConfig->MaxGain);
   if(isnan(pNextExp->Gain))
	   pNextExp->Gain = pCurrentExp->Gain;
   else if(((pCurrentExp->Gain - pNextExp->Gain) < 0.046875) && ((pCurrentExp->Gain - pNextExp->Gain) > -0.046875))
	   pNextExp->Gain = pCurrentExp->Gain;
   else if (pNextExp->Gain <= 2)
	   pNextExp->Gain = round(pNextExp->Gain * 16)/16;  // round to the nearest 16th
   else
	   pNextExp->Gain = round(pNextExp->Gain * 8)/8;  // round to the nearest 8th

   pNextExp->IntegrationLines = CLIP(pNextExp->IntegrationLines, pConfig->MinExpLines, pConfig->MaxExpLines);
   pNextExp->LED_Power = CLIP(pNextExp->LED_Power, pConfig->MinLED_PWR, pConfig->MaxLED_PWR);

   if(debugLog(0))
	   printf("E: %d G: %1.3f  L: %1.3f\n", pNextExp->IntegrationLines, pNextExp->Gain, pNextExp->LED_Power);
}

void CalcExpOV6948(AEExpParams *pCurrentExp, float DeltaEV, AEConfig *pConfig, AEExpParams *pNextExp)
{
	float CurrentEV = (float)log10(pCurrentExp->LED_Power/pConfig->NominalLED_PWR)/(float)log10(2.);

	double TargetEV = CurrentEV + DeltaEV;

	pNextExp->LED_Power= ((float)pow(2.0,TargetEV) * pConfig->NominalLED_PWR);

	pNextExp->LED_Power = CLIP(pNextExp->LED_Power, pConfig->MinLED_PWR, pConfig->MaxLED_PWR);
}

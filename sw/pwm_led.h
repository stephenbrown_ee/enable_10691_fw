
#ifndef PWM_LED_H
#define PWM_LED_H
//==============================================================

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "fpga_i2c1.h"


int setFaultLED (int state);

#endif
//==============================================================


#include <stdio.h>
#include <string.h>

#include "util.h"
#include "image_gen.h"
#include "sys_mgr.h"
#include "console.h"
#include "bus_io.h"
#include "bit_io.h"

//==============================================================

#define IMG_GEN_RESET            BIT(bus_imgGen, 0, 0)
#define IMG_GEN_SOURCE           BIT(bus_imgGen, 0, 1) // 0 = Sensor, 1 = Generate
#define IMG_GEN_ENABLE           BIT(bus_imgGen, 0, 2)
#define IMG_GEN_OVERLAY          BIT(bus_imgGen, 0, 3)
#define IMG_GEN_STARTCOLOR       FLD(bus_imgGen, 0, 5, 4) // First CFA pixel  0=R 1=Gr 2=Gb 3=B
#define IMG_GEN_GRADIENT         BIT(bus_imgGen, 0, 6)
#define IMG_GEN_RUN              BIT(bus_imgGen, 0, 8)
#define IMG_GEN_STEP             FLD(bus_imgGen, 0, 23, 16)
#define IMG_GEN_FRAME            FLD(bus_imgGen, 0, 31, 24)
#define IMG_GEN_COLHORMAX        FLD(bus_imgGen, 1, 11, 0)
#define IMG_GEN_ROWVERMAX        FLD(bus_imgGen, 1, 27, 16)
#define IMG_GEN_IMGHORMAX        FLD(bus_imgGen, 2, 11, 0)
#define IMG_GEN_IMGVERMAX        FLD(bus_imgGen, 2, 27, 16)
#define IMG_GEN_HORMAX           FLD(bus_imgGen, 3, 11, 0)
#define IMG_GEN_VERMAX           FLD(bus_imgGen, 3, 27, 16)

#define IMG_GEN_COLCNT           FLD(bus_imgGen, 4, 3, 0)
#define IMG_GEN_ROWCNT           FLD(bus_imgGen, 4, 6, 4)
#define IMG_GEN_FRAMECNT         FLD(bus_imgGen, 4, 15, 8)
#define IMG_GEN_VERCNT           FLD(bus_imgGen, 4, 27, 16)

#define IMG_GEN_RAM(row,col,clr) BUS_REG_SUB(bus_imgGen, 1, ((row)*64) + ((col)*4) + (clr))

//=======================================================================

void imageGenReset(void);
void imageGenRegClear(void);
//void imageGenLoad(int width, int height, int pattern);
//void imageGenGradient(int enable, int step);

//=======================================================================

int imageGenInit(int imgWidth, int imgHeight)
{
	imageGenReset();
	imageGenRegClear();
//	imageGenSetup(imgWidth, imgHeight, 175, 62, 0, 1);
	imageGenSource(0, 0);
	imageGenRun();

	return 0;
}

////-----------------------------------------------------------------------
//
//void imageGenSetup(int width, int height, int hIdle, int vIdle, int startColor, int pattern)
//{
//	imageGenReset();
//	imageGenRegClear();
//
//	// Line buffer requires at least...
//	//   8 idle clocks per line OR
//	//   3 idle clock per line and 1 idle line per frame
//
//	REG_SET(IMG_GEN_SOURCE, 1);
//	REG_SET(IMG_GEN_ENABLE, 0);
//	REG_SET(IMG_GEN_OVERLAY, 0);
//	REG_SET(IMG_GEN_FRAME, 0);
//	REG_SET(IMG_GEN_GRADIENT, 0);
//	REG_SET(IMG_GEN_STARTCOLOR, startColor);
//	REG_SET(IMG_GEN_COLHORMAX, 15);
//	REG_SET(IMG_GEN_ROWVERMAX, 3);
//	REG_SET(IMG_GEN_IMGHORMAX, width + 2 - 1);  // Add 2 for sync
//	REG_SET(IMG_GEN_IMGVERMAX, height - 1);
//	REG_SET(IMG_GEN_HORMAX, width + hIdle - 1);
//	REG_SET(IMG_GEN_VERMAX, height + vIdle - 1);
//
////	printf("Image gen: hor.%04d ver.%04d imghor.%04d imgver.%04d\n",
////			REG_GET(IMG_GEN_HORMAX) + 1, REG_GET(IMG_GEN_VERMAX) + 1, REG_GET(IMG_GEN_IMGHORMAX), REG_GET(IMG_GEN_IMGVERMAX));
//
//	if (pattern >= 0)
//	{
//		if (pattern == 7)
//			imageGenGradient(1, 1);
//		else if (pattern == 8)
//			imageGenGradient(1, 0);
//		else if (pattern == 9)
//			imageGenSolid(0,0,0);
//		else
//			imageGenLoad(width, height, pattern);
//	}
//}
//
//-----------------------------------------------------------------------

void imageGenSource(int source, int overlay)
{
	out(regUpdate, 1);
	REG_SET(IMG_GEN_SOURCE, source);
	REG_SET(IMG_GEN_OVERLAY, overlay);
	out(regUpdate, 0);
}

//-----------------------------------------------------------------------
//
//void imageGenSolid(int R, int G, int B)
//{
//	int row, col;
//
//	for (col = 0; col < 16; col++)
//		for (row = 0; row < 8; row++)
//		{
//			IMG_GEN_RAM(row, col, 0) = R;
//			IMG_GEN_RAM(row, col, 1) = G;
//			IMG_GEN_RAM(row, col, 2) = G;
//			IMG_GEN_RAM(row, col, 3) = B;
//		}
//
//	REG_SET(IMG_GEN_COLHORMAX, REG_GET(IMG_GEN_IMGHORMAX));
//	REG_SET(IMG_GEN_ROWVERMAX, REG_GET(IMG_GEN_IMGVERMAX));
//}
//
//-----------------------------------------------------------------------

void imageGenReset(void)
{
	REG_SET(IMG_GEN_RESET, 1);
	REG_SET(IMG_GEN_RESET, 0);
}

//-----------------------------------------------------------------------

void imageGenRegClear(void)
{
	regClear(bus_imgGen, 32);
}

//-----------------------------------------------------------------------

void imageGenRun(void)
{
	REG_SET(IMG_GEN_RESET, 0);
	REG_SET(IMG_GEN_ENABLE, 1);
}

//-----------------------------------------------------------------------
//
//void imageGenStop(void)
//{
//	REG_SET(IMG_GEN_ENABLE, 0);
//	int start = getMsec();
//
//	while (REG_GET(IMG_GEN_RUN))
//		if (getMsec() - start > 500)
//			return;
//}
//
////-----------------------------------------------------------------------
//
//void imageGenClearBuffer(void)
//{
//	imageGenReset();
//	imageGenRegClear();
//	imageGenSetup(4095, 4095, 150, 50, 1, 9);
//	imageGenSource(1, 0);
//	imageGenRun();
//}
//
////---------------------------------------------------------------
//
//typedef struct
//{
//	char code;
//	unsigned char R;
//	unsigned char G;
//	unsigned char B;
//} GenColor;
//
//GenColor color[] =
//{
//	{ '0',   0,   0,   0 }, // Black
//	{ '1',  32,  32,  32 },
//	{ '2',  64,  64,  64 },
//	{ '3',  96,  96,  96 },
//	{ '4', 128, 128, 128 },
//	{ '5', 160, 160, 160 },
//	{ '6', 192, 192, 192 },
//	{ '7', 224, 224, 224 },
//	{ '8', 255, 255, 255 }, // White
//	{ 'R', 255,   0,   0 },
//	{ 'G',   0, 255,   0 },
//	{ 'B',   0,   0, 255 },
//	{ 'C',   0, 255, 255 },
//	{ 'M', 255,   0, 255 },
//	{ 'Y', 255, 255,   0 },
//	{ 'a',   1,   0,   0 },
//	{ 'b',   2,   0,   0 },
//	{ 'c',   4,   0,   0 },
//	{ 'd',   8,   0,   0 },
//	{ 'e',  16,   0,   0 },
//	{ 'f',  32,   0,   0 },
//	{ 'g',  64,   0,   0 },
//	{ 'h', 128,   0,   0 },
//	{ 'i',   0,   1,   0 },
//	{ 'j',   0,   2,   0 },
//	{ 'k',   0,   4,   0 },
//	{ 'l',   0,   8,   0 },
//	{ 'm',   0,  16,   0 },
//	{ 'n',   0,  32,   0 },
//	{ 'o',   0,  64,   0 },
//	{ 'p',   0, 128,   0 },
//	{ 'q',   0,   0,   1 },
//	{ 'r',   0,   0,   2 },
//	{ 's',   0,   0,   4 },
//	{ 't',   0,   0,   8 },
//	{ 'u',   0,   0,  16 },
//	{ 'v',   0,   0,  32 },
//	{ 'w',   0,   0,  64 },
//	{ 'x',   0,   0, 128 },
//	{ 0, 0 }
//};
//
////-----------------------------------------------------------------------
//
//GenColor* findColor(char code)
//{
//	int n;
//	for (n = 0; color[n].code != 0; n++)
//		if (color[n].code == code)
//			return &color[n];
//	return 0;
//}
//
////-----------------------------------------------------------------------
//
//char* testImage[] =
//{
//    "000000." // 0
//    "123456."
//	"RGBCMY."
//	"888888:",
//
//    "RRRR."
//    "GGGG."
//	"BBBB."
//	"8888:", // 1
//
//    "0R."
//    "GB:", // 2
//
//    "0RGB."
//    "CMY8."
//    "0RGB."
//    "CMY8:", // 3
//
//    "000G000C000Y0000."
//    "0R000B000M000800."
//    "RRRGRRRCRRRYRRRR."
//    "R0RRRBRRRMRRR8RR."
//    "GGG0GGGCGGGYGGGG."
//    "GRGGGBGGGMGGG8GG."
//    "BBBGBBBCBBBYBBBB."
//    "BRBBB0BBBMBBB8BB:", // 4
//
//    "0808080808080808."
//    "RRR8abcdefgh0RRR."
//    "GGG8ijklmnop0GGG."
//    "BBB8qrstuvwx0BBB."
//    "0808080808080808."
//    "RGB8765432100RGB."
//    "CMY8765432100CMY."
//    "0808080808080808:", // 5
//
//    "876543210:" // 6
//};
//
////-----------------------------------------------------------------------
//
//void imageGenLoad(int width, int height, int pattern)
//{
//	int n;
//	int row = 0;
//	int col = 0;
//	GenColor* clr;
//	char* img = testImage[pattern];
//
//	for (n = 0; n < strlen(img); n++)
//	{
//		if ((img[n] == '.') || (img[n] == ':'))
//		{
//			IMG_GEN_RAM(row, col, 0) = 0;
//			IMG_GEN_RAM(row, col, 1) = 0;
//			IMG_GEN_RAM(row, col, 2) = 0;
//			IMG_GEN_RAM(row, col, 3) = 0;
//
//			if (img[n] == ':')
//				break;
//
//			row++;
//			col = 0;
//		}
//		else
//		{
//			clr = findColor(img[n]);
//			IMG_GEN_RAM(row, col, 0) = clr->B * 16;
//			IMG_GEN_RAM(row, col, 1) = clr->G * 16;
//			IMG_GEN_RAM(row, col, 2) = clr->G * 16;
//			IMG_GEN_RAM(row, col, 3) = clr->R * 16;
//
//			col++;
//		}
//	}
//
//	REG_SET(IMG_GEN_COLHORMAX, (width / col));
//	REG_SET(IMG_GEN_ROWVERMAX, (height / (row + 1)));
//}
//
//void imageGenGradient(int enable, int step)
//{
//	REG_SET(IMG_GEN_GRADIENT, enable);
//	REG_SET(IMG_GEN_STEP, step);
//}
//
//
//void imageGenOverlay(int enable)
//{
//	REG_SET(IMG_GEN_OVERLAY, enable);
//}

//=======================================================================

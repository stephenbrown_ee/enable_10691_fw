
#ifndef CONSOLE_H
#define CONSOLE_H

#include <stdio.h>

//==============================================================

#define CON_NONBLOCK 0x01
#define CON_ECHO     0x02
#define CON_EDIT     0x04
#define CON_ADDCR    0x08
#define CON_NOCHECK  0x10

//==============================================================

int consoleInit(int n, int path, int flags);

void consoleWorker(int n);

int consoleCheck(void);

void consolePrintf(const char *format, ...);

#define printf(...) consolePrintf(__VA_ARGS__)

//==============================================================

typedef void (*ConsoleWorkerFunction)(int id);

typedef int (*ConsoleCheckFunction)(int id);

typedef void (*ConsolePutcFunction)(int id, char c);

void consoleRegister(int id, ConsoleWorkerFunction workerFunction, ConsoleCheckFunction checkFunction, ConsolePutcFunction putcFunction);

void consoleProcessChar(int id, char c);

#endif
//==============================================================


#ifndef SENSORINPUT_H_
#define SENSORINPUT_H_
//==============================================================

int sensorInputInit(void);
void sensorInputSetup(void);
int getFrameCount(void);
void sensorCtlSetup(void);
void sensorCtlSet (uint16_t address, uint8_t value);
void blackAverage(int set);
void calibrateBlankLevel(void);

#endif

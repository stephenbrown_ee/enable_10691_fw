#include "xcvr_reconfig.h"
#include "console.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "parallel_out.h"
#include <stdio.h>

#define XCVR_CH_NUM       FLD(bus_xcvr, 8, 31, 0)
#define XCVR_CTL_STATUS   FLD(bus_xcvr, 10, 31, 0)
#define XCVR_ERROR        1<<9
#define XCVR_BUSY         1<<8
#define XCVR_READ         1<<1
#define XCVR_WRITE        1<<0
#define XCVR_PMA_OFFSET   FLD(bus_xcvr, 11, 31, 0)
#define XCVR_VOD          0
#define XCVR_PRETAP       1
#define XCVR_POSTTAP      2
#define XCVR_DATA         FLD(bus_xcvr, 12, 31, 0)
#define XCVR_REG(reg)     FLD(bus_xcvr, reg, 31, 0)


void set_xcvr_vod(int data)
{
	int i = 0;
	for (i = 0; i < 4; i++) {
		REG_SET(XCVR_CH_NUM, i);
		REG_SET(XCVR_PMA_OFFSET, XCVR_VOD);
		REG_SET(XCVR_DATA, data);
	}
	REG_SET(XCVR_CTL_STATUS, XCVR_WRITE);
}

void test_vod (int start, int stop)
{
	int i = start;
	for (i = start; i <= stop; i++)
	{
		set_xcvr_vod(i);
		printf("VOD: %d\n", i);
		waitMsec(10000);
	}
}

void xcvr_write(int reg, int data)
{
	REG_SET(XCVR_REG(reg), data);
}

void xcvr_read(int reg)
{
	REG_SET(XCVR_PMA_OFFSET, reg);
	REG_SET(XCVR_CTL_STATUS, XCVR_READ);
	waitMsec(100);
	printf("Busy: %x\n", REG_GET(XCVR_CTL_STATUS));
	printf("Value: %x\n", REG_GET(XCVR_DATA));
}

void set_xcvr_pre(int data)
{
	int i = 0;
	for (i = 0; i < 4; i++) {
		REG_SET(XCVR_CH_NUM, i);
		REG_SET(XCVR_PMA_OFFSET, XCVR_PRETAP);
		REG_SET(XCVR_DATA, data);
	}
	REG_SET(XCVR_CTL_STATUS, XCVR_WRITE);
}


void test_pre (void)
{
	int i = 0;
	for (i = 0; i <= 15; i++)
	{
		set_xcvr_pre(i);
		printf("PRE: %d\n", i);
		waitMsec(10000);
	}
}

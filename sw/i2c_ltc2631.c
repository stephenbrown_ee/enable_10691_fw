

#include "fpga_i2c0.h"
#include "lcd_display_out.h"
#include "hps2fpga_comm.h"

#define INPUT_REG_ONLY     0x00
#define UPDATE_DAC         0x10
#define WRITE_AND_UPDATE   0x30
#define POWER_DOWN         0x40
#define SEL_INT_REF        0x60
#define SEL_EXT_REF        0x70


void initLED(int enable, int value)
{
	if (enable) {
		out(LED_DRV_PWM, ON);
		out(LED_DRV_SHDN, ON);
		waitMsec(5);
		out(LED_DRV_PWM, OFF);
		out(LED_DRV_SHDN, OFF);
		waitMsec(50);
		out(LED_DRV_PWM, ON);
		out(LED_DRV_SHDN, ON);
	}
	else {
		out(LED_DRV_PWM, OFF);
		out(LED_DRV_SHDN, OFF);
	}
	setDAC((float)value);
	printf("Init LED %d %d\n", enable, value);
}

void setDAC (float value)
{
	float digValue = ((4095/2) * value * 340)/100000;  // input value is number from 0 to 100, digValue is equiv. to 0 to 340mA
	dacSet(WRITE_AND_UPDATE, (int)digValue);
	currentSetting = value;
	if(value < 1)
		value = 1;
	setLedState((int)value);
}

// The LTC2631 DAC controls the ADJ pin to the LTM8040 LED Driver.
// The voltage at the ADJ pin controls the constant current to the LED.
// Iled = 1A * VADJ/1.25V
// DigValue = 4095 * (VADJ/2.5V)
// Thus, DigValue = (4095/2) * Iled
/*
 *     DAC digital value        VOUT (V)         LED Current (mA)
 *           143                 0.0875               70
 *           287                 0.175                140
 *           430                 0.2625               210
 *           573                 0.350                280
 *           717                 0.4375               350
 */
void dacSet(uint8_t address, uint16_t digValue)
{
	int baud = 20000;
	digValue = digValue << 4;
	i2c_vsync(bus_i2cMas, 1);
	i2c(bus_i2cMas, baud, 1, 1, 0, 0, 0, 0x44, 0);
	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 0, address >> 0, 0);
	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 0, digValue >> 8, 0);
	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 1, (digValue >> 0) & 0xFF, 0);
	i2c_vsync(bus_i2cMas, 0);
}

void upLED (void)
{
	uint8_t value = 0;
	if (currentSetting < 100)
	{
		if (outPinStatus(LED_DRV_SHDN) == OFF) {
			initLED(1, 1);
		} else {
			value = currentSetting + 1;
			setDAC((float)value);
		}
	}
}

void downLED (void)
{
	uint8_t value = 0;
	if (currentSetting > 0 )
	{
		if (currentSetting <= 1) {
			initLED(0,0);
		}
		else {
			value = currentSetting - 1;
			setDAC((float)value);
		}
	}
}

void setLED (float value)
{
	if (value == 0) {
		initLED(0,0);
	}
	else if (outPinStatus(LED_DRV_SHDN) == OFF) {
		initLED(1, value);
	}
	else {
		setDAC((float)value);
	}
}

void forceLED(int value)
{
	int digValue = ((4095/2) * value * 340)/100000;  // input value is number from 0 to 100, digValue is equiv. to 0 to 340mA
	dacSet(WRITE_AND_UPDATE, digValue);
	currentSetting = (float)value;
	if(value < 1)
		value = 1;
	setLedState(value);
}

void sweepLED(void)
{
	for (int n=0; n<3; n++) {
		for (float i = 0.1; i<2.0; i+=0.05)
		{
			setLED(i);
			waitMsec(30);
		}
		for (float i = 2.0; i > 0; i-=0.05)
		{
			setLED(i);
			waitMsec(30);
		}
	}
}

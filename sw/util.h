
#ifndef UTIL_H
#define UTIL_H
//==============================================================

#define ENUM_NAME(e) e, #e

#define MAX(a,b)              (((a) > (b)) ? (a) : (b))
#define MIN(a,b)              (((a) < (b)) ? (a) : (b))
#define ABS(a)                (((a) > 0)   ? (a) : -(a))
#define CLIP(a,min,max)       (((a) < (min)) ? (min) : (((a) > (max)) ? (max) : (a)))
#define RAND(min, max)        ((min) + (rand() % ((max)-(min)+1)))
#define MOD_INCREMENT(a, m)   (((a) + 1 < (m)) ? ((a) + 1) : (0))
#define MOD_DECREMENT(a, m)   (((a) > 0) ? ((a) - 1) : ((m) - 1))

#define BYTE(w, n)                  (((w) >> (n * 8)) & 0xFF)
#define MAKE_WORD(b3, b2, b1, b0)   ((((b3) & 0xFF) << 24) | (((b2) & 0xFF) << 16) | (((b1) & 0xFF) << 8) | ((((b0) & 0xFF) << 0)))

#endif
//==============================================================

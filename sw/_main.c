/*
 * _main.c
 * 
 * 	This is the main application that controls the ISP and other peripherals in the MIDASystem.
 *
 */

#define NOT_DEBUG 1

#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "stdio.h"
#include "sys/alt_irq.h"
#include "priv/alt_legacy_irq.h"

//**************************************************//
//				ISP INCLUDES						//
//**************************************************//

#include <time.h>
#include <unistd.h>

#include "astar.h"
#include "bit_io.h"
#include "bayer_interpolate.h"
#include "color_matrix.h"
#include "console.h"
#include "debug.h"
#include "gamma_lut.h"
#include "paxel_capture.h"
#include "stats.h"
#include "sys_mgr.h"
#include "sensor.h"
#include "util.h"
#include "white_bal.h"
#include "sensor.h"
#include "cursor_overlay.h"
#include "sensorInput.h"
#include "frame_buffer.h"
#include "hps_frame_buffer.h"
#include "image_gen.h"
#include "fpga_i2c0.h"
#include "fpga_i2c1.h"
#include "parallel_out.h"
#include "image_resizer.h"
#include "lcd_display_out.h"
#include "filter.h"
#include "defect_pixel.h"
#include "pwm_led.h"
#include "pix_correction.h"
#include "hps2fpga_comm.h"
#include "pll_reconfig.h"

//**** FPGA VERSION ********************************
// This must be changed for every release
static int MAJOR_VERSION = 0x03;
static int MINOR_VERSION = 0x01;
static int BUILD_NUMBER  = 0x00;
//**************************************************

sensor_type_t sensor  = OV6946;
static int scale_select = 1;
static int change_scale = 0;
static int powerUp = 0;
static int powerDown = 0;
volatile static int init_calibrate = 0;
static int baseline_scope_flash = 0;
static int touchscreen_up_arrow = 0;
static int touchscreen_down_arrow = 0;
static int agcOn = 1;
volatile static ButtonPress_t buttonPress = 0xFF;
static int deVersion = 0;
static int longPressFlag = 0;

volatile int edge_capture;
volatile int trig_capture;

enum {NORMAL, LOADING, STARTING};

#define START_CAL     0x40
#define VIDEO_CAPTURE 0x20
#define UPLED         0x10
#define DOWNLED       0x8
#define CLEAR_FLASH   0x4
#define BUTTON_FLG    0x2
#define SCALE         0x1

//******* Function prototypes **********************

void scale(int sel);

//**************************************************//

#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
static void handle_button_interrupts(void* context)
#else
static void handle_button_interrupts(void* context, alt_u32 id)
#endif
{
	volatile int* edge_capture_ptr = (volatile int*) context;
	*edge_capture_ptr = IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_0_BASE);

	if (*edge_capture_ptr & SCALE)   // scaleButton
	{
		change_scale = 1;
	}
	else if (*edge_capture_ptr & BUTTON_FLG)  // scope button pressed
	{
		if ((longPressFlag == 0) && (init_calibrate == 0))
			buttonPress = checkButtonFlag(sensor, deVersion);
		if(buttonPress == DECR_BRIGHTNESS_LONG_PRESS)
			init_calibrate = 1;
	}
	else if (*edge_capture_ptr & CLEAR_FLASH)
	{
		baseline_scope_flash = 1;
	}
	else if (*edge_capture_ptr & DOWNLED)   // downLED
	{
		touchscreen_down_arrow = 1;
	}
	else if (*edge_capture_ptr & UPLED)  // upLED
	{
		touchscreen_up_arrow = 1;
	}
	else if (*edge_capture_ptr & VIDEO_CAPTURE)  // videoCapture
	{
		// if video is active, then this button stops the video
		// else it starts the video
		if (startVideo == 1) {
			startVideo = 0;
		} else {
			startVideo = 1;
		}
	}
	else if (*edge_capture_ptr & START_CAL)   // start_calibration
	{
		init_calibrate = 1;
	}
	else if (*edge_capture_ptr & 0x80)   // touchscreen_scale
	{
		change_scale = 1;
	}

	// Reset the push button edge capture register
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_0_BASE, 0);
	IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_0_BASE);
}

//----------------------------------------------------------------------------------------

#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
static void trigger_interrupt(void* context)
#else
static void handle_button_interrupts(void* context, alt_u32 id)
#endif
{
	volatile int* trig_capture_ptr = (volatile int*) context;
	*trig_capture_ptr = IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_2_BASE);
	int state = IORD_ALTERA_AVALON_PIO_DATA(PIO_2_BASE);

	if (*trig_capture_ptr & 0x1)
	{
		if(state & 0x1) {
			powerUp = 1;
			setHandleState(CONNECTED);
		} else {
			powerDown = 1;
			setHandleState(NOT_CONNECTED);
		}
	}

	// If HOT PLUG DETECT is HIGH, turn ON redriver OEn
	if (*trig_capture_ptr & 0x2)
	{
		if(state & 0x1) {
			out(HDMI_REDRIVER_OEn, 0);
		} else {
			out(HDMI_REDRIVER_OEn, 1);
		}
	}



	// Reset the push button edge capture register
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_2_BASE, 0);
	IORD_ALTERA_AVALON_PIO_EDGE_CAP(PIO_2_BASE);
}

//----------------------------------------------------------------------------------------
void init_pio_interrupts()
{
	// Recast the edge_capture pointer to match the alt_irq_register() function prototype
	void* edge_capture_ptr = (void*) &edge_capture;
	void* trig_capture_ptr = (void*) &trig_capture;
	// Enable 2 interrupts
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(PIO_0_BASE, 0x3FF);
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(PIO_2_BASE, 0x3);
	// Reset the edge capture register
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_0_BASE, 0x00);
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PIO_2_BASE, 0x00);

	// Register the interrupt handler
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
    alt_ic_isr_register(PIO_0_IRQ_INTERRUPT_CONTROLLER_ID, PIO_0_IRQ,
      handle_button_interrupts, edge_capture_ptr, 0x0);
    alt_ic_isr_register(PIO_2_IRQ_INTERRUPT_CONTROLLER_ID, PIO_2_IRQ,
    		trigger_interrupt, trig_capture_ptr, 0x0);
#else
	alt_irq_register(PIO_0_BASE, edge_capture_ptr, (alt_isr_func)handle_button_interrupts);
#endif
}

//----------------------------------------------------------------------------------------

//**************************************************//
//				ISP FUNCTION PROTOTYPES				//
//**************************************************//

void mainBanner(void);
time_t systemTimestamp(void);

// The start color value is the first color and row of the CFA pattern
// Acceptable values
// 0: rggb
// 1: grbg
// 2: gbgr
// 3: bggr
void pipeStartcolor(int color)
{
	bayerIntStartcolor(color);
	whiteBalStartcolor(color);
	paxCaptureStartcolor(color);
}

void pipeMono(void)
{
	whiteBalBypass(1);
	bayerIntBypass(1, 1);
	colorMatrixBypass(1);
	gammaLutBypass(1);
}

void pipeColor(int bal, int mat, int gam)
{
	whiteBalBypass(0);
	bayerIntBypass(0, 0);
	colorMatrixBypass(0);
	gammaLutBypass(!gam);

	pipeStartcolor(3);

	if (bal)
		whiteBalSet(sensorDefaultBalance());
	else
		whiteBalNull();

	if (mat)
		colorMatrixSetup(sensorDefaultMatrix());
	else
		colorMatrixNull();
}

void pipeBypass(int bypass)
{
	colorMatrixBypass(bypass);	    // Color Matrix
	gammaLutBypass(bypass);		    // Gamma LUT
	whiteBalBypass(bypass);		    // White Balance
}


void scale(int sel)
{
	out(regUpdate, 1);
	imgResizerSetup(sensor, sel);
	parOutSetupLCD(sensor, sel, 0, 0, 0);
	setScaleReg(sel);
	out(regUpdate, 0);
}

//=======================================================================

void pipeInit(sensor_type_t sensor, int init, outPath_t outPath)
{
	int imgWidth;
	int imgHeight;
	if (sensor == OV6946) {
		imgWidth = 400;
		imgHeight = 400;
	} else {
		imgWidth = 200;
		imgHeight = 200;
	}

	initPll();
	initPot(0x5);
	sensorInputInit();
	sensorInputSetup();
	sensorCtlSetup();
	imageGenInit(imgWidth, imgHeight);
	initCorr(imgWidth);
	statsInit();
	statsSet(sensor, 1);
	filterInit(imgWidth, 100);
	paxCaptureInit();
	paxCaptureSetup(imgWidth, imgHeight);
	gammaLutInit();
//	gammaLutSetup(70);
	colorMatrixInit();
	bayerIntInit();
	bayerBufInit(imgWidth, 100);
	whiteBalInit();
	cursorOverlayInit();
	blinkerOn(3, 3, 0, imgHeight - 4);

    pipeColor(1, 1, 1);
    if (init == 1) {
		frameBufferInit(imgWidth, imgHeight, 1);
		hpsFrameBufferInit(imgWidth, imgHeight, 1);
#if NOT_DEBUG
		initHps2FpgaComm(MAJOR_VERSION, MINOR_VERSION, BUILD_NUMBER);
#endif
    } else {
    	frameBufferSetup(imgWidth, imgHeight, 2, 1);
		hpsFrameBufferSetup(imgWidth, imgHeight, 2, 1);
    }
	imgResizerSetup(sensor, scale_select);
    imgResizerHdmiSetup(outPath, sensor, 0);
	setScaleReg(1);
	parOutSetup(outPath, sensor, 0);
	parOutSetupLCD(sensor, scale_select, 0, 0, 0);

	astarInit(sensor);
	astarSetup(1);
}


time_t systemTimestamp(void)
{
	return symbolAddress("TIMESTAMP", 0);
}

void mainBanner(void)
{
	time_t qsysSwTimestamp = SYSID_QSYS_0_TIMESTAMP;
	time_t qsysHwTimestamp = QSYS_REG(SYSID_QSYS_0_BASE, 1);

	time_t sTimestamp = systemTimestamp();
//	time_t fTimestamp = flashTimestamp();

	printf("System ID: %08X\n", QSYS_REG(SYSID_QSYS_0_BASE, 0));
	printf("SW timestamp: %10d (%08X) = GMT %s\n", (int)qsysSwTimestamp, (int)qsysSwTimestamp, ctime(&qsysSwTimestamp));
	printf("HW timestamp: %10d (%08X) = GMT %s\n", (int)qsysHwTimestamp, (int)qsysHwTimestamp, ctime(&qsysHwTimestamp));
	printf("System  timestamp: %10d (%08X) = GMT %s\n", (int)sTimestamp, (int)sTimestamp, ctime(&sTimestamp));
//	printf("Flash   timestamp: %10d (%08X) = GMT %s\n", (int)fTimestamp, (int)fTimestamp, ctime(&fTimestamp));
	printf("Status: %04X\n", getStatus());
}

//**************************************************//


int main()
{


  int result  = 0;
  ErrorCode_t status = SUCCESS;
//  int handleStatus = 0;
  int frameCount = 0;
  int lastTime = -500;
  int printed = 0;
//  int ovlyStatus = 0;
  int faultCount = 0;
  int handleComsError = 0;
  int vidLossError = 0;
  uint8_t tag = 0;
  int lastAGC = 1;
//  int buttonTimeOut = 0;
  outPath_t outPath = HDMI;
//  int buttonInterval = 0;

  sysLedState(4);
  result += busInit(AVALON_SLAVE_BRIDGE_0_BASE);
  result += sysInit();
//  result += consoleInit(0, 0,  CON_NONBLOCK);  // Enables debug in the Nios Console
  result += consoleInit(1, (int)"/dev/uart_0", CON_NONBLOCK | CON_ECHO | CON_EDIT | CON_NOCHECK);


  printf("                       \n\n\n\n");
  printf("boot @ %d msec\n", getMsec());
  printf("Welcome to Enable Inc's ISP console\n");
  mainBanner();

  result += debugInit();

  outPath = inp(USER_DIPSW0);
  if (outPath == HDMI) {
	out(FX3_RESETn, 0);
  } else {
	out(FX3_RESETn, 1);
  }

  pipeInit(OV6946, 1, outPath);      // Initialize the ISP
  lcdOutSetup(0);        // Initialize LCD module
  setLCDmode(STARTING);
  init_pio_interrupts(); // Set up the interrupts
  startHpsRead(1);
  setScaleReg(1);

#if NOT_DEBUG
  while (passwordDone() == 0) {
	  waitMsec(100);
	  if (faultCount < 600) {
		  if (getOvlyState() == NOT_FINISHED)
			  faultCount++;
		  else
			  faultCount = 0;
	  } else {
		  setFaultLED(1);
	  }
  }
#endif

  printf("Entered password @ %d msec\n", getMsec());
  setLCDmode(LOADING);
  int state = IORD_ALTERA_AVALON_PIO_DATA(PIO_2_BASE);

  if (state & 0x1){  // Read the trigger signal (programmed to be set HIGH if present)
	  setHandleState(CONNECTED);
	  setScopeState2Hps(CONNECTED);
	  out(SCOPE_DETECT_LED, LED_ON);
//	  sensor = getSensorID();
	  sensor = OV6946;
	  if ((sensor == OV6946) || (sensor == OV6948)) {
		  if (sensor == OV6948) {
			  pipeInit(OV6948, 0, outPath);
		  }
		  astarRun(1);
		  sensorSetup(sensor);   // Power up and configure the OV426 bridge
		  deVersion = handleInit(1, sensor);         // Set the initial state of the LED on the handle
		  readModelNum();
		  if(sensor == OV6946) {
			  tag = getEEPROMTag();
			  if (tag == TAG_VALUE) {
				  rw_calibration_data(sensor);
				  pixCorBypass(0);
			  } else {
				  pixCorBypass(1);
			  }
		  }
		  tag = getWBTag();
		  if (tag == WBTAG_VALUE) {
			  readWBfromEEPROM();
		  }
		  calibrateBlankLevel();
		  setScopeInitialized2Hps(FINISHED);
		  setLCDmode(NORMAL);
		  sysLedState(status + 2);
		  powerUp   = 0;
	  }
	  else {
		  setFaultLED(2);
		  setErrorReg(LOSSOFVIDEO, ON);
		  printf("Invalid sensor type. Please re-insert scope.\n");
	  }
  } else {
	  setHandleState(NOT_CONNECTED);
	  setScopeState2Hps(NOT_CONNECTED);
	  out(SCOPE_DETECT_LED, LED_OFF);
	  setHDMImode(1);
	  sysLedState(3);
	  powerDown = 0;
  }

  // If HOT PLUG is high, then turn ON
  if (state & 0x2) {
		if (outPath == HDMI) {
			out(HDMI_REDRIVER_OEn, 0);
		} else {
			out(HDMI_REDRIVER_OEn, 1);
		}
  }
  //**************************************************//
  sysLedOn(1);
  init_calibrate = 0;
  start_calibration(0);
  printf("Main Loop...\n");
  while (1)
  {

    //**************************************************//
    //					MAIN LOOP   					//
    //**************************************************//

//	consoleWorker(0);  // Only if Nios Console is needed for debug (causes frequent crashes)
	consoleWorker(1);
	consoleWorker(-1);

	if(getHandleState() == NOT_CONNECTED)
	{
		setFaultLED(0);
		vidLossError = 0;
		handleComsError = 0;
		if(powerDown) {
			setScopeState2Hps(NOT_CONNECTED);
			setScopeInitialized2Hps(NOT_FINISHED);
			out(SCOPE_DETECT_LED, LED_OFF);
			setDEVersion(0);
			clearErrorReg();
			setLCDmode(2);
			handleInit(0, 0);
			setHDMImode(1);
			powerDown = 0;
		}
		if ((getMsec() - lastTime) > 500) {
			lastTime = getMsec();
			setScopeState2Hps(NOT_CONNECTED);
			clearErrorReg();
		}
	} else {
		if(powerUp) {
			setLCDmode(LOADING);
			setScopeState2Hps(CONNECTED);
			out(SCOPE_DETECT_LED, LED_ON);
//			sensor_type_t new_sensor = getSensorID();
			sensor_type_t new_sensor = OV6946;
			if ((new_sensor == OV6946) || (new_sensor == OV6948)) {
				if (sensor != new_sensor) {
					pipeInit(new_sensor, 0, outPath);
					sensor = new_sensor;
				}
				parOutSetupLCD(sensor, scale_select, 0, 0, 0 );
				if (sensor == OV6946) {
					astarRun(1);
				} else if (sensor == OV6948){
					astarRun(1);
				}
				sensorSetup(sensor);         // Power up and configure the OV426 bridge
				setSensorType(sensor);
				deVersion = handleInit(1, sensor);         // Set the initial state of the LED on the handle
                readModelNum();
                if(sensor == OV6946) {
                	tag = getEEPROMTag();
					if (tag == TAG_VALUE) {
						rw_calibration_data(sensor);
						pixCorBypass(0);
					} else {
						pixCorBypass(1);
					}
                }
				tag = getWBTag();
				if (tag == WBTAG_VALUE) {
				  readWBfromEEPROM();
				}
				calibrateBlankLevel();
				setScopeInitialized2Hps(FINISHED);
				setLCDmode(NORMAL);
			} else {
				setFaultLED(2);
				setErrorReg(LOSSOFVIDEO, ON);
				printf("Invalid sensor type. Please re-insert scope.\n");
				vidLossError = 1;
			}
			setHDMImode(0);
			powerUp = 0;
		}

		// Looking for up/down arrow press interrupt every iteration
		if (sensor == OV6946) {
			correctionWorker();
		}

		astarWorker(0, sensor);
		if (agcOn == 0) {
			if (touchscreen_up_arrow) {
				upLED();
				clearTriggerReg(1);
				touchscreen_up_arrow = 0;
			}
			if (touchscreen_down_arrow) {
				downLED();
				clearTriggerReg(2);
				touchscreen_down_arrow = 0;
			}
		}

		if ((getMsec() - lastTime) > 200) {
			hpsStatusReg();
//			handleStatus = checkStatus();
//			handleStatus = SUCCESS;
			agcOn = getAGCState();
			if (lastAGC != agcOn) {
				if (agcOn) {
					astarRun(1);
				} else {
					astarRun(0);
				}
			}

			if (init_calibrate == 1) {
				printf("Calibration initiated.\n");
				calibrate(sensor, 0x26);  // Assume it's a scope with NVRAM, so 0x26 version
				start_calibration(0);
				init_calibrate = 0;
			}
			if (baseline_scope_flash == 1) {
				if (manufMode() == 1) {
					baselineScopeFlash();
					printf("Baselined scope flash.\n");
				}
				baseline_scope_flash = 0;
			}
			if (setBatchReg() == 1) {
				if (manufMode() == 1) {
					setBatchNum();
					printf("Set Batch Num in Scope.\n");
				}
			}
			if (getBatchReg() == 1) {
				getBatchNum();
			}

			if (getClickBal() == 1) {
				clickBal(sensor);
				clearClickBal();
			}

			if (change_scale == 1) {
				scale_select = !scale_select;
				scale(scale_select);
				clearTriggerReg(0);
				change_scale = 0;
			}

			if (getROI() == 1) {
				setROI(getROIMode(), sensor);
			}

			if (setModelNum() == 1) {
				writeModelNum();
				clearSetModel();
			}

			if (getModelNum() == 1) {
				readModelNum();
				clearGetModel();
			}

			if(setOutputSel() == 1)
			{
				outPath = getOutputSel();
				imgResizerHdmiSetup(outPath, sensor, 0);
				parOutSetup(outPath, sensor, 0);
				if (outPath == HDMI) {
					out(outClkSelect, HDMI);
					out(HDMI_REDRIVER_OEn, 0);
					out(FX3_RESETn, 0);
				} else {
					out(outClkSelect, UVC);
					out(HDMI_REDRIVER_OEn, 1);
					out(FX3_RESETn, 1);
				}
			}
			if (frameCount == getFrameCount()) {
				if (IORD_ALTERA_AVALON_PIO_DATA(PIO_2_BASE) & 0x1) {
					setFaultLED(2);
					setErrorReg(LOSSOFVIDEO, ON);
					vidLossError = 1;
					if(!printed) {
						printf("Loss of video signal.\n");
						printed = 1;
					}
				} else {
					if(handleComsError == 0)
						setFaultLED(0);
					vidLossError = 0;
					setErrorReg(LOSSOFVIDEO, OFF);
					setHandleState(NOT_CONNECTED);
					setScopeState2Hps(NOT_CONNECTED);
				}
			} else {
				setErrorReg(LOSSOFVIDEO, OFF);
				vidLossError = 0;
				if ((status == SUCCESS) && (handleComsError == 0))
					setFaultLED(0);
			}
//			if (getError() || (handleStatus != SUCCESS)) {
//				if (IORD_ALTERA_AVALON_PIO_DATA(PIO_2_BASE) & 0x1) {
//					checkButtonFlag(sensor, agcOn);
//					if (getError()) {
//						setFaultLED(2);
//						handleComsError = 1;
//						setErrorReg(LOSSOFCOMM2HANDLE, ON);
//						printf("CRC ERROR: Unable to communicate with handle.\n");
//					}
//				} else {
//					if(vidLossError == 0)
//						setFaultLED(0);
//					handleComsError = 0;
//					setErrorReg(LOSSOFCOMM2HANDLE, OFF);
//					setHandleState(NOT_CONNECTED);
//					setScopeState2Hps(NOT_CONNECTED);
//				}
//			} else {
//				setErrorReg(LOSSOFCOMM2HANDLE, OFF);
//				setScopeState2Hps(CONNECTED);
//				handleComsError = 0;
//				if ((status == SUCCESS) && (vidLossError == 0))
//					setFaultLED(0);
//			}
			setBrightness();
			frameCount = getFrameCount();
			lastTime = getMsec();
			lastAGC = agcOn;
		}
	}


    //**************************************************//
  }

  return 0;
}


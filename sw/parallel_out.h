
#ifndef PARALLEL_OUT_H
#define PARALLEL_OUT_H
//==============================================================

#include "pipe.h"
#include "sensor.h"

typedef enum {HDMI, UVC} outPath_t;

int parOutInit(void);
void parOutSetup(outPath_t outPath, sensor_type_t sensor, int sel);
void parOutTest(int source, int mode, int r, int g, int b);
void setHDMImode(int source);

#endif
//==============================================================

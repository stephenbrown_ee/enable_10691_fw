
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "pipe.h"
#include "parallel_out.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"

//=======================================================================

#define PAR_OUT_RESET            BIT(bus_parHDMI, 0, 0)
#define PAR_OUT_SOURCE           FLD(bus_parHDMI, 0, 5, 4)
#define PAR_OUT_HORSCALE         FLD(bus_parHDMI, 0, 11, 8)
#define PAR_OUT_VERSCALE         FLD(bus_parHDMI, 0, 15, 12)
#define PAR_OUT_HORMAX           FLD(bus_parHDMI, 1, 11, 0)  // Last hor count in line
#define PAR_OUT_VERMAX           FLD(bus_parHDMI, 1, 27, 16) // Last ver count in frame
#define PAR_OUT_TESTDATAR        FLD(bus_parHDMI, 2, 7, 0)
#define PAR_OUT_TESTDATAG        FLD(bus_parHDMI, 2, 15, 8)
#define PAR_OUT_TESTDATAB        FLD(bus_parHDMI, 2, 23, 16)
#define PAR_OUT_VERVS0           FLD(bus_parHDMI, 3, 11, 0)   // First line of no VS
#define PAR_OUT_VERVS1           FLD(bus_parHDMI, 3, 27, 16)  // First line of active VS
#define PAR_OUT_VERDE0           FLD(bus_parHDMI, 4, 11, 0)   // First line of no DE
#define PAR_OUT_VERDE1           FLD(bus_parHDMI, 4, 27, 16)  // First line of active DE
#define PAR_OUT_VERPX0           FLD(bus_parHDMI, 5, 11, 0)   // First line of no FIFO reads
#define PAR_OUT_VERPX1           FLD(bus_parHDMI, 5, 27, 16)  // First line of FIFO reads
#define PAR_OUT_HORVS0           FLD(bus_parHDMI, 6, 11, 0)   // Hor pos of VS fall
#define PAR_OUT_HORVS1           FLD(bus_parHDMI, 6, 27, 16)  // Hor pos of VS rise
#define PAR_OUT_HORHS0           FLD(bus_parHDMI, 7, 11, 0)   // Hor pos of HS fall
#define PAR_OUT_HORHS1           FLD(bus_parHDMI, 7, 27, 16)  // Hor pos of HS rise
#define PAR_OUT_HORDE0           FLD(bus_parHDMI, 8, 11, 0)   // Hor pos of DE fall
#define PAR_OUT_HORDE1           FLD(bus_parHDMI, 8, 27, 16)  // Hor pos of DE rise
#define PAR_OUT_HORPX0           FLD(bus_parHDMI, 9, 11, 0)   // Hor pos of end of FIFO reads
#define PAR_OUT_HORPX1           FLD(bus_parHDMI, 9, 27, 16)  // Hor pos of start of FIFO reads

//=======================================================================

int parOutInit(void)
{
	int result = 0;

	regClear(bus_parHDMI, 8);

	REG_SET(PAR_OUT_RESET, 1);

	return result;
}

//-----------------------------------------------------------------------

void parOutSetup(outPath_t outPath, sensor_type_t sensor, int sel)
{
	int hSize, vSize, vFBlack;
	REG_SET(PAR_OUT_RESET, 1);

	ImageInfo imgInfo;
	ImageInfo* img = &imgInfo;

	if (outPath == HDMI) {
		if (sensor == OV6946) {
			hSize = 1000;
			vSize = 1000;
		}
		else if (sensor == OV6948) {
			hSize = 600;
			vSize = 600;
		}

		img->hSizeOut = hSize;
		img->vSizeOut = vSize;

		img->hFPorch = 88;
		img->hSync   = 44;
		img->hBPorch = 148;

		img->vFPorch = 4;
		img->vSync   = 5;
		img->vBPorch = 36;

		img->hLBlack = (1920 - img->hSizeOut) / 2;
		img->hRBlack = (1920 - img->hSizeOut) / 2;
		vFBlack  = (1080 - img->hSizeOut) / 2;
		img->vBBlack = (1080 - img->hSizeOut) / 2;

		REG_SET(PAR_OUT_HORPX1, (img->hLBlack) );
		REG_SET(PAR_OUT_HORPX0, (img->hLBlack + img->hSizeOut) );
		REG_SET(PAR_OUT_VERVS1, img->vSizeOut + vFBlack + img->vBBlack + img->vFPorch - 1);
		REG_SET(PAR_OUT_VERVS0, img->vSizeOut + vFBlack + img->vBBlack + img->vFPorch + img->vSync - 1);
	}
	else {
		if (sensor == OV6946) {
			if(sel) {
				hSize = 476;
				vSize = 476;
			}
			else {
				hSize = 400;
				vSize = 400;
			}
		}
		else if (sensor == OV6948) {
			if(sel == 2) {
				hSize = 476;
				vSize = 476;
			} else if (sel == 1){
				hSize = 200;
				vSize = 200;
			}
			else {
				hSize = 400;
				vSize = 400;
			}
		}
		img->hSizeOut = hSize;
		img->vSizeOut = vSize;

		// Line time is 31.769 usec
		img->hFPorch = 700;
		img->hSync   = 27;
		img->hBPorch = 700;

		// Frame time is 33.333 msec
		img->vFPorch = 1;
		img->vSync   = 567;
		img->vBPorch = 1;

		img->hLBlack = (640 - img->hSizeOut) / 2;
		img->hRBlack = (640 - img->hSizeOut) / 2;
		vFBlack  = (480 - img->hSizeOut) / 2;
		img->vBBlack = (480 - img->hSizeOut) / 2;
		REG_SET(PAR_OUT_HORPX1, (img->hLBlack) + 1 );
		REG_SET(PAR_OUT_HORPX0, (img->hLBlack + img->hSizeOut + 1) );
		REG_SET(PAR_OUT_VERVS1, img->vSizeOut + vFBlack + img->vBBlack + img->vFPorch + img->vSync + img->vBPorch - 1);
		REG_SET(PAR_OUT_VERVS0, img->vSizeOut + vFBlack + img->vBBlack + 1);
	}
	// Horizontal count starts with first pixel
	// Vertical count starts with first imager line

	REG_SET(PAR_OUT_HORMAX, (img->hLBlack + img->hSizeOut + img->hRBlack + img->hFPorch + img->hSync + img->hBPorch) - 1);
	REG_SET(PAR_OUT_VERMAX,                  img->vSizeOut + vFBlack + img->vBBlack + img->vFPorch + img->vSync + img->vBPorch - 1);

	REG_SET(PAR_OUT_HORDE1, 0);
	REG_SET(PAR_OUT_HORDE0, (img->hLBlack + img->hSizeOut + img->hRBlack) );

	REG_SET(PAR_OUT_HORHS1, (img->hLBlack + img->hSizeOut + img->hRBlack + img->hFPorch) );
	REG_SET(PAR_OUT_HORHS0, (img->hLBlack + img->hSizeOut + img->hRBlack + img->hFPorch + img->hSync) );


	REG_SET(PAR_OUT_HORVS1, (img->hLBlack + img->hSizeOut + img->hRBlack + img->hFPorch) );
	REG_SET(PAR_OUT_HORVS0, (img->hLBlack + img->hSizeOut + img->hRBlack + img->hFPorch) );

	REG_SET(PAR_OUT_VERDE1, 0);
	REG_SET(PAR_OUT_VERDE0, img->vSizeOut + img->vBBlack + vFBlack);

	REG_SET(PAR_OUT_VERPX1, vFBlack);
	REG_SET(PAR_OUT_VERPX0, img->vSizeOut + vFBlack);

	REG_SET(PAR_OUT_HORSCALE, 80);

	REG_SET(PAR_OUT_RESET, 0);
}

//-----------------------------------------------------------------------

void parOutTest(int source, int mode, int r, int g, int b)
{
	int n;

	REG_SET(PAR_OUT_SOURCE, source);

	if (source == 2)
	{
		for (n = 0; n < 4; n++)
		{
			switch (mode)
			{
				case 0:
					REG_SET(PAR_OUT_TESTDATAR, r);
					REG_SET(PAR_OUT_TESTDATAG, g);
					REG_SET(PAR_OUT_TESTDATAB, b);
					break;
				case 1:
					REG_SET(PAR_OUT_TESTDATAR, n * 1024);
					REG_SET(PAR_OUT_TESTDATAG, n * 1024);
					REG_SET(PAR_OUT_TESTDATAB, n * 1024);
					break;
			}
		}
	}
}

void setHDMImode(int source)
{
	REG_SET(PAR_OUT_SOURCE, source);
}

//=======================================================================

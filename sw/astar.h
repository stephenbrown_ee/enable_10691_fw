
#ifndef ASTAR_H
#define ASTAR_H
//==============================================================

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "debug.h"
#include "sys_mgr.h"
#include "console.h"
#include "pipe.h"
#include "stats.h"
#include "sensor.h"
#include "paxel_capture.h"
#include "white_bal.h"
#include "cursor_overlay.h"

int astarInit(sensor_type_t sensor);
void astarSetup(int interval);
void astarWorker(int run, sensor_type_t sensor);
void astarStop(void);
void astarRun(int ae);
void astarResume(void);
void clickBal(sensor_type_t sensor);
void recAE (int aeRange);
void setPixPerPax(int pixels);
void recAE (int aeRange);

#endif
//==============================================================

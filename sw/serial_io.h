
#ifndef SERIAL_IO_H
#define SERIAL_IO_H
//==============================================================

#include "bus_io.h"

#define I2C_SLV_RESET(sel)           BIT((sel), 0, 0)
#define I2C_SLV_ENABLE(sel)          BIT((sel), 0, 1)
#define I2C_SLV_CHANGE(sel)          FLD((sel), 0, 15, 8)
#define I2C_SLV_HOLD(sel)            FLD((sel), 0, 23, 16)
#define I2C_SLV_ADDRESS(sel)         FLD((sel), 0, 30, 24)

#define I2C_SLV_REG(sel, reg)        BUS_REG((sel), (reg))

#endif
//==============================================================

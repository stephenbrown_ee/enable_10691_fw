

#include <stdio.h>
#include <system.h>
#include <time.h>

#include "sys/alt_irq.h"
#include "sys/alt_alarm.h"
#include "sys/alt_flash.h"
#include "altera_avalon_pio_regs.h"

#include "console.h"
#include "debug.h"
#include "util.h"
#include "sys_mgr.h"
#include "fpga_i2c0.h"
#include "fpga_i2c1.h"
#include "sensorInput.h"
#include "lcd_display_out.h"
#include "sensor.h"
#include "hps2fpga_comm.h"

//=======================================================================

#define SYSTEM_CLOCK_FREQ     TIMER_1_FREQ
#define SYS_CLK_FREQ          ALT_CPU_FREQ

#define TIMESTAMP_TO_USEC(ts) (((ts) * 1000000) / TIMER_1_FREQ)
#define TIMESTAMP_TO_MSEC(ts) (((ts) * 1000)    / TIMER_1_FREQ)
#define USEC_TO_TIMESTAMP(us) (((us) * TIMER_1_FREQ) / 1000000)
#define MSEC_TO_TIMESTAMP(ms) (((ms) * TIMER_1_FREQ) / 1000)

//--------------------------------------------------------------

#define SYS_STATUS     FLD(bus_sysMgr, 0, 31, 28)
#define SYS_LED        BIT(bus_sysMgr, 0, 0)
#define SYS_RESET      FLD(bus_sysMgr, 0, 15, 8)

#define WATCHDOG_COUNT BUS_REG(bus_sysMgr, 1)
#define USEC_COUNTER   BUS_REG(bus_sysMgr, 2)
#define MSEC_COUNTER   BUS_REG(bus_sysMgr, 3)

//==============================================================


#define BIT_REV(a)   BUS_REG(bus_bitRev, (a))

//=======================================================================

#define TIMER_1_REG(reg)         QSYS_REG(TIMER_1_BASE, reg)


alt_u32 tickCallback(void* context);

//=======================================================================

static int ledState = 0;
static int ledCount = 0;
static alt_alarm tickAlarm;
static int hdmiState = 0;
static int handleState = 0;
static int crcError = 0;

#define HDMI_ON   1
#define HDMI_OFF  0

//=======================================================================

int sysInit(void)
{
	alt_alarm_start(&tickAlarm, 10, tickCallback, 0);

	return 0;
}

//--------------------------------------------------------------

ButtonPress_t checkButtonFlag (sensor_type_t sensor, int deVersion)
{
	int data = 0;
	int timeOutCnt = 0;
	int timeOutMax = 2;
	ButtonPress_t result;

	result = processDEButton(sensor, deVersion);
	data = IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);

	while (((data & 0x2) || (result == CRC_FAILURE)) && (timeOutCnt < timeOutMax))
	{
		result = processDEButton(sensor, deVersion);
		data = IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);
		timeOutCnt++;
	}

	if (timeOutCnt >= timeOutMax)
	{
		crcError = 1;
//		handleState = NOT_CONNECTED;
//		printf("Button Flag reset timed out. Reset system or connect handle.\n");
	} else {
		handleState = CONNECTED;
		crcError = 0;
	}

	return result;
}

//--------------------------------------------------------------

void reset(int hold)
{
	REG_SET(SYS_RESET, hold ? 0xA5 : 0x5A);
}

//-----------------------------------------------------------------------

void setHandleState(int state)
{
	handleState = state;
}

//-----------------------------------------------------------------------

int getHandleState(void)
{
	return handleState;
}
//-----------------------------------------------------------------------

int getButtons(void)
{
	return IORD_ALTERA_AVALON_PIO_DATA(PIO_2_BASE);
}

//-----------------------------------------------------------------------

int getError(void)
{
	return crcError;
}

//-----------------------------------------------------------------------

void setError(int state)
{
	crcError = state;
}

//-----------------------------------------------------------------------

unsigned int getUsec(void)
{
	return USEC_COUNTER;
}

//-----------------------------------------------------------------------

unsigned int getMsec(void)
{
	return MSEC_COUNTER;
}

//-----------------------------------------------------------------------

void waitUsec(unsigned int usec)
{
	unsigned int start = USEC_COUNTER;
	while (USEC_COUNTER - start < usec);
}

//-----------------------------------------------------------------------

void waitMsec(unsigned int msec)
{
	unsigned int start = MSEC_COUNTER;
	while (MSEC_COUNTER - start < msec);
}

//-----------------------------------------------------------------------

int sysClockFreq(void)
{
	return SYSTEM_CLOCK_FREQ;
}

//-----------------------------------------------------------------------

void setWatchdog(int msec)
{
	REG_SET(WATCHDOG_COUNT, msec);
}

//-----------------------------------------------------------------------

void setStatus(int status)
{
	REG_SET(SYS_STATUS, status);
}

//-----------------------------------------------------------------------

int getStatus(void)
{
	return REG_GET(SYS_STATUS);
}

//-----------------------------------------------------------------------

void sysLedState(int state)
{
	ledState = state;
}

//-----------------------------------------------------------------------

//void faultLedState(int state)
//{
//	int mask = IORD_ALTERA_AVALON_PIO_DATA(PIO_1_BASE) & 0xFD;
//	IOWR_ALTERA_AVALON_PIO_DATA(PIO_1_BASE, ((state & 1)<<1) | mask);
//}
//-----------------------------------------------------------------------

void getPioState (void)
{
	int state = IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);
	printf("PIO state: %x\n", state);
}

//-----------------------------------------------------------------------

void sysLedOn(int on)
{
	REG_SET(SYS_LED, on);
}

//-----------------------------------------------------------------------

int getHdmiState (void)
{
	return hdmiState;
}

//-----------------------------------------------------------------------

alt_u32 tickCallback(void* context)
{
	int mask = IORD_ALTERA_AVALON_PIO_DATA(PIO_1_BASE);
	// state 0 = continuous fast blink
	if (ledState == 0)
	{
		ledCount++;
		IOWR_ALTERA_AVALON_PIO_DATA(PIO_1_BASE, (ledCount & 1) | (mask & 0xFE));
		return 50;
	}

	// state 1+ slow grouped blinks
	ledCount = (ledCount + 1) % (ledState * 2);
	IOWR_ALTERA_AVALON_PIO_DATA(PIO_1_BASE, (ledCount & 1) | (mask & 0xFE));

	return (ledCount == 1) ? 700 : 100;
}

//-----------------------------------------------------------------------

char timeString[20];

char* ctime(const time_t* t)
{
	struct tm* stm = localtime(t);
	sprintf(timeString, "%02d:%02d:%02d %04d/%02d/%02d", stm->tm_hour, stm->tm_min, stm->tm_sec, 1900 + stm->tm_year, 1 + stm->tm_mon, stm->tm_mday);
	return timeString;
}


//=======================================================================

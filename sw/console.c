
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>

#include "console.h"
#include "debug.h"
#include "util.h"
#include "sys_mgr.h"

// SMALL_LIB is defined or not in Makefile
// ALT_CFLAGS := -DSMALL_LIB

//=======================================================================

void consoleWorkerFunction(int id);
int consoleCheckFunction(int id);
void consolePutcFunction(int id, char c);

//=======================================================================

#define LINE_SIZE   200
#define SAVE_LINES   16

static char lineSave[SAVE_LINES][LINE_SIZE];
static int linesSaved;
static int lineSaveCount = SAVE_LINES;

static char buffer[LINE_SIZE];
static int bufferCount;
static int escape;

static char debugLine[LINE_SIZE];

typedef struct
{
	ConsoleWorkerFunction workerFunction;
	ConsoleCheckFunction checkFunction;
	ConsolePutcFunction putcFunction;
	int flags;
	FILE* fd;
	char checkChar;
} ConsoleInfo;

#define MAX_CONSOLES 4

static ConsoleInfo console[MAX_CONSOLES];
static int activeConsole = -1;

//=======================================================================

int consoleInit(int id, int path, int flags)
{
	if (id >= MAX_CONSOLES)
		return -1;

	console[id].flags = flags;

	// Custom console will register its own functions
	if (path < 0)
		return 0;

#ifndef SMALL_LIB
	if (path != 0)
		console[id].fd = fopen((char*)path, "r+");
#endif

	if (console[id].flags & CON_NONBLOCK)
		fcntl((int)console[id].fd, F_SETFL, O_NONBLOCK);

	consoleRegister(id, consoleWorkerFunction, consoleCheckFunction, consolePutcFunction);

	return 0;
}

//-----------------------------------------------------------------------

void consoleRegister(int id, ConsoleWorkerFunction workerFunction, ConsoleCheckFunction checkFunction, ConsolePutcFunction putcFunction)
{
	if (id >= MAX_CONSOLES)
		return;

	console[id].workerFunction = workerFunction;
	console[id].checkFunction  = checkFunction;
	console[id].putcFunction   = putcFunction;
}

//-----------------------------------------------------------------------

void consoleSaveLine (char* line, int count)
{
	int n;

	if (count + 1 > sizeof(lineSave[0]))
		return;

	for (n = SAVE_LINES - 1; n > 0; n--)
		strcpy(lineSave[n], lineSave[n - 1]);

	memmove(lineSave[0], line, count);
	lineSave[0][count - 1] = 0;

	if (linesSaved < SAVE_LINES)
		linesSaved++;
}

//-----------------------------------------------------------------------

void consoleProcessChar(int id, char c)
{
	int n;
	static char last = 0;

	if (id != activeConsole)
	{
		int ac = activeConsole;
		activeConsole = -1;
		printf("\nACTIVE CONSOLE: %d > %d (%d)\n", ac, id, c);
		activeConsole = id;
	}

	//--- Escape sequences ---
	if (c == '\e')
	{
		escape = 1;
	}
	else if (escape == 1)
	{
		if (c == '[')
		{
			escape = 2;
		}
		else
			escape = 0;
    }
    else if (escape == 2)
    {
		//--- Up/Down arrow keys recall saved lines ---

		if (((c == 'A') || (c == 'B')) && (linesSaved > 0))
		{
			//--- Erase terminal line ---
			while (bufferCount > 0)
			{
				console[id].putcFunction(id, '\b');
				console[id].putcFunction(id, ' ');
				console[id].putcFunction(id, '\b');
				bufferCount--;
			}

			//--- Select saved line ---
			if (c == 'A')
				lineSaveCount = MOD_INCREMENT(lineSaveCount, linesSaved);
			else
				lineSaveCount = MOD_DECREMENT(lineSaveCount, linesSaved);

			//--- Show saved line ---
			bufferCount = strlen(lineSave[lineSaveCount]);
			for (n = 0; n < bufferCount; n++)
			{
				buffer[n] = lineSave[lineSaveCount][n];
				console[id].putcFunction(id, lineSave[lineSaveCount][n]);
			}
		}

		//--- Right arrow recalls all remaining characters from saved line ---
		else if ((c == 'C') && (linesSaved > 0))
		{
			while (bufferCount < strlen(lineSave[lineSaveCount]))
			{
				buffer[bufferCount] = lineSave[lineSaveCount][bufferCount];
				console[id].putcFunction(id, lineSave[lineSaveCount][bufferCount]);
				bufferCount++;
			}
		}

		//--- Left arrow erases line ---
		else if (c == 'D')
		{
			while (bufferCount > 0)
			{
				console[id].putcFunction(id, '\b');
				console[id].putcFunction(id, ' ');
				console[id].putcFunction(id, '\b');
				bufferCount--;
			}
		}
		escape = 0;
    }

	//--- Newline or return ---
    else if ((c == '\n') || (c == '\r'))
	{
    	// If both cr and lf are received, ignore the extra one
    	if (((c == '\n') && (last != '\r')) || ((c == '\r') && (last != '\n')))
    	{
			if ((console[id].flags & CON_ECHO) && (bufferCount > 0))
			{
				console[id].putcFunction(id, '\n');
				if (console[id].flags & CON_ADDCR)
					console[id].putcFunction(id, '\r');
			}

			buffer[bufferCount++] = '\n';
			buffer[bufferCount++] = 0;
			if ((bufferCount > 1) && (bufferCount < LINE_SIZE))
				consoleSaveLine(buffer, bufferCount);
			strcpy(debugLine, buffer);
			bufferCount = 0;
    	}
	}

	//--- Backspace ---
    else if ((c == '\b') && (bufferCount > 0) && (console[id].flags & CON_EDIT))
    {
		console[id].putcFunction(id, '\b');
		console[id].putcFunction(id, ' ');
		console[id].putcFunction(id, '\b');
		bufferCount--;
	}

	//--- Other characters ---
    else if (bufferCount + 1 < LINE_SIZE)
	{
		if (console[id].flags & CON_ECHO)
			console[id].putcFunction(id, c);

		buffer[bufferCount++] = c;
	}

	last = c;
}

//-----------------------------------------------------------------------

void consoleWorker(int id)
{
	if (id >= MAX_CONSOLES)
		return;

	else if (id < 0)
	{
		if (debugLine[0] != 0)
			debugProcessLine(debugLine);
		debugLine[0] = 0;
	}

	else if (console[id].workerFunction != 0)
		console[id].workerFunction(id);
}

//-----------------------------------------------------------------------

int consoleCheck(void)
{
	if (bufferCount > 0)
		return 1;

	int id;
	for (id = 0; id < MAX_CONSOLES; id++)
		if (((console[id].flags & CON_NOCHECK) == 0) && (console[id].flags & CON_NONBLOCK) && (console[id].checkFunction != 0))
			if (console[id].checkFunction(id))
				return 1;

	return 0;
}

//-----------------------------------------------------------------------

void consolePrintf(const char *format, ...)
{
	va_list args;
	char buffer[LINE_SIZE];

	va_start(args, format);
	vsnprintf(buffer, sizeof buffer, format, args);
	va_end(args);

	int id;
	for (id = 0; id < MAX_CONSOLES; id++)
		if (((activeConsole < 0) || (activeConsole == id)) && console[id].putcFunction != 0)
		{
			char* cp = buffer;
			while (*cp != 0)
			{
				console[id].putcFunction(id, *cp);
				if ((console[id].flags & CON_ADDCR) && (*cp == '\n'))
					console[id].putcFunction(id, '\r');
				cp++;
			}
		}
}

//-----------------------------------------------------------------------

void consoleWorkerFunction(int id)
{
	char c = 0;
	if (console[id].flags & CON_NONBLOCK)
	{
		if (console[id].checkChar != 0)
		{
			c = console[id].checkChar;
			console[id].checkChar = 0;
		}
		else if (console[id].fd == 0)
			read(0, &c, 1);
#ifndef SMALL_LIB
		else
			fread(&c, 1, 1, console[id].fd);
#endif
	}
	else
	{
		if (console[id].fd == 0)
			read(0, &c, 1);
#ifndef SMALL_LIB
		else
			fread(&c, 1, 1, console[id].fd);
#endif
	}

	if (c > 0)
		consoleProcessChar(id, c);
}

//-----------------------------------------------------------------------

void consolePutcFunction(int id, char c)
{
	if (console[id].fd == 0)
		putchar(c);
	else
		fputc(c, console[id].fd);
	waitUsec(150);
}

//-----------------------------------------------------------------------

int consoleCheckFunction(int id)
{
	if (console[id].checkChar > 0)
		return 1;

	char c = 0;
	if (console[id].fd == 0)
		read(0, &c, 1);
#ifndef SMALL_LIB
	else
		fread(&c, 1, 1, console[id].fd);
#endif

	if (c > 0)
	{
		console[id].checkChar = c;
		return 1;
	}

	return 0;
}

//=======================================================================


#ifndef FRAME_BUFFER_H
#define FRAME_BUFFER_H
//==============================================================

#define OUTFRAME_WIDTH       1920
#define OUTFRAME_HEIGHT      1080

int frameBufferInit(int imgWidth, int imgHeight, int silent);
void frameBufferSetup(int imgWidth, int imgHeight, int burstSize, int silent);

#endif
//==============================================================

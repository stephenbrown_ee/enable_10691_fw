
#ifndef COLOR_H
#define COLOR_H
//==============================================================

typedef enum
{
	R = 0,
	G = 1,
	B = 2,
	Y = 0,
	U = 1,
	V = 2
} ColorChannel;

typedef struct
{
	int s;
	int scale;
} Scalar;

typedef struct
{
	int c[3];
	int scale;
} Color;

typedef struct
{
	int k[3][3];
	int scale;
} Matrix;

//==============================================================

Color zeroColor;
Color unityColor;

Matrix zeroMatrix;
Matrix unityMatrix;
Matrix rgbToMonMatrix;
Matrix rgbToYuvMatrix;
Matrix yuvToRgbMatrix;

//==============================================================

Scalar makeScalar(int s, int scale);
Color  makeColor(int c0, int c1, int c2, int scale);
Matrix makeMatrix(int k00, int k01, int k02, int k10, int k11, int k12, int k20, int k21, int k22, int scale);

Matrix matrixByColor(Matrix ma, Color cb);
Matrix matrixByMatrix(Matrix ma, Matrix mb);

Color colorByScalar(Color ca, Scalar sb);
Color colorByColor(Color ca, Color cb);
Color colorTransform(Color ca, Matrix mb);

Matrix setMatrixStrength(Matrix ma, int strength);

#endif
//==============================================================

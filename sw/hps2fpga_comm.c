
#include "hps2fpga_comm.h"
#include "stats.h"
#include <time.h>


// These are the registers that communicate between the HPS and the FPGA
#define DE_VERSION                  FLD(bus_hps2fpga, 0, 31, 24)
#define FPGA_MAJOR_VERSION          FLD(bus_hps2fpga, 0, 23, 16)
#define FPGA_MINOR_VERSION          FLD(bus_hps2fpga, 0, 15, 8)
#define FPGA_BUILD_NUMBER           FLD(bus_hps2fpga, 0, 7, 0)
#define HPS_I2C_FAIL                BIT(bus_hps2fpga, 1, 0)
#define HPS_RTC_BATTERY_DEAD        BIT(bus_hps2fpga, 1, 1)
#define HPS_RTC_BATTERY_LOW         BIT(bus_hps2fpga, 1, 2)
#define HPS_UPDATED                 BIT(bus_hps2fpga, 1, 3)
#define HPS_OVLY_INIT_DONE          BIT(bus_hps2fpga, 1, 4)
#define HPS_IGNORE_ALPHA            BIT(bus_hps2fpga, 1, 5)
#define HPS_PASSWORD_DONE           BIT(bus_hps2fpga, 1, 6)
#define DE_BATCH_NUM                FLD(bus_hps2fpga, 1, 31, 16)
#define HPS_EXP_DATE_REG_LO         FLD(bus_hps2fpga, 2, 31, 0)
#define HPS_EXP_DATE_REG_HI         FLD(bus_hps2fpga, 3, 31, 0)
#define FPGA2HPS_INITIATE_READ      BIT(bus_hps2fpga, 4, 0)
#define FPGA2HPS_CAPTURE_IMG        BIT(bus_hps2fpga, 4, 1)
#define FPGA2HPS_CAPTURE_VIDEO      BIT(bus_hps2fpga, 4, 2)
#define FPGA2HPS_SCOPE_STATE        BIT(bus_hps2fpga, 4, 3)
#define TOGGLE_SCALE                BIT(bus_hps2fpga, 4, 4)
#define SCALE_STATUS                BIT(bus_hps2fpga, 4, 5)
#define MANUFACTURING_MODE          BIT(bus_hps2fpga, 4, 6)  // password is 6101965
#define SCOPE_INITIALIZED           BIT(bus_hps2fpga, 4, 7)
#define SET_SCOPE_TIME              BIT(bus_hps2fpga, 4, 8)
#define BASELINE_SCOPE_FLASH        BIT(bus_hps2fpga, 4, 9)
#define SET_BATCH_NUM               BIT(bus_hps2fpga, 4, 10)
#define GET_BATCH_NUM               BIT(bus_hps2fpga, 4, 11)
#define START_CALIBRATION           BIT(bus_hps2fpga, 4, 12)
#define SENSOR_TYPE                 BIT(bus_hps2fpga, 4, 13)
#define SET_AGC                     BIT(bus_hps2fpga, 4, 14)
#define LOSS_OF_VIDEO_REG           BIT(bus_hps2fpga, 4, 16)
#define LOSS_OF_COMM_2_HANDLE       BIT(bus_hps2fpga, 4, 17)
#define FAILURE_TO_UPDATE_DE_FLASH  BIT(bus_hps2fpga, 4, 18)
#define LOSS_OF_COMM_2_HPS_RTC      BIT(bus_hps2fpga, 4, 19)
#define EXPIRED_DE_REG              BIT(bus_hps2fpga, 4, 20)
#define BAD_LOT_NUMBER              BIT(bus_hps2fpga, 4, 21)
#define SET_MODEL_NUMBER            BIT(bus_hps2fpga, 4, 22)
#define GET_MODEL_NUMBER            BIT(bus_hps2fpga, 4, 23)
#define SET_OUTPUT_SEL              BIT(bus_hps2fpga, 4, 24)
#define OUTPUT_SEL                  BIT(bus_hps2fpga, 4, 25)
#define SET_REF_ANGLE               BIT(bus_hps2fpga, 4, 26)
#define SET_ROT_DIR                 BIT(bus_hps2fpga, 4, 27)
#define GET_ROT_DIR                 BIT(bus_hps2fpga, 4, 28)
#define FLAG_VIDEO_PRESS            BIT(bus_hps2fpga, 4, 29)
// registers 5 and 6 reserved,  MAXBFR not used
#define HPS_MAXBFR                  FLD(bus_hps2fpga, 5, 3, 0)
#define HPS_LED_STATE               FLD(bus_hps2fpga, 7, 7, 0)
#define ARROW_DOWN_LED              BIT(bus_hps2fpga, 7, 8)
#define ARROW_UP_LED                BIT(bus_hps2fpga, 7, 9)
#define CLICK_BAL                   BIT(bus_hps2fpga, 7, 10)
#define ROI_SET                     BIT(bus_hps2fpga, 7, 11)
#define ROI_MODE                    BIT(bus_hps2fpga, 7, 12)
#define LCD_BRIGHTNESS_DUTY         FLD(bus_hps2fpga, 7, 22, 16)
#define FPGA_LUM                    FLD(bus_hps2fpga, 8, 31, 24)
#define ROI_Y                       FLD(bus_hps2fpga, 8, 21, 11)
#define ROI_X                       FLD(bus_hps2fpga, 8, 10, 0)
#define ROI_HEIGHT                  FLD(bus_hps2fpga, 9, 21, 11)
#define ROI_WIDTH                   FLD(bus_hps2fpga, 9, 10, 0)
#define SCOPE_MODEL_NUM				FLD(bus_hps2fpga, 10, 16, 0)


//=======================================================================

static int prevState = 0;

//=======================================================================


int initHps2FpgaComm (int major, int minor, int build)
{
	regClear(bus_i2cMasMSP430, 7);
	REG_SET(FPGA_MAJOR_VERSION, major);
	REG_SET(FPGA_MINOR_VERSION, minor);
	REG_SET(FPGA_BUILD_NUMBER, build);
	REG_SET(SET_AGC, 1);  // Initial state is auto-exposure ON
	REG_SET(ROI_X, 0);
	REG_SET(ROI_Y, 0);
	REG_SET(ROI_WIDTH, 400);
	REG_SET(ROI_HEIGHT, 400);
	return 0;
}

ErrorCode_t getRTCData(uint32_t* exp_hi, uint32_t* exp_lo, int verbose)
{

	REG_SET(FPGA2HPS_INITIATE_READ, 1);

	int count = 0;

	while ((REG_GET(HPS_UPDATED) != 1) && (count < 100))
	{
		waitMsec(50);
		count++;
	}

	REG_SET(FPGA2HPS_INITIATE_READ, 0);
	if (count == 100)
	{
		setFaultLED(1);
		if(verbose)
			printf("HPS timeout on waiting for update.\n");
		return HPS_TIMEOUT;
	}

	*exp_hi = REG_GET(HPS_EXP_DATE_REG_HI);
	*exp_lo = REG_GET(HPS_EXP_DATE_REG_LO);
	if(verbose)
		printf("Current Time: %08x%08x\n", REG_GET(HPS_EXP_DATE_REG_HI), REG_GET(HPS_EXP_DATE_REG_LO));

	return SUCCESS;
}

ErrorCode_t hpsStatusReg(void)
{
//	printf("Checking HPS status...\n");
	if (REG_GET(HPS_I2C_FAIL))
	{
		setFaultLED(1);
//		printf("HPS I2C Failure.\n");
		return HPS_I2C_FAILURE;
	}
	else if (REG_GET(HPS_RTC_BATTERY_DEAD))
	{
		setFaultLED(1);
//		printf("Real-Time Clock Battery Dead.\n");
		return RTC_BATTERY_DEAD;
	}
	else if (REG_GET(HPS_RTC_BATTERY_LOW))
	{
		setFaultLED(1);
//		printf("Real-Time Clock Batter LOW.\n");
		return RTC_BATTERY_LOW;
	}
	return SUCCESS;
}

ErrorCode_t compareTime(uint32_t rtcDate_hi, uint32_t rtcDate_lo, uint32_t expDate_hi, uint32_t expDate_lo)
{
	struct tm rtc;
	struct tm de;
	time_t rtcTime;
	time_t deTime;
	double difference;
	uint32_t maxDifference = 3*3600;  // 3 hours in seconds

	printf("Comparing RTC and DE Time...\n");
	printf("DE Time: %08x%08x\n", expDate_hi, expDate_lo);
	printf("RTC Time: %08x%08x\n", rtcDate_hi, rtcDate_lo);

	int year = ((rtcDate_hi >> 28) & 0xF)*1000 + ((rtcDate_hi >> 24) & 0xF)*100 + ((rtcDate_hi >> 20) & 0xF)*10 + ((rtcDate_hi >> 16) & 0xF);
	rtc.tm_year =  year - 1900;
	int month = ((rtcDate_hi >> 12) & 0xF)*10 + ((rtcDate_hi >> 8) & 0xF);
	rtc.tm_mon  = month;
	int day = ((rtcDate_hi >> 4) & 0xF)*10 + ((rtcDate_hi >> 0) & 0xF);
	rtc.tm_mday = day;
	int hour = ((rtcDate_lo >> 28) & 0xF)*10 + ((rtcDate_lo >> 24) & 0xF);
	rtc.tm_hour = hour;
	int min = ((rtcDate_lo >> 20) & 0xF)*10 + ((rtcDate_lo >> 16) & 0xF);
	rtc.tm_min  = min;
	int sec = ((rtcDate_lo >> 12) & 0xF)*10 + ((rtcDate_lo >> 8) & 0xF);
	rtc.tm_sec  = sec;
	rtcTime = mktime(&rtc);

	year = ((expDate_hi >> 28) & 0xF)*1000 + ((expDate_hi >> 24) & 0xF)*100 + ((expDate_hi >> 20) & 0xF)*10 + ((expDate_hi >> 16) & 0xF);
	de.tm_year = year - 1900;
	month = ((expDate_hi >> 12) & 0xF)*10 + ((expDate_hi >> 8) & 0xF);
	de.tm_mon  = month;
	day = ((expDate_hi >> 4) & 0xF)*10 + ((expDate_hi >> 0) & 0xF);
	de.tm_mday = day;
	hour = ((expDate_lo >> 28) & 0xF)*10 + ((expDate_lo >> 24) & 0xF);
	de.tm_hour = hour;
	min = ((expDate_lo >> 20) & 0xF)*10 + ((expDate_lo >> 16) & 0xF);
	de.tm_min  = min;
	sec = ((expDate_lo >> 12) & 0xF)*10 + ((expDate_lo >> 8) & 0xF);
	de.tm_sec  = sec;
	deTime = mktime(&de);

	difference = difftime(rtcTime, deTime);
	printf("Time diff: %f\n", difference);

	if ((difference < 0) || (difference > maxDifference)) {
		setFaultLED(2);
		setErrorReg(EXPIREDDE, ON);
		printf("Expired scope.\n");
		return EXPIRED_DE;
	}

	return SUCCESS;
}

void captureImage(void)
{
	REG_SET(FPGA2HPS_CAPTURE_IMG, 1);
}

void captureVideo(int enable)
{
	REG_SET(FPGA2HPS_CAPTURE_VIDEO, enable);
}

void readcptr (void)
{
	printf("Image Capture Value: %d\n", REG_GET(FPGA2HPS_CAPTURE_IMG));
	printf("Video Capture Value: %d\n", REG_GET(FPGA2HPS_CAPTURE_VIDEO));
}

void setMaxHpsBfr (int max)
{
	REG_SET(HPS_MAXBFR, max);
}

void setLedState (int state)
{
	REG_SET(HPS_LED_STATE, state);
}

void setScopeState2Hps (int state)
{
	REG_SET(FPGA2HPS_SCOPE_STATE, state);
}

void setScopeInitialized2Hps (int state)
{
	REG_SET(SCOPE_INITIALIZED, state);
}

int getScopeInitialized2Hps (void)
{
	return REG_GET(SCOPE_INITIALIZED);
}

int getOvlyState (void)
{
	return (REG_GET(HPS_OVLY_INIT_DONE) & 0x1);
}

void setBrightness (void)
{
	int duty = REG_GET(LCD_BRIGHTNESS_DUTY);
	if (duty != prevState) {
		if (duty == 0) {
			setLCDBrightness(100);
		}
		else if (duty < 50) {
			setLCDBrightness(50);
		}
		else if (duty > 100) {
			setLCDBrightness(100);
		}
		else {
			setLCDBrightness(duty);
		}
	}
	prevState = duty;
}

void setErrorReg (int error, int set)
{

	switch (error) {
		case LOSSOFVIDEO:
			REG_SET(LOSS_OF_VIDEO_REG, set);
			break;
		case LOSSOFCOMM2HANDLE:
			REG_SET(LOSS_OF_COMM_2_HANDLE, set);
			break;
		case FAIL2UPDATEFLASH:
			REG_SET(FAILURE_TO_UPDATE_DE_FLASH, set);
			break;
		case LOSSOFCOMM2HPSRTC:
			REG_SET(LOSS_OF_COMM_2_HPS_RTC, set);
			break;
		case EXPIREDDE:
			REG_SET(EXPIRED_DE_REG, set);
			break;
		case BADLOTNUMBER:
			REG_SET(BAD_LOT_NUMBER, set);
			break;
		default: printf("Invalid error state. \n"); break;
	}

}

void clearErrorReg (void)
{
	REG_SET(LOSS_OF_VIDEO_REG, 0);
	REG_SET(LOSS_OF_COMM_2_HANDLE, 0);
	REG_SET(FAILURE_TO_UPDATE_DE_FLASH, 0);
	REG_SET(LOSS_OF_COMM_2_HPS_RTC, 0);
	REG_SET(EXPIRED_DE_REG, 0);
	REG_SET(BAD_LOT_NUMBER, 0);
}
void clearTriggerReg (int reg)
{
	if (reg == 0) {
		REG_SET(TOGGLE_SCALE, 0);
	} else if (reg == 1) {
		REG_SET(ARROW_UP_LED, 0);
	} else if (reg == 2) {
		REG_SET(ARROW_DOWN_LED, 0);
	}

}

void setTriggerReg (int reg)
{
	if (reg == 0) {
		REG_SET(TOGGLE_SCALE, 1);
	} else if (reg == 1) {
		REG_SET(ARROW_UP_LED, 1);
	} else if (reg == 2) {
		REG_SET(ARROW_DOWN_LED, 1);
	}

}

void setScaleReg (int reg)
{
	REG_SET(SCALE_STATUS, reg);
}

int getScaleReg (void)
{
	return REG_GET(SCALE_STATUS);
}

void setDEVersion (int byte)
{
	REG_SET(DE_VERSION, byte);
}

int getDEVersion (void)
{
	return (REG_GET(DE_VERSION));
}

void start_calibration (int set)
{
	REG_SET(START_CALIBRATION, set);
}

int passwordDone (void)
{
	return REG_GET(HPS_PASSWORD_DONE);
}

int manufMode (void)
{
	return REG_GET(MANUFACTURING_MODE);
}

void trigger_scope_baseline(int set)
{
	REG_SET(BASELINE_SCOPE_FLASH, set);
}

void get_batch_bit (int set)
{
	REG_SET(GET_BATCH_NUM, set);
}

void set_batch_bit (int set)
{
	REG_SET(SET_BATCH_NUM, set);
}

void setHpsBatchNumReg (uint16_t batchNum)
{
	REG_SET(DE_BATCH_NUM, batchNum);
}

uint16_t getHpsBatchNumReg (void)
{
	return REG_GET(DE_BATCH_NUM);
}

void hpsReg(void)
{
	printf("set batch: %x\n", REG_GET(SET_BATCH_NUM));
	printf("get batch: %x\n", REG_GET(GET_BATCH_NUM));
}

void setSensorType (int sensor_type) {
	REG_SET(SENSOR_TYPE, sensor_type);
}

int getAGCState(void) {
	return REG_GET(SET_AGC);
}

void setAGCState (int state) {
	REG_SET(SET_AGC, state);
}

void setHpsLum(int lum) {
	REG_SET(FPGA_LUM, lum);
}

int setBatchReg(void)
{
	return REG_GET(SET_BATCH_NUM);
}

int getBatchReg(void)
{
	return REG_GET(GET_BATCH_NUM);
}

void ignoreAlpha (int set) {
	REG_SET(HPS_IGNORE_ALPHA, set);
}

int getClickBal(void)
{
	return REG_GET(CLICK_BAL);
}

void clearClickBal(void)
{
	REG_SET(CLICK_BAL, 0);
}

int getROIMode(void)
{
	return REG_GET(ROI_MODE);
}

int getROI (void)
{
	return REG_GET(ROI_SET);
}

void setROI(int mode, sensor_type_t sensor)
{
	uint16_t x, y, w, h;
	int sensorXY = (sensor == OV6948) ? 200 : 400;

	if (mode == 1) {
		x = REG_GET(ROI_X);
		y = REG_GET(ROI_Y);
		w = REG_GET(ROI_WIDTH);
		h = REG_GET(ROI_HEIGHT);
		// Make numbers even to calculate the correct average
		if(x%2 != 0) {
			x = x - 1;
		}
		if(y%2 != 0) {
			y = y - 1;
		}
		if(w%2 != 0) {
			w = w - 1;
		}
		if(h%2 != 0) {
			h = h - 1;
		}
		printf("ROI:\nx = %d\ny = %d\nw = %d\nh = %d\n", x, y, w, h);
		setAEwindow(x,y,w,h, sensor);
	}
	else
	{
		REG_SET(ROI_X, 0);
		REG_SET(ROI_Y, 0);
		REG_SET(ROI_WIDTH, sensorXY);
		REG_SET(ROI_HEIGHT, 400);
		setAEwindow(0, 0, sensorXY, sensorXY, sensor);
	}
	REG_SET(ROI_SET, 0);  // clear the set bit, so condition can be met again
}

int getModelNum(void)
{
	return REG_GET(GET_MODEL_NUMBER);
}

void clearGetModel(void)
{
	REG_SET(GET_MODEL_NUMBER, 0);
}

void setModel(void)
{
	REG_SET(SET_MODEL_NUMBER, 1);
}

int setModelNum(void)
{
	return REG_GET(SET_MODEL_NUMBER);
}

void clearSetModel(void)
{
	REG_SET(SET_MODEL_NUMBER, 0);
}

void setModelNumReg (int modelNum)
{
	REG_SET(SCOPE_MODEL_NUM, modelNum);
}

int getModelNumReg(void)
{
	return REG_GET(SCOPE_MODEL_NUM);
}

void testOutputSel()
{
	REG_SET(SET_OUTPUT_SEL, 1);
}

int setOutputSel()
{
	return REG_GET(SET_OUTPUT_SEL);
}

int getOutputSel()
{
	REG_SET(SET_OUTPUT_SEL, 0);
	return REG_GET(OUTPUT_SEL);
}

void outputSel(int sel)
{
	REG_SET(OUTPUT_SEL, sel);
	testOutputSel();
}
void flagVideoPressToHps(int set)
{
	REG_SET(FLAG_VIDEO_PRESS, set);
}
//=======================================================================

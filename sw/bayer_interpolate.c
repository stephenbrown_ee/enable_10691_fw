
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "bayer_interpolate.h"

//=======================================================================

#define LIN_BFR_RESET            BIT(bus_bayBfr, 0, 0)
#define LIN_BFR_BYPASS           BIT(bus_bayBfr, 0, 1)
#define LIN_BFR_HCOUNT			 FLD(bus_bayBfr, 1, 15, 0)
#define LIN_BFR_WAITCNTMAX       FLD(bus_bayBfr, 1, 25, 16)

#define BAY_INT_RESET            BIT(bus_bayInt, 0, 0)
#define BAY_INT_BYPASS           BIT(bus_bayInt, 0, 1)
#define BAY_INT_SIMPLE           BIT(bus_bayInt, 0, 2)
#define BAY_INT_STARTCOLOR       FLD(bus_bayInt, 0, 5, 4)

//=======================================================================

void bayerBufInit(int Hpix, int wait)
{
	REG_SET(LIN_BFR_HCOUNT, (0xFFFF & Hpix));
	REG_SET(LIN_BFR_WAITCNTMAX, wait);
}

//-----------------------------------------------------------------------

int bayerIntInit(void)
{
	regClear(bus_bayBfr, 8);
	regClear(bus_bayInt, 8);

	REG_SET(LIN_BFR_RESET, 1);
	REG_SET(BAY_INT_RESET, 1);

	REG_SET(LIN_BFR_BYPASS, 0);
	REG_SET(BAY_INT_BYPASS, 0);
	REG_SET(BAY_INT_SIMPLE, 0);
	REG_SET(BAY_INT_STARTCOLOR, 0);

	REG_SET(BAY_INT_RESET, 0);
	REG_SET(LIN_BFR_RESET, 0);

	return 0;
}

//-----------------------------------------------------------------------

void bayerIntBypass(int buffer, int bayer)
{
	REG_SET(LIN_BFR_BYPASS, buffer);
	REG_SET(BAY_INT_BYPASS, bayer);
}

//-----------------------------------------------------------------------

void bayerIntSimple(int simple)
{
	REG_SET(BAY_INT_SIMPLE, simple);
}

//-----------------------------------------------------------------------

void bayerIntStartcolor(int color)
{
	REG_SET(BAY_INT_STARTCOLOR, color);
}

//=======================================================================

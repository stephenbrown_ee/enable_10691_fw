

#include "fpga_i2c0.h"

#define ADV7513_I2C_ADDRESS  0x72

#define BIT0 (1<<0)
#define BIT1 (1<<1)
#define BIT2 (1<<2)
#define BIT3 (1<<3)
#define BIT4 (1<<4)
#define BIT5 (1<<5)
#define BIT6 (1<<6)
#define BIT7 (1<<7)

void i2cSet_adv7513(uint8_t reg, uint8_t dat);

typedef struct
{
	uint8_t reg;
	uint8_t dat;
} RegisterData_t;

//----------------------------------------------------------------------------------------
// Initialization for ADV7513 - DVI mode RGB 444 (https://ez.analog.com/thread/44408)
// Don't use Analog Devices application note 1270.  There are errors.
RegisterData_t hdmi_regDat [] =
{
		{	0x01,	0x00 },  // Set N Value
		{	0x02,	0x18 },  // Set N Value
		{   0x03,   0x00 },  // Set N Value
		{   0x15,   0x00 },  // Input 444 with separate syncs
		{	0x16,	0x60 },  // 44.1kHz fs
		{	0x18,	0x46 },  // CSC disabled
		{   0x41,   0x10 },  // Power Down control
		{   0x49,   0xa8 },  // Set Dither_mode
		{   0x4c,   0x00 },  // 8 bit output
		{   0x55,   0x00 },  // Set RGB444 in AVinfo frame
		{   0x55,   0x08 },  // Set active format aspect
		{   0x96,   0x20 },  // HPD interrupt clear
		{   0x98,   0x03 },  // ADI required write
		{   0x99,   0x02 },  // ADI required write

		{   0x9c,   0x30 },  // ADI required write
		{   0x9d,   0x61 },  // Set clock divide
		{   0xa2,   0xa4 },  // ADI required write
		{   0xa3,   0xa4 },  // ADI required write
		{   0xa5,   0x44 },  // ADI required write

		{   0xaf,   0x14 },  // Set DVI mode
		{   0xba,   0xA0 },  // Adjust clock delay
		{   0xd1,   0xff },  // ADI recommended write
		{   0xe4,   0x60 },  // ADI required write

		{   0xfa,   0x7d },  // Nbr of times to search for good phase

	{ 0, 0 }
};

//---------------------------------------------------------------------------------------

void initADV7513(void)
{

	int n;
		for (n = 0; hdmi_regDat[n].reg != 0; n++)
			i2cSet_adv7513(hdmi_regDat[n].reg, hdmi_regDat[n].dat);
}


void i2cSet_adv7513(uint8_t reg, uint8_t dat)
{
	int baud = 50000;
	i2c(bus_i2cMas, baud, 1, 1, 0, 0, 0, ADV7513_I2C_ADDRESS, 0);
	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 0, reg >> 0, 0);
	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 1, (dat >> 0) & 0xFF, 0);
}

int i2cGet_adv7513(uint8_t reg)
{
	unsigned char value;

	unsigned int baud = 50000;
	i2c(bus_i2cMas, baud, 1, 1, 0, 0, 0, ADV7513_I2C_ADDRESS, 0);
	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 0, reg >> 0, 0);
	i2c(bus_i2cMas, baud, 1, 1, 0, 0, 0, (ADV7513_I2C_ADDRESS + 1), 0);
	i2c(bus_i2cMas, baud, 0, 0, 1, 1, 1, 0x00, &value);
	return value;
}

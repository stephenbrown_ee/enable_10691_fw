

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "console.h"
#include "debug.h"
#include "util.h"
#include "sys_mgr.h"

//==============================================================

extern char _binary_symbols_txt_start;

// Compacted function symbol table is linked in absolute .symbols section
static char*  symbolTable = &_binary_symbols_txt_start;

typedef int (*FuncPtr)(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11, int a12, int a13, int a14);

int executeLine(const char* line);
int debugHandlerInt(const char* line);

static char lastLine[2][100];
static int toggleState = 2;
static int logOn = 0;

static DebugHandler debugHandler = 0;

//==============================================================

UINT debugEvaluate(const char* string)
{
	UINT arg;

	// Symbol
	if ((arg = symbolAddress(string, 0)) != 0)
		return arg;

	// String argument
	if (string[0] == '$')
		return (int)&string[1];

	// Decimal argument
	if (string[0] == '=')
		return strtol(&string[1], 0, 10);

	// Hex argument
	return strtoul(string, 0, 16);
}

//--------------------------------------------------------------

int debugInit(void)
{

	return 0;
}

//--------------------------------------------------------------

void debugProcessLine(char* line)
{
	if (toggleState < 2)
	{
		if ((line[0] == '\n') || (line[0] == '\r'))
		{
			printf("%s", lastLine[toggleState]);
			executeLine(lastLine[toggleState]);
			toggleState = 1 - toggleState;
			return;
		}
		else
			toggleState = 2;
	}

	executeLine(line);

	if (!strstr(line, "repeat") && !strstr(line, "toggle"))
	{
		strcpy(lastLine[1], lastLine[0]);
		strcpy(lastLine[0], line);
	}
}

//-----------------------------------------------------------------------

int executeLine(const char* line)
{
	char args[16][50];
	unsigned int  arg[15];
	FuncPtr func = 0;
	unsigned int  result;
	unsigned int start;
	int n;

	for (n = 0; n <= 15; n++)
		args[n][0] = 0;

	// Parse line, copy substrings to args array.
	const char* ptr = line;
	n = 0;
	int c = 0;
	while (*ptr != 0)
	{
		while(isspace(*ptr))
			ptr++;

		while (!isspace(*ptr) && (*ptr != 0))
			args[n][c++] = *ptr++;

		args[n][c] = 0;
		n++;
		c = 0;

		if (n > 15)
		{
			printf("Too many arguments\n");
			break;
		}
	}

	// First arg should be a function name.
	if (args[0][0] == 0)
	{
		return debugHandlerInt(line);
	}

	func = (FuncPtr)symbolAddress(args[0], 0);

	if (func == 0)
	{
		if (debugHandlerInt(line) == 0)
			return 0;
		printf("UNDEFINED: (%s) %d\n", line, line[0]);
		return 1;
	}
	else
	{
		for (n = 1; n <= 14; n++)
			arg[n] = debugEvaluate(args[n]);

		start = getUsec();

		result = func(arg[1], arg[2], arg[3], arg[4], arg[5], arg[6], arg[7], arg[8], arg[9], arg[10], arg[11], arg[12], arg[13], arg[14]);

		start = getUsec() - start;
	}

	printf("(%d usec) result = %X\n", start, result);

	return 1;
}

//-----------------------------------------------------------------------

int debugHandlerInt(const char* line)
{
	if (line[0] == 'q')
		logOn = 0;
	else if (line[0] == 'w')
		logOn = 1;
	else
	{
		if (debugHandler != 0)
			return debugHandler(line);
		return -1;
	}

	return 0;
}

//-----------------------------------------------------------------------

void debugSetHandler(DebugHandler handler)
{
	debugHandler = handler;
}

//-----------------------------------------------------------------------

int debugLog(int topic)
{
	return logOn;
}

//-----------------------------------------------------------------------

void repeat(int count, int msec)
{
	int n;
	for (n = 0; (n < count) || (count == 0); n++)
	{
		executeLine(lastLine[0]);
		waitMsec(msec);
		if (consoleCheck())
			break;
	}
}

//-----------------------------------------------------------------------

void toggle(void)
{
	printf("%s", lastLine[1]);
	executeLine(lastLine[1]);
	toggleState = 0;
}

//-----------------------------------------------------------------------

void stop(void)
{
	toggleState = 2;
}

//--------------------------------------------------------------

// Search symbolTable and return address if found
unsigned int symbolAddress(const char* symbol, const char* table)
{
	if (table == 0)
		table = symbolTable;

	char string[50];

	// Add leading space, trailing colon so we don't find embedded strings
	sprintf(string, " %s:", symbol);

	char* ptr = strstr(table, string);
	if (ptr == 0)
		return 0;

	// Advance to start of address
	ptr += strlen(string);

	return strtoul(ptr, 0, 16);
}

//-----------------------------------------------------------------------

UINT debugGet (UINT address, UINT size)
{
  if (size == 1)
    return *((UCHAR*) address);
  else if (size == 2)
    return *((USHORT*) address);
  else
    return *((UINT*) address);
}

//-----------------------------------------------------------------------

void debugSet (UINT address, UINT size, UINT value)
{
  if (size == 1)
    *((UCHAR*) address) = value;
  else if (size == 2)
    *((USHORT*) address) = value;
  else
    *((UINT*) address) = value;
}

//-----------------------------------------------------------------------

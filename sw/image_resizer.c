
#include "image_resizer.h"
#include "parallel_out.h"


#define IMG_RSZ_RESET            BIT(bus_imgRsz, 0, 0)
#define IMG_RSZ_TEST             BIT(bus_imgRsz, 0, 1)
#define IMG_RSZ_ENABLE           BIT(bus_imgRsz, 0, 2)
#define IMG_RSZ_IMGHORMAXOUT     FLD(bus_imgRsz, 1, 10, 0)   // horizontal pixel count + 1
#define IMG_RSZ_IMGVERMAXOUT     FLD(bus_imgRsz, 1, 21, 11)  // vertical pixel count - 1
#define IMG_RSZ_IMGHORMAXINP     FLD(bus_imgRsz, 2, 10, 0)
#define IMG_RSZ_IMGVERMAXINP     FLD(bus_imgRsz, 2, 21, 11)
#define IMG_RSZ_ROW_COEFF		 FLD(bus_imgRsz, 3, 10, 0)
#define IMG_RSZ_COL_COEFF        FLD(bus_imgRsz, 3, 20, 10)
#define IMG_RSZ_HORMAXOUT        FLD(bus_imgRsz, 4, 15, 0)
#define IMG_RSZ_VERMAXOUT        FLD(bus_imgRsz, 4, 31, 16)
#define IMG_RSZ_MODMAX			 FLD(bus_imgRsz, 5, 2, 0)


#define IMG_RSZ_HDMI_RESET            BIT(bus_imgRsz_hdmi, 0, 0)
#define IMG_RSZ_HDMI_TEST             BIT(bus_imgRsz_hdmi, 0, 1)
#define IMG_RSZ_HDMI_ENABLE           BIT(bus_imgRsz_hdmi, 0, 2)
#define IMG_RSZ_HDMI_IMGHORMAXOUT     FLD(bus_imgRsz_hdmi, 1, 10, 0)   // horizontal pixel count + 1
#define IMG_RSZ_HDMI_IMGVERMAXOUT     FLD(bus_imgRsz_hdmi, 1, 21, 11)  // vertical pixel count - 1
#define IMG_RSZ_HDMI_IMGHORMAXINP     FLD(bus_imgRsz_hdmi, 2, 10, 0)
#define IMG_RSZ_HDMI_IMGVERMAXINP     FLD(bus_imgRsz_hdmi, 2, 21, 11)
#define IMG_RSZ_HDMI_ROW_COEFF		  FLD(bus_imgRsz_hdmi, 3, 10, 0)
#define IMG_RSZ_HDMI_COL_COEFF        FLD(bus_imgRsz_hdmi, 3, 20, 10)
#define IMG_RSZ_HDMI_HORMAXOUT        FLD(bus_imgRsz_hdmi, 4, 15, 0)
#define IMG_RSZ_HDMI_VERMAXOUT        FLD(bus_imgRsz_hdmi, 4, 31, 16)
#define IMG_RSZ_HDMI_MODMAX			  FLD(bus_imgRsz_hdmi, 5, 2, 0)

void imgResizerSetup(sensor_type_t sensor, int sel)
{
	ImageInfo imgInfo;
	ImageInfo* img = &imgInfo;
	int hTotalClocks;
	int vTotalLines;
	if (sensor == OV6946) {
		img->vSize = 400;
		img->hSize = 400;
		if (sel)
		{
			img->hSizeOut = 768;
			img->vSizeOut = 768;
			hTotalClocks = 800;
			vTotalLines  = 2 + img->vSizeOut;
		} else {
			img->hSizeOut = 400;
			img->vSizeOut = 400;
			hTotalClocks = 450;
			vTotalLines  = 2 + img->vSizeOut;
		}
	} else {
		img->vSize = 200;
		img->hSize = 200;
		if (sel)
		{
			img->hSizeOut = 600;
			img->vSizeOut = 600;
			hTotalClocks = 650;
			vTotalLines  = 2 + img->vSizeOut;
		} else {
			img->hSizeOut = 400;
			img->vSizeOut = 400;
			hTotalClocks = 450;
			vTotalLines  = 2 + img->vSizeOut;
		}
	}
	// The calculations for the timing are done in a separate Excel file (timing.xlsx).
	// Basically the output frame time must match the input frame time, excluding the vertical blanking.

	REG_SET(IMG_RSZ_IMGHORMAXOUT, img->hSizeOut);
	REG_SET(IMG_RSZ_IMGVERMAXOUT, img->vSizeOut);
	REG_SET(IMG_RSZ_IMGHORMAXINP, img->hSize);
	REG_SET(IMG_RSZ_IMGVERMAXINP, img->vSize);
	int colCoeff = (int)262144/img->hSize;
	int rowCoeff = (int)262144/img->vSize;
	REG_SET(IMG_RSZ_COL_COEFF, colCoeff);
	REG_SET(IMG_RSZ_ROW_COEFF, rowCoeff);
	REG_SET(IMG_RSZ_HORMAXOUT, hTotalClocks - 1);
	REG_SET(IMG_RSZ_VERMAXOUT, vTotalLines - 1);
	REG_SET(IMG_RSZ_MODMAX, 0);
	REG_SET(IMG_RSZ_TEST, 0);
	REG_SET(IMG_RSZ_ENABLE, 1);
	REG_SET(IMG_RSZ_RESET, 1);
	REG_SET(IMG_RSZ_RESET, 0);

}

void resizerReset (int enable)
{
	REG_SET(IMG_RSZ_RESET, enable);
}


void horClocks (int horClocks)
{
	out(regUpdate, 1);
	REG_SET(IMG_RSZ_HORMAXOUT, horClocks - 1);
	out(regUpdate, 0);
}

void vLines (int lines)
{
	REG_SET(IMG_RSZ_VERMAXOUT, lines);
}

void mod (int line)
{
	REG_SET(IMG_RSZ_MODMAX, line);
}

void rszTest (int enable)
{
	REG_SET(IMG_RSZ_TEST, enable);
}

void imgResizerHdmiSetup(outPath_t outPath, sensor_type_t sensor, int sel)
{
	ImageInfo imgInfo;
	ImageInfo* img = &imgInfo;
	int hTotalClocks;
	int vTotalLines;

	if (outPath == HDMI) {
		if (sensor == OV6946) {
			img->vSize = 400;
			img->hSize = 400;
			img->hSizeOut = 1000;
			img->vSizeOut = 1000;
		} else {
			img->vSize = 200;
			img->hSize = 200;
			img->hSizeOut = 600;
			img->vSizeOut = 600;
		}
		hTotalClocks = img->hSizeOut + 50;
		vTotalLines  = 2 + img->vSizeOut;
		REG_SET(IMG_RSZ_HDMI_MODMAX, 2);
	}
	else {
		if (sensor == OV6946) {
			img->vSize = 400;
			img->hSize = 400;
			if (sel)
			{
				img->hSizeOut = 476;
				img->vSizeOut = 476;
				hTotalClocks = 600;
				vTotalLines  = 2 + img->vSizeOut;
			} else {
				img->hSizeOut = 400;
				img->vSizeOut = 400;
				hTotalClocks = 450;
				vTotalLines  = 2 + img->vSizeOut;
			}
		} else {
			img->vSize = 200;
			img->hSize = 200;
			if (sel == 2)
			{
				img->hSizeOut = 476;
				img->vSizeOut = 476;
				hTotalClocks = 600;
				vTotalLines  = 2 + img->vSizeOut;
			} else if (sel == 1){
				img->hSizeOut = 200;
				img->vSizeOut = 200;
				hTotalClocks = 450;
				vTotalLines  = 2 + img->vSizeOut;
			}
			else {
				img->hSizeOut = 400;
				img->vSizeOut = 400;
				hTotalClocks = 450;
				vTotalLines  = 2 + img->vSizeOut;
			}
		}
		REG_SET(IMG_RSZ_HDMI_MODMAX, 0);
	}

	// The calculations for the timing are done in a separate Excel file (timing.xlsx).
	// Basically the output frame time must match the input frame time, excluding the vertical blanking.

	REG_SET(IMG_RSZ_HDMI_IMGHORMAXOUT, img->hSizeOut);
	REG_SET(IMG_RSZ_HDMI_IMGVERMAXOUT, img->vSizeOut);
	REG_SET(IMG_RSZ_HDMI_IMGHORMAXINP, img->hSize);
	REG_SET(IMG_RSZ_HDMI_IMGVERMAXINP, img->vSize);
	int colCoeff = (int)262144/img->hSize;
	int rowCoeff = (int)262144/img->vSize;
	REG_SET(IMG_RSZ_HDMI_COL_COEFF, colCoeff);
	REG_SET(IMG_RSZ_HDMI_ROW_COEFF, rowCoeff);
	REG_SET(IMG_RSZ_HDMI_HORMAXOUT, hTotalClocks - 1);
	REG_SET(IMG_RSZ_HDMI_VERMAXOUT, vTotalLines - 1);
	REG_SET(IMG_RSZ_HDMI_TEST, 0);
	REG_SET(IMG_RSZ_HDMI_ENABLE, 1);
	REG_SET(IMG_RSZ_HDMI_RESET, 1);
	REG_SET(IMG_RSZ_HDMI_RESET, 0);

}

void rTest (int enable)
{
	REG_SET(IMG_RSZ_HDMI_TEST, enable);
}

void vline (int lines)
{
	REG_SET(IMG_RSZ_HDMI_VERMAXOUT, lines);
}
void modh (int line)
{
	REG_SET(IMG_RSZ_HDMI_MODMAX, line);
}

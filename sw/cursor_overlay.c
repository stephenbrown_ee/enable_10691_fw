

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "cursor_overlay.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"

//=======================================================================

#define CRS_OVR_RESET            BIT(bus_crsOvr, 0, 0)
#define CRS_OVR_ENABLE           BIT(bus_crsOvr, 0, 1)
#define CRS_OVR_SOURCE           BIT(bus_crsOvr, 0, 2)
#define CRS_OVR_ANTSIZE          FLD(bus_crsOvr, 0, 7, 4)
#define CRS_OVR_ANTRATE          FLD(bus_crsOvr, 0, 15, 8)
#define CRS_OVR_HORSTART         FLD(bus_crsOvr, 1, 11, 0)
#define CRS_OVR_VERSTART         FLD(bus_crsOvr, 1, 26, 16)
#define CRS_OVR_HORSTOP          FLD(bus_crsOvr, 2, 11, 0)
#define CRS_OVR_VERSTOP          FLD(bus_crsOvr, 2, 26, 16)
#define CRS_OVR_OVHORSTART       FLD(bus_crsOvr, 3, 11, 0)
#define CRS_OVR_OVHORSTOP        FLD(bus_crsOvr, 3, 27, 16)
#define CRS_OVR_OVVERSTART       FLD(bus_crsOvr, 4, 11, 0)
#define CRS_OVR_OVVERSTOP        FLD(bus_crsOvr, 4, 27, 16)

//=======================================================================

int cursorOverlayInit(void)
{
	regClear(bus_crsOvr, 8);

	REG_SET(CRS_OVR_RESET, 1);
	REG_SET(CRS_OVR_RESET, 0);

	return 0;
}

//-----------------------------------------------------------------------

void cursorOn(int x, int y, int width, int height)
{
	REG_SET(CRS_OVR_SOURCE, 0);
	REG_SET(CRS_OVR_ENABLE, 1);
	REG_SET(CRS_OVR_ANTSIZE, 2);
	REG_SET(CRS_OVR_ANTRATE, 1);

	REG_SET(CRS_OVR_HORSTART, x);
	REG_SET(CRS_OVR_VERSTART, y);
	REG_SET(CRS_OVR_HORSTOP, ((x + width)) - 1);
	REG_SET(CRS_OVR_VERSTOP, ((y + height)) - 1);
}



void cursorOff(void)
{
	REG_SET(CRS_OVR_ENABLE, 0);
}


void blinkerOn(int width, int height, int x, int y)
{
	REG_SET(CRS_OVR_SOURCE, 1);
	REG_SET(CRS_OVR_ANTRATE, 15);
	REG_SET(CRS_OVR_OVHORSTART, x);
	REG_SET(CRS_OVR_OVVERSTART, y);
	REG_SET(CRS_OVR_OVHORSTOP, (x + width) );
	REG_SET(CRS_OVR_OVVERSTOP, (y + height));
}

void blinkerOff (void)
{
	REG_SET(CRS_OVR_SOURCE, 0);
}
//=======================================================================

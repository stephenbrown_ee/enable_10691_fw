
#ifndef WHITE_BAL_H
#define WHITE_BAL_H
//==============================================================

#include "color.h"

int whiteBalInit(void);
void whiteBalBypass(int bypass);
void whiteBalSet(Color gain);
void whiteBalRgb(int r, int g, int b, int scale);
void whiteBalNull(void);
void whiteBalBlack(int black);
void whiteBalClip(int clip);
void whiteBalStartcolor(int color);
void blackSubtract (int clip);

#endif
//==============================================================

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "gamma_lut.h"

//=======================================================================

#define GAM_LUT_RESET            BIT(bus_gamLut, 0, 0)
#define GAM_LUT_BYPASS           BIT(bus_gamLut, 0, 1)
#define GAM_LUT_TABLE(c,n)       BUS_REG_SUB(bus_gamLut, 1+(c), (n))

//=======================================================================

int gammaLutInit(void)
{
	regClear(bus_gamLut, 8);

	REG_SET(GAM_LUT_RESET,    0);
	REG_SET(GAM_LUT_BYPASS,   0);

	return 0;
}

//-----------------------------------------------------------------------

void gammaLutBypass(int bypass)
{
	REG_SET(GAM_LUT_BYPASS, bypass);
}


//-----------------------------------------------------------------------

////-----------------------------------------------------------------------
//
//// old default: gammaLutSetup(41680, 9990, 0);
//
//void gammaLutSetupOld(int d, int k, int verbose)
//{
//	// i = initial increment 10000 = 1;
//	// k = increment multiplier 9999 = 0.9999
//	unsigned int n;
//	unsigned int v = 0; // table value << 13
//
//	d = ((d << 14) + 5000) / 10000;
//	k = ((k << 14) + 5000) / 10000;
//
//	for (n = 0; n <= 4095; n++)
//	{
//		unsigned int t = MIN(v >> 14, 4095);
//
//		GAM_LUT_TABLE(0, n) = t;
//		GAM_LUT_TABLE(1, n) = t;
//		GAM_LUT_TABLE(2, n) = t;
//
//		if (verbose)
//			printf("%4d - %5d %5d\n", n, d, t);
//
//		v = v + d;
//		d = (d * k) >> 14;
//	}
//
//}
//
////-----------------------------------------------------------------------
//
//void gammaLutSetup(int gamma)
//{
//	float g = (float)gamma / 100;
//	int n;
//
//	for (n = 0; n <= 4095; n++)
//	{
//		float i = (float)n / 4096;
//		float o = powf(i, g);
//
//		unsigned int t = o * 4095;
//
//		GAM_LUT_TABLE(0, n) = t;
//		GAM_LUT_TABLE(1, n) = t;
//		GAM_LUT_TABLE(2, n) = t;
//
//	}
//
//}
//
////-----------------------------------------------------------------------
//
//void gammaLutDual(int mid, int slope)
//{
//	float m = (float)mid / 100;
//	float s = (float)slope / 100;
//	int n;
//
//	for (n = 0; n <= 4095; n++)
//	{
//		float i = (float)n / 4096;
//
//		float p = (m - i) * s;
//		float o = 1.0 / (1.0 + powf(10.0, p));
//
//		unsigned int t = o * 4095;
//
//		GAM_LUT_TABLE(0, n) = t;
//		GAM_LUT_TABLE(1, n) = t;
//		GAM_LUT_TABLE(2, n) = t;
//
//	}
//
//}
//
////-----------------------------------------------------------------------
//
//void gammaLutLinear(void)
//{
//	int n;
//	for (n = 0; n <= 4095; n++)
//	{
//		GAM_LUT_TABLE(0, n) = n;
//		GAM_LUT_TABLE(1, n) = n;
//		GAM_LUT_TABLE(2, n) = n;
//	}
//}

//=======================================================================


#include <string.h>
#include <stdio.h>

#include "bus_io.h"
#include "util.h"

//=======================================================================

#define LIN_BFR_RESET            BIT(bus_fltBfr, 0, 0)
#define LIN_BFR_BYPASS           BIT(bus_fltBfr, 0, 1)
#define LIN_BFR_HCOUNT			 FLD(bus_fltBfr, 1, 15, 0)
#define LIN_BFR_WAITCNTMAX       FLD(bus_fltBfr, 1, 25, 16)

#define SPA_FIL_RESET            BIT(bus_spaFlt, 0, 0)
#define SPA_FIL_BYPASS           BIT(bus_spaFlt, 0, 1)
#define SPA_FIL_SIMPLE           BIT(bus_spaFlt, 0, 2)
#define SPA_FIL_STARTCOLOR       FLD(bus_spaFlt, 0, 5, 4)       // First CFA pixel  0=R 1=Gr 2=Gb 3=B

// Red/Blue smooth coefficients
#define SPA_FIL_RBXS             FLD(bus_spaFlt, 2,  5,  0)  // X - diagonal pixels
#define SPA_FIL_RBTS             FLD(bus_spaFlt, 2, 21, 16)  // T - vertical / horizontal pixels
#define SPA_FIL_RBCS             FLD(bus_spaFlt, 3,  5,  0)  // C - center pixel

// Red/Blue edge coefficients
#define SPA_FIL_RBXE		     FLD(bus_spaFlt, 2, 13,  8)
#define SPA_FIL_RBTE             FLD(bus_spaFlt, 2, 29, 24)
#define SPA_FIL_RBCE             FLD(bus_spaFlt, 3, 13,  8)

#define SPA_FIL_RBEDGEMAX        FLD(bus_spaFlt, 3, 27, 16)

// Green smooth coefficients
#define SPA_FIL_GXS              FLD(bus_spaFlt, 4,  5,  0)
#define SPA_FIL_GTS              FLD(bus_spaFlt, 4, 21, 16)
#define SPA_FIL_GCS              FLD(bus_spaFlt, 5,  5,  0)

// Green edge coefficients
#define SPA_FIL_GXE              FLD(bus_spaFlt, 4, 13,  8)
#define SPA_FIL_GTE              FLD(bus_spaFlt, 4, 29, 24)
#define SPA_FIL_GCE              FLD(bus_spaFlt, 5, 13,  8)

#define SPA_FIL_GEDGEMAX         FLD(bus_spaFlt, 5, 27, 16)

//=======================================================================

void filterDefault(void);
void filterNull(void);

//=======================================================================

void filterInit(int Hpix, int wait)
{
	regClear(bus_fltBfr, 8);
	regClear(bus_spaFlt, 8);

	REG_SET(LIN_BFR_RESET, 1);
	REG_SET(SPA_FIL_RESET, 1);

	REG_SET(SPA_FIL_BYPASS, 1);
	REG_SET(SPA_FIL_SIMPLE, 0);

	filterDefault();
	REG_SET(LIN_BFR_HCOUNT, Hpix);
	REG_SET(LIN_BFR_WAITCNTMAX, wait);

	REG_SET(LIN_BFR_RESET, 0);
	REG_SET(SPA_FIL_RESET, 0);
}

//-----------------------------------------------------------------------

void filterStartcolor(int color)
{
	REG_SET(SPA_FIL_STARTCOLOR, color);
}

//-----------------------------------------------------------------------

void filterBypass(int buffer, int filter)
{
	REG_SET(LIN_BFR_BYPASS, buffer);
	REG_SET(SPA_FIL_BYPASS, filter);
}

//-----------------------------------------------------------------------

void filterSimple(int simple)
{
	REG_SET(SPA_FIL_SIMPLE, simple);
}

//-----------------------------------------------------------------------

//void filterDump(void)
//{
//	printf("pixel:          G   R/B\n");
//	printf("threshold:    %3d   %3d\n", REG_GET(SPA_FIL_GEDGEMAX), REG_GET(SPA_FIL_GEDGEMAX));
//	printf("\n");
//	printf("edge:     X:  %3d   %3d\n", REG_GET(SPA_FIL_GXE), REG_GET(SPA_FIL_RBXE));
//	printf("          T:  %3d   %3d\n", REG_GET(SPA_FIL_GTE), REG_GET(SPA_FIL_RBTE));
//	printf("          C:  %3d   %3d\n", REG_GET(SPA_FIL_GCE), REG_GET(SPA_FIL_RBCE));
//	printf("        sum:  %3d   %3d\n", REG_GET(SPA_FIL_GXE) + REG_GET(SPA_FIL_GTE) + REG_GET(SPA_FIL_GCE), REG_GET(SPA_FIL_RBXE) + REG_GET(SPA_FIL_RBTE) + REG_GET(SPA_FIL_RBCE));
//	printf("\n");
//	printf("smooth:   X:  %3d   %3d\n", REG_GET(SPA_FIL_GXS), REG_GET(SPA_FIL_RBXS));
//	printf("          T:  %3d   %3d\n", REG_GET(SPA_FIL_GTS), REG_GET(SPA_FIL_RBTS));
//	printf("          C:  %3d   %3d\n", REG_GET(SPA_FIL_GCS), REG_GET(SPA_FIL_RBCS));
//	printf("        sum:  %3d   %3d\n", REG_GET(SPA_FIL_GXS) + REG_GET(SPA_FIL_GTS) + REG_GET(SPA_FIL_GCS), REG_GET(SPA_FIL_RBXS) + REG_GET(SPA_FIL_RBTS) + REG_GET(SPA_FIL_RBCS));
//}

//-----------------------------------------------------------------------

void filterThreshold(int gMax, int rbMax)
{
	REG_SET(SPA_FIL_GEDGEMAX,  gMax);
	REG_SET(SPA_FIL_RBEDGEMAX, rbMax);
}

//-----------------------------------------------------------------------

// x,t,c coefficients should total 64

void filterEdgeRb(int x, int t, int c)
{
	REG_SET(SPA_FIL_RBXE, x );
	REG_SET(SPA_FIL_RBTE, t );
	REG_SET(SPA_FIL_RBCE, c );
}

//-----------------------------------------------------------------------

void filterSmoothRb(int x, int t, int c)
{
	REG_SET(SPA_FIL_RBXS, x);
	REG_SET(SPA_FIL_RBTS, t );
	REG_SET(SPA_FIL_RBCS, c );
}

//-----------------------------------------------------------------------

void filterSmoothG(int x, int t, int c)
{
	REG_SET(SPA_FIL_GXS, x );
	REG_SET(SPA_FIL_GTS, t );
	REG_SET(SPA_FIL_GCS, c );
}

//-----------------------------------------------------------------------

void filterEdgeG(int x, int t, int c)
{
	REG_SET(SPA_FIL_GXE, x );
	REG_SET(SPA_FIL_GTE, t );
	REG_SET(SPA_FIL_GCE, c );
}

//-----------------------------------------------------------------------

void filterDefault(void)
{
	filterThreshold(20,20);

	filterEdgeRb(4, 4, 56);
	filterSmoothRb(8, 16, 40);

	filterEdgeG(4, 0, 60);
	filterSmoothG(16, 8, 40);
}

//-----------------------------------------------------------------------

void filterNull(void)
{
	filterThreshold(0,0);

	filterEdgeRb(0, 0, 63);
	filterEdgeG(0, 0, 63);

	filterSmoothRb(0, 0, 0);
	filterSmoothG(0, 0, 0);
}

//=======================================================================

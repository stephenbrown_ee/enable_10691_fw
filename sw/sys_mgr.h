
#ifndef SYS_MGR_H
#define SYS_MGR_H
//==============================================================

#define Bit4
#define Bit3

#include "bus_io.h"
#include "sensor.h"
#include "fpga_i2c1.h"

#define QSYS_REG(adr, reg)  (((volatile int*)(adr))[reg])
#define QSYS_MEM(adr)       (((volatile int*)(adr))[0])

#define MANUFACTURING_VPU  1
#define NOT_CONNECTED   0
#define CONNECTED       1

#define NOT_FINISHED 0
#define FINISHED     1

//==============================================================

int sysInit(void);
void sysLedState(int state);
void sysLedOn(int on);

void waitUsec(unsigned int usec);
void waitMsec(unsigned int msec);
unsigned int getUsec(void);
unsigned int getMsec(void);

int sysClockFreq(void);

void setWatchdog(int msec);
void setStatus(int status);
int getStatus(void);

void banner(void);
void reset(int hold);

ButtonPress_t checkButtonFlag(sensor_type_t sensor, int deVersion);
int getHandleState(void);
void setHandleState(int state);
int getError(void);
void setError(int state);
void faultLedState(int state);

#endif
//==============================================================

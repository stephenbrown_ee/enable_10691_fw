
#ifndef DEBUG_H
#define DEBUG_H
//==============================================================

#include <time.h>

typedef unsigned int   UINT;
typedef unsigned short USHORT;
typedef unsigned char  UCHAR;

typedef int (*DebugHandler)(const char* line);

int debugInit(void);

void debugProcessLine(char* line);

void debugSetHandler(DebugHandler handler);

int debugLog(int topic);

unsigned int symbolAddress(const char* symbol, const char* table);

void dml (UINT* address, UINT count, char same);
void repeat(int count, int msec);

#endif
//==============================================================

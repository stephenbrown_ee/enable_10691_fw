
#include "pix_correction.h"
#include "white_bal.h"
#include "color.h"

typedef struct {
	uint8_t  min;
	uint8_t  max;
	uint32_t avg;
} CorrStats_t;

int gainSteps[7] =
{
	0x10,
	0x18,
	0x20,
	0x28,
	0x30,
	0x38,
	0x3e
};

#define PIX_COR_RESET               BIT(bus_pixCor, 0, 0)
#define PIX_COR_BYPASS              BIT(bus_pixCor, 0, 1)
#define PIX_COR_COLLECT_EN          BIT(bus_pixCor, 0, 2)
#define PIX_COR_SHIFT               BIT(bus_pixCor, 0, 3)
#define PIX_COR_SELRAMREG           FLD(bus_pixCor, 0, 8, 6)
#define PIX_COR_SELCOR1             FLD(bus_pixCor, 0, 11, 9)
#define PIX_COR_SELCOR2             FLD(bus_pixCor, 0, 14, 12)
#define PIX_COR_BILINEARFACTOR      FLD(bus_pixCor, 0, 18, 15)
#define PIX_COR_MULTIPLIER          FLD(bus_pixCor, 1, 23, 0)
#define PIX_COR_CORRECT_RAM(c,n)    BUS_REG_SUB(bus_pixCor, c, n)

//=======================================================================

void initCorr (int horSize)
{
	// This creates a constant multiplier that will be used to calculate
	// the average value of each black column
	int multiplier = (int)(262144/horSize);  // Shift back right by 18 done in HW
	REG_SET(PIX_COR_RESET, 1);
	REG_SET(PIX_COR_BYPASS, 1);
	REG_SET(PIX_COR_SELCOR1, 0);
	REG_SET(PIX_COR_SELCOR2, 0);
	REG_SET(PIX_COR_MULTIPLIER, multiplier);
	REG_SET(PIX_COR_RESET, 0);
}

void pixCorBypass (int bypass)
{
	REG_SET(PIX_COR_BYPASS, bypass);
}

void collect (int enable)
{
	REG_SET(PIX_COR_COLLECT_EN, enable);
}

int readRam (int num, int address)
{
	return PIX_COR_CORRECT_RAM(num, address); // Remember number must be 1 or greater
}

void writeRam (int num, int address, int data)
{
	PIX_COR_CORRECT_RAM(num, address) = data;
}

CorrStats_t correct (int horSize)
{
	uint8_t data = 0;
	int i = 0;
	int n = 0;
	int j = 0;
	CorrStats_t stats = {255, 0, 0};

	for (i = 1; i <= 7; i++) {
		for (j = 0; j <= 11; j++) {
			startEEPROMWrite(512*(i-1) + 32*j);
			for (n = 0; n < 32; n++)
			{
				data = readRam(i, 32*j + n);
				setSeqEEPROMByte(data, (n == 31) ? 1 : 0);
				stats.avg += data;
				if (data > stats.max)
					stats.max = data;
				if (data < stats.min)
					stats.min = data;
			}

		}
		startEEPROMWrite(512*(i-1) + 384);
		for (n = 384; n < 400; n++) {
			data = readRam(i, n);
			setSeqEEPROMByte(data, (n == 399) ? 1 : 0);
			stats.avg += data;
			if (data > stats.max)
				stats.max = data;
			if (data < stats.min)
				stats.min = data;
		}

		stats.avg = stats.avg/horSize;
		printf("Average(%d): %d  Max: %d  Min: %d\n", i, stats.avg, stats.max, stats.min);
		j = 0;
	}

	return stats;
}

void calibrate (sensor_type_t sensor, int deVersion)
{
	int i = 0;
	start_calibration(1);
	initLED(0,0);
	if (sensor == OV6946) {
		setExposure(200);
		for (i = 0; i <= 6; i++) {
			setGain(gainSteps[i]);
			REG_SET(PIX_COR_SELRAMREG, i);
			waitMsec(50);
			collect(0);
			waitMsec(10);
			collect(1);
			waitMsec(100);
		}
		if (deVersion >= 0x26) {
			correct(400);
		}
		waitMsec(100);
//		sensorSet(0x3503, 0x20);
	} else {
		REG_SET(PIX_COR_SELRAMREG, 0);
		waitMsec(50);
		collect(0);
		waitMsec(10);
		collect(1);
		waitMsec(100);
		if (deVersion >= 0x26)
			correct(200);
	}

	waitMsec(1);
	if(deVersion >= 0x26) {
		reset_i2c();
		waitMsec(1);
		setEEPROMTag();
		waitMsec(1);
		reset_i2c();
	}
	pixCorBypass(0);
	initLED(1,25);

	start_calibration(0);
}

// This function will read correction data from the NVRAM on the scope, then write to the FPGA RAM in the module
// that makes the correction
void rw_calibration_data (sensor_type_t sensor)
{
	uint8_t data = 0;
	int n = 0;
	int i = 0;
	int j = 0;
	reset_i2c();
	printf("Start cal @ %d msec\n", getMsec());
	if (sensor == OV6946) {
		for (i = 1; i <= 7; i++) {
			for (j = 0; j <= 11; j++) {
				startEEPROMRead(512*(i-1) + 32*j);
				for (n = 0; n <= 31; n++) {
					data = getSeqEEPROMByte((n == 31) ? 1 : 0);
					writeRam(i, (32*j + n), data);
				}
			}
			// Need to start a new page
			startEEPROMRead(512*(i-1) + 384);
			for (n = 384; n < 400; n++) {
				data = getSeqEEPROMByte((n == 399) ? 1 : 0);
				writeRam(i, n, data);
			}
		}
	}
	else {
		for (j = 0; j <= 5; j++) {
			startEEPROMRead(32*j);
			for (n = 0; n < 31; n++) {
				data = getSeqEEPROMByte((n == 31) ? 1 : 0);
				writeRam(1, (32*j + n), data);
			}
		}
		// Need to start a new page
		startEEPROMRead(192);
		for (n = 192; n < 200; n++) {
			data = getSeqEEPROMByte((n == 199) ? 1 : 0);
			writeRam(i, n, data);
		}
	}
	printf("End cal @ %d msec\n", getMsec());
	reset_i2c();
	printf("NVRAM calibration complete. (n = %d)\n", n);
}

int correctionWorker(void) {
	int gainValue;
	gainValue = getGain();
	if (gainValue > gainSteps[5]) {
		REG_SET(PIX_COR_BILINEARFACTOR, gainSteps[6] - gainValue);
		REG_SET(PIX_COR_SELCOR1, 5);
		REG_SET(PIX_COR_SELCOR2, 6);
	}
	else if (gainValue > gainSteps[4]) {
		REG_SET(PIX_COR_BILINEARFACTOR, gainSteps[5] - gainValue);
		REG_SET(PIX_COR_SELCOR1, 4);
		REG_SET(PIX_COR_SELCOR2, 5);
	}
	else if (gainValue > gainSteps[3]) {
		REG_SET(PIX_COR_BILINEARFACTOR, gainSteps[4] - gainValue);
		REG_SET(PIX_COR_SELCOR1, 3);
		REG_SET(PIX_COR_SELCOR2, 4);
	}
	else if (gainValue > gainSteps[2]) {
		REG_SET(PIX_COR_BILINEARFACTOR, gainSteps[3] - gainValue);
		REG_SET(PIX_COR_SELCOR1, 2);
		REG_SET(PIX_COR_SELCOR2, 3);
	}
	else if (gainValue > gainSteps[1]) {
		REG_SET(PIX_COR_BILINEARFACTOR, gainSteps[2] - gainValue);
		REG_SET(PIX_COR_SELCOR1, 1);
		REG_SET(PIX_COR_SELCOR2, 2);
	}
	else if (gainValue > gainSteps[0]) {
		REG_SET(PIX_COR_BILINEARFACTOR, gainSteps[1] - gainValue);
		REG_SET(PIX_COR_SELCOR1, 0);
		REG_SET(PIX_COR_SELCOR2, 1);
	}
	else {
		REG_SET(PIX_COR_BILINEARFACTOR, 0);
		REG_SET(PIX_COR_SELCOR1, 0);
		REG_SET(PIX_COR_SELCOR2, 0);
	}
	return 0;
}
//=======================================================================

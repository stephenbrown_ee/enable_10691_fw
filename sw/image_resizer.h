
#ifndef IMAGE_RESIZER_H_
#define IMAGE_RESIZER_H_
//==============================================================

#include <stdint.h>
#include <stdio.h>
#include "bit_io.h"
#include "pipe.h"
#include "sensor.h"
#include "parallel_out.h"

void imgResizerSetup(sensor_type_t sensor, int sel);
void imgResizerHdmiSetup(outPath_t outPath, sensor_type_t sensor, int sel);



#endif

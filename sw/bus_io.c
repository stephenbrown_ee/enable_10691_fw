

#include "bus_io.h"

//=======================================================================

int busBaseAddress;
int dmaBaseAddress;

//=======================================================================

int busInit(int baseAddress)
{
	busBaseAddress = baseAddress;
	return 0;
}

//--------------------------------------------------------------

int dmaInit(int baseAddress)
{
	dmaBaseAddress = baseAddress;
	return 0;
}

//--------------------------------------------------------------

void regSetFld(int sel, int reg, int hi, int lo, int value)
{
	REG_SET(FLD(sel, reg, hi, lo), value);
}

//--------------------------------------------------------------

int regGetFld(int sel, int reg, int hi, int lo)
{
	return REG_GET(FLD(sel, reg, hi, lo));
}

//--------------------------------------------------------------

void regSetBit(int sel, int reg, int lo, int value)
{
	REG_SET(BIT(sel, reg, lo), value);
}

//--------------------------------------------------------------

int regGetBit(int sel, int reg, int lo)
{
	return REG_GET(BIT(sel, reg, lo));
}

//--------------------------------------------------------------

void regSet(int sel, int sub, int reg, int value)
{
	BUS_REG_SUB(sel, sub, reg) = value;
}

//--------------------------------------------------------------

int regGet(int sel, int sub, int reg)
{
	return BUS_REG_SUB(sel, sub, reg);
}

//-----------------------------------------------------------------------

void regClear(BusSel sel, int max)
{
	int n;
	for (n = 0; n <= max; n++)
		BUS_REG(sel, n) = 0;
}

//=======================================================================


#ifndef SERIAL_I2C_H
#define SERIAL_I2C_H
//==============================================================

#include "bus_io.h"

int i2c(BusSel sel, int baud, int start, int transmit, int receive, int nak, int stop, unsigned char txData, unsigned char* rxData);
void i2c_vsync(BusSel sel, int set);

#endif
//==============================================================

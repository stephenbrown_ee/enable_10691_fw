

#include "defect_pixel.h"



#define DEF_PIX_RESET               BIT(bus_defPix, 0, 0)
#define DEF_PIX_BYPASS              BIT(bus_defPix, 0, 1)
#define DEF_PIX_THRESHOLD           FLD(bus_defPix, 0, 27, 16)

//=======================================================================

int defPixInit (int threshold)
{
	REG_SET(DEF_PIX_RESET, 1);
	REG_SET(DEF_PIX_BYPASS, 0);
	REG_SET(DEF_PIX_THRESHOLD, threshold);
	REG_SET(DEF_PIX_RESET, 0);
	return 0;
}

//=======================================================================


#include "astar.h"
#include "IntraVu_AE.h"
#include "sensorInput.h"

//=======================================================================

#define FRAME_STEP 3

void aeProcess(AEPaxelImage *pImg, sensor_type_t sensor);
void awbProcess(int start, int click);

static int astarRunning = 0;
static int astarStart   = 0;
static int astar_Resume = 0;

static int statsInterval = 1;
static int lastFrameCount = 0;

static int aeOn  = 0;
//static int awbOn = 0;

static int aeWasOn  = 1;
//static int awbWasOn = 1;

static AEConfig gAEConfig;
static AEExpParams gAEExpParams;
static AEPaxelImage gAEPaxelImage;

//=======================================================================

int astarInit(sensor_type_t sensor)
{

	gAEPaxelImage.AE_Img.Width = PAX_WIDTH;
	gAEPaxelImage.AE_Img.Height = PAX_HEIGHT;
	gAEPaxelImage.ExpSettings.Gain = 1.0;
	gAEPaxelImage.ExpSettings.IntegrationLines = 200;
	gAEPaxelImage.ExpSettings.LED_Power = 25.0;
	Init_AE(&gAEConfig);

	return 0;
}

//-----------------------------------------------------------------------

void astarSetup(int interval)
{
	statsInterval = interval;
}

//-----------------------------------------------------------------------

void astarRun(int ae)
{
	astarStart   = 1;
	astarRunning = 0;
	astar_Resume = 0;
	aeOn         = ae;
//	awbOn        = awb;
}

//-----------------------------------------------------------------------

void astarStop(void)
{
	astarStart   = 0;
	astarRunning = 0;
	astar_Resume = 0;
	aeWasOn      = aeOn;
//	awbWasOn     = awbOn;
	aeOn         = 0;
//	awbOn        = 0;
}

//-----------------------------------------------------------------------

void astarResume(void)
{
	astarStart   = 0;
	astarRunning = 0;
	astar_Resume = 1;
	aeOn         = aeWasOn;
//	awbOn        = awbWasOn;
}

//-----------------------------------------------------------------------

void aeProcess(AEPaxelImage *pImg, sensor_type_t sensor)
{
//	if(debugLog(0))
//		printf("Start es: %d\n", statsActualFrame());
	statsCollect(&pImg->AE_Img);

	AE_Compute(pImg, &gAEConfig, &gAEExpParams, sensor);

	setAEParams(&pImg->ExpSettings, &gAEExpParams, sensor);
//	if(debugLog(0))
//		printf("End es: %d\n", statsActualFrame());
}

void aeTest(int sel, int target, int max_iter)
{
	PaxelImage *AE_Img = &gAEPaxelImage.AE_Img;
	int gain = 0x10;
	int lines = target;
	float LumaEst = 0;
	int i = 0;
	if((sel == 0) || (sel ==1))
		max_iter = 44;
	for (i = 0; i < max_iter; i++)
	{
		if(sel == 0) {
			setExposure(lines);
			setGain(gain);
		}
		else if (sel == 1) {
			setGain(gain);
		}
		else if (sel == 2) {
			setExposure(lines);
		}
		waitMsec(400);
		statsCollect(AE_Img);
		LumaEst = ComputeLumaEstimate(AE_Img, &gAEConfig);
		if(sel == 0) {
			printf("%d  %f \n", lines, LumaEst);
			gain++;
			lines = (target*16)/gain;
		}
		else if (sel == 1) {
			printf("%f \n", LumaEst);
			gain++;
		}
		else if (sel == 2) {
			printf("%d  %f \n", lines, LumaEst);
			lines = lines - (target/max_iter);
		}

	}

}

void damp(int dampingFactor)
{
	gAEConfig.DampingFactor = (float)dampingFactor/100;
}
//-----------------------------------------------------------------------

void astarWorker(int run, sensor_type_t sensor)
{
	if (astarRunning || run)
	{
		if ((getFrameCount() - lastFrameCount) > statsInterval)
		{
			if (aeOn || run) {
				aeProcess(&gAEPaxelImage, sensor);
				lastFrameCount = getFrameCount();
			}
		}
	}
	else if (astarStart || astar_Resume)
	{
		astarStart   = 0;
		astarRunning = 1;
	}
}

//-----------------------------------------------------------------------

void awbProcess(int start, int click)
{
	Color rgb;
	Color oldBal =  sensorDefaultBalance();
	Color bal = sensorDefaultBalance();

	if (click)
		rgb = paxAverageInpRgb();

	if ((rgb.c[R] == 0) || (rgb.c[G] == 0) || (rgb.c[B] == 0))
		return;

	// Fix highest color at full scale gain, other two relative to it
	if ((rgb.c[R] > rgb.c[G]) && (rgb.c[R] > rgb.c[B])) // Red is highest
	{
		bal.c[R] = bal.scale;
		bal.c[G] = (rgb.c[R] * bal.scale) / rgb.c[G];
		bal.c[B] = (rgb.c[R] * bal.scale) / rgb.c[B];
	}
	else if ((rgb.c[G] > rgb.c[R]) && (rgb.c[G] > rgb.c[B])) // Green is highest
	{
		bal.c[R] = (rgb.c[G] * bal.scale) / rgb.c[R];
		bal.c[G] = bal.scale;
		bal.c[B] = (rgb.c[G] * bal.scale) / rgb.c[B];
	}
	else // Blue is lowest
	{
		bal.c[R] = (rgb.c[B] * bal.scale) / rgb.c[R];
		bal.c[G] = (rgb.c[B] * bal.scale) / rgb.c[G];
		bal.c[B] = bal.scale;
	}

	if (click)
		printf(" - Click: oldBal %4d %4d %4d > NewBal  %4d %4d %4d\n", oldBal.c[R], oldBal.c[G], oldBal.c[B], bal.c[R], bal.c[G], bal.c[B]);

//	img->whiteBal = colorByColor(oldBal, img->dispBalance);
//	img->whiteGain = colorByScalar(img->whiteBal, img->inpGain);

	if (click) // Full correction immediately
		oldBal = bal;

	setDefaultBalance(oldBal.c[0], oldBal.c[1], oldBal.c[2]);
	whiteBalSet(oldBal);
	writeWBtoEEPROM(oldBal.c[0], oldBal.c[1], oldBal.c[2]);

}

//-----------------------------------------------------------------------

void clickBal(sensor_type_t sensor)
{
	int lastMsec = getMsec();
	paxCapture();
	awbProcess(0, 1);
	while((getMsec() - lastMsec) < 1000);
	cursorOff();
	if (sensor == OV6946)
		blinkerOn(3,3,0,396);
	else
		blinkerOn(3,3,0,196);
}

//-----------------------------------------------------------------------

void recAE (int aeRange) {

	if (aeRange == 0x1) {
		gAEConfig.RhoAveSetPoint = gAEConfig.RhoAveSetPoint + 0.05;
	}
	else if (aeRange == 0x2) {
		if(gAEConfig.RhoAveSetPoint > 0)
			gAEConfig.RhoAveSetPoint = gAEConfig.RhoAveSetPoint - 0.05;
	}

}

//=======================================================================



#include "pwm_led.h"


#define PWM_LED_RESET                BIT(bus_pwmLed, 0, 0)
#define PWM_LED_ENABLE               BIT(bus_pwmLed, 0, 1)
#define PWM_LED_COUNTMAX             FLD(bus_pwmLed, 1, 23, 0)  // This the number of msec clock cyles in one period
#define PWM_LED_RISECOUNT            FLD(bus_pwmLed, 2, 23, 0)  // This is the clock count where the PWM signal rises
#define PWM_LED_FALLCOUNT            FLD(bus_pwmLed, 3, 23, 0)  // This is the clock count where the PWM signal falls


//=======================================================================

int setFaultLED (int state)
{

	REG_SET(PWM_LED_RESET, 1);
	REG_SET(PWM_LED_ENABLE, 0);

	switch (state)
	{
		case 0:
			break;
		case 1:
			REG_SET(PWM_LED_COUNTMAX, 1000); // Arbitrary for 100% duty cycle
			REG_SET(PWM_LED_RISECOUNT, 0);
			REG_SET(PWM_LED_FALLCOUNT, 0);  // If fall count equals rise count, then LED is always ON (Duty cycle = 100%)
			REG_SET(PWM_LED_RESET, 0);
			REG_SET(PWM_LED_ENABLE, 1);
			break;
		case 2:
			REG_SET(PWM_LED_COUNTMAX, 1000); //  1 second period
			REG_SET(PWM_LED_RISECOUNT, 0);
			REG_SET(PWM_LED_FALLCOUNT, 500);  // ON for 0.5 second
			REG_SET(PWM_LED_RESET, 0);
			REG_SET(PWM_LED_ENABLE, 1);
			break;
		default:
			break;
	}

	return 0;
}

//=======================================================================

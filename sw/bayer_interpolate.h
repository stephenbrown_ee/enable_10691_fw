
#ifndef BAYER_INTERPOLATE_H
#define BAYER_INTERPOLATE_H
//==============================================================

int  bayerIntInit(void);
void bayerIntBypass(int buffer, int bayer);
void bayerIntSimple(int simple);
void bayerIntStartcolor(int color);
void bayerBufInit(int Hpix, int wait);

#endif
//==============================================================

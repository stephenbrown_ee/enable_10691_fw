
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "debug.h"
#include "console.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "paxel_capture.h"
#include "cursor_overlay.h"
#include "color.h"
#include "stats.h"

//=======================================================================

#define PAX_INP_RESET            BIT(bus_paxInp, 0, 0)
#define PAX_INP_CLEAR            BIT(bus_paxInp, 0, 1)
#define PAX_INP_ENABLE           BIT(bus_paxInp, 0, 2)
#define PAX_INP_RUN              BIT(bus_paxInp, 0, 6)
#define PAX_INP_HORSTART         FLD(bus_paxInp, 1, 11, 0)
#define PAX_INP_VERSTART         FLD(bus_paxInp, 1, 27, 16)

#define PAX_INP_PIX(row,col)     BUS_REG_SUB(bus_paxInp, 1, ((row)*32) + (col))

#define PAX_OUT_RESET            BIT(bus_paxOut, 0, 0)
#define PAX_OUT_CLEAR            BIT(bus_paxOut, 0, 1)
#define PAX_OUT_ENABLE           BIT(bus_paxOut, 0, 2)
#define PAX_OUT_RUN              BIT(bus_paxOut, 0, 6)
#define PAX_OUT_HORSTART         FLD(bus_paxOut, 1, 11, 0)
#define PAX_OUT_VERSTART         FLD(bus_paxOut, 1, 27, 16)

#define PAX_OUT_RAM(row,col)     BUS_REG_SUB(bus_paxOut, 1, ((row)*32) + (col))
#define PAX_OUT_PIX(row,col,c)   ((PAX_OUT_RAM(row,col) >> (c * 10)) & 0x3FF)

//=======================================================================

static int xPax = 0;
static int yPax = 0;
static int paxMoved = 0;

static int hSize = 0;
static int vSize = 0;
static int startcolor = 0;

static int row = 7;
static int col = 7;

int commandHandler(const char* line);

//=======================================================================

int paxCaptureInit(void)
{
	debugSetHandler(commandHandler);

	regClear(bus_paxInp, 4);
	regClear(bus_paxOut, 4);

	REG_SET(PAX_INP_RESET, 1);
	REG_SET(PAX_OUT_RESET, 1);

	REG_SET(PAX_INP_RESET, 0);
	REG_SET(PAX_OUT_RESET, 0);

	return 0;
}

//-----------------------------------------------------------------------

void paxCaptureSetup(int hSize_, int vSize_)
{
	hSize = hSize_;
	vSize = vSize_;

	if (!paxMoved)
	{
		xPax = hSize / 2;
		yPax = vSize / 2;
	}
}

//-----------------------------------------------------------------------

void paxCaptureStartcolor(int color)
{
	startcolor = color;
}

//-----------------------------------------------------------------------

int paxCapture(void)
{
	REG_SET(PAX_INP_HORSTART, xPax);
	REG_SET(PAX_INP_VERSTART, yPax);

	REG_SET(PAX_OUT_HORSTART, xPax);
	REG_SET(PAX_OUT_VERSTART, yPax);

	REG_SET(PAX_INP_CLEAR, 1);
	REG_SET(PAX_OUT_CLEAR, 1);
	waitUsec(100);
	REG_SET(PAX_INP_CLEAR, 0);
	REG_SET(PAX_OUT_CLEAR, 0);

	cursorOn(xPax - 8, yPax, 16, 16);

	REG_SET(PAX_INP_ENABLE, 1); // Output module is enabled by input

	int start = getMsec();
	while (REG_GET(PAX_INP_ENABLE) || REG_GET(PAX_INP_RUN) || REG_GET(PAX_OUT_RUN))
		if (getMsec() - start > 1000)
			return -1;

	return 0;
}

//-----------------------------------------------------------------------

void paxDump(int io)
{
	int cc, xx, yy;

	for (yy = 0; yy <= 15; yy++)
	{
		if (io & 1)
		{
			for (xx = 0; xx <= 15; xx++)
				printf("%04d ", PAX_INP_PIX(yy, xx));
			printf("\n");
		}

		if (io & 2)
		{
			for (cc = 0; cc <= 2; cc++)
			{
				for (xx = 0; xx <= 15; xx++)
					printf("%04d ", PAX_OUT_PIX(yy, xx, cc));
				printf("\n");
			}
		}
		printf("\n");
	}
}

//-----------------------------------------------------------------------

Color paxAverageInpRgb(void)
{
	int xx, yy;
	int sum[4] = {0,0,0,0};

	for (yy = 0; yy <= 15; yy += 2)
		for (xx = 0; xx <= 15; xx += 2)
		{
			sum[0] += PAX_INP_PIX(yy + 0, xx + 0);
			sum[1] += PAX_INP_PIX(yy + 0, xx + 1);
			sum[2] += PAX_INP_PIX(yy + 1, xx + 0);
			sum[3] += PAX_INP_PIX(yy + 1, xx + 1);
		}

	Color co;

	co.c[R] = sum[startcolor] / 64;

	if ((startcolor == 0) || (startcolor == 3))
		co.c[G] = (sum[1] + sum[2]) / 128;
	else
		co.c[G] = (sum[0] + sum[3]) / 128;

	co.c[B] = sum[3 - startcolor] / 64;

	co.scale = 4095;

	return co;
}

//-----------------------------------------------------------------------

Color paxAverageOutRgb(void)
{
	int cc, xx, yy;
	int sum[3] = {0,0,0};

	for (yy = 0; yy <= 15; yy++)
		for (cc = 0; cc <= 2; cc++)
			for (xx = 0; xx <= 15; xx++)
				sum[cc] += PAX_OUT_PIX(yy, xx, cc);

	Color co;

	co.c[R] = sum[0] / 256;
	co.c[G] = sum[1] / 256;
	co.c[B] = sum[2] / 256;

	co.scale = 4095;

	return co;
}

//-----------------------------------------------------------------------

void paxCap(void)
{
	if (paxCapture() != 0)
		return;

	paxDump(3);
}

//-----------------------------------------------------------------------

void paxAvg(int hide)
{
	Color rgb;

	if (paxCapture() != 0)
		return;

	if ((hide & 1) == 0)
	{
		rgb = paxAverageInpRgb();
		printf("RGB: in %5d %5d %5d   ", rgb.c[R], rgb.c[G], rgb.c[B]);
	}

	if ((hide & 2) == 0)
	{
		rgb = paxAverageOutRgb();
		printf("out %5d %5d %5d", rgb.c[R], rgb.c[G], rgb.c[B]);
	}

	printf("\n");
}

//-----------------------------------------------------------------------

void paxSet(int x, int y)
{
	xPax = x;
	yPax = y;

	REG_SET(PAX_INP_HORSTART, xPax - 8);
	REG_SET(PAX_INP_VERSTART, yPax - 8);
	REG_SET(PAX_OUT_HORSTART, xPax - 8);
	REG_SET(PAX_OUT_VERSTART, yPax - 8);

	cursorOn(x - 8, y - 8, 16, 16);
}

//-----------------------------------------------------------------------

void paxCtr(void)
{
	xPax = hSize / 2;
	yPax = vSize / 2;
	paxMoved = 1;
}

//-----------------------------------------------------------------------

void chartRead(int cols, int rows, int xOrig, int yOrig, int xStep, int yStep)
{
	int row, col, x, y;
	for (row = 0; row < rows; row++)
		for (col = 0; col < cols; col++)
		{
			x = xOrig + (col * xStep);
			y = yOrig + (row * yStep);
			//printf("%d.%d:%4d.%4d    ", col, row, x, y);
			paxSet(x, y);
			waitMsec(50);
			if (paxCapture() == 0)
			{
				Color rgb = paxAverageInpRgb();
				printf("%2d %2d    %5d %5d %5d\n", col, row, rgb.c[R], rgb.c[G], rgb.c[B]);
			}
			waitMsec(50);
		}
}

void chart(void)
{
	chartRead(6, 4, 350, 300, 110, 110);
}

//-----------------------------------------------------------------------

int commandHandler(const char* line)
{
	static int mode = 0; // 0 - pax capture, 1 = awb paxel
	static int step = 32;
	static char dir = '5';
	int n;

	for (n = 0; line[n] != 0; n++)
	{
		if (line[n] == '/')
			mode = ~mode;
		else if (line[n] == '+')
			step = 256;
		else if (line[n] == '-')
			step = 32;
		else if (line[n] == '*')
			step = 4;

		else if ((line[n] >= '1') && (line[n] <= '9'))
			dir = line[n];

		else if (line[n] == '\n') {
			printf("\n");
			return 0;
		}
		else
			return -1;
	}

	if (mode == 0)
	{
		switch (dir)
		{
			case '5': break;
			case '7': xPax -= step; yPax -= step; break;
			case '8':               yPax -= step; break;
			case '9': xPax += step; yPax -= step; break;
			case '6': xPax += step; break;
			case '3': xPax += step; yPax += step; break;
			case '2':               yPax += step; break;
			case '1': xPax -= step; yPax += step; break;
			case '4': xPax -= step; break;
			default: break;
		}

		xPax = CLIP(xPax, 0, hSize);
		yPax = CLIP(yPax, 0, vSize);

		paxMoved = 1;

		printf("%4d, %4d = ", xPax + 8, yPax + 8);

		paxSet(xPax, yPax);

		paxAvg(0);

//		pixelSamplerSet(xPax + 8, yPax + 8);
//		pixelSamplerRun();
//		pixelSamplerDump();
	}
	else
	{
		switch (dir)
		{
			case '5': break;
			case '7': col -= 1; row -= 1; break;
			case '8':           row -= 1; break;
			case '9': col += 1; row -= 1; break;
			case '6': col += 1;           break;
			case '3': col += 1; row += 1; break;
			case '2':           row += 1; break;
			case '1': col -= 1; row += 1; break;
			case '4': col -= 1; break;
			default: break;
		}

		col = CLIP(col, 0, 15);
		row = CLIP(row, 0, 15);

//		statsReportPaxel(col, row);
	}

	return 0;
}

//-----------------------------------------------------------------------

void help(void)
{
	printf("\n");
	printf("dir  7 8 9    + fast\n");
	printf("     4 5 6    - slow\n");
	printf("     1 2 3    * step\n");
	printf("              / mode\n");
}

//=======================================================================

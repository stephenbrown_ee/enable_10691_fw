
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "white_bal.h"

//=======================================================================

#define WHI_BAL_RESET            BIT(bus_whiBal, 0, 0)
#define WHI_BAL_BYPASS           BIT(bus_whiBal, 0, 1)
#define WHI_BAL_STARTCOLOR       FLD(bus_whiBal, 0, 5, 4)
#define WHI_BAL_CLIP             FLD(bus_whiBal, 0, 27, 16)
#define WHI_BAL_GAIN0            FLD(bus_whiBal, 1, 12, 0)      // Gain   (13 bit unsigned) 0 to 8191 = 0 to +15.998 (512 = X1)
#define WHI_BAL_GAIN1            FLD(bus_whiBal, 2, 12, 0)
#define WHI_BAL_GAIN2            FLD(bus_whiBal, 3, 12, 0)
#define WHI_BAL_GAIN3            FLD(bus_whiBal, 4, 12, 0)
#define WHI_BAL_OFFSET(c)        FLD(bus_whiBal, 1+c, 27, 16)
#define WHI_BAL_CLIP0            FLD(bus_whiBal, 5, 11, 0)
#define WHI_BAL_CLIP1            FLD(bus_whiBal, 5, 27, 16)
#define WHI_BAL_CLIP2            FLD(bus_whiBal, 6, 11, 0)
#define WHI_BAL_CLIP3            FLD(bus_whiBal, 6, 27, 16)

//=======================================================================

static int startcolor = 0;

static unsigned char colorMap[4][4] =
{
	{ 0, 1, 1, 2 },
	{ 1, 0, 2, 1 },
	{ 1, 2, 0, 1 },
	{ 2, 1, 1, 0 }
};

//=======================================================================

int whiteBalInit(void)
{
	regClear(bus_whiBal, 8);

	REG_SET(WHI_BAL_RESET,    0);
	REG_SET(WHI_BAL_BYPASS,   0);
	REG_SET(WHI_BAL_STARTCOLOR, 0);
	REG_SET(WHI_BAL_CLIP,  4094);
	REG_SET(WHI_BAL_GAIN0,  512);
	REG_SET(WHI_BAL_GAIN1,  512);
	REG_SET(WHI_BAL_GAIN2,  512);
	REG_SET(WHI_BAL_GAIN3,  512);
	REG_SET(WHI_BAL_CLIP0, 4094);
	REG_SET(WHI_BAL_CLIP1, 4094);
	REG_SET(WHI_BAL_CLIP2, 4094);
	REG_SET(WHI_BAL_CLIP3, 4094);
	int c;
	for (c = 0; c <= 3; c++)
		REG_SET(WHI_BAL_OFFSET(c), 8);


	return 0;
}

//-----------------------------------------------------------------------

void whiteBalBypass(int bypass)
{
	REG_SET(WHI_BAL_BYPASS, bypass);
}

//-----------------------------------------------------------------------

void whiteBalStartcolor(int color)
{
	startcolor = color;
}

//-----------------------------------------------------------------------

void whiteBalSet(Color gain)
{
	REG_SET(WHI_BAL_GAIN0, (gain.c[colorMap[startcolor][0]] * 512) / gain.scale);
	REG_SET(WHI_BAL_GAIN1, (gain.c[colorMap[startcolor][1]] * 512) / gain.scale);
	REG_SET(WHI_BAL_GAIN2, (gain.c[colorMap[startcolor][2]] * 512) / gain.scale);
	REG_SET(WHI_BAL_GAIN3, (gain.c[colorMap[startcolor][3]] * 512) / gain.scale);
}

//-----------------------------------------------------------------------

void whiteBalRgb(int r, int g, int b, int scale)
{
	if (scale == 0)
		scale = 256;

	Color wb = makeColor(r, g, b, scale);

	whiteBalSet(wb);
}

//-----------------------------------------------------------------------

void whiteBalNull(void)
{
	REG_SET(WHI_BAL_GAIN0, 512);
	REG_SET(WHI_BAL_GAIN1, 512);
	REG_SET(WHI_BAL_GAIN2, 512);
	REG_SET(WHI_BAL_GAIN3, 512);
}

//-----------------------------------------------------------------------

void whiteBalClip(int clip)
{
	REG_SET(WHI_BAL_CLIP, clip);
}

//-----------------------------------------------------------------------

void wbClip(int clip0, int clip1, int clip2, int clip3)
{
	REG_SET(WHI_BAL_CLIP0, clip0);
	REG_SET(WHI_BAL_CLIP1, clip1);
	REG_SET(WHI_BAL_CLIP2, clip2);
	REG_SET(WHI_BAL_CLIP3, clip3);
}

void blackSubtract (int clip)
{
	int c;
	for (c = 0; c <= 3; c++)
		REG_SET(WHI_BAL_OFFSET(c), clip);

}
//-----------------------------------------------------------------------
/*
void whiteBalSetup(int scale, int adj, int r, int g, int b)
{
	ImageInfo* img = &imgNorm;
	if (scale == 0)
		scale = 1000;
	Color bal = makeColor(r, g, b, scale);

	img->whiteBal = bal;

	if (adj)
		img->whiteGain = colorByScalar(bal, img->inpGain);
	else
		img->whiteGain = bal;

	pipeWhiBalSet(zeroColor, img->whiteGain);
}
*/
//=======================================================================

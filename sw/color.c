

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "color.h"
#include "util.h"

//=======================================================================

Matrix rgbToYuvMatrix =
{
//  Yo: Ri    Gi    Bi   Uo: Ri    Gi    Bi   Vo: Ri    Gi    Bi
	{{  299,  587,  114 },{ -147, -289,  436 },{  615, -515, -100 }}, 1000
};

Matrix yuvToRgbMatrix =
{
//  Ro: Yi    Ui    Vi   Go: Yi    Ui    Vi   Bo: Yi    Ui    Vi
	{{ 1000,    0, 1140 },{ 1000, -395, -581 },{ 1000, 2032,    0 }}, 1000
};

// RGB to monochrome - Luma out all 3 channels
Matrix rgbToMonMatrix = {{{ 299,  587,  114 },{ 299,  587,  114  },{ 299,  587,  114  }}, 1000};

Matrix unityMatrix = {{{ 1, 0, 0 },{ 0, 1, 0 },{ 0, 0, 1 }}, 1};
Matrix zeroMatrix  = {{{ 0, 0, 0 },{ 0, 0, 0 },{ 0, 0, 0 }}, 1};

Color zeroColor  = {{0,0,0}, 1};
Color unityColor = {{1,1,1}, 1};

//=======================================================================

Color makeColor(int c0, int c1, int c2, int scale)
{
	Color cout;

	cout.c[0] = c0;
	cout.c[1] = c1;
	cout.c[2] = c2;
	cout.scale = scale;

	return cout;
}

//-----------------------------------------------------------------------

Scalar makeScalar(int s, int scale)
{
	Scalar sout;

	sout.s = s;
	sout.scale = scale;

	return sout;
}
//-----------------------------------------------------------------------

Matrix makeMatrix(int k00, int k01, int k02, int k10, int k11, int k12, int k20, int k21, int k22, int scale)
{
	Matrix mat;

	mat.scale = scale;
	mat.k[0][0] = k00;  mat.k[1][0] = k10;  mat.k[2][0] = k20;
	mat.k[0][1] = k01;  mat.k[1][1] = k11;  mat.k[2][1] = k21;
	mat.k[0][2] = k02;  mat.k[1][2] = k12;  mat.k[2][2] = k22;

	return mat;
}

//-----------------------------------------------------------------------

Color colorByScalar(Color ca, Scalar sb)
{
	Color cout;

	int co;
	for (co = 0; co <= 2; co++)
		cout.c[co] = (ca.c[co] * sb.s) / sb.scale;
	cout.scale = ca.scale;

	return cout;
}

//-----------------------------------------------------------------------

Color colorByColor(Color ca, Color cb)
{
	Color cout;

	int co;
	for (co = 0; co <= 2; co++)
		cout.c[co] = (ca.c[co] * cb.c[co]) / cb.scale;
	cout.scale = ca.scale;

	return cout;
}

//-----------------------------------------------------------------------

Matrix matrixByColor(Matrix ma, Color cb)
{
	Matrix mout;

	int co, ci;
	for (co = 0; co <= 2; co++)
		for (ci = 0; ci <= 2; ci++)
			mout.k[co][ci] = (ma.k[co][ci] * cb.c[ci]) / cb.scale;
	mout.scale = ma.scale;

	return mout;
}

//-----------------------------------------------------------------------

Matrix matrixByMatrix(Matrix ma, Matrix mb)
{
	Matrix mout;

	int co, ci;
	for (co = 0; co <= 2; co++)
		for (ci = 0; ci <= 2; ci++)
			mout.k[co][ci] = (ma.k[co][ci] * mb.k[co][ci]) / mb.scale;
	mout.scale = ma.scale;

	return mout;
}

//-----------------------------------------------------------------------

Color colorTransform(Color ca, Matrix mb)
{
	Color cout;

	int co;
	for (co = 0; co <= 2; co++)
		cout.c[co] = ((ca.c[0] * mb.k[co][0]) + (ca.c[1] * mb.k[co][1]) + (ca.c[2] * mb.k[co][2])) / mb.scale;
	cout.scale = ca.scale;

	return cout;
}

//-----------------------------------------------------------------------

Matrix setMatrixStrength(Matrix ma, int strength)
{
	// strength (0-15)
	Matrix mout;
	int ci,co;

	for (co = 0; co <= 2; co++)
	{
		int sum = 0;
		int ratio[3];
		for (ci = 0; ci <= 2; ci++)
		{
			if (ma.k[co][ci] == ma.scale)
				ratio[ci] = ma.scale;
			else
				ratio[ci] = (ma.k[co][ci] * strength) / 15;

			sum += ratio[ci];
		}

		for (ci = 0; ci <= 2; ci++)
		{
			if (ratio[ci] == ma.scale)
				mout.k[co][ci] = (ma.scale * ma.scale) / sum;
			else
				mout.k[co][ci] = (ratio[ci] * ma.scale) / sum;
		}
	}

	mout.scale = ma.scale;

	return mout;
}

//=======================================================================

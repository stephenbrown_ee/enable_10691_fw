
#ifndef COLOR_MATRIX_H
#define COLOR_MATRIX_H
//==============================================================

#include "color.h"

int colorMatrixInit(void);
void colorMatrixBypass(int bypass);
void colorMatrixNull(void);
void colorMatrixMono(void);
void colorMatrixClip(int clip0, int clip1, int clip2);
void colorMatrixSet(Matrix mat);
void colorMatrixTest(int k00, int k10, int k20, int k01, int k11, int k21, int k02, int k12, int k22, int scale);
void colorMatrixSetup(Matrix mat);

#endif
//==============================================================

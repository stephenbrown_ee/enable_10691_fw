

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "color.h"
#include "color_matrix.h"

//=======================================================================

#define COR_MAT_RESET            BIT(bus_corMat, 0, 0)
#define COR_MAT_BYPASS           BIT(bus_corMat, 0, 1)
#define COR_MAT_CLIPSAFE         BIT(bus_corMat, 0, 2)
#define COR_MAT_CLIPLEVEL        FLD(bus_corMat, 1, 11, 0)
#define COR_MAT_CLIPOUT0         FLD(bus_corMat, 1, 27, 16)
#define COR_MAT_CLIPOUT1         FLD(bus_corMat, 2, 11, 0)
#define COR_MAT_CLIPOUT2         FLD(bus_corMat, 2, 27, 16)
#define COR_MAT_GAIN(ci,co)      FLD(bus_corMat, 3+((co)*3)+(ci), 11, 0)   // Gain   (12 bit signed) -2048 to +2047 = -8.000 to +7.996 (256 = X1)

//=======================================================================

int colorMatrixInit(void)
{
	regClear(bus_corMat, 16);

	REG_SET(COR_MAT_RESET,        0);
	REG_SET(COR_MAT_BYPASS,       0);
	REG_SET(COR_MAT_CLIPSAFE,     1);
	REG_SET(COR_MAT_CLIPLEVEL, 4094);
	REG_SET(COR_MAT_CLIPOUT0,  4094);
	REG_SET(COR_MAT_CLIPOUT1,  4094);
	REG_SET(COR_MAT_CLIPOUT2,  4094);

	colorMatrixNull();

	return 0;
}

//-----------------------------------------------------------------------

void colorMatrixBypass(int bypass)
{
	REG_SET(COR_MAT_BYPASS, bypass);
}

//-----------------------------------------------------------------------

void colorMatrixNull(void)
{
	colorMatrixSet(unityMatrix);
}

//-----------------------------------------------------------------------

void colorMatrixMono(void)
{
	colorMatrixSet(rgbToMonMatrix);
}

//-----------------------------------------------------------------------

void colorMatrixSet(Matrix mat)
{
	int ci,co;
	for (co = 0; co <= 2; co++)
		for (ci = 0; ci <= 2; ci++)
			REG_SET(COR_MAT_GAIN(ci,co), (mat.k[co][ci] * 256) / mat.scale);
}

//-----------------------------------------------------------------------

void colorMatrixClip(int clip0, int clip1, int clip2)
{
	REG_SET(COR_MAT_CLIPOUT0,  clip0);
	REG_SET(COR_MAT_CLIPOUT1,  clip1);
	REG_SET(COR_MAT_CLIPOUT2,  clip2);
}

//-----------------------------------------------------------------------

void colorMatrixTest(int k00, int k10, int k20, int k01, int k11, int k21, int k02, int k12, int k22, int scale)
{
	colorMatrixSet(makeMatrix(k00, k10, k20, k01, k11, k21, k02, k12, k22, scale));
}

//-----------------------------------------------------------------------

void colorMatrixSetup(Matrix mat)
{
	colorMatrixSet(mat);
}

//=======================================================================

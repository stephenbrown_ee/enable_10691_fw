
#ifndef FPGA_I2C1_H_
#define FPGA_I2C1_H_
//==============================================================

#include <stdint.h>
#include <stdio.h>
#include "bit_io.h"
#include "serial_i2c.h"
#include "sensor.h"

#define AT24CM02_ADDR  0xA0

#define TAG_ADDRESS         0x7FFF
#define TAG_VALUE           0x5A

#define WBTAG_ADDRESS       0x7FFE
#define WBTAG_VALUE         0xA5

#define MODEL_NUM_ADDR_LO   0x7FF0
#define MODEL_NUM_ADDR_HI   0x7FF1

#define WB_R_ADDR_LO   0x7FF2
#define WB_R_ADDR_HI   0x7FF3

#define WB_G_ADDR_LO   0x7FF4
#define WB_G_ADDR_HI   0x7FF5

#define WB_B_ADDR_LO   0x7FF6
#define WB_B_ADDR_HI   0x7FF7

// These are the possible DE Microcontroller Button States
// These are possible values of the Key Input Register (except for CRC_FAILURE)
// See Table 1 of IV-SDS
typedef enum
{
	DECR_BRIGHTNESS            = 0,
	DECR_BRIGHTNESS_LONG_PRESS = 1,
	RECORD                     = 2,
	RECORD_LONG_PRESS          = 3,
	INCR_BRIGHTNESS            = 4,
	INCR_BRIGHTNESS_LONG_PRESS = 5,
	CRC_FAILURE                = 6,
	CONNECTED_NOPRESS          = 0xFF
} ButtonPress_t;


typedef enum
{
	SUCCESS,
	BAD_LOT_NUMBER,
	EXPIRED_DE,
	COMM_FAIL_LOTNUM,
	COMM_FAIL_EXPDATE,
	HPS_TIMEOUT,
	HPS_I2C_FAILURE,
	RTC_BATTERY_DEAD,
	RTC_BATTERY_LOW
} ErrorCode_t;

int handleInit(int enable, sensor_type_t sensor);
ButtonPress_t processDEButton(sensor_type_t sensor, int agcOn);
ErrorCode_t checkDE(int verbose);
int checkStatus(void);
int setControlReg(uint16_t value);
void setScopeTimeForce (void);
int getSwVersionReg(void);
void baselineScopeFlash (void);
void setBatchNum (void);
void getBatchNum (void);
void setEEPROMByte(uint32_t addr, uint8_t data);
uint8_t getEEPROMByte(uint32_t addr);
void startEEPROMRead (uint32_t addr);
void setEEPROMTag (void);
uint8_t getEEPROMTag (void);
uint8_t getSeqEEPROMByte (int stop);
void startEEPROMWrite  (uint32_t addr);
void setSeqEEPROMByte (uint8_t data, int stop);
void reset_i2c (void);
void writeModelNum (void);
int readModelNum (void);
void writeWBtoEEPROM (int r, int g, int b);
void readWBfromEEPROM(void);
uint8_t getWBTag (void);

#endif

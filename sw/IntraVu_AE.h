
#ifndef INTRAVU_AE_H
#define INTRAVU_AE_H

/* 
	The IntraVu AE modulates: 
		- Lines of Integration
		- Gain
		- LED illumination

	to achieve proper exposure where the code value set point
	defined by Maximum Scene reflectance handling (RhoMax) and 
	average scene reflectance (RhoAve).

	The AE routine takes in a paxelixed image and the associated 
	exposure parameters used to expose that image.

	The paxelized image is an estimate of the scene luminance.


*/


#define PAX_WIDTH  8
#define PAX_HEIGHT 8
//#define PIXELS_PER_PAXEL  2500 // ((400/PAX_WIDTH) * (400/PAX_HEIGHT))

typedef enum {OV6946, OV6948} sensor_type_t;

typedef struct tagPaxelImage {unsigned int Width;
							  unsigned int Height;
							  unsigned int pRGBPlanes[3];       //RGB Planer Data
							  unsigned int pRGBNumSatPlanes; /*RGB Planer Data*/ }  PaxelImage;


typedef struct tagAEExpParams {float LED_Power;
                               unsigned int IntegrationLines;
							   float Gain;} AEExpParams;

typedef struct tagAEPaxelImage { AEExpParams ExpSettings;
								 PaxelImage AE_Img; }  AEPaxelImage;


typedef struct tagAEConfig {unsigned int  MaxIntegrationLines;
							float  LineTimeus;         
							float  ApertureFNo;        
							float  DampingFactor;       
							unsigned int  ClippedWhite;       
							unsigned  BlackLevelOffset;    
							float  RhoMax;             
							float  RhoAveSetPoint;     
							int    MinExpLines;
							int    MaxExpLines;
							float  MaxGain;             
							float  MinGain;              
							float  MinLED_PWR;  
							float  NominalLED_PWR;
							float  MaxLED_PWR;
							float  WBGains[3];
							float  CCMat[9];} AEConfig;

	

void Init_AE(AEConfig *pAEConfig);
int AE_Compute(AEPaxelImage *pImage, AEConfig *pConfig, AEExpParams  *pResult, sensor_type_t sensor);
float ComputeLumaEstimate(PaxelImage *pImg, AEConfig *pAEConfig);

#endif


#ifndef CURSOR_OVERLAY_H
#define CURSOR_OVERLAY_H
//==============================================================

int cursorOverlayInit(void);
void cursorOn(int x, int y, int width, int height);
void cursorOff(void);
void blinkerOn(int width, int height, int x, int y);

#endif
//==============================================================

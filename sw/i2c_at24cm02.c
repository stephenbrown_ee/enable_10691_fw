

#include "fpga_i2c1.h"
#include "hps2fpga_comm.h"
#include "white_bal.h"


void setEEPROMByte(uint32_t addr, uint8_t data)
{
	int baud = 50000;
	int ack = 0;
	int timeout = 0;
	while ((ack == 0) && (timeout < 100) ) {
		ack = i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, AT24CM02_ADDR | ((addr >> 15) & 0x6), 0);
		timeout++;
	}
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, ((addr >> 8) & 0xFF), 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, (addr >> 0) & 0xFF, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 1, data, 0);

}

uint8_t getEEPROMByte(uint32_t addr)
{
	uint8_t data = 0;
	int baud = 50000;
	int ack = 0;
	int timeout = 0;
	while ((ack == 0) && (timeout < 100) ) {
		ack = i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, AT24CM02_ADDR | ((addr >> 15) & 0x6), 0);
		timeout++;
	}
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, ((addr >> 8) & 0xFF), 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, (addr >> 0) & 0xFF, 0);
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, AT24CM02_ADDR | 0x1, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 1, 1, 0, &data);

	return data;
}

// This function will set the starting address for a sequential read of one page of data (256 bytes)
void startEEPROMRead (uint32_t addr) {
	int baud = 50000;
	int ack = 0;
	int timeout = 0;
	while ((ack == 0) && (timeout < 100) ) {
		ack = i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, AT24CM02_ADDR | ((addr >> 15) & 0x6), 0);
		timeout++;
	}
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, ((addr >> 8) & 0xFF), 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, (addr >> 0) & 0xFF, 0);
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, AT24CM02_ADDR | 0x1, 0);
}

// This function should be preceded by startEEPROMRead then be called sequentially for maximum of one page boundary (256 bytes)
uint8_t getSeqEEPROMByte (int stop) {
	uint8_t data = 0;
	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, stop, stop, 0, &data);
	return data;
}

void startEEPROMWrite  (uint32_t addr)
{
	int baud = 50000;
	int ack = 0;
	int timeout = 0;
	while ((ack == 0) && (timeout < 100) ) {
		ack = i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, AT24CM02_ADDR | ((addr >> 15) & 0x6), 0);
		timeout++;
	}
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, ((addr >> 8) & 0xFF), 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, (addr >> 0) & 0xFF, 0);
}

void setSeqEEPROMByte (uint8_t data, int stop)
{
	i2c(bus_i2cMasMSP430, 50000, 0, 1, 0, 0, stop, data, 0);
}

void setEEPROMTag (void)
{
	setEEPROMByte(TAG_ADDRESS, TAG_VALUE);
}

uint8_t getEEPROMTag (void)
{
	uint8_t data = getEEPROMByte(TAG_ADDRESS);
	printf("TAG = 0x%x\n", data);
	return data;
}

// This sends a start and stop condition to reset the i2c lines
void reset_i2c (void)
{
	i2c(bus_i2cMasMSP430, 50000, 1, 0, 0, 0, 1, 0, 0);
}

void writeModelNum (void)
{
	int data = getModelNumReg();
	setEEPROMByte(MODEL_NUM_ADDR_LO, data & 0xFF);
	setEEPROMByte(MODEL_NUM_ADDR_HI, (data >> 8) & 0xFF);
}

int readModelNum (void)
{
	int model_hi, model_lo, model;
	model_lo = getEEPROMByte(MODEL_NUM_ADDR_LO);
	model_hi = getEEPROMByte(MODEL_NUM_ADDR_HI);

	model = ((model_hi & 0xFF) << 8) | (model_lo & 0xFF);
	setModelNumReg(model);

	if ((model == 91) || (model == 92) || (model == 991) || (model == 992) || (model == 9991) || (model == 9992) )
		return 1;
	else
		return 0;
}

void writeWBtoEEPROM (int r, int g, int b)
{
	setEEPROMByte(WB_R_ADDR_LO, r & 0xFF);
	setEEPROMByte(WB_R_ADDR_HI, (r >> 8) & 0xFF);
	setEEPROMByte(WB_G_ADDR_LO, g & 0xFF);
	setEEPROMByte(WB_G_ADDR_HI, (g >> 8) & 0xFF);
	setEEPROMByte(WB_B_ADDR_LO, b & 0xFF);
	setEEPROMByte(WB_B_ADDR_HI, (b >> 8) & 0xFF);
	setEEPROMByte(WBTAG_ADDRESS, WBTAG_VALUE);
}

void readWBfromEEPROM(void)
{
	int r_lo, r_hi, r, g_lo, g_hi, g, b_lo, b_hi, b;

	r_lo = getEEPROMByte(WB_R_ADDR_LO);
	r_hi = getEEPROMByte(WB_R_ADDR_HI);
	r = ((r_hi & 0xFF) << 8) | (r_lo & 0xFF);

	g_lo = getEEPROMByte(WB_G_ADDR_LO);
	g_hi = getEEPROMByte(WB_G_ADDR_HI);
	g = ((g_hi & 0xFF) << 8) | (g_lo & 0xFF);

	b_lo = getEEPROMByte(WB_B_ADDR_LO);
	b_hi = getEEPROMByte(WB_B_ADDR_HI);
	b = ((b_hi & 0xFF) << 8) | (b_lo & 0xFF);

	printf("White Bal: %d %d %d\n", r, g, b);
	setDefaultBalance(r, g, b);
	whiteBalSet(makeColor(r, g, b, 1000));
}


uint8_t getWBTag (void)
{
	uint8_t data = getEEPROMByte(WBTAG_ADDRESS);
	return data;
}

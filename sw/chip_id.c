

#include "chip_id.h"

//=======================================================================

#define CHIP_ID_LOWER32              FLD(bus_chipID, 0, 31, 0)
#define CHIP_ID_UPPER32              FLD(bus_chipID, 1, 31, 0)

//=======================================================================

int getChipID(uint32_t* chipID_hi, uint32_t* chipID_lo, int verbose)
{
	
	*chipID_lo = REG_GET(CHIP_ID_LOWER32);
	*chipID_hi = REG_GET(CHIP_ID_UPPER32);

	if(verbose)
		printf("CHIP ID: 0x%08x%08x\n", *chipID_hi, *chipID_lo);

	return 0;
}

//=======================================================================

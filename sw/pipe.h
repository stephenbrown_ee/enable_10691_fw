
#ifndef PIPE_H
#define PIPE_H
//==============================================================

#include "color.h"

typedef struct
{
	int percent[8];
	int lumaMid;
	int lumaAll;
} ExposureStats;

typedef struct ImageInfo
{
	int silent;
	int mode;
	int other;
	int source;     // 0=Imager 1=Simulator 2=Generator
	int subsource;
	int sensorFreq;
	int pixelFreq;
	int outputFreq;
	int pixelsPerFrame;
	int usecPerFrame;
	int outClockInv;
	int startColor; // 0=R 1=Gr 2=Gb 3=B
	int zoom;       // Output zoom - 1 or 2

	int hSize;      // Imager image width
	int vSize;      // Imager image height
	int hBlank;     // Imager hor blanking
	int vBlank;     // Imager ver blanking

	int outFormat;  //
	int hSizeOut;
	int vSizeOut;
	int hBlankOut;  // hSizeOut + hBlankOut = total hor time
	int vBlankOut;  // vSizeOut + vBlankOut = total ver time

	int hLBlack;    // Left black   - From DE=1 to start of pixels
	int hRBlack;    // Right black  - From end of pixels to DE=0
	int vBBlack;    // Bottom black - Lines of DE=1 after pixels

	// hBlankOut is sum of these:
	int hFPorch;    // Hor sync front porch
	int hSync;      // Hor sync width
	int hBPorch;    // Hor sync back porch

	// vBlankOut is sum of these:
	int vFPorch;    // Ver sync front porch
	int vSync;      // Ver sync width (lines)
	int vBPorch;    // Ver sync back porch

	int senClip;     // Clip level to hide sensor saturation non uniformity
	int senBlack;
	int whiteClip;    // Arbitrary white point through pipe
	Scalar inpGain;   // Computed gain to get whiteClip from senClip
	Color whiteBal;   // Current white balance
	Color whiteGain;  // whiteBal * inpGain
	Matrix senMatrix; // Sensor color correction matrix
	Matrix corMatrix; // Adjusted matrix applied
	Color senBalance; // Initial rough balance for sensor CFA in daylight
	Color dispBalance; // Correction for display color shift

	int horPaxels;
	int verPaxels;
	int paxHorStart;
	int paxVerStart;
	int paxWidth;
	int paxHeight;
	int statsShift;

	int bypassPipe;
	int bypassWhite;
	int bypassFilter;
	int bypassBayer;
	int bypassMatrix;
	int bypassGamma;
	int singleOut;
	int invertSyncOut;
} ImageInfo;

//==============================================================

void pipeClockSetup(ImageInfo* img, int onTheFly); // This should be in main

//==============================================================

#endif
//==============================================================

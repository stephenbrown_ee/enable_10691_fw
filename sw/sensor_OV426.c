
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include "util.h"
#include "pipe.h"
#include "console.h"
#include "sensor.h"
#include "sys_mgr.h"
#include "bit_io.h"
#include "serial_i2c.h"
#include "math.h"
#include "fpga_i2c0.h"
#include "white_bal.h"
#include "math.h"
#include "sensorInput.h"

//=======================================================================

typedef struct
{
	uint16_t reg;
	uint8_t  dat;
} RegisterData;

#define MAX_DAC_VAL   716
#define DAC_STEP      2   // This is equivalent to about 1 mA of LED current per step
#define MAX_EXPOSURE  120 //
#define DEFAULT_WHITE_BAL 10

static int gain_setting = 0;

static Matrix senMatrix = {{{ 1963, -831, -132 },{ -364, 1616, -252 },{  -11, -629, 1641 }}, 1000};
static Color senBalance [] = 	{{{ 1235, 1000, 1093 }, 1000}  // 0
								,{{ 1228, 1000, 1197 }, 1000}
								,{{ 1198, 1000, 1343 }, 1000}
								,{{ 1188, 1000, 1385 }, 1000}
								,{{ 1175, 1000, 1444 }, 1000}
								,{{ 1175, 1000, 1453 }, 1000}  // 5
								,{{ 1172, 1000, 1475 }, 1000}
								,{{ 1171, 1000, 1492 }, 1000}
								,{{ 1170, 1000, 1506 }, 1000}
								,{{ 1170, 1000, 1513 }, 1000}
								,{{ 1170, 1000, 1524 }, 1000}  // 10
								,{{ 1150, 1000, 1580 }, 1000}  // 11
								};

//=======================================================================

Matrix sensorDefaultMatrix(void)
{
	return senMatrix;
}

//-----------------------------------------------------------------------

Color sensorDefaultBalance(void)
{
	return senBalance[DEFAULT_WHITE_BAL];
}

//-----------------------------------------------------------------------

void setDefaultBalance(int r, int g, int b)
{
	Color rgb = makeColor(r, g, b, 1000);
	senBalance[DEFAULT_WHITE_BAL] = rgb;
}

//-----------------------------------------------------------------------

RegisterData regDatOV6946 [] =
{
		{	0x0103	,	0x01 },
		{	0x3025	,	0x06 },
		{   0x3026  ,   0x1c },
		{   0x3205  ,   0x01 },
		{	0x0100	,	0x01 },

		{	0x3024	,	0x04 },
		{   0x3209  ,   0x03 }, // bit 0: Enable AEC auto update
		{   0x3701  ,   0x40 },
		{   0x3702  ,   0x4c },
		{   0x3003  ,   0x31 },
		{   0x3004  ,   0x00 },

		{   0x3204  ,   0x87 }, // 0x87
		{   0x3028  ,   0x80 }, // 0xb0
		{   0x3027  ,   0x30 }, // Was 0x22, changing to 0x20 for loading capacity and flicker issue with long cables

		{   0x5a40  ,   0x05 },
		{   0x3a19  ,   0x3e },
		{   0x5a00  ,   0x04 },

		{   0x4009  ,   0x18 },
		{   0x4005  ,   0x1a },
		{   0x3020  ,   0x09 },
		{   0x3024  ,   0x00 },

		{   0x3a0f  ,   WPT },  // WPT - stable range high limit (from unstable to stable)
		{   0x3a10  ,   BPT },  // BPT - stable range low limit (from unstable to stable)
		{   0x3a1b  ,   WPT2 },  // WPT2 - stable range high limit (from stable to unstable)
		{   0x3a1e  ,   BPT2 },  // BPT2 - stable range low limit (from stable to unstable)
		{   0x5690  ,   0x03 },
		{   0x5684  ,   0x01 },
		{   0x5685  ,   0x90 },
		{   0x5686  ,   0x01 },
		{   0x5687  ,   0x90 },

		{   0x3203  ,   0x27 },
		{   0x4052  ,   0x01 },

		{   0x302a  ,   0x01 },
		{   0x3a01  ,   0x10 },  // Sets the minimum exposure and eliminated blinking
		{ 0x3503, 0x23 },  // Turns off AGC/AEC
		{ 0x350b, 0x10 },  // Sets the real gain to 1.0x

	{ 0, 0 }
};

RegisterData regDatOV6948 [] =
{
		{	0x0103	,	0x01 },
		{	0x3025	,	0x02 },
		{   0x3026  ,   0x1c },
		{   0x3003  ,   0x13 },
		{	0x0100	,	0x01 },

//		{	0x3024	,	0x04 },
//		{   0x3209  ,   0x03 }, // bit 0: Enable AEC auto update
		{   0x3701  ,   0x40 },
		{   0x3702  ,   0x38 },
//		{   0x3003  ,   0x31 },

//		{   0x3204  ,   0x87 }, // 0x87
		{   0x3028  ,   0x80 }, // 0xb0
		{   0x3027  ,   0x30 }, // Was 0x22, changing to 0x20 for loading capacity and flicker issue with long cables
		{   0x3004  ,   0x06 }, // Adjust sampling position

//		{   0x5a40  ,   0x05 },
//		{   0x3a19  ,   0x3e },
		{   0x5a00  ,   0x04 },

		{ 0x3503, 0x23 },  // Turns off AGC/AEC
		{ 0x350b, 0x10 },  // Sets the real gain to 1.0x

		{   0x4009  ,   0x18 },
		{   0x4005  ,   0x1a },
		{   0x3020  ,   0x09 },
		{   0x3024  ,   0x00 },

//		{   0x3a0f  ,   0x58 },
//		{   0x3a10  ,   0x48 },
//		{   0x3a1b  ,   0x64 },
//		{   0x3a1e  ,   0x40 },

//		{   0x3203  ,   0x27 },
		{   0x4052  ,   0x01 },

//		{   0x302a  ,   0x01 },
//		{   0x3a01  ,   0x10 },  // Sets the minimum exposure and eliminated blinking

	{ 0, 0 }
};


//-----------------------------------------------------------------------

void setAEParams (AEExpParams *currentExp, AEExpParams *nextExp, sensor_type_t sensor)
{
	if(sensor == OV6946) {
		int gain = nextExp->Gain * 16;
		setGain(gain);
		setExposure(nextExp->IntegrationLines);
	}
	if(currentExp->LED_Power != nextExp->LED_Power) {
		setLED(nextExp->LED_Power);
	}
	*currentExp = *nextExp;
}
//-----------------------------------------------------------------------

void sensorSetup(sensor_type_t sensor)
{
//	int n;
//	if (sensor == OV6946) {
//		for (n = 0; regDatOV6946[n].reg != 0; n++)
//			sensorSet(regDatOV6946[n].reg, regDatOV6946[n].dat);
//	} else {
//		for (n = 0; regDatOV6948[n].reg != 0; n++)
//			sensorSet(regDatOV6948[n].reg, regDatOV6948[n].dat);
//	}
//	waitMsec(5);
//	sensorSet(0x3205, 0x00);
	setExposure(200);
	setGain(0x10);
}

//-----------------------------------------------------------------------

void sensorOff(void)
{
	out(IMAGER_RESET_, 0);
	out(IMAGER_PWDN, 1);
}

//-----------------------------------------------------------------------

int sensorGet(uint16_t address)
{
//	unsigned char value;
//
//	unsigned int baud = 20000;
//	i2c(bus_i2cMas, baud, 1, 1, 0, 0, 0, 0x6c, 0);
//	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 0, address >> 8, 0);
//	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 0, address >> 0, 0);
//	i2c(bus_i2cMas, baud, 1, 1, 0, 0, 0, 0x6d, 0);
//	i2c(bus_i2cMas, baud, 0, 0, 1, 1, 1, 0x00, &value);
//	return value;
	return 0;
}

//-----------------------------------------------------------------------

void sensorSet(uint16_t address, uint8_t value)
{
//	int baud = 20000;
//	i2c(bus_i2cMas, baud, 1, 1, 0, 0, 0, 0x6c, 0);
//	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 0, address >> 8, 0);
//	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 0, address >> 0, 0);
//	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 1, value >> 0, 0);
	sensorCtlSet(address, value);
}

sensor_type_t getSensorID()
{
	int sensor_type = 0;
	int prev_sensor_type = 0;
	int timeout = 0;
	int success_cnt = 0;
	out(IMAGER_PWDN, 1);
	out(IMAGER_RESET_, 0);
	out(IMAGER_PWDN, 0);
	waitMsec(20);
	out(IMAGER_RESET_, 1);
	waitMsec(20);
	sensorSet(0x0103, 0x01);  // Reset
	sensorSet(0x0100, 0x01);  // Streaming
	sensorSetup(OV6946);
	while ((success_cnt < 5) && (timeout < 100)) {
		waitMsec(50);
		sensor_type = sensorGet(0x300b);
		if (sensor_type == prev_sensor_type) {
			success_cnt++;
		} else {
			success_cnt = 0;
		}
		prev_sensor_type = sensor_type;
		timeout++;
	}
	if (timeout == 100) {
		printf("Sensor ID check timed out.\n");
		sensor_type = 0;
	}
	if (sensor_type == 0x46) {
		printf("Sensor type: OV6946\n");
		return OV6946;
	}
	else if (sensor_type == 0x48) {
		printf("Sensor type: OV6948\n");
		return OV6948;
	}
	else {
		printf("Invalid sensor type: %x\n", sensor_type);
		return -1;
	}
}

//-----------------------------------------------------------------------

int maxExposure (void)
{
	return MAX_EXPOSURE;
}







//-----------------------------------------------------------------------

void setExposure (int lines) {
	uint8_t hiByte = (lines >> 8);
	uint8_t loByte = (lines & 0xFF);

	sensorSet(0x1, loByte);
	sensorSet(0x0, hiByte);

//	if (lines < (DEFAULT_WHITE_BAL + 1)) {
//		whiteBalSet(senBalance[lines - 1]);
//	}
//	else {
//		whiteBalSet(senBalance[DEFAULT_WHITE_BAL]);
//	}
}

//-----------------------------------------------------------------------

void setGain (int gain) {
//	sensor gain register
//  gain = 2^n * (x + 16)/16
//  where n is upper 4 bits, x is lower 4 bits

	gain_setting = gain;  // set variable for FPN correction

	// change value to meet register criteria
	if(gain < 32)
	{
		gain = gain - 16;
	}
	else
	{
		gain = gain/2;
	}
	sensorSet(0x2, gain);
}

//-----------------------------------------------------------------------

int getGain (void)
{
	return gain_setting;
}

//-----------------------------------------------------------------------

void sweepExp(void)
{
	for (int i = 1; i<100; i+=1)
	{
		setExposure(i);
		waitMsec(50);
	}
	for (int i = 100; i > 0; i-=1)
	{
		setExposure(i);
		waitMsec(50);
	}
}

//-----------------------------------------------------------------------

void sweepGain(void)
{
	for (int i = 0x10; i<0x30; i+=1)
	{
		setGain(i);
		waitMsec(50);
	}
	for (int i = 0x30; i > 0x10; i-=1)
	{
		setGain(i);
		waitMsec(50);
	}
}
//=======================================================================


#ifndef FILTER_H
#define FILTER_H
//==============================================================

void filterInit(int Hpix, int wait);
void filterStartcolor(int color);
void filterBypass(int buffer, int filter);

#endif
//==============================================================


#ifndef BIT_IO_H
#define BIT_IO_H
//==============================================================

#define Bit4
#define Bit3

#include "bus_io.h"

//==============================================================

typedef enum BitInpPin
{
	outClkLocked,
	USER_DIPSW2,
	USER_DIPSW1,
	USER_DIPSW0,
	BITINPPIN_MAX
} BitInpPin;

//--------------------------------------------------------------

typedef enum BitOutPin
{
	outClkSelect,
	SCOPE_DETECT_LED,
	FX3_PMODE0,
	FX3_RESETn,        // Reset signal to Cypress Fx3
	BLEN,              // 0: Disable backlight on LCD  1: Enable
	regUpdate,         // signals to FPGA that registers are updating so don't load new values yet so they stay synchronous
	IMAGER_RESET_,
	IMAGER_PWDN,
	HDMI_REDRIVER_OEn,
    LED_DRV_PWM,
    LED_DRV_SHDN,
    outputClockInvert,
	BITOUTPIN_MAX
} BitOutPin;

//==============================================================

void out(BitOutPin pin, int value);
int inp(BitInpPin pin);
int outPinStatus(BitOutPin pin);

#endif
//==============================================================

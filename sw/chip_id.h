//==============================================================
// Stephen Brown
//==============================================================
#ifndef CHIP_ID_H
#define CHIP_ID_H
//==============================================================

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"

int getChipID(uint32_t* chipID_hi, uint32_t* chipID_lo, int verbose);


#endif
//==============================================================



#include "fpga_i2c0.h"

#define MCP4552_SLV_ADDR 0x58

#define WIPER_0_ADDR  0x00

void setResistor (int value);
void digPotSet(uint8_t address, uint8_t digValue);

void initPot(int value)
{
	setResistor(value);
	printf("Init Pot %d\n", value);
}

void setResistor (int value)
{
	digPotSet(WIPER_0_ADDR, value & 0xFF);
}


/*
 *  The MCP4552 has 257 steps 
 *  The steps should go from 75 to 5kohms of resistance
 */
void digPotSet(uint8_t address, uint8_t digValue)
{
	int baud = 20000;
	i2c(bus_i2cMas, baud, 1, 1, 0, 0, 0, MCP4552_SLV_ADDR, 0);
	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 0, address >> 0, 0);
	i2c(bus_i2cMas, baud, 0, 1, 0, 0, 1, (digValue >> 0) & 0xFF, 0);
}



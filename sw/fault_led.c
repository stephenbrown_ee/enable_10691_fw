

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "pipe.h"
#include "fault_led.h"
#include "bus_io.h"
#include "bit_io.h"
#include "sys_mgr.h"
#include "console.h"

//=======================================================================

#define PWM_FLT_RESET                BIT(bus_pwmFlt, 0, 0)
#define PWM_FLT_ENABLE               BIT(bus_pwmFlt, 0, 1)
#define PWM_FLT_COUNTMAX             FLD(bus_pwmFlt, 1, 23, 0)  // This the number of 100MHz clock cyles in one period
#define PWM_FLT_RISECOUNT            FLD(bus_pwmFlt, 2, 23, 0)  // This is the clock count where the PWM signal rises
#define PWM_FLT_FALLCOUNT            FLD(bus_pwmFlt, 3, 23, 0)  // This is the clock count where the PWM signal falls

//=======================================================================

void faultLEDState(int ledState)
{
	
}

void pwmFaultSetup(int periodClocks, int dutyPercent)
{
	int fallCount = periodClocks * dutyPercent / 100;
//	printf("PWM fall count: %d\n", fallCount);
	
	REG_SET(PWM_FLT_RESET, 1);
	REG_SET(PWM_FLT_RESET, 0);
	REG_SET(PWM_FLT_ENABLE, 0);
	REG_SET(PWM_FLT_COUNTMAX, periodClocks);
	REG_SET(PWM_FLT_RISECOUNT, 0);
	REG_SET(PWM_FLT_FALLCOUNT, fallCount);
	
	REG_SET(PWM_FLT_ENABLE, 1);
}

//=======================================================================

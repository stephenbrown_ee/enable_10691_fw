

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "pipe.h"
#include "frame_buffer.h"
#include "image_gen.h"
#include "hps_frame_buffer.h"
#include "hps2fpga_comm.h"

//=======================================================================

#define FRM_BFR_RESET               BIT(bus_frmBuf, 0, 0)
#define FRM_BFR_DDRGLOBALRESET      BIT(bus_frmBuf, 0, 1)
#define FRM_BFR_DDRSOFTRESET        BIT(bus_frmBuf, 0, 2)
#define FRM_BFR_ENABLE              BIT(bus_frmBuf, 0, 3)
#define FRM_BFR_BURSTSIZE           FLD(bus_frmBuf, 0, 7, 4)
#define FRM_BFR_EN_INPFIFO          BIT(bus_frmBuf, 0, 12)
#define FRM_BFR_STARTCLR            BIT(bus_frmBuf, 0, 13)
#define FRM_BFR_STOPREAD            BIT(bus_frmBuf, 0, 14)
#define FRM_BFR_STARTRD             BIT(bus_frmBuf, 0, 15)
#define FRM_BFR_INITDONE            BIT(bus_frmBuf, 0, 28)
#define FRM_BFR_CALSUCCESS          BIT(bus_frmBuf, 0, 29)
#define FRM_BFR_CALFAIL             BIT(bus_frmBuf, 0, 30)
#define FRM_BFR_CLEARBUFFER         BIT(bus_frmBuf, 0, 31)
#define FRM_BFR_ADRSTART            FLD(bus_frmBuf, 1, 19, 0)
#define FRM_BFR_SIZE			    FLD(bus_frmBuf, 1, 23, 20)
#define FRM_BFR_RAMINPSTARTADR	    FLD(bus_frmBuf, 1, 31, 24)
#define FRM_BFR_OUTADRSTART         FLD(bus_frmBuf, 2, 19, 0)
#define FRM_BFR_TIMEOUT             FLD(bus_frmBuf, 2, 31, 20)
//#define FRM_BFR_IMGADDR         FLD(bus_frmBuf, 2, 25, 19)
#define FRM_BFR_INPBRSTS_PERLINE    FLD(bus_frmBuf, 3,  7, 0)
#define FRM_BFR_INPSTRIDE_BLNKING   FLD(bus_frmBuf, 3, 21, 8)
#define FRM_BFR_FIFOLIMIT           FLD(bus_frmBuf, 3, 30, 22)
#define FRM_BFR_OUTBRSTS_PERLINE    FLD(bus_frmBuf, 4,  7, 0)
#define FRM_BFR_OUTSTRIDE_BLNKING   FLD(bus_frmBuf, 4, 21, 8)
#define FRM_BFR_LINESTART           FLD(bus_frmBuf, 4, 23, 22)
#define FRM_BFR_INPVS_MAX           FLD(bus_frmBuf, 5, 15, 0)  // experimentally derived number of clocks to assert inpVS after frame end sync to allow FIFO time to flush
#define FRM_BFR_WAITFORFIFOMAX      FLD(bus_frmBuf, 5, 27, 16)
#define FRM_BFR_HORMAX              FLD(bus_frmBuf, 6, 11, 0)  // Total length of line (active + blanking)
#define FRM_BFR_IMGHORMAX           FLD(bus_frmBuf, 6, 23, 12) // Total pixels in line
#define FRM_BFR_IMGVERMAX           FLD(bus_frmBuf, 7, 11, 0)  // Total lines in image
#define FRM_BFR_RAMOUTSTARTADDR       FLD(bus_frmBuf, 7, 19, 12) // Last address in each line of ram output blocks (used to flip image)



//=======================================================================

int frameBufferInit(int imgWidth, int imgHeight, int silent)
{
	int burstSize = 2;
	int cyclicBfrMax = 15;

	REG_SET(FRM_BFR_EN_INPFIFO, 0);
	REG_SET(FRM_BFR_STOPREAD, 1);
	REG_SET(FRM_BFR_ENABLE, 0);
	REG_SET(FRM_BFR_RESET, 1);
	regClear(bus_frmBuf, 8);
	REG_SET(FRM_BFR_RESET, 0);
	REG_SET(FRM_BFR_STARTCLR, 0);

	REG_SET(FRM_BFR_INPVS_MAX, 256);
	REG_SET(FRM_BFR_TIMEOUT, 100);
	REG_SET(FRM_BFR_WAITFORFIFOMAX, 8);

	frameBufferSetup(imgWidth, imgHeight, burstSize, silent);

	REG_SET(FRM_BFR_BURSTSIZE, burstSize);
	REG_SET(FRM_BFR_SIZE, cyclicBfrMax);
	setMaxHpsBfr(cyclicBfrMax);
	REG_SET(FRM_BFR_EN_INPFIFO, 1);
//	REG_SET(FRM_BFR_STOPREAD, 0);
	REG_SET(FRM_BFR_ENABLE, 1);
	return 0;
}

//-----------------------------------------------------------------------

void frameBufferSetup(int imgHeight, int imgWidth, int burstSize, int silent)
{

	int pixelsPerTransfer = 4;
	int pixelsPerBurst = burstSize*pixelsPerTransfer; // The number of pixels in a line divided by pixels per burst cannot be fractional
	int horBlanking = 100;
	int inpAdrStart = (imgHeight - 1) * (imgWidth/4);

	int inpBurstsPerLine = (imgWidth / pixelsPerBurst);  // This number cannot be fractional
	int outBurstsPerLine = imgWidth / pixelsPerBurst;
	int inpStrideBlanking = -2 * (imgWidth/4) ;

	int outStrideBlanking = 0;
	int outAdrStart = 0;

//  Remove print statements or PIO interrupt hangs processor
	if (!silent) {
		printf("Output Address Start: %d\n", outAdrStart);
		printf("Output bursts per line: %d\n", outBurstsPerLine);
		printf("Output stride blanking: %d\n", outStrideBlanking);
		printf("Input bursts per line: %d\n", inpBurstsPerLine);
	}
	REG_SET(FRM_BFR_INPSTRIDE_BLNKING, inpStrideBlanking);
	REG_SET(FRM_BFR_INPBRSTS_PERLINE, inpBurstsPerLine);
	REG_SET(FRM_BFR_ADRSTART, inpAdrStart);
//	REG_SET(FRM_BFR_RAMINPSTARTADR, imgWidth/4 - 1);
	REG_SET(FRM_BFR_RAMINPSTARTADR, 0);

	REG_SET(FRM_BFR_OUTADRSTART, outAdrStart);
	REG_SET(FRM_BFR_OUTSTRIDE_BLNKING, outStrideBlanking);

	REG_SET(FRM_BFR_HORMAX, imgWidth + horBlanking);
	REG_SET(FRM_BFR_IMGHORMAX, imgWidth);
	REG_SET(FRM_BFR_IMGVERMAX, imgHeight);
	REG_SET(FRM_BFR_RAMOUTSTARTADDR, 0);
	REG_SET(FRM_BFR_FIFOLIMIT, 512 - imgWidth/4 - 1);
	REG_SET(FRM_BFR_LINESTART, 2);
	REG_SET(FRM_BFR_OUTBRSTS_PERLINE, outBurstsPerLine);
}


//=======================================================================

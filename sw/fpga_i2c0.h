
#ifndef FPGA_I2C0_H_
#define FPGA_I2C0_H_
//==============================================================

#include <stdint.h>
#include <stdio.h>
#include "bit_io.h"
#include "serial_i2c.h"
#include "sys_mgr.h"
#include "hps2fpga_comm.h"

void initLED(int enable, int value);
void initADV7513(void);
void setDAC(float value);
int i2cGet_adv7513(uint8_t reg);
void i2cSet_adv7513(uint8_t reg, uint8_t dat);
void dacSet(uint8_t address, uint16_t digValue);
void upLED(void);
void downLED(void);
void setLED(float value);
void initPot(int value);


#endif


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "sys_mgr.h"
#include "console.h"
#include "pipe.h"
#include "stats.h"
#include "cursor_overlay.h"
#include "astar.h"

//=======================================================================

#define EXP_STS_RESET            BIT(bus_expSts, 0, 0)
#define EXP_STS_CLEAR            BIT(bus_expSts, 0, 1)
#define EXP_STS_ENABLE           BIT(bus_expSts, 0, 2)
#define EXP_STS_START            BIT(bus_expSts, 0, 3)
#define EXP_STS_RUN              BIT(bus_expSts, 0, 6)
#define EXP_STS_READFRAME        BIT(bus_expSts, 0, 7)
#define EXP_STS_FRAME            FLD(bus_expSts, 0, 31, 24)
#define EXP_STS_COLHORMAX        FLD(bus_expSts, 1, 11, 0)  // Width of col - 1
#define EXP_STS_ROWVERMAX        FLD(bus_expSts, 1, 27, 16) // Height of row - 1
#define EXP_STS_CLIPTHRESH       FLD(bus_expSts, 2, 11, 0)  // Threshold for clipping
#define EXP_STS_HORMIN           FLD(bus_expSts, 3, 11, 0)  // Start position of stats
#define EXP_STS_HORMAX           FLD(bus_expSts, 3, 27, 16)  // Start position of stats
#define EXP_STS_VERMIN           FLD(bus_expSts, 4, 11, 0)  // Start position of stats
#define EXP_STS_VERMAX           FLD(bus_expSts, 4, 27, 16)  // Start position of stats
#define EXP_STS_R_FRAME          FLD(bus_expSts, 5, 31, 0)
#define EXP_STS_G_FRAME          FLD(bus_expSts, 6, 31, 0)
#define EXP_STS_B_FRAME          FLD(bus_expSts, 7, 31, 0)
#define EXP_STS_C_FRAME          FLD(bus_expSts, 8, 31, 0)
#define EXP_STS_R_CNT            FLD(bus_expSts, 9, 31, 0)
#define EXP_STS_G_CNT            FLD(bus_expSts, 10, 31, 0)
#define EXP_STS_B_CNT            FLD(bus_expSts, 11, 31, 0)


//=======================================================================

static ImageInfo imgInfo;

static ImageInfo* imgNorm = 0;

static int statsMarkEnable = 0;

//=======================================================================

int statsInit(void)
{

	regClear(bus_expSts, 4);

	REG_SET(EXP_STS_RESET, 1);

	REG_SET(EXP_STS_RESET, 0);


	return 0;
}

//-----------------------------------------------------------------------

int statsSetup(ImageInfo* img, sensor_type_t sensor)
{
	imgNorm = img;

	img->horPaxels   = PAX_WIDTH;
	img->verPaxels   = PAX_HEIGHT;
	img->paxWidth    = img->hSize / img->horPaxels;
	img->paxHeight   = img->vSize / img->verPaxels;

	if (!img->silent)
	{
		printf("Stats: hSize.%04d vSize.%04d horPaxels.%04d verPaxels.%04d\n",
			img->hSize, img->vSize, img->horPaxels, img->verPaxels);
	}

	REG_SET(EXP_STS_COLHORMAX, img->paxWidth - 1);
	REG_SET(EXP_STS_ROWVERMAX, img->paxHeight - 1);
	REG_SET(EXP_STS_CLIPTHRESH, img->whiteClip);
	REG_SET(EXP_STS_ENABLE, 1);
	setAEwindow(0,0,img->hSize,img->vSize, sensor);

	return 0;
}

void statsSet(sensor_type_t sensor, int silent)
{
	ImageInfo* img = &imgInfo;

	memset(img, 0, sizeof(ImageInfo));

	img->silent = silent;
	if (sensor == OV6946) {
		img->hSize = 400;
		img->vSize = 400;
	}
	else
	{
		img->hSize = 200;
		img->vSize = 200;
	}

	img->whiteClip = 971;

	statsSetup(img, sensor);

}

//-----------------------------------------------------------------------

void statsMarkPaxel(int col, int row)
{
	ImageInfo* img = imgNorm;
	int x = (col * img->paxWidth);
	int y = (row * img->paxHeight);

	cursorOn(x, y, img->paxWidth, img->paxHeight);
}

//-----------------------------------------------------------------------

void statsMark(int enable)
{
	statsMarkEnable = enable;
}

//-----------------------------------------------------------------------

int statsCollect(PaxelImage *paxImg)
{
	int r_cnt = REG_GET(EXP_STS_R_CNT);
	int g_cnt = REG_GET(EXP_STS_G_CNT);
	int b_cnt = REG_GET(EXP_STS_B_CNT);
	int rf = REG_GET(EXP_STS_R_FRAME);
	int gf = REG_GET(EXP_STS_G_FRAME);
	int bf = REG_GET(EXP_STS_B_FRAME);

	if(debugLog(0)) {
		printf("Rf: %d  Gf: %d  Bf: %d\n", rf, gf, bf);
		printf("Rc: %d  Gc: %d  Bc: %d\n", r_cnt, g_cnt, b_cnt);
	}
	int r = rf / r_cnt;
	int g = gf / g_cnt;
	int b = bf / b_cnt;
	paxImg->pRGBPlanes[0] = r;
	paxImg->pRGBPlanes[1] = g;
	paxImg->pRGBPlanes[2] = b;
	paxImg->pRGBNumSatPlanes = REG_GET(EXP_STS_C_FRAME);
	if(debugLog(0))
		printf("R: %d  G: %d  B: %d\n", r, g, b);

	return 0;
}

//-----------------------------------------------------------------------

int statsActualFrame(void)
{
	return REG_GET(EXP_STS_FRAME);
}

//-----------------------------------------------------------------------

void setAEwindow(uint16_t xstart, uint16_t ystart, uint16_t width, uint16_t height, sensor_type_t sensor)
{
	REG_SET(EXP_STS_HORMIN, xstart);
	REG_SET(EXP_STS_HORMAX, xstart + width);
	REG_SET(EXP_STS_VERMIN, ystart);
	REG_SET(EXP_STS_VERMAX, ystart + height);
	REG_SET(EXP_STS_COLHORMAX, width/PAX_WIDTH - 1);
	REG_SET(EXP_STS_ROWVERMAX, height/PAX_HEIGHT - 1);
}


//=======================================================================

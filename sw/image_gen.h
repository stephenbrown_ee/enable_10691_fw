
#ifndef IMAGE_GEN_H
#define IMAGE_GEN_H
//==============================================================

int imageGenInit(int imgWidth, int imgHeight);
void imageGenStartcolor(int color);
void imageGenSource(int source, int overlay);
void imageGenSetup(int width, int height, int hIdle, int vIdle, int startColor, int pattern);
void imageGenPatt(int pattern, int scale);
void imageGenSolid(int R, int G, int B);
void imageGenRun(void);
void imageGenClearBuffer(void);

#endif
//==============================================================

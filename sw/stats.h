
#ifndef STATS_H
#define STATS_H
//==============================================================

#include "pipe.h"
#include "sensor.h"
#include "IntraVu_AE.h"

//==============================================================

int statsInit(void);
int statsSetup(ImageInfo* img, sensor_type_t sensor);
void statsSet(sensor_type_t sensor, int silent);
int statsActualFrame(void);
int statsReady(void);
int statsCollect(PaxelImage *paxImg);
void statsMark(int enable);
int statsStart(void);
void setAEwindow(uint16_t xstart, uint16_t ystart, uint16_t width, uint16_t height, sensor_type_t sensor);

#endif
//==============================================================



#include "fpga_i2c0.h"
#include "fpga_i2c1.h"
#include "chip_id.h"
#include "hps2fpga_comm.h"
#include "pix_correction.h"

// These are the addresses for the different controls on the MSP430
#define CONTROL_REG_WO     0x00
#define STATUS_REG_RO      0x01
#define KEYINPUT_REG       0x02
#define KEYINPUT_REG_RO    0x03
#define LOTNUM_REG_WO      0x04
#define LOTNUM_REG_RO      0x05
#define EXP_DATE_REG_WO    0x08
#define EXP_DATE_REG_RO    0x09
#define SW_VERSION_REG_WO  0x0A
#define SW_VERSION_REG_RO  0x0B
#define BATCH_NUM_REG_WO   0x0C
#define BATCH_NUM_REG_RO   0x0D
#define ADC_REG_WO         0x0E
#define ADC_REG_RO         0x0F


//----- Function prototypes ----------------------------
void setExpDateReg(uint8_t offset, uint8_t value);
void setLotNumReg(uint8_t offset, uint8_t value);


int handleInit(int enable, sensor_type_t sensor)
{
	int deSWversion = 0;

	if (sensor == OV6946)
		currentSetting = 25;
	else
		currentSetting = 1;

	initLED(enable, currentSetting);
	if (setControlReg(0)) {
		deSWversion = getSwVersionReg();
		printf("DE SW version: %x\n", deSWversion);
		getBatchNum();
	}
	else {
		printf("No MCU in scope.\n");
	}
	setDEVersion(deSWversion);

	return deSWversion;
}


int setControlReg(uint16_t value)
{
	int baud = 50000;
	int ack = 0;
	ack = i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, CONTROL_REG_WO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, value >> 8, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 1, (value >> 0) & 0xFF, 0);
	return ack;
}

int getStatusReg(void)
{
	uint8_t hiByte;
	uint8_t loByte;
	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, STATUS_REG_RO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &hiByte);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 1, 0x00, &loByte);

	if (loByte == (CONTROL_REG_WO ^ hiByte))
		return hiByte;
	else
		return CRC_FAILURE;
}

int getSwVersionReg(void)
{
	uint8_t hiByte;
	uint8_t loByte;
	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, SW_VERSION_REG_RO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &hiByte);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 1, 0x00, &loByte);

	if (loByte == (SW_VERSION_REG_WO ^ hiByte))
		return hiByte;
	else
		return CRC_FAILURE;
}

int checkStatus(void)
{
	int status;
	int timeOut = 0;

	status = getStatusReg();
	while ((status == CRC_FAILURE) && (timeOut < 3)) {
		status = getStatusReg();
		timeOut++;
	}
	return status;
}

int getKeyInputReg(void)
{
	uint8_t hiByte;
	uint8_t loByte;
	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, KEYINPUT_REG_RO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &hiByte);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 1, 0x00, &loByte);

	if (loByte == (KEYINPUT_REG ^ hiByte))
		return hiByte;
	else {
//		printf("Bad CRC - Data Received: 0x%x%x\n", hiByte, loByte);
		return CRC_FAILURE;
	}
}

void setBatchNumReg(uint8_t offset, uint8_t value)
{
	int baud = 50000;

	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, BATCH_NUM_REG_WO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, offset, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, value, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 1, (BATCH_NUM_REG_WO ^ offset ^ value), 0);
	waitMsec(50);
}

void setBatchNumRegSequence (uint16_t batch_num_value)
{
	setBatchNumReg(0, (uint8_t)((batch_num_value >> 8) & 0xFF));
	setBatchNumReg(1, (uint8_t)((batch_num_value >> 0) & 0xFF));
	setBatchNumReg(2, (uint8_t)(((batch_num_value >> 8) & 0xFF) ^ ((batch_num_value >> 0) & 0xFF)));
}

uint16_t getBatchNumReg (int verbose)
{
	uint16_t batchNumReg;
	uint8_t byte0, byte1, crcByte;

	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, BATCH_NUM_REG_RO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte1);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 1, 0x00, &crcByte);

	batchNumReg = ((byte0 << 8) | (byte1 << 0));

	if ((crcByte == (byte0 ^ byte1 )) || (batchNumReg == 0xFFFF))
	{
		return batchNumReg;
	} else {
		if(verbose)
			printf("Bad Batch Num CRC:  0x%02x%02x%02x\n", byte0, byte1, crcByte);
		return CRC_FAILURE;
	}
}

int getAngle (void)
{
	uint16_t AdcReg;
	uint8_t byte0, byte1;
	int scale = 320;

	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, ADC_REG_RO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 1, 0x00, &byte1);

	AdcReg = ((byte1 << 8) | (byte0 << 0));

	return ((AdcReg * scale) / 1024);
}

void setLotNumRegSequence(uint32_t chipID_lo)
{
	setLotNumReg(0, (uint8_t)((chipID_lo >> 24) & 0xFF));
	setLotNumReg(1, (uint8_t)((chipID_lo >> 16) & 0xFF));
	setLotNumReg(2, (uint8_t)((chipID_lo >> 8) & 0xFF));
	setLotNumReg(3, (uint8_t)((chipID_lo >> 0) & 0xFF));
	setLotNumReg(4, (uint8_t)(((chipID_lo >> 24) & 0xFF) ^ ((chipID_lo >> 16) & 0xFF) ^ ((chipID_lo >> 8) & 0xFF) ^ ((chipID_lo >> 0) & 0xFF)));
}

void setLotNumReg(uint8_t offset, uint8_t value)
{
	int baud = 50000;

	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, LOTNUM_REG_WO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, offset, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, value, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 1, (LOTNUM_REG_WO ^ offset ^ value), 0);
	waitMsec(50);
}

int getLotNumReg(int verbose)
{
	uint32_t lotNumReg;
	uint8_t byte0, byte1, byte2, byte3, crcByte;

	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, LOTNUM_REG_RO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte1);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte2);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte3);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 1, 0x00, &crcByte);

	lotNumReg = ((byte0 << 24) | (byte1 << 16) | (byte2 << 8) | (byte3 << 0));

	if ((crcByte == (byte0 ^ byte1 ^ byte2 ^ byte3)) || (lotNumReg == 0xFFFFFFFF))
	{
		return lotNumReg;
	} else {
		if(verbose)
			printf("Bad Lot Num CRC:  0x%02x%02x%02x%02x%02x\n", byte0, byte1, byte2, byte3, crcByte);
		return CRC_FAILURE;
	}
}

int getExpDateReg(uint32_t* expDateReg_upper, uint32_t* expDateReg_lower, int verbose)
{
	uint8_t byte0, byte1, byte2, byte3, byte4, byte5, byte6, crcByte;

	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, EXP_DATE_REG_RO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte1);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte2);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte3);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte4);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte5);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte6);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 1, 0x00, &crcByte);

	if (crcByte == (byte0 ^ byte1 ^ byte2 ^ byte3 ^ byte4 ^ byte5 ^ byte6))
	{
		*expDateReg_upper = ((byte0 << 24) | (byte1 << 16) | (byte2 << 8) | (byte3 << 0));
		*expDateReg_lower = ((byte4 << 24) | (byte5 << 16) | (byte6 << 8)) ;
		return 0;
	} else {
		if(verbose)
			printf("Bad Exp Date CRC:  0x%02x%02x%02x%02x%02x%02x%02x%02x\n", byte0, byte1, byte2, byte3, byte4, byte5, byte6, crcByte);
		return CRC_FAILURE;
	}
}

void setExpDateRegSequence(uint32_t expDate_hi, uint32_t expDate_lo)
{
	uint8_t byte0, byte1, byte2, byte3, byte4, byte5, byte6, crcByte;
	byte0 = (uint8_t)((expDate_hi >> 24) & 0xFF);
	byte1 = (uint8_t)((expDate_hi >> 16) & 0xFF);
	byte2 = (uint8_t)((expDate_hi >> 8) & 0xFF);
	byte3 = (uint8_t)((expDate_hi >> 0) & 0xFF);
	byte4 = (uint8_t)((expDate_lo >> 24) & 0xFF);
	byte5 = (uint8_t)((expDate_lo >> 16) & 0xFF);
	byte6 = (uint8_t)((expDate_lo >> 8) & 0xFF);
	crcByte = (byte0 ^ byte1 ^ byte2 ^ byte3 ^ byte4 ^ byte5 ^ byte6);

	setExpDateReg(0, byte0);
	setExpDateReg(1, byte1);
	setExpDateReg(2, byte2);
	setExpDateReg(3, byte3);

	setExpDateReg(4, byte4);
	setExpDateReg(5, byte5);
	setExpDateReg(6, byte6);
	setExpDateReg(7, crcByte);
}

void setExpDateReg(uint8_t offset, uint8_t value)
{
	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, EXP_DATE_REG_WO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, offset, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 0, value, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 1, 0, 0, 1, (EXP_DATE_REG_WO ^ offset ^ value), 0);
	waitMsec(5);
}

ButtonPress_t processDEButton(sensor_type_t sensor, int deVersion)
{
	ButtonPress_t data = 0;

	// Read the key input address in the MSP430 to determine which button was pressed
	data = (ButtonPress_t)getKeyInputReg();

	switch (data)
	{
		// No difference between long press and short press for the IntraVuLite product
		case DECR_BRIGHTNESS:
			break;
		case DECR_BRIGHTNESS_LONG_PRESS:
			break;
		// Do something in the future to record video
		case RECORD:
			// Capture an Image only
			captureImage();
			break;
		case RECORD_LONG_PRESS:
			// if video is active, then this button stops the video
			// else it starts the video
			if (startVideo == 1) {
				startVideo = 0;
			} else {
				startVideo = 1;
			}
			captureVideo(startVideo);
			break;

		case INCR_BRIGHTNESS:
		case INCR_BRIGHTNESS_LONG_PRESS:
//			if ((sensor == OV6946) || ((sensor == OV6948) && (agcOn == 0))) {
//				if (agcOn) {
//					recAE(2);
//				}
//				else {
//					upLED();
//				}
//			}
			break;
		case CRC_FAILURE:
			break;
		case CONNECTED_NOPRESS:
			break;
		default:
			break;
	}

	setControlReg(0);
	waitMsec(1);

	return data;
}


//ErrorCode_t checkDE(int verbose)
//{
//	uint32_t expDate_hi, expDate_lo;
//	uint32_t chipID_hi, chipID_lo;
//	uint32_t lotNumReg = 0;
//	uint32_t deSWversion = 0;
//	ButtonPress_t result;
//	int timeOut = 0;
//
//	// always print details for the manufacturing version
//	verbose = 1;
//
//	deSWversion = getSwVersionReg();
//	printf("DE version: %x\n", deSWversion);
//	setDEVersion(deSWversion);
//
//
//	lotNumReg = getLotNumReg(verbose);           // Get the lot number register from the DE
//	while ((lotNumReg == CRC_FAILURE) && (timeOut < 3))	{
//		if (getStatusReg() & 0x4) {  // Bit 2 must be a flash write failure indication
//			setControlReg(0);
//			if(verbose)
//				printf("DE Flash write failure. Trying again...\n");
//		}
//		lotNumReg = getLotNumReg(verbose);
//		timeOut++;
//	}
//	printf("Lot Num %08lx\n", lotNumReg);
//
////	if(timeOut == 3) {
////		setErrorReg(LOSSOFCOMM2HANDLE, ON);
////		return COMM_FAIL_LOTNUM;
////  }
//
//	timeOut = 0;
//	getChipID(&chipID_hi, &chipID_lo, verbose);  // Get the unique chip ID from the FPGA
//	printf("Chip ID %08lx %08lx\n", chipID_hi, chipID_lo);
//
//	result = getExpDateReg(&expDate_hi, &expDate_lo, verbose);
//
//	if (result == CRC_FAILURE) {
//		while ((result == CRC_FAILURE) && (timeOut < 3)) {
//			setControlReg(0);
//			result = getExpDateReg(&expDate_hi, &expDate_lo, verbose);
//			timeOut++;
//		}
////		if(timeOut == 3) {
////          setErrorReg(LOSSOFCOMM2HANDLE, ON);
////			return COMM_FAIL_EXPDATE;
////		}
//	}
//	printf("Exp Data %08lx %08lx\n", expDate_hi, expDate_lo);
//	return SUCCESS;
//
//}

ErrorCode_t checkDE(int verbose)
{
	uint32_t expDate_hi, expDate_lo;
	uint32_t rtcDate_hi, rtcDate_lo;
	uint32_t chipID_hi, chipID_lo;
	uint32_t lotNumReg = 0;
	ErrorCode_t status;
	ButtonPress_t result;
	int timeOut = 0;


	lotNumReg = getLotNumReg(verbose);           // Get the lot number register from the DE
	getChipID(&chipID_hi, &chipID_lo, verbose);  // Get the unique chip ID from the FPGA

	if (lotNumReg == 0xFFFFFFFF)       // An unprogrammed lot number and CRC fail end up with the same value
	{
		setLotNumRegSequence(chipID_lo);    // Try setting the lot number
		lotNumReg = getLotNumReg(verbose);  // Get the lot number register from the DE
		while (((lotNumReg == CRC_FAILURE)  || (lotNumReg != chipID_lo)) && (timeOut < 3))	{
			if (getStatusReg() & 0x4) {
				setControlReg(0);
				if(verbose)
					printf("DE Flash write failure (Lot num). Trying again...\n");
			}
			setLotNumRegSequence(chipID_lo);    // Try setting the lot number
			lotNumReg = getLotNumReg(verbose);
			timeOut++;
		}
		if (timeOut == 3) { // If it's not the same or the CRC failed
			setFaultLED(2);
			setErrorReg(FAIL2UPDATEFLASH, ON);
			return COMM_FAIL_LOTNUM;
		} else {
			if (verbose)
				printf("New lot number programmed to device.\n");
		}
	} else if (lotNumReg != chipID_lo) {  // If the lot number programmed in the DE does not match the unique chip ID of the FPGA
		setErrorReg(BADLOTNUMBER, ON);
		setFaultLED(2);
		return BAD_LOT_NUMBER;
	}

	timeOut = 0;

	status = getRTCData(&rtcDate_hi, &rtcDate_lo, verbose);   // Get the RTC data from the HPS
	if(status != SUCCESS) {
		return hpsStatusReg();
	}

	result = getExpDateReg(&expDate_hi, &expDate_lo, verbose);
	if (expDate_hi == 0xFFFFFFFF)
	{
		setExpDateRegSequence(rtcDate_hi, rtcDate_lo);
		result = getExpDateReg(&expDate_hi, &expDate_lo, verbose);
		while (((result == CRC_FAILURE)  || (rtcDate_hi != expDate_hi)) && (timeOut < 3))	{
			if (getStatusReg() & 0x4) {
				setControlReg(0);
				if(verbose)
					printf("DE Flash write failure (exp date). Trying again...\n");
			}
			setExpDateRegSequence(rtcDate_hi, rtcDate_lo);    // Try setting the expiration date
			result = getExpDateReg(&expDate_hi, &expDate_lo, verbose);
			timeOut++;
		}
		if (timeOut == 3) {
			setFaultLED(2);
			setErrorReg(FAIL2UPDATEFLASH, ON);
			return COMM_FAIL_EXPDATE;
		} else {
			if(verbose)
				printf("Programmed new expiration date into flash.\n");
			return SUCCESS;
		}
	} else if (result == CRC_FAILURE){
		while ((result == CRC_FAILURE) && (timeOut < 3)) {
			setControlReg(0);
			result = getExpDateReg(&expDate_hi, &expDate_lo, verbose);
			timeOut++;
		}
		if(timeOut == 3) {
			setFaultLED(2);
			setErrorReg(FAIL2UPDATEFLASH, ON);
			return COMM_FAIL_EXPDATE;
		}
	}

	status = compareTime(rtcDate_hi, rtcDate_lo, expDate_hi, expDate_lo);

	return status;
}

int printExpDate(void)
{
	uint8_t byte0, byte1, byte2, byte3, byte4, byte5, byte6, crcByte;

	int baud = 50000;
	i2c(bus_i2cMasMSP430, baud, 1, 1, 0, 0, 0, EXP_DATE_REG_RO, 0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte0);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte1);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte2);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte3);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte4);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte5);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 0, 0x00, &byte6);
	i2c(bus_i2cMasMSP430, baud, 0, 0, 1, 0, 1, 0x00, &crcByte);


	printf("Exp Date:  0x%02x%02x%02x%02x%02x%02x%02x%02x\n", byte0, byte1, byte2, byte3, byte4, byte5, byte6, crcByte);
	return 0;
}

void setScopeTimeForce (void)
{
	uint32_t rtcDate_hi, rtcDate_lo;
	uint32_t chipID_hi, chipID_lo;
	getRTCData(&rtcDate_hi, &rtcDate_lo, 1);
	setExpDateRegSequence(rtcDate_hi, rtcDate_lo);
	getChipID(&chipID_hi, &chipID_lo, 1);  // Get the unique chip ID from the FPGA
	setLotNumRegSequence(chipID_lo);    // Try setting the lot number
//	set_scope_time_bit(0);
}

void baselineScopeFlash (void)
{
	uint32_t lotNumReg = 0;
	uint32_t expDateHi, expDateLo;
	int timeout = 0;
	while ((lotNumReg != 0xFFFFFFFF) && (timeout < 3) ) {
		setLotNumRegSequence(0xFFFFFFFF);
		waitMsec(50);
		lotNumReg = getLotNumReg(1);
		timeout++;
	}
	if (timeout == 3)
		printf("Lot Number wasn't programmed.\n");
	timeout = 0;
	while ((expDateHi != 0xFFFFFFFF) && (timeout < 3)) {
		setExpDateRegSequence(0xFFFFFFFF, 0xFFFFFFFF);
		waitMsec(50);
		getExpDateReg(&expDateHi, &expDateLo, 1);
		timeout++;
	}
	if (timeout == 3)
		printf("Expiration date wasn't programmed.\n");

	printf("Scope flash baselined.\n");
	trigger_scope_baseline(0);
}

void setBatchNum (void)
{
	uint16_t hpsBatchNumReg = 0;
	hpsBatchNumReg = getHpsBatchNumReg();
	setBatchNumRegSequence(hpsBatchNumReg);
    printf("Batch Num: %d\n", hpsBatchNumReg);
    set_batch_bit(0);
}

void getBatchNum (void)
{
	uint16_t batchNumReg = 0;
	batchNumReg = getBatchNumReg(1);
	setHpsBatchNumReg(batchNumReg);
	printf("Scope Lot Number: %d\n", batchNumReg);
	get_batch_bit(0);

}


#ifndef PAXEL_CAPTURE_H
#define PAXEL_CAPTURE_H
//==============================================================

#include "color.h"

int paxCaptureInit(void);
void paxCaptureStartcolor(int color);
void paxCaptureSetup(int hSize, int vSize);
int paxCapture(void);
void paxSet(int x, int y);
void paxOff(void);
Color paxAverageInpRgb(void);
Color paxAverageOutRgb(void);

#endif
//==============================================================

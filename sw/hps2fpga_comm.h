
#ifndef HPS2FPGA_COMM_H
#define HPS2FPGA_COMM_H
//==============================================================

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "fpga_i2c1.h"
#include "lcd_display_out.h"
#include "pwm_led.h"

enum { LOSSOFVIDEO, LOSSOFCOMM2HANDLE, FAIL2UPDATEFLASH, LOSSOFCOMM2HPSRTC, EXPIREDDE, BADLOTNUMBER };
enum {OFF, ON};
enum {LED_ON, LED_OFF};

//*** GLOBAL VARIABLES ******************
float currentSetting;
int startVideo;
//***************************************

ErrorCode_t getRTCData(uint32_t* exp_hi, uint32_t* exp_lo, int verbose);
ErrorCode_t hpsStatusReg(void);
ErrorCode_t compareTime(uint32_t rtcDate_hi, uint32_t rtcDate_lo, uint32_t expDate_hi, uint32_t expDate_lo);

void captureImage(void);
void captureVideo(int enable);
void setMaxHpsBfr (int max);
void setLedState(int state);
int initHps2FpgaComm (int major, int minor, int build);
void setScopeState2Hps (int state);
void setBrightness (void);
void setErrorReg (int error, int set);
void clearTriggerReg (int reg);
void setScaleReg (int reg);
void setDEVersion (int byte);
void setScopeInitialized2Hps (int state);
void start_calibration (int set);
int passwordDone (void);
int manufMode (void);
void clearErrorReg (void);
void trigger_scope_baseline(int set);
void get_batch_bit (int set);
void set_batch_bit (int set);
void setHpsBatchNumReg (uint16_t batchNum);
uint16_t getHpsBatchNumReg (void);
int getScopeInitialized2Hps (void);
void setSensorType (int sensor_type);
int getOvlyState (void);
int getAGCState (void);
int getCrop (void);
void clearCrop (void);
int getGain(void);
int getExp(void);
int getWindow(void);
void clearButtons(void);
void setHpsExposure (int exp);
void setHpsGain(int gain);
void setHpsLum(int lum);
void setHpsAeRange(uint8_t bpt, uint8_t bpt2, uint8_t wpt, uint8_t wpt2);
int getAE(void);
int setBatchReg(void);
int getBatchReg(void);
int getClickBal(void);
void clearClickBal(void);
int getROIMode(void);
int getROI (void);
void setROI(int mode, sensor_type_t sensor);
int getModelNum(void);
void clearGetModel(void);
int setModelNum(void);
void clearSetModel(void);
void setModelNumReg(int modelNum);
int getModelNumReg(void);
int setOutputSel();
int getOutputSel();
void flagVideoPressToHps(int set);

#endif
//==============================================================

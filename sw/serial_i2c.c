
#include <stdio.h>
#include <time.h>

#include "util.h"
#include "serial_i2c.h"
#include "serial_io.h"
#include "sys_mgr.h"

//=======================================================================

#define I2C_TXDATA       FLD(sel, 0, 7, 0)
#define I2C_START        BIT(sel, 0, 8)
#define I2C_TRANSMIT     BIT(sel, 0, 9)
#define I2C_RECEIVE      BIT(sel, 0, 10)
#define I2C_NAK          BIT(sel, 0, 11)
#define I2C_STOP         BIT(sel, 0, 12)
#define I2C_RUN          BIT(sel, 0, 13)
#define I2C_TXACK        BIT(sel, 0, 16)
#define I2C_COMPHASE     FLD(sel, 0, 19, 17)
#define I2C_BAUDDIV      FLD(sel, 0, 31, 20)
#define I2C_REG          FLD(sel, 0, 31, 0)
#define I2C_RXDATA       FLD(sel, 1, 7, 0)
#define I2C_VSYNC        BIT(sel, 2, 0)

//=======================================================================

int i2c(BusSel sel, int baud, int start, int transmit, int receive, int nak, int stop, unsigned char txData, unsigned char* rxData)
{
	int div = MIN(4095, sysClockFreq() / (baud * 3));

//	REG_SET(I2C_BAUDDIV,  div);
//	REG_SET(I2C_START,    start);
//	REG_SET(I2C_TRANSMIT, transmit);
//	REG_SET(I2C_RECEIVE,  receive);
//	REG_SET(I2C_NAK,      nak);
//	REG_SET(I2C_STOP,     stop);
//	REG_SET(I2C_TXDATA,   txData);

	REG_SET(I2C_REG, ((div & 0xFFF) << 20) | ((stop & 1) << 12) | ((nak & 1) << 11) | ((receive & 1) << 10) | ((transmit & 1) << 9) | ((start & 1) << 8) | ((txData & 0xFF) << 0));

	REG_SET(I2C_RUN, 1);
	waitMsec(1);
	int usec = getUsec();
	while ((REG_GET(I2C_COMPHASE) != 0) && (getUsec() - usec < 100000))
		waitUsec(100);

	if (receive && (rxData != 0))
		*rxData = REG_GET(I2C_RXDATA);

	return (REG_GET(I2C_TXACK) == 0);
}

void i2c_vsync(BusSel sel, int set) {
	REG_SET(I2C_VSYNC, set);
}
//=======================================================================

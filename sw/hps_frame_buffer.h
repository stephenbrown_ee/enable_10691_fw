
#ifndef HPS_FRAME_BUFFER_H
#define HPS_FRAME_BUFFER_H
//==============================================================

int hpsFrameBufferInit(int imgWidth, int imgHeight, int silent);
void hpsFrameBufferSetup(int imgWidth, int imgHeight, int burstSize, int silent);
void startHpsRead (int enable);

#endif
//==============================================================

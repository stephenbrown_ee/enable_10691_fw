
#ifndef BUS_IO_H
#define BUS_IO_H
//==============================================================

#define Bit4
#define Bit3

#include "../src/BusSel.h"

//==============================================================

int busInit(int baseAddress);
int dmaInit(int baseAddress);

extern int busBaseAddress;
extern int dmaBaseAddress;

void regClear(BusSel sel, int count);

void regSetFld(int sel, int reg, int hi, int lo, int value);
int  regGetFld(int sel, int reg, int hi, int lo);
void regSetBit(int sel, int reg, int lo, int value);
int  regGetBit(int sel, int reg, int lo);
void regSet(int sel, int sub, int reg, int value);
int  regGet(int sel, int sub, int reg);

//==============================================================

#define BUS_BASE               busBaseAddress
#define BUS_ADR(sel)           (BUS_BASE + ((sel) << 20))
#define BUS_ADR_SUB(sel, sub)  (BUS_BASE + (((sel) & 0xFF) << 20) + (((sub) & 0xF) << 16))

#define BUS_REG(sel, reg)	       (((volatile int*)(BUS_ADR(sel)))[reg])
#define BUS_REG_SUB(sel, sub, reg) (((volatile int*)(BUS_ADR_SUB((sel), (sub))))[reg])

#define DMA_BASE			      dmaBaseAddress
#define DMA_FRM_ADR(frm)          (DMA_BASE + ((frm) << 22))
#define DMA_FRM(frm, pxl)	      (((volatile int*)(DMA_FRM_ADR(frm)))[pxl])

// Internal use
#define FLD_SEL(fld)              (((fld) >> 24) & 0xFF)
#define FLD_REG(fld)              (((fld) >> 16) & 0xFF)
#define FLD_HI(fld)               (((fld) >>  8) & 0x1F)
#define FLD_LO(fld)               (((fld) >>  0) & 0x1F)
#define FLD_MASK(fld)             ((0xFFFFFFFF << FLD_LO(fld)) & (0xFFFFFFFF >> (31 - FLD_HI(fld))))
#define REG(fld)                  BUS_REG(FLD_SEL(fld), FLD_REG(fld))

// Use these
#define FLD(sel, reg, hi, lo)     ((((sel) & 0xFF) << 24) | (((reg) & 0xFF) << 16) | (((hi) & 0x1F) << 8) | (((lo) & 0x1F) << 0))
#define BIT(sel, reg, bit)        FLD(sel, reg, bit, bit)

#define REG_GET(fld)              ((REG(fld) & FLD_MASK(fld)) >> FLD_LO(fld))
#define REG_SET(fld, val)         (REG(fld) = (REG(fld) & ~FLD_MASK(fld)) | (((val) << FLD_LO(fld)) & FLD_MASK(fld)))

#endif
//==============================================================


#ifndef SENSOR_H_
#define SENSOR_H_
//==============================================================

#include <stdint.h>
#include <stdio.h>
#include "color.h"
#include "IntraVu_AE.h"

void sensorSetup(sensor_type_t sensor);
Matrix sensorDefaultMatrix(void);
Color sensorDefaultBalance(void);
void sensorOff(void);
int sensorGet(uint16_t address);
void sensorSet(uint16_t address, uint8_t value);
sensor_type_t getSensorID(void);
void setExposure (int lines);
int maxExposure(void);
void recExposure (int inc, int dec);
void recGain (int inc, int dec);
void recLum(void);
void setDefaultBalance(int r, int g, int b);
void setGain(int gain);
void setAEParams (AEExpParams *currentExp, AEExpParams *nextExp, sensor_type_t sensor);
int getGain(void);

#define WPT   62
#define BPT   52
#define WPT2  68
#define BPT2  46

#endif
//==============================================================

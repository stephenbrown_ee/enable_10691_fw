

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "pipe.h"
#include "hps_frame_buffer.h"
#include "image_gen.h"

//=======================================================================

#define HPS_FRM_BFR_RESET               BIT(bus_hpsFb, 0, 0)
#define HPS_FRM_BFR_ENABLE              BIT(bus_hpsFb, 0, 3)
#define HPS_FRM_BFR_BURSTSIZE           FLD(bus_hpsFb, 0, 7, 4)
#define HPS_FRM_BFR_STOPREAD            BIT(bus_hpsFb, 0, 14)
#define HPS_FRM_BFR_STARTRD             BIT(bus_hpsFb, 0, 15)
#define HPS_FRM_BFR_STOPOVLYREAD        BIT(bus_hpsFb, 0, 16)
#define HPS_FRM_BFR_SIZE			    FLD(bus_hpsFb, 1, 23, 20)
#define HPS_FRM_BFR_OUTADRSTART         FLD(bus_hpsFb, 2, 19, 0)
#define HPS_FRM_BFR_TIMEOUT             FLD(bus_hpsFb, 2, 31, 20)
#define HPS_FRM_BFR_LINESTART           FLD(bus_hpsFb, 3, 1, 0)
#define HPS_FRM_BFR_OUTBRSTS_PERLINE    FLD(bus_hpsFb, 4,  7, 0)
#define HPS_FRM_BFR_OUTSTRIDE_BLNKING   FLD(bus_hpsFb, 4, 20, 8)
#define HPS_FRM_BFR_OVLYBRSTS_PERLINE   FLD(bus_hpsFb, 5,  7, 0)
#define HPS_FRM_BFR_OVLYSTRIDE_BLNKING  FLD(bus_hpsFb, 5, 20, 8)
#define HPS_FRM_BFR_HORMAX              FLD(bus_hpsFb, 6, 11, 0)  // Total length of line (active + blanking)
#define HPS_FRM_BFR_IMGHORMAX           FLD(bus_hpsFb, 6, 23, 12) // Total pixels in line
#define HPS_FRM_BFR_IMGVERMAX           FLD(bus_hpsFb, 7, 11, 0)  // Total lines in image
#define HPS_FRM_BFR_FIFOLIMIT     		FLD(bus_hpsFb, 7, 20, 12)
#define HPS_FRM_BFR_OVLYFIFOLIMIT  		FLD(bus_hpsFb, 7, 31, 21)



//=======================================================================

int hpsFrameBufferInit(int imgWidth, int imgHeight, int silent)
{
	int burstSize = 2;

	REG_SET(HPS_FRM_BFR_STOPREAD, 1);
	REG_SET(HPS_FRM_BFR_ENABLE, 0);
	REG_SET(HPS_FRM_BFR_RESET, 1);
	regClear(bus_hpsFb, 8);
	REG_SET(HPS_FRM_BFR_RESET, 0);

	REG_SET(HPS_FRM_BFR_TIMEOUT, 1000);

	hpsFrameBufferSetup(imgWidth, imgHeight, burstSize, silent);

	REG_SET(HPS_FRM_BFR_BURSTSIZE, burstSize);
	REG_SET(HPS_FRM_BFR_SIZE, 15);
	REG_SET(HPS_FRM_BFR_STOPREAD, 0);
	REG_SET(HPS_FRM_BFR_ENABLE, 1);
	return 0;
}

//-----------------------------------------------------------------------

void hpsFrameBufferSetup(int imgHeight, int imgWidth, int burstSize, int silent)
{

	int pixelsPerTransfer = 4;
	int pixelsPerBurst = burstSize*pixelsPerTransfer; // The number of pixels in a line divided by pixels per burst cannot be fractional
	int horBlanking = 100;  // This number is arbitrary, the logic will check the parallel out FIFO to see if there's room for a line

	int outBurstsPerLine = imgWidth / pixelsPerBurst;
	int ovlyBurstsPerLine = 1024 / pixelsPerBurst;

	REG_SET(HPS_FRM_BFR_OUTADRSTART, 0);
	REG_SET(HPS_FRM_BFR_OUTSTRIDE_BLNKING, 0);
	REG_SET(HPS_FRM_BFR_OVLYSTRIDE_BLNKING, 0);

	REG_SET(HPS_FRM_BFR_HORMAX, imgWidth + horBlanking);
	REG_SET(HPS_FRM_BFR_IMGHORMAX, imgWidth);
	REG_SET(HPS_FRM_BFR_IMGVERMAX, imgHeight);
	REG_SET(HPS_FRM_BFR_FIFOLIMIT, 512 - imgWidth/4 - 1);
	REG_SET(HPS_FRM_BFR_OVLYFIFOLIMIT, 2048 - (burstSize * ovlyBurstsPerLine) - 1);
	REG_SET(HPS_FRM_BFR_LINESTART, 2);
	REG_SET(HPS_FRM_BFR_OUTBRSTS_PERLINE, outBurstsPerLine);
	REG_SET(HPS_FRM_BFR_OVLYBRSTS_PERLINE, ovlyBurstsPerLine);  // overlay image width / pixels per burst
}

//-----------------------------------------------------------------------


void startHpsRead (int enable)
{
	REG_SET(HPS_FRM_BFR_STARTRD, enable);
}

void linestart (int line)
{
	REG_SET(HPS_FRM_BFR_LINESTART, line);
}

//=======================================================================


#ifndef GAMMA_LUT_H
#define GAMMA_LUT_H
//==============================================================

int gammaLutInit(void);
void gammaLutBypass(int bypass);

#endif
//==============================================================

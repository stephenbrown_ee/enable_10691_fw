

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "pipe.h"
#include "lcd_display_out.h"
#include "bus_io.h"
#include "bit_io.h"
#include "sys_mgr.h"
#include "console.h"

//=======================================================================

#define PAR_OUT_LCD_RESET            BIT(bus_parLCD, 0, 0)
#define PAR_OUT_LCD_SOURCE           FLD(bus_parLCD, 0, 5, 4)
#define PAR_OUT_LCD_HORSCALE         FLD(bus_parLCD, 0, 20, 8)
#define PAR_OUT_LCD_MANOVLYEN        BIT(bus_parLCD, 0, 21)
#define PAR_OUT_LCD_MODCOUNT         FLD(bus_parLCD, 0, 29, 22)
#define PAR_OUT_LCD_HORMAX           FLD(bus_parLCD, 1, 11, 0)  // Last hor count in line
#define PAR_OUT_LCD_VERMAX           FLD(bus_parLCD, 1, 27, 16) // Last ver count in frame
#define PAR_OUT_LCD_VERCR0           FLD(bus_parLCD, 2, 11, 0)
#define PAR_OUT_LCD_VERCR1           FLD(bus_parLCD, 2, 27, 16)
#define PAR_OUT_LCD_VERVS0           FLD(bus_parLCD, 3, 11, 0)   // First line of no VS
#define PAR_OUT_LCD_VERVS1           FLD(bus_parLCD, 3, 27, 16)  // First line of active VS
#define PAR_OUT_LCD_VERDE0           FLD(bus_parLCD, 4, 11, 0)   // First line of no DE
#define PAR_OUT_LCD_VERDE1           FLD(bus_parLCD, 4, 27, 16)  // First line of active DE
#define PAR_OUT_LCD_VERPX0           FLD(bus_parLCD, 5, 11, 0)   // First line of no FIFO reads
#define PAR_OUT_LCD_VERPX1           FLD(bus_parLCD, 5, 27, 16)  // First line of FIFO reads
#define PAR_OUT_LCD_HORVS0           FLD(bus_parLCD, 6, 11, 0)   // Hor pos of VS fall
#define PAR_OUT_LCD_HORVS1           FLD(bus_parLCD, 6, 27, 16)  // Hor pos of VS rise
#define PAR_OUT_LCD_HORHS0           FLD(bus_parLCD, 7, 11, 0)   // Hor pos of HS fall
#define PAR_OUT_LCD_HORHS1           FLD(bus_parLCD, 7, 27, 16)  // Hor pos of HS rise
#define PAR_OUT_LCD_HORDE0           FLD(bus_parLCD, 8, 11, 0)   // Hor pos of DE fall
#define PAR_OUT_LCD_HORDE1           FLD(bus_parLCD, 8, 27, 16)  // Hor pos of DE rise
#define PAR_OUT_LCD_HORPX0           FLD(bus_parLCD, 9, 11, 0)   // Hor pos of end of FIFO reads
#define PAR_OUT_LCD_HORPX1           FLD(bus_parLCD, 9, 27, 16)  // Hor pos of start of FIFO reads
#define PAR_OUT_LCD_UPPERLEFTX       FLD(bus_parLCD, 10, 15, 0)   // Hor pos of LOGO start
#define PAR_OUT_LCD_UPPERLEFTY       FLD(bus_parLCD, 10, 31, 16)  // Ver pos of LOGO start
#define PAR_OUT_LCD_LOGOWIDTH        FLD(bus_parLCD, 11, 15, 0)
#define PAR_OUT_LCD_LOGOHEIGHT       FLD(bus_parLCD, 11, 31, 16)
#define PAR_OUT_LCD_MSGX             FLD(bus_parLCD, 12, 15, 0)   // Hor pos of LOGO start
#define PAR_OUT_LCD_MSGY             FLD(bus_parLCD, 12, 31, 16)  // Ver pos of LOGO start
#define PAR_OUT_LCD_MSGWIDTH         FLD(bus_parLCD, 13, 15, 0)
#define PAR_OUT_LCD_MSGHEIGHT        FLD(bus_parLCD, 13, 31, 16)
#define PAR_OUT_LCD_HORCR0           FLD(bus_parLCD, 14, 11, 0)   // Hor pos of end of Crop
#define PAR_OUT_LCD_HORCR1           FLD(bus_parLCD, 14, 27, 16)  // Hor pos of start of Crop
#define PAR_OUT_LCD_BARX             FLD(bus_parLCD, 15, 11, 0)   // Hor pos of end of Crop
#define PAR_OUT_LCD_BARY             FLD(bus_parLCD, 15, 27, 16)  // Hor pos of start of Crop

#define LCD_OUT_RESET                BIT(bus_lcddsp, 0, 0)
#define LCD_OUT_SOURCE               BIT(bus_lcddsp, 0, 1)
#define LCD_OUT_USESYNC              BIT(bus_lcddsp, 0, 2)
#define LCD_OUT_INVERTSYNC           BIT(bus_lcddsp, 0, 3)
#define LCD_OUT_LOCKED               BIT(bus_lcddsp, 0, 31)
#define LCD_OUT_HORMAX               FLD(bus_lcddsp, 1, 15, 0)   // These counts are used for image generation only (testing purposes)
#define LCD_OUT_VERMAX               FLD(bus_lcddsp, 1, 31, 16)  // 
#define LCD_OUT_IMGHORMAX            FLD(bus_lcddsp, 2, 15, 0)   //
#define LCD_OUT_IMGVERMAX            FLD(bus_lcddsp, 2, 31, 16)  //

#define PWM_GEN_RESET                BIT(bus_pwm, 0, 0)
#define PWM_GEN_ENABLE               BIT(bus_pwm, 0, 1)
#define PWM_GEN_COUNTMAX             FLD(bus_pwm, 1, 23, 0)  // This the number of usec clock cyles in one period
#define PWM_GEN_RISECOUNT            FLD(bus_pwm, 2, 23, 0)  // This is the clock count where the PWM signal rises
#define PWM_GEN_FALLCOUNT            FLD(bus_pwm, 3, 23, 0)  // This is the clock count where the PWM signal falls

//=======================================================================

int parOutInitLCD(void)
{
	int result = 0;

	regClear(bus_parLCD, 8);

	REG_SET(PAR_OUT_LCD_RESET, 1);
	REG_SET(PAR_OUT_LCD_RESET, 0);

	return result;
}

//-----------------------------------------------------------------------

void parOutSetupLCD(sensor_type_t sensor, int sel, int crop_percent, int hShift, int vShift )
{
	int hSize, vSize;
	int horCR0, horCR1;
	int verCR0, verCR1;
	int offset_from_center = 64;
//	REG_SET(PAR_OUT_LCD_RESET, 1);

	if ((crop_percent > 100) || (crop_percent < 0))
		crop_percent = 0;

	if (sensor == OV6946) {
		if(sel) {
			hSize = 768;
			vSize = 768;
		}
		else {
			hSize = 400;
			vSize = 400;
		}
	}
	else if (sensor == OV6948) {
		if(sel) {
			hSize = 600;
			vSize = 600;
		}
		else {
			hSize = 400;
			vSize = 400;
		}
	}


	ImageInfo imgInfoLCD;
	ImageInfo* imgLCD = &imgInfoLCD;

	imgLCD->hSizeOut = hSize;
	imgLCD->vSizeOut = vSize;

	// Recommended 
	imgLCD->hFPorch = 10;
	imgLCD->hSync   = 300;
	imgLCD->hBPorch = 10;

	imgLCD->vFPorch = 4;
	imgLCD->vSync   = 38;
	imgLCD->vBPorch = 4;

	imgLCD->hLBlack = ((1024 - imgLCD->hSizeOut) / 2) + offset_from_center;
	imgLCD->hRBlack = ((1024 - imgLCD->hSizeOut) / 2) - offset_from_center;
	int vFBlack  = (768 - imgLCD->hSizeOut) / 2;
	imgLCD->vBBlack = (768 - imgLCD->hSizeOut) / 2;

	// Horizontal count starts with first pixel
	// Vertical count starts with first imager line

	REG_SET(PAR_OUT_LCD_HORMAX, (imgLCD->hLBlack + imgLCD->hSizeOut + imgLCD->hRBlack + imgLCD->hFPorch + imgLCD->hSync + imgLCD->hBPorch) - 1);
	REG_SET(PAR_OUT_LCD_VERMAX,                  imgLCD->vSizeOut + vFBlack + imgLCD->vBBlack + imgLCD->vFPorch + imgLCD->vSync + imgLCD->vBPorch - 1);
                   
	REG_SET(PAR_OUT_LCD_HORDE1, 0);
	REG_SET(PAR_OUT_LCD_HORDE0, (imgLCD->hLBlack + imgLCD->hSizeOut + imgLCD->hRBlack) );
	REG_SET(PAR_OUT_LCD_HORPX1, (imgLCD->hLBlack) );
	REG_SET(PAR_OUT_LCD_HORPX0, (imgLCD->hLBlack + imgLCD->hSizeOut) );

	horCR1 = imgLCD->hLBlack + (crop_percent * imgLCD->hSizeOut)/100 + hShift;
	horCR0 = imgLCD->hLBlack + ((100 - crop_percent)*imgLCD->hSizeOut)/100 + hShift;
	horCR1 = CLIP(horCR1, 0, 1024);
	horCR0 = CLIP(horCR0, 0, 1024);

	REG_SET(PAR_OUT_LCD_HORCR1, horCR1);
	REG_SET(PAR_OUT_LCD_HORCR0, horCR0);
                   
	REG_SET(PAR_OUT_LCD_HORHS1, (imgLCD->hLBlack + imgLCD->hSizeOut + imgLCD->hRBlack + imgLCD->hFPorch) );
	REG_SET(PAR_OUT_LCD_HORHS0, (imgLCD->hLBlack + imgLCD->hSizeOut + imgLCD->hRBlack + imgLCD->hFPorch + imgLCD->hSync) );
                   
	REG_SET(PAR_OUT_LCD_VERVS1, imgLCD->vSizeOut + vFBlack + imgLCD->vBBlack + imgLCD->vFPorch - 1);
	REG_SET(PAR_OUT_LCD_VERVS0, imgLCD->vSizeOut + vFBlack + imgLCD->vBBlack + imgLCD->vFPorch + imgLCD->vSync - 1);
                   
	REG_SET(PAR_OUT_LCD_HORVS1, (imgLCD->hLBlack + imgLCD->hSizeOut + imgLCD->hRBlack + imgLCD->hFPorch) );
	REG_SET(PAR_OUT_LCD_HORVS0, (imgLCD->hLBlack + imgLCD->hSizeOut + imgLCD->hRBlack + imgLCD->hFPorch) );
                   
	REG_SET(PAR_OUT_LCD_VERDE1, 0);
	REG_SET(PAR_OUT_LCD_VERDE0, imgLCD->vSizeOut + imgLCD->vBBlack + vFBlack);

	verCR1 = vFBlack + (crop_percent*(imgLCD->vSizeOut))/100 + vShift;
	verCR0 = vFBlack + ((100 - crop_percent)*imgLCD->vSizeOut)/100 + vShift;
	verCR1 = CLIP(verCR1, 0, 768);
	verCR0 = CLIP(verCR0, 0, 768);

	REG_SET(PAR_OUT_LCD_VERCR1, verCR1);
	REG_SET(PAR_OUT_LCD_VERCR0, verCR0);

	REG_SET(PAR_OUT_LCD_VERPX1, vFBlack);
	REG_SET(PAR_OUT_LCD_VERPX0, imgLCD->vSizeOut + vFBlack);
	
	REG_SET(PAR_OUT_LCD_HORSCALE, 96);                   
	REG_SET(PAR_OUT_LCD_MODCOUNT, 6);  // 1 pixel filled every MODCOUNT frames (total width = LOGOWIDTH), so time_to_fill = LOGOWIDTH/(60/MODCOUNT)

	REG_SET(PAR_OUT_LCD_UPPERLEFTY, 352);
	REG_SET(PAR_OUT_LCD_LOGOWIDTH, 186);  // Enable 186 // Steris 195
	REG_SET(PAR_OUT_LCD_LOGOHEIGHT, 43);  // Enable 43  // Steris 42
	REG_SET(PAR_OUT_LCD_BARY, 430);

//	REG_SET(PAR_OUT_LCD_RESET, 0);
//	printf("horCR1: %d\nhorCR0: %d\nxstart: %d\nwidth: %d\nverCR1: %d\nverCR0: %d\nystart: %d\nheight: %d\n", horCR1,horCR0,xstart,width,verCR1,verCR0,ystart,height);
}

//-----------------------------------------------------------------------

void setLCDBrightness(int dutyPercent)
{
	int periodClocks = 2000;   // 1000000/2000 = 500 Hz
	int fallCount = periodClocks * dutyPercent / 100;
//	printf("PWM fall count: %d\n", fallCount);
	
	REG_SET(PWM_GEN_RESET, 1);
	REG_SET(PWM_GEN_RESET, 0);
	REG_SET(PWM_GEN_ENABLE, 0);
	REG_SET(PWM_GEN_COUNTMAX, periodClocks);
	REG_SET(PWM_GEN_RISECOUNT, 0);
	REG_SET(PWM_GEN_FALLCOUNT, fallCount);
	
	REG_SET(PWM_GEN_ENABLE, 1);
}

//-----------------------------------------------------------------------------

void lcdOutSetup (int enableTest)
{
	int horBlanking = 320;  // This is recommended in the Mitsubishi AA104XF02--T1 datasheet  
	int verBlanking = 46;   // This timing is for test pattern only, normal input timing is created by parallelOut module
	int imgHorSize  = 1024;
	int imgVerSize  = 768;
	
	out(BLEN, 0);
	REG_SET(LCD_OUT_RESET, 1);
	REG_SET(LCD_OUT_USESYNC, 1);
	REG_SET(LCD_OUT_INVERTSYNC, 1);
	REG_SET(LCD_OUT_HORMAX, horBlanking + imgHorSize);
	REG_SET(LCD_OUT_IMGHORMAX, imgHorSize);
	REG_SET(LCD_OUT_VERMAX, verBlanking + imgVerSize);
	REG_SET(LCD_OUT_IMGVERMAX, imgVerSize);
	REG_SET(LCD_OUT_SOURCE, enableTest);
	REG_SET(LCD_OUT_RESET, 0);

	setLCDBrightness(100);   // Set pwm to Mitsubishi monitor LED driver duty cycle at 500 Hz
    waitMsec(250);
	out(BLEN, 1);   // Enable Mitsubishi monitor backlight LED driver
}

//-----------------------------------------------------------------------------

void lcdOff (void)
{
	REG_SET(LCD_OUT_RESET, 1);
	out(BLEN, 0);
	setLCDBrightness(0);
}


void setLogo(int x, int y, int width, int height)
{
	REG_SET(PAR_OUT_LCD_UPPERLEFTX, x);
	REG_SET(PAR_OUT_LCD_UPPERLEFTY, y);
	REG_SET(PAR_OUT_LCD_LOGOWIDTH, width);
	REG_SET(PAR_OUT_LCD_LOGOHEIGHT, height);
}

void setMsg(int x, int y, int width, int height)
{
	REG_SET(PAR_OUT_LCD_MSGX, x);
	REG_SET(PAR_OUT_LCD_MSGY, y);
	REG_SET(PAR_OUT_LCD_MSGWIDTH, width);
	REG_SET(PAR_OUT_LCD_MSGHEIGHT, height);
}

void setLCDmode(int source)
{
	REG_SET(PAR_OUT_LCD_SOURCE, source);
	if (source == 2) {
		REG_SET(PAR_OUT_LCD_UPPERLEFTX, 410);
		REG_SET(PAR_OUT_LCD_BARX, 413);
	}
	else {
		REG_SET(PAR_OUT_LCD_UPPERLEFTX, 410 + 64);
		REG_SET(PAR_OUT_LCD_BARX, 413 + 64);
	}
}

// This sets the width of a color bar when source = 1
void setScale (int horScale)
{
	REG_SET(PAR_OUT_LCD_HORSCALE, horScale);
}
//=======================================================================


#ifndef LCD_DISPLAY_OUT_H
#define LCD_DISPLAY_OUT_H
//==============================================================

#include "pipe.h"
#include "sensor.h"
#include "util.h"


void setLCDBrightness(int dutyPercent);
void lcdOutSetup (int enableTest);
void parOutSetupLCD(sensor_type_t sensor, int sel, int crop_percent, int hShift, int vShift );
void lcdOff(void);
void processSwitches(int dipsw0, int dipsw1, int dipsw2);
void setBarDisplay(int state);
void setLCDmode(int source);

#endif
//==============================================================

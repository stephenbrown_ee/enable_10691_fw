// Stephen Brown
// sensorInput.c
//
// Description:  
// Register settings for initializing the sensor simulator and sensor input modules in the FPGA.
///

//================================================================
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "sensorInput.h"
//================================================================

#define SEN_CTL_RESET            BIT(bus_senCtl, 0, 0)
#define SEN_CTL_ADDR             FLD(bus_senCtl, 1, 7, 0)
#define SEN_CTL_SPI_0_REG        FLD(bus_senCtl, 1, 15, 8)
#define SEN_CTL_SPI_1_REG        FLD(bus_senCtl, 1, 23, 16)
#define SEN_CTL_SPI_2_REG        FLD(bus_senCtl, 1, 31, 24)
#define SEN_CTL_SPI_3_REG        FLD(bus_senCtl, 2, 7, 0)
#define SEN_CTL_SPI_4_REG        FLD(bus_senCtl, 2, 15, 8)
#define SEN_CTL_SPI_5_REG        FLD(bus_senCtl, 2, 23, 16)
#define SEN_CTL_SPI_6_REG        FLD(bus_senCtl, 2, 31, 24)
#define SEN_CTL_SPI_7_REG        FLD(bus_senCtl, 3, 7, 0)
#define SEN_CTL_WAIT_CNT         FLD(bus_senCtl, 3, 11, 8)

#define SEN_INP_RESET            BIT(bus_senInp, 0, 0)
#define SEN_INP_AUTO_BLACK       BIT(bus_senInp, 0, 1)
#define SEN_INP_ENABLE_BLC       BIT(bus_senInp, 0, 2)
#define SEN_INP_MULTIPLIER       FLD(bus_senInp, 0, 12, 3)
//#define SEN_INP_BLACK_END        FLD(bus_senInp, 0, 16, 14)  // This is the row when black data ends
#define SEN_INP_ALPHA            FLD(bus_senInp, 0, 22, 17)
#define SEN_INP_CLEAR_BLANK      BIT(bus_senInp, 0, 23)
#define SEN_INP_BLANK_LEVEL      FLD(bus_senInp, 1, 11, 0)
#define SEN_INP_BLACK_LEVEL      FLD(bus_senInp, 1, 28, 16)
#define SEN_INP_BLACK_COUNT      FLD(bus_senInp, 2, 7, 0)
#define SEN_INP_ROW_START        FLD(bus_senInp, 2, 16, 8)
#define SEN_INP_BLACK_TARGET     FLD(bus_senInp, 2, 24, 17)
#define SEN_INP_SYNC_START       FLD(bus_senInp, 3, 8, 0)
#define SEN_INP_BLANK_MAX        FLD(bus_senInp, 4, 11, 0)
#define SEN_INP_BLANK_ERROR      FLD(bus_senInp, 4, 27, 16)
#define SEN_INP_BLANK_START      FLD(bus_senInp, 5, 11, 0)
#define SEN_INP_CLKPERFRAME      FLD(bus_senInp, 6, 31, 0)
#define SEN_INP_FRAMECOUNT       FLD(bus_senInp, 7, 31, 0)
#define SEN_INP_BLACK_AVG        FLD(bus_senInp, 8, 11, 0)


#define DEFAULT_BLANK_LEVEL  1250
#define DEFAULT_BLACK_LEVEL  1650
//================================================================

int sensorInputInit (void)
{
	REG_SET(SEN_INP_RESET, 1);
	REG_SET(SEN_CTL_RESET, 1);
	REG_SET(SEN_INP_RESET, 0);
	REG_SET(SEN_CTL_RESET, 0);
	return 0;
}

void sensorInputSetup(void)
{
	REG_SET(SEN_INP_BLANK_LEVEL, DEFAULT_BLANK_LEVEL);
	REG_SET(SEN_INP_BLACK_LEVEL, DEFAULT_BLACK_LEVEL);
	REG_SET(SEN_INP_BLACK_COUNT, 76);
	REG_SET(SEN_INP_ROW_START, 7);
	REG_SET(SEN_INP_BLACK_TARGET, 70);
	REG_SET(SEN_INP_ALPHA, 30);
	REG_SET(SEN_INP_MULTIPLIER, 655); // 2^19/800
	REG_SET(SEN_INP_ENABLE_BLC, 1);
	REG_SET(SEN_INP_AUTO_BLACK, 0);
	REG_SET(SEN_INP_SYNC_START, 100);
}

void sensorCtlSetup(void)
{
	REG_SET(SEN_CTL_ADDR, 0xA6);
	REG_SET(SEN_CTL_SPI_0_REG, 0x01);
	REG_SET(SEN_CTL_SPI_1_REG, 0x20);
	REG_SET(SEN_CTL_SPI_2_REG, 0x10);
	REG_SET(SEN_CTL_SPI_3_REG, 0x23);
	REG_SET(SEN_CTL_SPI_4_REG, 0x89);
	REG_SET(SEN_CTL_SPI_5_REG, 0);
	REG_SET(SEN_CTL_SPI_6_REG, 0);
	REG_SET(SEN_CTL_SPI_7_REG, 0);

	REG_SET(SEN_CTL_WAIT_CNT, 8);
}

void totalclk (void)
{
	printf("Clocks Per Frame: %d\n", REG_GET(SEN_INP_CLKPERFRAME));
}

int getFrameCount(void)
{
	return REG_GET(SEN_INP_FRAMECOUNT);
}

void sensorCtlSet (uint16_t address, uint8_t value)
{
	switch (address)
	{
		case 0: REG_SET(SEN_CTL_SPI_0_REG, value); break;
		case 1: REG_SET(SEN_CTL_SPI_1_REG, value); break;
		case 2: REG_SET(SEN_CTL_SPI_2_REG, value); break;
		case 3: REG_SET(SEN_CTL_SPI_3_REG, value); break;
		case 4: REG_SET(SEN_CTL_SPI_4_REG, value); break;
		case 5: REG_SET(SEN_CTL_SPI_5_REG, value); break;
		case 6: REG_SET(SEN_CTL_SPI_6_REG, value); break;
		case 7: REG_SET(SEN_CTL_SPI_7_REG, value); break;
	}
}

void blackAverage (int set)
{
	int average = REG_GET(SEN_INP_BLACK_AVG);
	printf("Average = %d\n", average);
	if(set) {
		REG_SET(SEN_INP_BLACK_LEVEL, average - 60);
	}
}

void rowStart(int row)
{
	REG_SET(SEN_INP_ROW_START, row);
}

void autoBlack(int enable)
{
	REG_SET(SEN_INP_AUTO_BLACK, enable);
}

void black_target(int target)
{
	REG_SET(SEN_INP_BLACK_TARGET, target);
}

void enBLC(int enable)
{
	REG_SET(SEN_INP_ENABLE_BLC, enable);
}

void al(int alpha)
{
	REG_SET(SEN_INP_ALPHA, alpha);
}

void pb(void)
{
	printf("Black avg: %d\n", REG_GET(SEN_INP_BLACK_AVG));
}

void blank(int level)
{
	REG_SET(SEN_INP_BLANK_LEVEL, level);
}

void black(int level)
{
	REG_SET(SEN_INP_BLACK_LEVEL, level);
}

void getBlank(void)
{
	printf("Blank End: %d\n", REG_GET(SEN_INP_BLANK_MAX));
	printf("Blank Start: %d\n", REG_GET(SEN_INP_BLANK_START));
}

void clearBlank(void)
{
	REG_SET(SEN_INP_CLEAR_BLANK, 1);
	waitMsec(100);
	REG_SET(SEN_INP_CLEAR_BLANK, 0);
}

void getLevels(void)
{
	printf("Blank level: %d\n", REG_GET(SEN_INP_BLANK_LEVEL));
	printf("Black level: %d\n", REG_GET(SEN_INP_BLACK_LEVEL));
	printf("Black avg: %d\n", REG_GET(SEN_INP_BLACK_AVG));
}

void getBlankError()
{
	printf("Blank Error level: %d\n", REG_GET(SEN_INP_BLANK_ERROR));
}

void calibrateBlankLevel(void)
{
	int timeOut = 0;
	int blankLevel = DEFAULT_BLANK_LEVEL;
	int prevFrame = getFrameCount();
	waitMsec(165);
	int frame = getFrameCount();
	while (((frame - prevFrame) < 5) && (timeOut < 8))
	{
		blankLevel = blankLevel - 50;
		REG_SET(SEN_INP_BLANK_LEVEL, blankLevel);
		prevFrame = frame;
		waitMsec(165);
		frame = getFrameCount();
		printf("Frm: %d  Prev: %d  Blnk:  %d\n", frame, prevFrame, blankLevel);
		timeOut++;
	}
	if (timeOut == 8)
		printf("Blank Cal timed out.\n");
	else
		blackAverage(1);
}

void syncStart (int row)
{
	REG_SET(SEN_INP_SYNC_START, row);
}

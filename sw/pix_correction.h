
#ifndef PIX_CORRECTION_H
#define PIX_CORRECTION_H
//==============================================================

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"
#include "fpga_i2c0.h"
#include "sensor.h"

void initCorr (int horSize);
void calibrate (sensor_type_t sensor, int deVersion);
void pixCorBypass (int bypass);
void rw_calibration_data (sensor_type_t sensor);
int correctionWorker(void);

#endif
//==============================================================

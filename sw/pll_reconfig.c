
#include "pll_reconfig.h"
#include "console.h"
#include "system.h"
#include "io.h"

/* Multiply Reconfig Register with 4 when you use IOWR_32DIRECT*/
#define MODE      		0x00
#define STATUS    		0x04
#define START     		0x08
#define N_COUNTER 		0x0C
#define M_COUNTER 		0x10
#define C_COUNTER 		0x14
#define DPS_COUNTER		0x18
#define FRAC_COUNTER 	0x1C
#define BS_COUNTER		0x20
#define CPS_COUNTER		0x24
#define C0_COUNTER 		0x28
#define C1_COUNTER 		0x2C

void dps (uint16_t nShifts);

// Clock C0 = adc clock = 8 MHz

void initPll(void)
{
	dps(32);
}

// The VCO frequency for C0 is 320 MHz
// Every phase shift is equal to 1/8 of the VCO frequency
// The adc clock is 8 MHz
// So every phase shift is 1/320 of the adc clock
// So it makes sense to shift the clock in multiples of 10 or 1/32 of the adc clock period
void dps(uint16_t nShifts)
{
	unsigned int status_reconfig;

	printf(" \n Dynamic Phase Shift C0 selected \n");

	IOWR_32DIRECT(PLL_RECONFIG_0_BASE, MODE, 0x01);

	//DPS C0 for nShift steps in negative direction
	IOWR_32DIRECT(PLL_RECONFIG_0_BASE, DPS_COUNTER, nShifts);

	//Write anything to Start Register to Reconfiguration
	IOWR_32DIRECT(PLL_RECONFIG_0_BASE, START, 0x01);
	do {
	  	status_reconfig=IORD_32DIRECT(PLL_RECONFIG_0_BASE, STATUS);}
		while ((!status_reconfig) & 0x01);

	printf(" \n %d-Step Negative DPS on C0 Completed !! \n", nShifts);
}

void readPll(int reg)
{
	int reconfig_bit=IORD_32DIRECT(PLL_RECONFIG_0_BASE, reg);
	printf("\n Value in register is %x \n", reconfig_bit);
}


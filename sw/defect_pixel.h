
#ifndef DEFECT_PIXEL_H
#define DEFECT_PIXEL_H
//==============================================================

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "util.h"
#include "bus_io.h"
#include "sys_mgr.h"
#include "console.h"

int defPixInit (int threshold);

#endif
//==============================================================

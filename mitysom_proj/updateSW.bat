


:: Update MIF
quartus_cdb _MIDASystem -c _MIDASystem --update_mif

:: Start Assembler
quartus_asm --read_settings_files=on --write_settings_files=off _MIDASystem -c _MIDASystem

:: Convert programming file to RBF
quartus_cpf -c SOFtoRBF.cof

copy /Y _MIDASystem.rbf C:\_INTRAVU\vpu_fpga_fw\share\

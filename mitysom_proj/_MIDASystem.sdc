
derive_pll_clocks
derive_clock_uncertainty

create_clock -name {IMAGER_PCLK} -period 41.667 -waveform { 0.000 20.833 } [get_ports {IMAGER_PCLK}]
create_clock -name {CLOCK_50MHZ} -period 20.000 -waveform { 0.000 10.000 } [get_ports {CLOCK_50MHZ}]
create_clock -name {GXB_REFCLK1} -period 13.468 -waveform { 0.000  6.734 } [get_ports {GXB_REFCLK1}]
#create_clock -name {hdmiHalfClock} -period 13.468 -waveform { 0.000 6.734 } [get_nets {FrameBuffer:frameBuffer|outClock}]
#create_clock -name {usecClock}   -period 1000.000 -waveform { 0.000 500.000 } [get_keepers {SystemManager:sysMgr|usecClock}]
create_clock -name {msecClock}   -period 1000000.000 -waveform { 0.000 500000.000 } [get_keepers {SystemManager:sysMgr|msecClock}]
create_clock -name {HPS_I2C0_SCL} -period 1000 [get_ports {HPS_I2C0_SCL}]

#set_max_delay -from [get_clocks outClock_inst|outclkpll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk] -to [get_clocks outClock_inst|outclkpll_inst|altera_pll_i|general[3].gpll~PLL_OUTPUT_COUNTER|divclk] 16.835
#set_min_delay -from [get_clocks outClock_inst|outclkpll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk] -to [get_clocks outClock_inst|outclkpll_inst|altera_pll_i|general[3].gpll~PLL_OUTPUT_COUNTER|divclk] 10.101


set_false_path -from [get_clocks {IMAGER_PCLK}] -to [get_clocks {processor|*|divclk}]
set_false_path -from [get_clocks {processor|*|divclk}] -to  [get_clocks {IMAGER_PCLK}]

set_false_path -from [get_clocks {outClock_inst|*|divclk}]    -to [get_clocks {processor|*|divclk}]
set_false_path -from [get_clocks {processor|*|divclk}] -to [get_clocks {outClock_inst|*|divclk}]

set_false_path -from [get_clocks {CLOCK_50MHZ}] -to [get_clocks {processor|*|divclk}]
set_false_path -from [get_clocks {processor|*|divclk}] -to  [get_clocks {CLOCK_50MHZ}]

set_false_path -from [get_clocks {CLOCK_50MHZ}] -to [get_clocks {outClock_inst|*|divclk}]
set_false_path -from [get_clocks {outClock_inst|*|divclk}] -to  [get_clocks {CLOCK_50MHZ}]

set_false_path -from [get_clocks {CLOCK_50MHZ}] -to [get_clocks {IMAGER_PCLK}]
set_false_path -from [get_clocks {IMAGER_PCLK}] -to  [get_clocks {CLOCK_50MHZ}]

set_false_path -from [get_clocks {hps0|*|h2f_user0_clk}] -to [get_clocks {IMAGER_PCLK}]
set_false_path -from [get_clocks {IMAGER_PCLK}] -to  [get_clocks {hps0|*|h2f_user0_clk}]

set_false_path -from [get_clocks {hps0|*|h2f_user0_clk}] -to [get_clocks {outClock_inst|*|divclk}]
set_false_path -from [get_clocks {outClock_inst|*|divclk}] -to  [get_clocks {hps0|*|h2f_user0_clk}]

set_false_path -from [get_clocks {hps0|*|h2f_user0_clk}] -to [get_clocks {processor|*|divclk}]
set_false_path -from [get_clocks {processor|*|divclk}] -to  [get_clocks {hps0|*|h2f_user0_clk}]

set_false_path -from [get_clocks {processor|processorPll|*|divclk}] -to [get_clocks {tmds_ser_inst|*|txpmalocalclk}]
set_false_path -from [get_clocks {tmds_ser_inst|*|txpmalocalclk}] -to [get_clocks {processor|processorPll|*|divclk}]

set_false_path -from [get_clocks {outClock_inst|*|divclk}] -to [get_clocks {tmds_ser_inst|*|txpmalocalclk}]
set_false_path -from [get_clocks {tmds_ser_inst|*|txpmalocalclk}] -to [get_clocks {outClock_inst|*|divclk}]

set_false_path -from [get_clocks {CLOCK_50MHZ}] -to [get_clocks {tmds_ser_inst|*|txpmalocalclk}]
set_false_path -from [get_clocks {tmds_ser_inst|*|txpmalocalclk}] -to  [get_clocks {CLOCK_50MHZ}]

set_false_path -from [get_clocks {hps0|*|h2f_user0_clk}] -to [get_clocks {tmds_ser_inst|*|txpmalocalclk}]
set_false_path -from [get_clocks {tmds_ser_inst|*|txpmalocalclk}] -to  [get_clocks {hps0|*|h2f_user0_clk}]

set_false_path -from [get_clocks {IMAGER_PCLK}] -to [get_clocks {tmds_ser_inst|*|txpmalocalclk}]
set_false_path -from [get_clocks {tmds_ser_inst|*|txpmalocalclk}] -to  [get_clocks {IMAGER_PCLK}]

set_false_path -from [get_clocks {processor|processorPll|*|divclk}] -to [get_clocks {processor|sys|*|divclk}]
set_false_path -from [get_clocks {processor|sys|*|divclk}] -to [get_clocks {processor|processorPll|*|divclk}]

set_false_path -from [get_clocks {processor|sys|*|divclk}] -to [get_clocks {tmds_ser_inst|*|txpmalocalclk}]
set_false_path -from [get_clocks {tmds_ser_inst|*|txpmalocalclk}] -to [get_clocks {processor|sys|*|divclk}]

set_max_delay -from [get_keepers ParallelOut*frameCount*] 50


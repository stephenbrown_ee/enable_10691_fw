::run build
quartus_map --read_settings_files=on --write_settings_files=off _MIDASystem -c _MIDASystem

quartus_fit --read_settings_files=off --write_settings_files=off _MIDASystem -c _MIDASystem

quartus_asm --read_settings_files=on --write_settings_files=off _MIDASystem -c _MIDASystem

::Convert programming file to RBF
quartus_cpf -c SOFtoRBF.cof

copy /Y _MIDASystem.rbf D:\_INTRAVU\vpu_fpga_fw\share\

quartus_sta _MIDASystem -c _MIDASystem
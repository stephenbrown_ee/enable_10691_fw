
module system (
	adcclk_clk,
	adcclk_pll_locked_export,
	bus_address,
	bus_chipselect,
	bus_read,
	bus_readdata,
	bus_write,
	bus_writedata,
	clk_clk,
	clock_reset_reset_n,
	inp_export,
	out_export,
	sensorclk_clk,
	trigger_export,
	uart_rxd,
	uart_txd);	

	output		adcclk_clk;
	output		adcclk_pll_locked_export;
	output	[25:0]	bus_address;
	output		bus_chipselect;
	output		bus_read;
	input	[31:0]	bus_readdata;
	output		bus_write;
	output	[31:0]	bus_writedata;
	input		clk_clk;
	input		clock_reset_reset_n;
	input	[9:0]	inp_export;
	output	[7:0]	out_export;
	output		sensorclk_clk;
	input	[1:0]	trigger_export;
	input		uart_rxd;
	output		uart_txd;
endmodule

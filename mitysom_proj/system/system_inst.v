	system u0 (
		.adcclk_clk               (<connected-to-adcclk_clk>),               //            adcclk.clk
		.adcclk_pll_locked_export (<connected-to-adcclk_pll_locked_export>), // adcclk_pll_locked.export
		.bus_address              (<connected-to-bus_address>),              //               bus.address
		.bus_chipselect           (<connected-to-bus_chipselect>),           //                  .chipselect
		.bus_read                 (<connected-to-bus_read>),                 //                  .read
		.bus_readdata             (<connected-to-bus_readdata>),             //                  .readdata
		.bus_write                (<connected-to-bus_write>),                //                  .write
		.bus_writedata            (<connected-to-bus_writedata>),            //                  .writedata
		.clk_clk                  (<connected-to-clk_clk>),                  //               clk.clk
		.clock_reset_reset_n      (<connected-to-clock_reset_reset_n>),      //       clock_reset.reset_n
		.inp_export               (<connected-to-inp_export>),               //               inp.export
		.out_export               (<connected-to-out_export>),               //               out.export
		.sensorclk_clk            (<connected-to-sensorclk_clk>),            //         sensorclk.clk
		.trigger_export           (<connected-to-trigger_export>),           //           trigger.export
		.uart_rxd                 (<connected-to-uart_rxd>),                 //              uart.rxd
		.uart_txd                 (<connected-to-uart_txd>)                  //                  .txd
	);


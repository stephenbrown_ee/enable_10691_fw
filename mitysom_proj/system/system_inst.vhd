	component system is
		port (
			adcclk_clk               : out std_logic;                                        -- clk
			adcclk_pll_locked_export : out std_logic;                                        -- export
			bus_address              : out std_logic_vector(25 downto 0);                    -- address
			bus_chipselect           : out std_logic;                                        -- chipselect
			bus_read                 : out std_logic;                                        -- read
			bus_readdata             : in  std_logic_vector(31 downto 0) := (others => 'X'); -- readdata
			bus_write                : out std_logic;                                        -- write
			bus_writedata            : out std_logic_vector(31 downto 0);                    -- writedata
			clk_clk                  : in  std_logic                     := 'X';             -- clk
			clock_reset_reset_n      : in  std_logic                     := 'X';             -- reset_n
			inp_export               : in  std_logic_vector(9 downto 0)  := (others => 'X'); -- export
			out_export               : out std_logic_vector(7 downto 0);                     -- export
			sensorclk_clk            : out std_logic;                                        -- clk
			trigger_export           : in  std_logic_vector(1 downto 0)  := (others => 'X'); -- export
			uart_rxd                 : in  std_logic                     := 'X';             -- rxd
			uart_txd                 : out std_logic                                         -- txd
		);
	end component system;

	u0 : component system
		port map (
			adcclk_clk               => CONNECTED_TO_adcclk_clk,               --            adcclk.clk
			adcclk_pll_locked_export => CONNECTED_TO_adcclk_pll_locked_export, -- adcclk_pll_locked.export
			bus_address              => CONNECTED_TO_bus_address,              --               bus.address
			bus_chipselect           => CONNECTED_TO_bus_chipselect,           --                  .chipselect
			bus_read                 => CONNECTED_TO_bus_read,                 --                  .read
			bus_readdata             => CONNECTED_TO_bus_readdata,             --                  .readdata
			bus_write                => CONNECTED_TO_bus_write,                --                  .write
			bus_writedata            => CONNECTED_TO_bus_writedata,            --                  .writedata
			clk_clk                  => CONNECTED_TO_clk_clk,                  --               clk.clk
			clock_reset_reset_n      => CONNECTED_TO_clock_reset_reset_n,      --       clock_reset.reset_n
			inp_export               => CONNECTED_TO_inp_export,               --               inp.export
			out_export               => CONNECTED_TO_out_export,               --               out.export
			sensorclk_clk            => CONNECTED_TO_sensorclk_clk,            --         sensorclk.clk
			trigger_export           => CONNECTED_TO_trigger_export,           --           trigger.export
			uart_rxd                 => CONNECTED_TO_uart_rxd,                 --              uart.rxd
			uart_txd                 => CONNECTED_TO_uart_txd                  --                  .txd
		);

